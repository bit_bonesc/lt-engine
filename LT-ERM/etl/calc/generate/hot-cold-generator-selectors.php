<?php
function hot_cold_generator_selectors($db,$months_in,$drawdate_in,$lottocode_in)
{
#http://localhost/lottotarget2/calc/tools-hot-cold-generator-selectors.php?m=3&drawdate=2016-08-16&id=ZA-P
#include_once("../include/config.php");
#include_once("../include/db-settings.php");
#include_once("../include/db-connect.php");
#include_once("../include/db-functions.php");
#include_once("functions-selectors.php");

/*
$drawdate_in = '';$lottocode_in = '';$months_in='';
if (isset($_REQUEST['m'])){	$months_in = $_REQUEST['m'];}
if (isset($_REQUEST['drawdate'])) {	$drawdate_in = $_REQUEST['drawdate'];}
if (isset($_REQUEST['id'])) {	$lottocode_in = $_REQUEST['id'];}

$db = db_connect($hostname, $username, $dbpassword, $databasename);
*/
$lotto_detail_array = array();
$lotto_detail_count = 0;
$lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode_in);
	 $x=0;
	 $guess_range = 3;
	 $LottoTable 	 = $lotto_detail_array[$x]['Lotto_Table'];
	 $ball1_active = $lotto_detail_array[$x]['ball1_active'];
	 $ball2_active = $lotto_detail_array[$x]['ball2_active'];
	 $ball3_active = $lotto_detail_array[$x]['ball3_active'];
	 if ($ball3_active) {$guess_range = 3;}
	 $ball4_active = $lotto_detail_array[$x]['ball4_active'];
	 if ($ball4_active) {$guess_range = 4;}
	 $ball5_active = $lotto_detail_array[$x]['ball5_active'];
	 if ($ball5_active) {$guess_range = 5;}
	 $ball6_active = $lotto_detail_array[$x]['ball6_active'];
	 if ($ball6_active) {$guess_range = 6;}
	 $ball7_active = $lotto_detail_array[$x]['ball7_active'];
	 if ($ball7_active) {$guess_range = 7;}
	 $bonus_active = $lotto_detail_array[$x]['bonus_active'];
	 $bonus2_active = $lotto_detail_array[$x]['bonus2_active'];
	 $bonus3_active = $lotto_detail_array[$x]['bonus3_active'];
	 $bonus4_active = $lotto_detail_array[$x]['bonus4_active'];
	 $powerball_active = $lotto_detail_array[$x]['powerball_active'];
	 $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
	 $lucky1_active = $lotto_detail_array[$x]['lucky1_active'];
	 $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
	 $ball_max = $lotto_detail_array[$x]['ball_max'];
	 $ball_max_pb = $lotto_detail_array[$x]['ball_max_pb'];
	 $ball_max_lucky = $lotto_detail_array[$x]['ball_max_lucky'];
	 $scheduled_draws = $lotto_detail_array[$x]['scheduled_draws'];
$pb_ind = 0;
if ($powerball_active)
{
	$pb_ind = 1;

}

#####DO DATE THING
#drawdate_snapshot_from
#drawdate_prediction_on

$drawdate_snapshot_from = $drawdate_in;
$drawdate_prediction_on = function_get_next_draw_date($drawdate_snapshot_from,$scheduled_draws);

$chart_type = 'HC';
$tablename =  db_return_chartselector_tablename($db,$chart_type);

$lotto_hot_cold_median_array = array();
$lotto_hot_cold_median_count = 0;
$lotto_hot_cold_median_array = db_return_lotto_hot_cold_median_detail($db,$months_in,$lottocode_in,0);
if ($pb_ind)
{
	$lotto_hot_cold_median_array_pb = array();
	$lotto_hot_cold_median_count_pb = 0;
	$lotto_hot_cold_median_array_pb = db_return_lotto_hot_cold_median_detail($db,$months_in,$lottocode_in,1);
}
$hot_cold_median_comma = $lotto_hot_cold_median_array[0]['hot_cold_median'];
$string_array_hot_median_cold = explode("|",$hot_cold_median_comma);
$hot_numbers = $string_array_hot_median_cold[0];
$cold_numbers = $string_array_hot_median_cold[1];
$median_numbers = $string_array_hot_median_cold[2];

if ($pb_ind)
{
	$hot_cold_median_comma_pb = $lotto_hot_cold_median_array_pb[0]['hot_cold_median'];
	$string_array_hot_median_cold_pb = explode("|",$hot_cold_median_comma_pb);
	$hot_numbers_pb = $string_array_hot_median_cold_pb[0];
	$cold_numbers_pb = $string_array_hot_median_cold_pb[1];
	$median_numbers_pb = $string_array_hot_median_cold_pb[2];
}
#print($hot_numbers);
#print($cold_numbers);
#print($median_numbers);
#####decompose hot numbers, 1st occurance is always the count
$string_hot_numbers = explode(",",$hot_numbers);
$hot_number_count = $string_hot_numbers[0];
#####decompose cold numbers, 1st occurance is always the count
$string_cold_numbers = explode(",",$cold_numbers);
$cold_number_count = $string_cold_numbers[0];
#####decompose median numbers, 1st occurance is always the count
$string_median_numbers = explode(",",$median_numbers);
$median_number_count = $string_median_numbers[0];
if ($pb_ind)
{
	#####decompose hot numbers, 1st occurance is always the count
	$string_hot_numbers_pb = explode(",",$hot_numbers_pb);
	$hot_number_count_pb = $string_hot_numbers_pb[0];
	#####decompose cold numbers, 1st occurance is always the count
	$string_cold_numbers_pb = explode(",",$cold_numbers_pb);
	$cold_number_count_pb = $string_cold_numbers_pb[0];
	#####decompose median numbers, 1st occurance is always the count
	$string_median_numbers_pb = explode(",",$median_numbers_pb);
	$median_number_count_pb = $string_median_numbers_pb[0];
}
#print($median_number_count);
#SORT NUMBERS HIGHEST FREQ FIRST
$sorted_hot_numbers = sort_numbers($hot_numbers);#print($sorted_hot_numbers);
//to-do: $sorted_hot_numbers = sort_numbers_ditributed($hot_numbers,$ball_max);#print($sorted_hot_numbers);

$sorted_cold_numbers = sort_numbers($cold_numbers);#print($sorted_cold_numbers);
$sorted_median_numbers = sort_numbers($median_numbers);#print($sorted_median_numbers);
if ($pb_ind)
{
	$sorted_hot_numbers_pb = sort_numbers($hot_numbers_pb);#print($sorted_hot_numbers);
	$sorted_cold_numbers_pb = sort_numbers($cold_numbers_pb);#print($sorted_cold_numbers);
	$sorted_median_numbers_pb = sort_numbers($median_numbers_pb);#print($sorted_median_numbers);
}

####CONSTRUCT SELECTORS
/*----------------------------------------------------------------------------*/
##START : HOT-SELECTOR
$sorted_hot_numbers_expanded = explode(',',$sorted_hot_numbers);
$ball1_H = $sorted_hot_numbers_expanded[0];
$ball2_H = $sorted_hot_numbers_expanded[1];
$ball3_H = $sorted_hot_numbers_expanded[2];
if ($ball4_active)	{$ball4_H = $sorted_hot_numbers_expanded[3];}
if ($ball5_active) 	{$ball5_H = $sorted_hot_numbers_expanded[4];}
if ($ball6_active)	{$ball6_H = $sorted_hot_numbers_expanded[5];}
if ($ball7_active)	{$ball7_H = $sorted_hot_numbers_expanded[6];}
#base - assume min 3 balls
$hot_board_selected =  $ball1_H.",".$ball2_H.",".$ball3_H;
$hot_board_selected_color = 'red,red,red';
if ($ball4_active) {$hot_board_selected = $hot_board_selected.",".$ball4_H;$hot_board_selected_color = $hot_board_selected_color.',red';}
if ($ball5_active) {$hot_board_selected = $hot_board_selected.",".$ball5_H;$hot_board_selected_color = $hot_board_selected_color.',red';}
if ($ball6_active) {$hot_board_selected = $hot_board_selected.",".$ball6_H;$hot_board_selected_color = $hot_board_selected_color.',red';}
if ($ball7_active) {$hot_board_selected = $hot_board_selected.",".$ball7_H;$hot_board_selected_color = $hot_board_selected_color.',red';}
if ($powerball_active)
{
	$sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
	$powerball = $sorted_hot_numbers_expanded_pb[0];
	$hot_board_selected = $hot_board_selected.'|'.$powerball;
	$hot_board_selected_color = $hot_board_selected_color.'|darkred';
	if ($powerball2_active)
	{
		$powerball2 = $sorted_hot_numbers_expanded_pb[1];
		$hot_board_selected = $hot_board_selected.','.$powerball2;
		$hot_board_selected_color = $hot_board_selected_color.',darkred';
	}
}
if ($powerball_active)	{$powerball_hot = $powerball;}
if ($powerball2_active)	{$powerball2_hot = $powerball2;}
###END : HOT SELECTOR
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START : COLD-SELECTOR
$sorted_cold_numbers_expanded = explode(',',$sorted_cold_numbers);
$ball1_C = $sorted_cold_numbers_expanded[0];
$ball2_C = $sorted_cold_numbers_expanded[1];
$ball3_C = $sorted_cold_numbers_expanded[2];
if ($ball4_active)	{$ball4_C = $sorted_cold_numbers_expanded[3];}
if ($ball5_active)	{$ball5_C = $sorted_cold_numbers_expanded[4];}
 //6 feb 2019 
 //added check for cold balls - senario ofr powerball MC selector, 2nd board used  b4,b5,b6, but b6 does not exist cause range avail in sorted list only has 5 picked out
 //To-Do - increase number of cold balls in list to work with !
if ($ball6_active)	{
	if (isset($sorted_cold_numbers_expanded[5])) 
		{$ball6_C = $sorted_cold_numbers_expanded[5];}
	else 
		{$ball6_C = $sorted_cold_numbers_expanded[0];}
}     
if ($ball7_active)	{
	if (isset($sorted_cold_numbers_expanded[6])) 
		{$ball7_C = $sorted_cold_numbers_expanded[6];}
	else 
		{$ball7_C = $sorted_cold_numbers_expanded[1];}
}
#base - assume min 3 balls
$cold_board_selected =  $ball1_C.",".$ball2_C.",".$ball3_C;
$cold_board_selected_color = 'blue,blue,blue';
if ($ball4_active) {$cold_board_selected = $cold_board_selected.",".$ball4_C;$cold_board_selected_color = $cold_board_selected_color.',blue';}
if ($ball5_active) {$cold_board_selected = $cold_board_selected.",".$ball5_C;$cold_board_selected_color = $cold_board_selected_color.',blue';}
if ($ball6_active) {$cold_board_selected = $cold_board_selected.",".$ball6_C;$cold_board_selected_color = $cold_board_selected_color.',blue';}
if ($ball7_active) {$cold_board_selected = $cold_board_selected.",".$ball7_C;$cold_board_selected_color = $cold_board_selected_color.',blue';}
if ($powerball_active)
{
	$sorted_cold_numbers_expanded_pb = explode(',',$sorted_cold_numbers_pb);
	$powerball = $sorted_cold_numbers_expanded_pb[0];
	$cold_board_selected = $cold_board_selected.'|'.$powerball;
	$cold_board_selected_color = $cold_board_selected_color.'|darkblue';
	if ($powerball2_active)
	{
		$powerball2 = $sorted_cold_numbers_expanded_pb[1];
		$cold_board_selected = $cold_board_selected.','.$powerball2;
		$cold_board_selected_color = $cold_board_selected_color.',darkblue';
	}
}
if ($powerball_active)	{$powerball_cold = $powerball;}
if ($powerball2_active)	{$powerball2_cold = $powerball2;}

##END : COLD SELECTOR
?><br><?php print("HOT BOARD:");print($hot_board_selected);
?><br><?php print("COLD BOARD:");print($cold_board_selected);
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START: HOT-COLD SELECTOR  5H+H; 5C+C
$combo_hot_cold_board_selected =  $hot_board_selected.":".$cold_board_selected;
$combo_hot_cold_board_selected_color = $hot_board_selected_color.":".$cold_board_selected_color;
?><br><?php print("HOT-COLD BOARD:");print($combo_hot_cold_board_selected);
##END : HOT-COLD SELECTOR
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START HOT-COLD SPLIT SELECTOR   (Board 1) 3H2C + H; (Board 2) 3C2H + C
#CONFIG OF SPLIT FOR HOT-COLD SPLIT SELECTOR
# max balls = 7 --> 4H 3C   / 4C 3H
#							6	-->	3H 3C		/ 3C 3H
#							5	-->	3H 2C		/ 3C 2H
#							4	-->	2H 2C		/ 2C 2H
#							3 -->	2H 1C		/ 2C 1H
$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 			{ $split_H_max = 4; $split_C_max = 3; $split_M_max = 0;}
else if ($balls_max == 6) { $split_H_max = 3; $split_C_max = 3; $split_M_max = 0;}
else if ($balls_max == 5) { $split_H_max = 3; $split_C_max = 2; $split_M_max = 0;}
else if ($balls_max == 4) { $split_H_max = 2; $split_C_max = 2; $split_M_max = 0;}
else if ($balls_max == 3) { $split_H_max = 2; $split_C_max = 1; $split_M_max = 0;}
#BOARD 1  3H2C + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball1_H;																							$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball1_H.",".$ball2_H;														$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H;							$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball4_H;	$part_H_color =  'red,red,red,red';}
			##Build up C
			if ($split_C_max == 1) { $part_C =  $ball1_C;																							$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball1_C.",".$ball2_C;														$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C;							$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C.",".$ball4_C;	$part_C_color =  'blue,blue,blue,blue';}
			$combo_hot_cold_split_board_selected_1 = $part_H.','.$part_C;
			$combo_hot_cold_split_board_selected_1_color = $part_H_color.','.$part_C_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_cold_split_board_selected_1 = $combo_hot_cold_split_board_selected_1.'|'.$powerball_hot;
				$combo_hot_cold_split_board_selected_1_color = $combo_hot_cold_split_board_selected_1_color.'|darkred';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_cold_split_board_selected_1 = $combo_hot_cold_split_board_selected_1.','.$powerball2_hot;
					$combo_hot_cold_split_board_selected_1_color = $combo_hot_cold_split_board_selected_1_color.',darkred';
				}
			}
#BOARD 2 3C2H + C
if ($balls_max == 7) 			{ $split_H_max = 3; $split_C_max = 4; $split_M_max = 0;}
else if ($balls_max == 6) { $split_H_max = 3; $split_C_max = 3; $split_M_max = 0;}
else if ($balls_max == 5) { $split_H_max = 2; $split_C_max = 3; $split_M_max = 0;}
else if ($balls_max == 4) { $split_H_max = 2; $split_C_max = 2; $split_M_max = 0;}
else if ($balls_max == 3) { $split_H_max = 1; $split_C_max = 2; $split_M_max = 0;}
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up  C
			if ($split_C_max == 1) { $part_C =  $ball3_C;																							$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball3_C.",".$ball4_C;														$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball3_C.",".$ball4_C.",".$ball5_C;							$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball3_C.",".$ball4_C.",".$ball5_C.",".$ball6_C;	$part_C_color =  'blue,blue,blue,blue';}
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball4_H;																							$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball4_H.",".$ball5_H;														$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball4_H.",".$ball5_H.",".$ball6_H;							$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball4_H.",".$ball5_H.",".$ball6_H.",".$ball7_H;	$part_H_color =  'red,red,red,red';}
			$combo_hot_cold_split_board_selected_2 = $part_C.','.$part_H;
			$combo_hot_cold_split_board_selected_2_color = $part_C_color.','.$part_H_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 2 - COLD POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_cold_split_board_selected_2 = $combo_hot_cold_split_board_selected_2.'|'.$powerball_cold;
				$combo_hot_cold_split_board_selected_2_color = $combo_hot_cold_split_board_selected_2_color.'|darkblue';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_cold_split_board_selected_2 = $combo_hot_cold_split_board_selected_2.','.$powerball2_cold;
					$combo_hot_cold_split_board_selected_2_color = $combo_hot_cold_split_board_selected_2_color.',darkblue';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_hot_cold_split_board_selected = $combo_hot_cold_split_board_selected_1.":".$combo_hot_cold_split_board_selected_2;
$combo_hot_cold_split_board_selected_color = $combo_hot_cold_split_board_selected_1_color.":".$combo_hot_cold_split_board_selected_2_color;
?><br><?php print("COMBO H-C-split:");print($combo_hot_cold_split_board_selected);
##END : HOT COLD SPLIT SELECTOR
/*----------------------------------------------------------------------------*/


// 30 july 2018  -   override scrap  HC selector and use HCs  as the new HC

$combo_hot_cold_board_selected = $combo_hot_cold_split_board_selected;
$combo_hot_cold_board_selected_color = $combo_hot_cold_split_board_selected_color;
#$combo_hot_cold_split_board_selected = '';
#$combo_hot_cold_split_board_selected_color = '';


/*----------------------------------------------------------------------------*/
##START : HOT-MEDIAN-COLD SELECTOR  2H3M+H; 3M2C + C
#CONFIG OF SPLIT FOR HOT-MEDIAN-COLD SPLIT SELECTOR
# max balls = 7 --> 3H 4M 						/  4M-3C
#							6	-->	3H 3M 						/	 3M-3C
#							5	-->	2H 3M     PB-1H   /  3M-2C PB-1C
#							4	-->	2H 2M 						/  2M-2C
#							3 -->	1H 2M 						/	 1M-2C
$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
$ball1_M = $sorted_median_numbers_expanded[0];
$ball2_M = $sorted_median_numbers_expanded[1];
$ball3_M = $sorted_median_numbers_expanded[2];
/*if ($ball4_active)	{$ball4_M = $sorted_median_numbers_expanded[3];}
if ($ball5_active)	{$ball5_M = $sorted_median_numbers_expanded[4];}
if ($ball6_active)	{$ball6_M = $sorted_median_numbers_expanded[5];}
if ($ball7_active)	{$ball7_M = $sorted_median_numbers_expanded[6];}
*/
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_median_numbers_expanded[3])) {$ball4_M = $sorted_median_numbers_expanded[3];}
if (isset($sorted_median_numbers_expanded[4])) {$ball5_M = $sorted_median_numbers_expanded[4];}
if (isset($sorted_median_numbers_expanded[5])) {$ball6_M = $sorted_median_numbers_expanded[5];}
if (isset($sorted_median_numbers_expanded[6])) {$ball7_M = $sorted_median_numbers_expanded[6];}

$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 			{ $split_H_max = 3; $split_C_max = 0; $split_M_max = 4;}
else if ($balls_max == 6) { $split_H_max = 3; $split_C_max = 0; $split_M_max = 3;}
else if ($balls_max == 5) { $split_H_max = 2; $split_C_max = 0; $split_M_max = 3;}
else if ($balls_max == 4) { $split_H_max = 2; $split_C_max = 0; $split_M_max = 2;}
else if ($balls_max == 3) { $split_H_max = 1; $split_C_max = 0; $split_M_max = 2;}
#BOARD 1  3H2M + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball1_H;																							$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball1_H.",".$ball2_H;														$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H;							$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball4_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball1_M;																							$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball1_M.",".$ball2_M;														$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M;							$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M;	$part_M_color =  'orange,orange,orange,orange';}
			$combo_hot_median_cold_board_selected_1 = $part_H.','.$part_M;
			$combo_hot_median_cold_board_selected_1_color = $part_H_color.','.$part_M_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_median_cold_board_selected_1 = $combo_hot_median_cold_board_selected_1.'|'.$powerball_hot;
				$combo_hot_median_cold_board_selected_1_color = $combo_hot_median_cold_board_selected_1_color.'|darkred';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_cold_board_selected_1 = $combo_hot_median_cold_board_selected_1.','.$powerball2_hot;
					$combo_hot_median_cold_board_selected_1_color = $combo_hot_median_cold_board_selected_1_color.',darkred';
				}
			}
#BOARD 2 3M2C+ C
if ($balls_max == 7) 			{ $split_H_max = 0; $split_C_max = 3; $split_M_max = 4;}
else if ($balls_max == 6) { $split_H_max = 0; $split_C_max = 3; $split_M_max = 3;}
else if ($balls_max == 5) { $split_H_max = 0; $split_C_max = 2; $split_M_max = 3;}
else if ($balls_max == 4) { $split_H_max = 0; $split_C_max = 2; $split_M_max = 2;}
else if ($balls_max == 3) { $split_H_max = 0; $split_C_max = 2; $split_M_max = 1;}
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball4_M;																							$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball4_M.",".$ball5_M;														$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M;							$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M.",".$ball7_M;	$part_M_color =  'orange,orange,orange,orange';}
			##Build up C
			if ($split_C_max == 1) { $part_C =  $ball1_C;																							$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball1_C.",".$ball2_C;														$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C;							$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C.",".$ball4_C;	$part_C_color =  'blue,blue,blue,blue';}
			$combo_hot_median_cold_board_selected_2 = $part_M.','.$part_C;
			$combo_hot_median_cold_board_selected_2_color = $part_M_color.','.$part_C_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 2 - COLD POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_median_cold_board_selected_2 = $combo_hot_median_cold_board_selected_2.'|'.$powerball_cold;
				$combo_hot_median_cold_board_selected_2_color = $combo_hot_median_cold_board_selected_2_color.'|darkblue';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_cold_board_selected_2 = $combo_hot_median_cold_board_selected_2.','.$powerball2_cold;
					$combo_hot_median_cold_board_selected_2_color = $combo_hot_median_cold_board_selected_2_color.',darkblue';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_hot_median_cold_board_selected = $combo_hot_median_cold_board_selected_1.":".$combo_hot_median_cold_board_selected_2;
$combo_hot_median_cold_board_selected_color = $combo_hot_median_cold_board_selected_1_color.":".$combo_hot_median_cold_board_selected_2_color;

?><br><?php print("COMBO H-M-C:");print($combo_hot_median_cold_board_selected);
##END : HOT-MEDIAN-COLD SELECTOR  3H2M+H; 3M2C + C
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START : HOT-MEDIAN-COLD-SPLIT SELECTOR         2H2M1C+H; 2H2M1C+C
#CONFIG OF SPLIT FOR HOT-MEDIAN-COLD SPLIT SPLIT SELECTOR
# max balls = 7 --> 2H 3M 2C						/  2H 3M 2C
#							6	-->	2H 2M 2C						/	 2H 2M 3C
#							5	-->	2H 2M 1C    PB-1H   /  2H 2M 1C PB-1C
#							4	-->	1H 2M 1C						/  1H 2M 1C
#							3 -->	1H 1M 1C						/	 1H 1M 1C
$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
$ball1_M = $sorted_median_numbers_expanded[0];
$ball2_M = $sorted_median_numbers_expanded[1];
$ball3_M = $sorted_median_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_median_numbers_expanded[3])) {$ball4_M = $sorted_median_numbers_expanded[3];}
if (isset($sorted_median_numbers_expanded[4])) {$ball5_M = $sorted_median_numbers_expanded[4];}
if (isset($sorted_median_numbers_expanded[5])) {$ball6_M = $sorted_median_numbers_expanded[5];}
if (isset($sorted_median_numbers_expanded[6])) {$ball7_M = $sorted_median_numbers_expanded[6];}

$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 			{ $split_H_max = 2; $split_C_max = 2; $split_M_max = 3;}
else if ($balls_max == 6) { $split_H_max = 2; $split_C_max = 2; $split_M_max = 2;}
else if ($balls_max == 5) { $split_H_max = 2; $split_C_max = 1; $split_M_max = 2;}
else if ($balls_max == 4) { $split_H_max = 1; $split_C_max = 1; $split_M_max = 2;}
else if ($balls_max == 3) { $split_H_max = 1; $split_C_max = 1; $split_M_max = 1;}
#BOARD 1  3H2M + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball1_H; 																						$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball1_H.",".$ball2_H; 													$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H; 							$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball4_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball1_M;																							$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball1_M.",".$ball2_M;														$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M;							$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M;	$part_M_color =  'orange,orange,orange,orange';}
			##Build up C
			if ($split_C_max == 1) { $part_C =  $ball1_C;																							$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball1_C.",".$ball2_C;														$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C;							$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C.",".$ball4_C;	$part_C_color =  'blue,blue,blue,blue';}
			$combo_hot_median_cold_split_board_selected_1 = $part_H.','.$part_M.','.$part_C;
			$combo_hot_median_cold_split_board_selected_1_color = $part_H_color.','.$part_M_color.','.$part_C_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_median_cold_split_board_selected_1 = $combo_hot_median_cold_split_board_selected_1.'|'.$powerball_hot;
				$combo_hot_median_cold_split_board_selected_1_color = $combo_hot_median_cold_split_board_selected_1_color.'|darkred';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_cold_split_board_selected_1 = $combo_hot_median_cold_split_board_selected_1.','.$powerball2_hot;
					$combo_hot_median_cold_split_board_selected_1_color = $combo_hot_median_cold_split_board_selected_1_color.',darkred';
				}
			}
#BOARD 2
if ($balls_max == 7) 			{ $split_H_max = 2; $split_C_max = 2; $split_M_max = 3;}
else if ($balls_max == 6) { $split_H_max = 2; $split_C_max = 2; $split_M_max = 2;}
else if ($balls_max == 5) { $split_H_max = 2; $split_C_max = 1; $split_M_max = 2;}
else if ($balls_max == 4) { $split_H_max = 1; $split_C_max = 1; $split_M_max = 2;}
else if ($balls_max == 3) { $split_H_max = 1; $split_C_max = 1; $split_M_max = 1;}
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball2_H;																	$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball3_H.",".$ball4_H;								$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball4_H.",".$ball5_H.",".$ball6_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball2_M;																	$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball3_M.",".$ball4_M;								$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M;	$part_M_color =  'orange,orange,orange';}
			##Build up C
			if ($split_C_max == 1) { $part_C =  $ball2_C;																	$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball3_C.",".$ball4_C;								$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball4_C.",".$ball5_C.",".$ball6_C;	$part_C_color =  'blue,blue,blue';}
			$combo_hot_median_cold_split_board_selected_2 = $part_H.','.$part_M.','.$part_C;
			$combo_hot_median_cold_split_board_selected_2_color = $part_H_color.','.$part_M_color.','.$part_C_color;
			##Build up powerball
			if ($powerball_active)    # BOARD 2 - COLD POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_median_cold_split_board_selected_2 = $combo_hot_median_cold_split_board_selected_2.'|'.$powerball_cold;
				$combo_hot_median_cold_split_board_selected_2_color = $combo_hot_median_cold_split_board_selected_2_color.'|darkblue';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_cold_split_board_selected_2 = $combo_hot_median_cold_split_board_selected_2.','.$powerball2_cold;
					$combo_hot_median_cold_split_board_selected_2_color = $combo_hot_median_cold_split_board_selected_2_color.',darkblue';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_hot_median_cold_split_board_selected = $combo_hot_median_cold_split_board_selected_1.":".$combo_hot_median_cold_split_board_selected_2;
$combo_hot_median_cold_split_board_selected_color = $combo_hot_median_cold_split_board_selected_1_color.":".$combo_hot_median_cold_split_board_selected_2_color;
?><br><?php print("COMBO H-M-C-split:");print($combo_hot_median_cold_split_board_selected);

##END : HOT-MEDIAN-COLD-SPLIT SELECTOR
/*----------------------------------------------------------------------------*/

//override 30 jul 18
//    HMCs --> HMC

$combo_hot_median_cold_board_selected = $combo_hot_median_cold_split_board_selected;
$combo_hot_median_cold_board_selected_color = $combo_hot_median_cold_split_board_selected_color;




/*----------------------------------------------------------------------------*/
##START MEDIAN  SELECTOR   (Board 1) 6M; (Board 2) 6M
#CONFIG OF SPLIT FOR HOT-COLD SPLIT SELECTOR

#print("Sorted median numbers:<");print($sorted_median_numbers);print(">");
$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
#BOARD 1  6M ;
$index = 0;
$ball1_M = $sorted_median_numbers_expanded[$index];$index++;
$ball2_M = $sorted_median_numbers_expanded[$index];$index++;
$ball3_M = $sorted_median_numbers_expanded[$index];$index++;
if ($ball4_active)	{$ball4_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball5_active) 	{$ball5_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball6_active)	{$ball6_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball7_active)	{$ball7_M = $sorted_median_numbers_expanded[$index];$index++;}
#base - assume min 3 balls
$median_board_selected =  $ball1_M.",".$ball2_M.",".$ball3_M;
$median_board_selected_color = 'orange,orange,orange';
if ($ball4_active) {$median_board_selected = $median_board_selected.",".$ball4_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball5_active) {$median_board_selected = $median_board_selected.",".$ball5_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball6_active) {$median_board_selected = $median_board_selected.",".$ball6_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball7_active) {$median_board_selected = $median_board_selected.",".$ball7_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($powerball_active)
{
	$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
	$powerball = $sorted_median_numbers_expanded_pb[0];
	//$powerball_brd_2 = $sorted_median_numbers_expanded_pb[1];
	$median_board_selected = $median_board_selected.'|'.$powerball;
	$median_board_selected_color = $median_board_selected_color.'|darkorange';
	if ($powerball2_active)
	{
		$powerball2 = $sorted_median_numbers_expanded_pb[1];
		//$powerball_brd_2 = $sorted_median_numbers_expanded_pb[0];
		$median_board_selected = $median_board_selected.','.$powerball2;
		$median_board_selected_color = $median_board_selected_color.',darkorange';
	}
}
if ($powerball_active)	{$powerball_hot = $powerball;}    // ? what is this 
if ($powerball2_active)	{$powerball2_hot = $powerball2;}
$median_board_selected_1 = $median_board_selected;
$median_board_selected_1_color = $median_board_selected_color;
#BOARD 2  6M ;	
//copy pase of above - only changed _2 + extend array   [7][8][9]..etc!!!!
//to-do check that index is valid - ie that there are enough median numbers to make up x2 boards.
//to-do randomise selection of x2 boards numbers so distubuted evenly accross number range [1-52]
$ball1_M = $sorted_median_numbers_expanded[$index];$index++;
$ball2_M = $sorted_median_numbers_expanded[$index];$index++;
$ball3_M = $sorted_median_numbers_expanded[$index];$index++;
if ($ball4_active)	{$ball4_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball5_active) 	{$ball5_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball6_active)	{$ball6_M = $sorted_median_numbers_expanded[$index];$index++;}
if ($ball7_active)	{$ball7_M = $sorted_median_numbers_expanded[$index];$index++;}
#base - assume min 3 balls
$median_board_selected =  $ball1_M.",".$ball2_M.",".$ball3_M;
$median_board_selected_color = 'orange,orange,orange';
if ($ball4_active) {$median_board_selected = $median_board_selected.",".$ball4_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball5_active) {$median_board_selected = $median_board_selected.",".$ball5_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball6_active) {$median_board_selected = $median_board_selected.",".$ball6_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($ball7_active) {$median_board_selected = $median_board_selected.",".$ball7_M; $median_board_selected_color = $median_board_selected_color.',orange';}
if ($powerball_active)
{
	$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
	$powerball = $sorted_median_numbers_expanded_pb[1];    /// update 6 feb 2019   use a different meduim ball  [0] --> [1] as board1 has [0]
	$median_board_selected = $median_board_selected.'|'.$powerball;
	$median_board_selected_color = $median_board_selected_color.'|darkorange';
	if ($powerball2_active)
	{
		$powerball2 = $sorted_median_numbers_expanded_pb[0];      //switch     
		$median_board_selected = $median_board_selected.','.$powerball2;
		$median_board_selected_color = $median_board_selected_color.',darkorange';
	}
}
if ($powerball_active)	{$powerball_hot = $powerball;}    // what is this ? 
if ($powerball2_active)	{$powerball2_hot = $powerball2;}
$median_board_selected_2 = $median_board_selected;
$median_board_selected_2_color = $median_board_selected_color;

## COMBINE BOARD 1 + BOARD 2
$median_board_selected = $median_board_selected_1.":".$median_board_selected_2;
$median_board_selected_color = $median_board_selected_1_color.":".$median_board_selected_2_color;
?><br><?php print("COMBO M :");print($median_board_selected);
##END : MEDIAN  SELECTOR
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
##START : HOT-MEDIAN  SELECTOR    (HM)     2H2M1C+H; 2H2M1C+C
#CONFIG OF SPLIT FOR HOT-MEDIAN  SELECTOR
# max balls = 7 --> 2H 3M 2C			/  2H 3M 2C
#			6	-->	2H 2M 2C		    /  2H 2M 3C
#			5	-->	2H 2M 1C    PB-1H   /  2H 2M 1C PB-1C
#			4	-->	1H 2M 1C			/  1H 2M 1C
#			3 -->	1H 1M 1C			/  1H 1M 1C
$sorted_hot_numbers_expanded = explode(',',$sorted_hot_numbers);
$ball1_H = $sorted_hot_numbers_expanded[0];
$ball2_H = $sorted_hot_numbers_expanded[1];
$ball3_H = $sorted_hot_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_hot_numbers_expanded[3])) {$ball4_H = $sorted_hot_numbers_expanded[3];}
if (isset($sorted_hot_numbers_expanded[4])) {$ball5_H = $sorted_hot_numbers_expanded[4];}
if (isset($sorted_hot_numbers_expanded[5])) {$ball6_H = $sorted_hot_numbers_expanded[5];}
if (isset($sorted_hot_numbers_expanded[6])) {$ball7_H = $sorted_hot_numbers_expanded[6];}

$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
$ball1_M = $sorted_median_numbers_expanded[0];
$ball2_M = $sorted_median_numbers_expanded[1];
$ball3_M = $sorted_median_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_median_numbers_expanded[3])) {$ball4_M = $sorted_median_numbers_expanded[3];}
if (isset($sorted_median_numbers_expanded[4])) {$ball5_M = $sorted_median_numbers_expanded[4];}
if (isset($sorted_median_numbers_expanded[5])) {$ball6_M = $sorted_median_numbers_expanded[5];}
if (isset($sorted_median_numbers_expanded[6])) {$ball7_M = $sorted_median_numbers_expanded[6];}

$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 	  { $split_H_max = 4;  $split_M_max = 3;}
else if ($balls_max == 6) { $split_H_max = 3;  $split_M_max = 3;}
else if ($balls_max == 5) { $split_H_max = 3;  $split_M_max = 2;}
else if ($balls_max == 4) { $split_H_max = 2;  $split_M_max = 2;}
else if ($balls_max == 3) { $split_H_max = 2;  $split_M_max = 1;}
#BOARD 1  3H3M + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball1_H; 												$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball1_H.",".$ball2_H; 							$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H; 				$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball4_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball1_M;												$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball1_M.",".$ball2_M;								$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M;				$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M;	$part_M_color =  'orange,orange,orange,orange';}
			
			$combo_hot_median_board_selected_1 = $part_H.','.$part_M;
			$combo_hot_median_board_selected_1_color = $part_H_color.','.$part_M_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				 $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				 $powerball = $sorted_hot_numbers_expanded_pb[0];
				$combo_hot_median_board_selected_1 = $combo_hot_median_board_selected_1.'|'.$powerball;
				$combo_hot_median_board_selected_1_color = $combo_hot_median_board_selected_1_color.'|darkred';
				if ($powerball2_active)
				{
					$powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_board_selected_1 = $combo_hot_median_board_selected_1.','.$powerball2;
					$combo_hot_median_board_selected_1_color = $combo_hot_median_board_selected_1_color.',darkred';
				}
			}
#BOARD 2
if ($balls_max == 7) 	  { $split_H_max = 3; $split_M_max = 4; }
else if ($balls_max == 6) { $split_H_max = 3; $split_M_max = 3; }
else if ($balls_max == 5) { $split_H_max = 2; $split_M_max = 3; }
else if ($balls_max == 4) { $split_H_max = 2; $split_M_max = 2; }
else if ($balls_max == 3) { $split_H_max = 1; $split_M_max = 2; }
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball2_H;									$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball3_H.",".$ball4_H;					$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball4_H.",".$ball5_H.",".$ball6_H;	$part_H_color =  'red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball2_M;									$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball3_M.",".$ball4_M;					$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M;	$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball5_M.",".$ball6_M.",".$ball7_M.",".$ball8_M;	$part_M_color =  'orange,orange,orange,orange';}

			$combo_hot_median_board_selected_2 = $part_M.",".$part_H;
			$combo_hot_median_board_selected_2_color = $part_M_color.",".$part_H_color;
			##Build up powerball
			if ($powerball_active)    # BOARD 2 - COLD POWERBALL
			{
				//$sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
				$powerball = $sorted_median_numbers_expanded_pb[0];    
				$combo_hot_median_board_selected_2 = $combo_hot_median_board_selected_2.'|'.$powerball;
				$combo_hot_median_board_selected_2_color = $combo_hot_median_board_selected_2_color.'|darkorange';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
					$powerball2 = $sorted_median_numbers_expanded_pb[1];    
					$combo_hot_median_board_selected_2 = $combo_hot_median_board_selected_2.','.$powerball2;
					$combo_hot_median_board_selected_2_color = $combo_hot_median_board_selected_2_color.',darkorange';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_hot_median_board_selected = $combo_hot_median_board_selected_1.":".$combo_hot_median_board_selected_2;
$combo_hot_median_board_selected_color = $combo_hot_median_board_selected_1_color.":".$combo_hot_median_board_selected_2_color;
?><br><?php print("COMBO H-M:");print($combo_hot_median_board_selected);

##END : HOT-MEDIAN  SELECTOR
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START : MEDIAN-COLD (MC) SELECTOR         
#CONFIG OF SPLIT FOR MEDIAN-COLD  SELECTOR
# max balls = 7 --> 2H 3M 2C			/  2H 3M 2C
#			6	-->	2H 2M 2C		    /  2H 2M 3C
#			5	-->	2H 2M 1C    PB-1H   /  2H 2M 1C PB-1C
#			4	-->	1H 2M 1C			/  1H 2M 1C
#			3 -->	1H 1M 1C			/  1H 1M 1C
$sorted_cold_numbers_expanded = explode(',',$sorted_cold_numbers);
$ball1_C = $sorted_cold_numbers_expanded[0];
$ball2_C = $sorted_cold_numbers_expanded[1];
$ball3_C = $sorted_cold_numbers_expanded[2];
#changed strategy for above as there should be more C numbers avail in pool.
if (isset($sorted_cold_numbers_expanded[3])) {$ball4_C = $sorted_cold_numbers_expanded[3];}
if (isset($sorted_cold_numbers_expanded[4])) {$ball5_C = $sorted_cold_numbers_expanded[4];}
if (isset($sorted_cold_numbers_expanded[5])) 
		{$ball6_C = $sorted_cold_numbers_expanded[5];}
else 
		{$ball6_C = $sorted_cold_numbers_expanded[0];}
if (isset($sorted_cold_numbers_expanded[6])) 
		{$ball7_C = $sorted_cold_numbers_expanded[6];}
else 
		{$ball7_C = $sorted_cold_numbers_expanded[1];}

$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
$ball1_M = $sorted_median_numbers_expanded[0];
$ball2_M = $sorted_median_numbers_expanded[1];
$ball3_M = $sorted_median_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_median_numbers_expanded[3])) {$ball4_M = $sorted_median_numbers_expanded[3];}
if (isset($sorted_median_numbers_expanded[4])) {$ball5_M = $sorted_median_numbers_expanded[4];}
if (isset($sorted_median_numbers_expanded[5])) {$ball6_M = $sorted_median_numbers_expanded[5];}
if (isset($sorted_median_numbers_expanded[6])) {$ball7_M = $sorted_median_numbers_expanded[6];}

$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 	  { $split_C_max = 3;  $split_M_max = 4;}
else if ($balls_max == 6) { $split_C_max = 3;  $split_M_max = 3;}
else if ($balls_max == 5) { $split_C_max = 2;  $split_M_max = 3;}
else if ($balls_max == 4) { $split_C_max = 2;  $split_M_max = 2;}
else if ($balls_max == 3) { $split_C_max = 1;  $split_M_max = 2;}
#BOARD 1  3H3M + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_C_max == 1) { $part_C =  $ball1_C; 												$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball1_C.",".$ball2_C; 							$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C; 				$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball1_C.",".$ball2_C.",".$ball3_C.",".$ball4_C;	$part_C_color =  'blue,blue,blue,blue';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball1_M;												$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball1_M.",".$ball2_M;								$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M;				$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M;	$part_M_color =  'orange,orange,orange,orange';}
			
			$combo_median_cold_board_selected_1 = $part_M.','.$part_C;
			$combo_median_cold_board_selected_1_color = $part_M_color.','.$part_C_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
				$powerball = $sorted_median_numbers_expanded_pb[0];
				$combo_median_cold_board_selected_1 = $combo_median_cold_board_selected_1.'|'.$powerball;
				$combo_median_cold_board_selected_1_color = $combo_median_cold_board_selected_1_color.'|darkorange';
				if ($powerball2_active)
				{
					## already done $powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$powerball2 = $sorted_median_numbers_expanded_pb[1];
					$combo_median_cold_board_selected_1 = $combo_median_cold_board_selected_1.','.$powerball2;
					$combo_median_cold_board_selected_1_color = $combo_median_cold_board_selected_1_color.',darkorange';
				}
			}
#BOARD 2
if ($balls_max == 7) 	  { $split_C_max = 4; $split_M_max = 3; }
else if ($balls_max == 6) { $split_C_max = 3; $split_M_max = 3; }
else if ($balls_max == 5) { $split_C_max = 3; $split_M_max = 2; }
else if ($balls_max == 4) { $split_C_max = 2; $split_M_max = 2; }
else if ($balls_max == 3) { $split_C_max = 2; $split_M_max = 1; }
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up H
			if ($split_C_max == 1) { $part_C =  $ball2_C;									$part_C_color =  'blue';}
			else if ($split_C_max == 2) { $part_C =  $ball3_C.",".$ball4_C;					$part_C_color =  'blue,blue';}
			else if ($split_C_max == 3) { $part_C =  $ball4_C.",".$ball5_C.",".$ball6_C;	$part_C_color =  'blue,blue,blue';}
			else if ($split_C_max == 4) { $part_C =  $ball5_C.",".$ball6_C.",".$ball7_C.",".$ball8_C;	$part_C_color =  'blue,blue,blue,blue';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball2_M;									$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball3_M.",".$ball4_M;					$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M;	$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball5_M.",".$ball6_M.",".$ball7_M.",".$ball8_M;	$part_M_color =  'orange,orange,orange,orange';}

			$combo_median_cold_board_selected_2 = $part_C.",".$part_M;
			$combo_median_cold_board_selected_2_color = $part_C_color.",".$part_M_color;
			##Build up powerball
			if ($powerball_active)    # BOARD 2 - COLD POWERBALL
			{
				##already done $sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
				## already done $powerball = $sorted_hot_numbers_expanded_pb[0];
				$sorted_cold_numbers_expanded_pb = explode(',',$sorted_cold_numbers_pb);
				$powerball = $sorted_cold_numbers_expanded_pb[1];
				$combo_median_cold_board_selected_2 = $combo_median_cold_board_selected_2.'|'.$powerball;
				$combo_median_cold_board_selected_2_color = $combo_median_cold_board_selected_2_color.'|darkblue';
				if ($powerball2_active)
				{
					$powerball2 = $sorted_cold_numbers_expanded_pb[1];
					$combo_median_cold_board_selected_2 = $combo_median_cold_board_selected_2.','.$powerball2;
					$combo_median_cold_board_selected_2_color = $combo_median_cold_board_selected_2_color.',darkblue';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_median_cold_board_selected = $combo_median_cold_board_selected_1.":".$combo_median_cold_board_selected_2;
$combo_median_cold_board_selected_color = $combo_median_cold_board_selected_1_color.":".$combo_median_cold_board_selected_2_color;
?><br><?php print("COMBO M-C:");print($combo_median_cold_board_selected);

##END : MEDIAN-COLD  SELECTOR
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
##START : HOT-MEDIAN-MEDIAN SELECTOR         2H2M1C+H; 2H2M1C+C
#CONFIG OF SPLIT FOR HOT-MEDIAN-COLD SPLIT SPLIT SELECTOR
# max balls = 7 --> 2H 3M 2M						/  2H 3M 2C
#			  6	-->	2H 2M 2M						/	 2H 2M 3C
#			  5	-->	2H 2M 1M    PB-1H   /  2H 2M 1C PB-1C
#			  4	-->	1H 2M 1M						/  1H 2M 1C
#			  3 -->	1H 1M 1M						/	 1H 1M 1C
$sorted_hot_numbers_expanded = explode(',',$sorted_hot_numbers);
$ball1_H = $sorted_hot_numbers_expanded[0];
$ball2_H = $sorted_hot_numbers_expanded[1];
$ball3_H = $sorted_hot_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_hot_numbers_expanded[3])) {$ball4_H = $sorted_hot_numbers_expanded[3];}
if (isset($sorted_hot_numbers_expanded[4])) {$ball5_H = $sorted_hot_numbers_expanded[4];}
if (isset($sorted_hot_numbers_expanded[5])) {$ball6_H = $sorted_hot_numbers_expanded[5];}
if (isset($sorted_hot_numbers_expanded[6])) {$ball7_H = $sorted_hot_numbers_expanded[6];}

$sorted_median_numbers_expanded = explode(',',$sorted_median_numbers);
$ball1_M = $sorted_median_numbers_expanded[0];
$ball2_M = $sorted_median_numbers_expanded[1];
$ball3_M = $sorted_median_numbers_expanded[2];
#changed strategy for above as there should be more M numbers avail in pool.
if (isset($sorted_median_numbers_expanded[3])) {$ball4_M = $sorted_median_numbers_expanded[3];}
if (isset($sorted_median_numbers_expanded[4])) {$ball5_M = $sorted_median_numbers_expanded[4];}
if (isset($sorted_median_numbers_expanded[5])) {$ball6_M = $sorted_median_numbers_expanded[5];}
if (isset($sorted_median_numbers_expanded[6])) {$ball7_M = $sorted_median_numbers_expanded[6];}
if (isset($sorted_median_numbers_expanded[7])) {$ball8_M = $sorted_median_numbers_expanded[7];}

$balls_max = 3;  # default
if ($ball4_active)	{$balls_max = 4;}
if ($ball5_active)	{$balls_max = 5;}
if ($ball6_active)	{$balls_max = 6;}
if ($ball7_active)	{$balls_max = 7;}
if ($balls_max == 7) 	  { $split_H_max = 2;  $split_M_max = 5;}
else if ($balls_max == 6) { $split_H_max = 2;  $split_M_max = 4;}
else if ($balls_max == 5) { $split_H_max = 2;  $split_M_max = 3;}
else if ($balls_max == 4) { $split_H_max = 1;  $split_M_max = 3;}
else if ($balls_max == 3) { $split_H_max = 1;  $split_M_max = 2;}
#BOARD 1  3H2M + H;
			##OLD $combo_hot_cold_split_board_selected_1 =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball1_C.",".$ball2_C;
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball1_H; 															$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball1_H.",".$ball2_H; 										$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H; 							$part_H_color =  'red,red,red';}
			else if ($split_H_max == 4) { $part_H =  $ball1_H.",".$ball2_H.",".$ball3_H.",".$ball4_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball1_M;															$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball1_M.",".$ball2_M;											$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M;							$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M;	$part_M_color =  'orange,orange,orange,orange';}
			else if ($split_M_max == 5) { $part_M =  $ball1_M.",".$ball2_M.",".$ball3_M.",".$ball4_M.",".$ball5_M;	$part_M_color =  'orange,orange,orange,orange,orange';}
			
			$combo_hot_median_median_board_selected_1 = $part_H.','.$part_M;
			$combo_hot_median_median_board_selected_1_color = $part_H_color.','.$part_M_color;
			##Build up powerball
			if ($powerball_active)     # BOARD 1 - HOT POWERBALL
			{
				$sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
			    $powerball = $sorted_hot_numbers_expanded_pb[0];
				//$sorted_median_numbers_expanded_pb = explode(',',$sorted_median_numbers_pb);
				//$powerball = $sorted_median_numbers_expanded_pb[0];
				$combo_hot_median_median_board_selected_1 = $combo_hot_median_median_board_selected_1.'|'.$powerball;
				$combo_hot_median_median_board_selected_1_color = $combo_hot_median_median_board_selected_1_color.'|darkred';
				if ($powerball2_active)
				{
					$powerball2 = $sorted_hot_numbers_expanded_pb[1];
					$combo_hot_median_median_board_selected_1 = $combo_hot_median_median_board_selected_1.','.$powerball2;
					$combo_hot_median_median_board_selected_1_color = $combo_hot_median_median_board_selected_1_color.',darkred';
				}
			}
#BOARD 2
if ($balls_max == 7) 	  { $split_H_max = 2;  $split_M_max = 5;}
else if ($balls_max == 6) { $split_H_max = 2;  $split_M_max = 4;}
else if ($balls_max == 5) { $split_H_max = 2;  $split_M_max = 3;}
else if ($balls_max == 4) { $split_H_max = 1;  $split_M_max = 3;}
else if ($balls_max == 3) { $split_H_max = 1;  $split_M_max = 2;}
## NB also do not use same numbers as selected above. ie. if 5 balls and already chosed b1,b2,b3 for cold then use b4,b5 for cold below
			$part_PB = '';
			##Build up H
			if ($split_H_max == 1) { $part_H =  $ball2_H;												$part_H_color =  'red';}
			else if ($split_H_max == 2) { $part_H =  $ball3_H.",".$ball4_H;								$part_H_color =  'red,red';}
			else if ($split_H_max == 3) { $part_H =  $ball4_H.",".$ball5_H.",".$ball6_H;	$part_H_color =  'red,red,red,red';}
			##Build up M
			if ($split_M_max == 1) { $part_M =  $ball2_M;												$part_M_color =  'orange';}
			else if ($split_M_max == 2) { $part_M =  $ball3_M.",".$ball4_M;								$part_M_color =  'orange,orange';}
			else if ($split_M_max == 3) { $part_M =  $ball4_M.",".$ball5_M.",".$ball6_M;	$part_M_color =  'orange,orange,orange';}
			else if ($split_M_max == 4) { $part_M =  $ball5_M.",".$ball6_M.",".$ball7_M.",".$ball8_M;	$part_M_color =  'orange,orange,orange,orange';}
			else if ($split_M_max == 5) { $part_M =  $ball6_M.",".$ball7_M.",".$ball8_M.",".$ball9_M.",".$ball10_M;	$part_M_color =  'orange,orange,orange,orange,orange';}
			


			$combo_hot_median_median_board_selected_2 = $part_H.','.$part_M;
			$combo_hot_median_median_board_selected_2_color = $part_H_color.','.$part_M_color;
			##Build up powerball
			if ($powerball_active)    # BOARD 2 - COLD POWERBALL
			{
				
				$sorted_hot_numbers_expanded_pb = explode(',',$sorted_hot_numbers_pb);
			    $powerball = $sorted_hot_numbers_expanded_pb[1];
				$combo_hot_median_median_board_selected_2 = $combo_hot_median_median_board_selected_2.'|'.$powerball;
				$combo_hot_median_median_board_selected_2_color = $combo_hot_median_median_board_selected_2_color.'|darkred';
				if ($powerball2_active)
				{
					$powerball2 = $sorted_hot_numbers_expanded_pb[0];
					$combo_hot_median_median_board_selected_2 = $combo_hot_median_median_board_selected_2.','.$powerball2;
					$combo_hot_median_median_board_selected_2_color = $combo_hot_median_median_board_selected_2_color.',darkred';
				}
			}
## COMBINE BOARD 1 + BOARD 2
$combo_hot_median_median_board_selected = $combo_hot_median_median_board_selected_1.":".$combo_hot_median_median_board_selected_2;
$combo_hot_median_median_board_selected_color = $combo_hot_median_median_board_selected_1_color.":".$combo_hot_median_median_board_selected_2_color;
?><br><?php print("COMBO H-M-M-split:");print($combo_hot_median_median_board_selected);

##END : HOT-MEDIAN-MEDIAN  SELECTOR
/*----------------------------------------------------------------------------*/



/* -decom 6 march 2017 - use II (added tablename as param)
selector_insert_entry($db,$months_in,'H'			,$hot_board_selected,$hot_board_selected_color,																								1,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
selector_insert_entry($db,$months_in,'C'			,$cold_board_selected,$cold_board_selected_color,   																					1,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
selector_insert_entry($db,$months_in,'H-C'		,$combo_hot_cold_board_selected,$combo_hot_cold_board_selected_color,													2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
selector_insert_entry($db,$months_in,'H-C-s'	,$combo_hot_cold_split_board_selected,$combo_hot_cold_split_board_selected_color,							2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
selector_insert_entry($db,$months_in,'H-M-C'	,$combo_hot_median_cold_board_selected,$combo_hot_median_cold_board_selected_color,			 			2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
selector_insert_entry($db,$months_in,'H-M-C-s',$combo_hot_median_cold_split_board_selected,$combo_hot_median_cold_split_board_selected_color,2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active);
*/

selector_insert_entry_II($db,$months_in,'H'		,$hot_board_selected,$hot_board_selected_color,													1,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
selector_insert_entry_II($db,$months_in,'C'		,$cold_board_selected,$cold_board_selected_color,   											1,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
selector_insert_entry_II($db,$months_in,'H-C'	,$combo_hot_cold_board_selected,$combo_hot_cold_board_selected_color,							2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
//selector_insert_entry_II($db,$months_in,'H-C-s'	,$combo_hot_cold_split_board_selected,$combo_hot_cold_split_board_selected_color,               2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
selector_insert_entry_II($db,$months_in,'H-M-C'	,$combo_hot_median_cold_board_selected,$combo_hot_median_cold_board_selected_color,			 	2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
//selector_insert_entry_II($db,$months_in,'H-M-C-s',$combo_hot_median_cold_split_board_selected,$combo_hot_median_cold_split_board_selected_color,2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);

// ADDED 30 Jul 2018
selector_insert_entry_II($db,$months_in,'M'		,$median_board_selected,$median_board_selected_color,											2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
//to-do:
selector_insert_entry_II($db,$months_in,'H-M'	,$combo_hot_median_board_selected,$combo_hot_median_board_selected_color,						2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
//to-do:
selector_insert_entry_II($db,$months_in,'M-C'	,$combo_median_cold_board_selected,$combo_median_cold_board_selected_color,						2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);
//to-do:
selector_insert_entry_II($db,$months_in,'H-M-M'	,$combo_hot_median_median_board_selected,$combo_hot_median_median_board_selected_color,			2,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename);

#db_disconnect($db);
#exit;
}



##below function moved into functions-selectors.php
function selector_insert_entry_DECOM_use_II($db,$months_in,$selector_code,$board_selected,$board_selected_color,$number_of_boards,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active)
{
	$LTcode = $months_in.'m/'.$selector_code;
	if ($number_of_boards == 2)
	{
		$entries = explode(":",$board_selected);
		$board_1 = $entries[0];
		$board_2 = $entries[1];
		$entries = explode(":",$board_selected_color);
		$board_1_color = $entries[0];
		$board_2_color = $entries[1];

	}
	else if ($number_of_boards == 1) {$board_1 = $board_selected;$board_1_color=$board_selected_color;}

	###BOARD1

	if ($powerball_active)
	{
		$board_1_array = explode("|",$board_1);
		$board_1 = $board_1_array[0];
		$board_1_pb = $board_1_array[1];
		$ball_db_pb = explode(",",$board_1_pb);
	}

		$ball_db = explode(",",$board_1);
		$ball1 = $ball_db[0];
		$ball2 = $ball_db[1];
		$ball3 = $ball_db[2];
		if ($ball4_active) {$ball4 = $ball_db[3];}
		if ($ball5_active) {$ball5 = $ball_db[4];}
		if ($ball6_active) {$ball6 = $ball_db[5];}
		if ($ball7_active) {$ball7 = $ball_db[6];}

		if ($powerball_active)  {$powerball = $ball_db_pb[0];}
		if ($powerball2_active) {$powerball2 = $ball_db_pb[1];}

		#####build up insert query
		$query_part1 = '';$query_part2 = '';$query_part3 = '';$query_part4 = '';$query_part5 = '';$query_part6 = '';$query_part7 = '';$query_part8 = '';
		$query_part9 = '';$query_part10 = '';$query_part11 = '';$query_part12 = '';$query_part13 = '';$query_part14 = '';$query_part15 = '';

		$query_part1 = "INSERT into Chart_Selector_Hot_Cold_LottoEntries(board,ball_color,LTcode,drawdate_snapshot_from,drawdate_prediction_on,drawdate,lottocode,ball1,ball2,ball3";
		if ($ball4_active) $query_part2 = ",ball4";
		if ($ball5_active) $query_part3 = ",ball5";
		if ($ball6_active) $query_part4 = ",ball6";
		if ($ball7_active) $query_part5 = ",ball7";
		if ($powerball_active) $query_part6 = ",powerball";
		if ($powerball2_active) $query_part7 = ",powerball2";
		$query_part8 = ") values('1','".$board_1_color."','".$LTcode."','".$drawdate_snapshot_from."','".$drawdate_prediction_on."','".$drawdate_prediction_on."','".$lottocode_in."','$ball1','$ball2','$ball3'";
		if ($ball4_active) $query_part9 = ",'$ball4'";
		if ($ball5_active) $query_part10 = ",'$ball5'";
		if ($ball6_active) $query_part11 = ",'$ball6'";
		if ($ball7_active) $query_part12 = ",'$ball7'";
		if ($powerball_active) $query_part13 = ",'$powerball'";
		if ($powerball2_active) $query_part14 = ",'$powerball2'";
		$query_part15 = ")";
		#$sql = "INSERT into LM_LottoEntries (board,LMcode,num_draws,lotto_id,ball1,ball2,ball3,ball4,ball5,powerball,ref_number,datetime_entered,account_id,lottoname_id,drawdate,lottocode) values(2,
		#'$LM_code','$draws','$lotto_id','$ball1','$ball2','$ball3','$ball4','$ball5','$powerball', '$ref_number', NOW(), '$acc_id','$lottoname_id','".$Draw_Date_Insert_db."','".$lottocode."')";
		$sql = $query_part1.$query_part2.$query_part3.$query_part4.$query_part5.$query_part6.$query_part7.$query_part8.$query_part9.$query_part10.$query_part11.$query_part12.$query_part13.$query_part14.$query_part15;
		#print($sql);
		db_insert_chart_selectors($db,$sql);


		###BOARD2
	if ($number_of_boards == 2)
	{
		if ($powerball_active)
		{
			$board_2_array = explode("|",$board_2);
			$board_2 = $board_2_array[0];
			$board_2_pb = $board_2_array[1];
			$ball_db_pb = explode(",",$board_2_pb);
		}

			$ball_db = explode(",",$board_2);
			$ball1 = $ball_db[0];
			$ball2 = $ball_db[1];
			$ball3 = $ball_db[2];
			if ($ball4_active) {$ball4 = $ball_db[3];}
			if ($ball5_active) {$ball5 = $ball_db[4];}
			if ($ball6_active) {$ball6 = $ball_db[5];}
			if ($ball7_active) {$ball7 = $ball_db[6];}

			if ($powerball_active)  {$powerball = $ball_db_pb[0];}
			if ($powerball2_active) {$powerball2 = $ball_db_pb[1];}

			#####build up insert query
			$query_part1 = '';$query_part2 = '';$query_part3 = '';$query_part4 = '';$query_part5 = '';$query_part6 = '';$query_part7 = '';$query_part8 = '';
			$query_part9 = '';$query_part10 = '';$query_part11 = '';$query_part12 = '';$query_part13 = '';$query_part14 = '';$query_part15 = '';

			$query_part1 = "INSERT into Chart_Selector_Hot_Cold_LottoEntries(board,ball_color,LTcode,drawdate_snapshot_from,drawdate_prediction_on,drawdate,lottocode,ball1,ball2,ball3";
			if ($ball4_active) $query_part2 = ",ball4";
			if ($ball5_active) $query_part3 = ",ball5";
			if ($ball6_active) $query_part4 = ",ball6";
			if ($ball7_active) $query_part5 = ",ball7";
			if ($powerball_active) $query_part6 = ",powerball";
			if ($powerball2_active) $query_part7 = ",powerball2";
			$query_part8 = ") values('2','".$board_2_color."','".$LTcode."','".$drawdate_snapshot_from."','".$drawdate_prediction_on."','".$drawdate_prediction_on."','".$lottocode_in."','$ball1','$ball2','$ball3'";
			if ($ball4_active) $query_part9 = ",'$ball4'";
			if ($ball5_active) $query_part10 = ",'$ball5'";
			if ($ball6_active) $query_part11 = ",'$ball6'";
			if ($ball7_active) $query_part12 = ",'$ball7'";
			if ($powerball_active) $query_part13 = ",'$powerball'";
			if ($powerball2_active) $query_part14 = ",'$powerball2'";
			$query_part15 = ")";
			#$sql = "INSERT into LM_LottoEntries (board,LMcode,num_draws,lotto_id,ball1,ball2,ball3,ball4,ball5,powerball,ref_number,datetime_entered,account_id,lottoname_id,drawdate,lottocode) values(2,
			#'$LM_code','$draws','$lotto_id','$ball1','$ball2','$ball3','$ball4','$ball5','$powerball', '$ref_number', NOW(), '$acc_id','$lottoname_id','".$Draw_Date_Insert_db."','".$lottocode."')";
			$sql = $query_part1.$query_part2.$query_part3.$query_part4.$query_part5.$query_part6.$query_part7.$query_part8.$query_part9.$query_part10.$query_part11.$query_part12.$query_part13.$query_part14.$query_part15;
			#print($sql);
			db_insert_chart_selectors($db,$sql);
		}

}


function sort_numbers_distributed($string_numbers,$ball_max)
{
	$string_numbers_array = explode(",",$string_numbers);
	$number_count = $string_numbers_array[0];

	// figure out distribution

	//find highest frequency_part and store in frequency_bucket array;
	$max_fequency = 0;     // invaladate by assigning max each-time and assign count per freq bucket.
	//sample : 11,  = count of 11
			//5-5,   = ball#5 has 5 occurances / frequency
			//6-8,
			//14-5,
			//17-6,
			//27-6,
			//30-5,
			//36-5,
			//42-5,
			//45-5,
			//46-6,
			//48-7     = ball#48 has 7 occurances / frequency
	$frequency_bucket = array();   // [freqency][count|number,number,number]     [5][6|5,14,30,36,42,45]
	$i=0;	
	$frequency_array = array();
	while ($i < $number_count){
		$number_freq_combo_array = $string_numbers_array[$i+1];
		$number_freq_combo = explode("-",$number_freq_combo_array);
		$number_part = $number_freq_combo[0];
		$frequency_part = $number_freq_combo[1];
        //to-do.......
		$i++;
	}


}


function bubble_sort($arr) {
    $size = count($arr);
    for ($i=0; $i<$size; $i++) {
        for ($j=0; $j<$size-1-$i; $j++) {
            if ($arr[$j+1] < $arr[$j]) {
                swap($arr, $j, $j+1);
            }
        }
    }
    return $arr;
}
 
function swap(&$arr, $a, $b) {
    $tmp = $arr[$a];
    $arr[$a] = $arr[$b];
    $arr[$b] = $tmp;
}
 

function sort_numbers_SANDBOX($string_numbers)
{
	$string_numbers_array = explode(",",$string_numbers);
	$number_count = $string_numbers_array[0];
	//  CANT USE AS FORMAT IS A-B  !!!
	print("<br/>INPUT STRING =(".$string_numbers.")");
	$string_numbers_out = '';
	$input_array = array();
	$output_array = array();
	$input_array = explode(",",$string_numbers);
	print("<br/>INPUT ARRAY =[".$input_array."]");
	$output_array = bubble_sort($input_array);
	print("<br/>OUTPUT ARRAY =[".$output_array."]");
	// put back into string format
	$size = count($output_array);
    for ($i=0; $i<$size; $i++) {
		if ($i==0) {$string_numbers_out = $output_array[$i];}
		else {$string_numbers_out = $string_numbers_out.','.$output_array[$i];}
	}
	print("<br/>OUTPUT STRING =(".$string_numbers_out.")");
 return $string_numbers_out;
}


function sort_numbers($string_numbers)
{
	$hot_max_1 = '0';$hot_max_2 = '';$hot_max_3 = '';$hot_max_4 = '';$hot_max_5 = '';$hot_max_6 = '';$hot_max_7 = '';
	$hot_max_8 = '0';$hot_max_9 = '';$hot_max_10 = '';$hot_max_11 = '';$hot_max_12 = '';$hot_max_13 = '';$hot_max_14 = '';
	$hot_max_15 = '';$hot_max_16 = '';$hot_max_17 = '';$hot_max_18 = '';$hot_max_19 = '';$hot_max_20 = '';$hot_max_21 = '';
	$hot_max_22 = '';$hot_max_23 = '';$hot_max_24 = '';$hot_max_25 = '';$hot_max_26 = '';$hot_max_27 = '';$hot_max_28 = '';
	$hot_max_29 = '';$hot_max_30 = '';

	$hot_max_1_freq = '0';$hot_max_2_freq = '0';$hot_max_3_freq = '0';$hot_max_4_freq = '0';$hot_max_5_freq = '0';$hot_max_6_freq = '0';$hot_max_7_freq = '0';
	$hot_max_8_freq = '0';$hot_max_9_freq = '0';$hot_max_10_freq = '0';$hot_max_11_freq = '0';$hot_max_12_freq = '0';$hot_max_13_freq = '0';$hot_max_14_freq = '0';
	$hot_max_15_freq = '0';$hot_max_16_freq = '0';$hot_max_17_freq = '0';$hot_max_18_freq = '0';$hot_max_19_freq = '0';$hot_max_20_freq = '0';$hot_max_21_freq = '0';
	$hot_max_22_freq = '0';$hot_max_23_freq = '0';$hot_max_24_freq = '0';$hot_max_25_freq = '0';$hot_max_26_freq = '0';$hot_max_27_freq = '0';$hot_max_28_freq = '0';
	$hot_max_29_freq = '0';$hot_max_30_freq = '0';

	$string_numbers_array = explode(",",$string_numbers);
	$number_count = $string_numbers_array[0];
	$i=0;
	##SORT
	$frequency_array = array();
	while ($i < $number_count)
	{
		$number_freq_combo_array = $string_numbers_array[$i+1];
		$number_freq_combo = explode("-",$number_freq_combo_array);
		$number_part = $number_freq_combo[0];
		$frequency_part = $number_freq_combo[1];

			if ($frequency_part >= $hot_max_1_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				$hot_max_3 = $hot_max_2;				$hot_max_3_freq = $hot_max_2_freq;
				$hot_max_2 = $hot_max_1;				$hot_max_2_freq = $hot_max_1_freq;
				# asign new high
				$hot_max_1 = $number_part;			$hot_max_1_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_2_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				$hot_max_3 = $hot_max_2;				$hot_max_3_freq = $hot_max_2_freq;
				# asign new
				$hot_max_2 = $number_part;				$hot_max_2_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_3_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				# asign new
				$hot_max_3 = $number_part;				$hot_max_3_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_4_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				# asign new
				$hot_max_4 = $number_part;				$hot_max_4_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_5_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				# asign new
				$hot_max_5 = $number_part;				$hot_max_5_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_6_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				# asign new
				$hot_max_6 = $number_part;				$hot_max_6_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_7_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				# asign new
				$hot_max_7 = $number_part;				$hot_max_7_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_8_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				# asign new
				$hot_max_8 = $number_part;				$hot_max_8_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_9_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				# asign new
				$hot_max_9 = $number_part;				$hot_max_9_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_10_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				# asign new
				$hot_max_10 = $number_part;				$hot_max_10_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_11_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				# asign new
				$hot_max_11 = $number_part;				$hot_max_11_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_12_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				# asign new
				$hot_max_12 = $number_part;				$hot_max_12_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_13_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				# asign new
				$hot_max_13 = $number_part;				$hot_max_13_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_14_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				$hot_max_15 = $hot_max_14;				$hot_max_15_freq = $hot_max_14_freq;
				# asign new
				
				$hot_max_14 = $number_part;				$hot_max_14_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_15_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				$hot_max_16 = $hot_max_15;				$hot_max_16_freq = $hot_max_15_freq;
				# asign new
				$hot_max_15 = $number_part;				$hot_max_15_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_16_freq)
			{
				#shift all others down$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				$hot_max_17 = $hot_max_16;				$hot_max_17_freq = $hot_max_16_freq;
				# asign new
				$hot_max_16 = $number_part;				$hot_max_16_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_17_freq)
			{
				#shift all others down$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				$hot_max_18 = $hot_max_17;				$hot_max_18_freq = $hot_max_17_freq;
				# asign new
				$hot_max_17 = $number_part;				$hot_max_17_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_18_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				$hot_max_19 = $hot_max_18;				$hot_max_19_freq = $hot_max_18_freq;
				# asign new
				$hot_max_18 = $number_part;				$hot_max_18_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_19_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;
				$hot_max_20 = $hot_max_19;				$hot_max_20_freq = $hot_max_19_freq;
				# asign new
				$hot_max_19 = $number_part;				$hot_max_19_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_20_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;
				$hot_max_21 = $hot_max_20;				$hot_max_21_freq = $hot_max_20_freq;

				# asign new
				$hot_max_20 = $number_part;				$hot_max_20_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_21_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;
				$hot_max_22 = $hot_max_21;				$hot_max_22_freq = $hot_max_21_freq;

				# asign new
				$hot_max_21 = $number_part;				$hot_max_21_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_22_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;
				$hot_max_23 = $hot_max_22;				$hot_max_23_freq = $hot_max_22_freq;

				# asign new
				$hot_max_22 = $number_part;				$hot_max_22_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_23_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;
				$hot_max_24 = $hot_max_23;				$hot_max_24_freq = $hot_max_23_freq;

				# asign new
				$hot_max_23 = $number_part;				$hot_max_23_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_24_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;
				$hot_max_25 = $hot_max_24;				$hot_max_25_freq = $hot_max_24_freq;

				# asign new
				$hot_max_24 = $number_part;				$hot_max_24_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_25_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;
				$hot_max_26 = $hot_max_25;				$hot_max_26_freq = $hot_max_25_freq;

				# asign new
				$hot_max_25 = $number_part;				$hot_max_25_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_26_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;
				$hot_max_27 = $hot_max_26;				$hot_max_27_freq = $hot_max_26_freq;

				# asign new
				$hot_max_26 = $number_part;				$hot_max_26_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_27_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;
				$hot_max_28 = $hot_max_27;				$hot_max_28_freq = $hot_max_27_freq;

				# asign new
				$hot_max_27 = $number_part;				$hot_max_27_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_28_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;
				$hot_max_29 = $hot_max_28;				$hot_max_29_freq = $hot_max_28_freq;

				# asign new
				$hot_max_28 = $number_part;				$hot_max_28_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_29_freq)
			{
				#shift all others down
				$hot_max_30 = $hot_max_29;				$hot_max_30_freq = $hot_max_29_freq;

				# asign new
				$hot_max_29 = $number_part;				$hot_max_29_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_30_freq)
			{
				#shift all others down

				# asign new
				$hot_max_30 = $number_part;				$hot_max_30_freq = $frequency_part;
			}
			
		###  $frequency_array[$i][$frequency_part] = $number_part;
		$i++;
	}
#print("SORTED:");
/*
print($hot_max_1);print(',');
print($hot_max_2);print(',');
print($hot_max_3);print(',');
print($hot_max_4);print(',');
print($hot_max_5);print(',');
print($hot_max_6);print(',');
print($hot_max_7);print(',');
*/
/* old method
$sorted_numbers1 = $hot_max_1.','.$hot_max_2.','.$hot_max_3.','.$hot_max_4.','.$hot_max_5.','.$hot_max_6.','.$hot_max_7;
$sorted_numbers2 = $hot_max_8.','.$hot_max_9.','.$hot_max_10.','.$hot_max_11.','.$hot_max_12.','.$hot_max_13.','.$hot_max_14;
$sorted_numbers3 = $hot_max_15.','.$hot_max_16.','.$hot_max_17.','.$hot_max_18.','.$hot_max_19.','.$hot_max_20.','.$hot_max_21;

$sorted_numbers = $sorted_numbers1.','.$sorted_numbers2.','.$sorted_numbers3;
*/

$sorted_numbers = '';
if (valid_number($hot_max_1)) {$sorted_numbers = $hot_max_1;}
if (valid_number($hot_max_2)) {$sorted_numbers = $sorted_numbers.','.$hot_max_2;}
if (valid_number($hot_max_3)) {$sorted_numbers = $sorted_numbers.','.$hot_max_3;}
if (valid_number($hot_max_4)) {$sorted_numbers = $sorted_numbers.','.$hot_max_4;}
if (valid_number($hot_max_5)) {$sorted_numbers = $sorted_numbers.','.$hot_max_5;}
if (valid_number($hot_max_6)) {$sorted_numbers = $sorted_numbers.','.$hot_max_6;}
if (valid_number($hot_max_7)) {$sorted_numbers = $sorted_numbers.','.$hot_max_7;}
if (valid_number($hot_max_8)) {$sorted_numbers = $sorted_numbers.','.$hot_max_8;}
if (valid_number($hot_max_9)) {$sorted_numbers = $sorted_numbers.','.$hot_max_9;}
if (valid_number($hot_max_10)) {$sorted_numbers = $sorted_numbers.','.$hot_max_10;}
if (valid_number($hot_max_11)) {$sorted_numbers = $sorted_numbers.','.$hot_max_11;}
if (valid_number($hot_max_12)) {$sorted_numbers = $sorted_numbers.','.$hot_max_12;}
if (valid_number($hot_max_13)) {$sorted_numbers = $sorted_numbers.','.$hot_max_13;}
if (valid_number($hot_max_14)) {$sorted_numbers = $sorted_numbers.','.$hot_max_14;}
if (valid_number($hot_max_15)) {$sorted_numbers = $sorted_numbers.','.$hot_max_15;}
if (valid_number($hot_max_16)) {$sorted_numbers = $sorted_numbers.','.$hot_max_16;}
if (valid_number($hot_max_17)) {$sorted_numbers = $sorted_numbers.','.$hot_max_17;}
if (valid_number($hot_max_18)) {$sorted_numbers = $sorted_numbers.','.$hot_max_18;}
if (valid_number($hot_max_19)) {$sorted_numbers = $sorted_numbers.','.$hot_max_19;}
if (valid_number($hot_max_20)) {$sorted_numbers = $sorted_numbers.','.$hot_max_20;}
if (valid_number($hot_max_21)) {$sorted_numbers = $sorted_numbers.','.$hot_max_21;}
if (valid_number($hot_max_22)) {$sorted_numbers = $sorted_numbers.','.$hot_max_22;}
if (valid_number($hot_max_23)) {$sorted_numbers = $sorted_numbers.','.$hot_max_23;}
if (valid_number($hot_max_24)) {$sorted_numbers = $sorted_numbers.','.$hot_max_24;}
if (valid_number($hot_max_25)) {$sorted_numbers = $sorted_numbers.','.$hot_max_25;}
if (valid_number($hot_max_26)) {$sorted_numbers = $sorted_numbers.','.$hot_max_26;}
if (valid_number($hot_max_27)) {$sorted_numbers = $sorted_numbers.','.$hot_max_27;}
if (valid_number($hot_max_28)) {$sorted_numbers = $sorted_numbers.','.$hot_max_28;}
if (valid_number($hot_max_29)) {$sorted_numbers = $sorted_numbers.','.$hot_max_29;}
if (valid_number($hot_max_30)) {$sorted_numbers = $sorted_numbers.','.$hot_max_30;}


return($sorted_numbers);
}
?>
