﻿<?php
function get_daterange_lottonumbers2016($db,$number_of_months,$drawdate)
{
		/*$current_drawdate_results = $drawdate;
	$drawdate_results_end = $drawdate;

	$sQuery = "select drawdate from ".$lottotable." order by drawdate desc";
	$result = mysql_query($sQuery);
	$myrow = mysql_fetch_array($result);
	$i_counter = 0;
	$start_counter = false;
	$continue_loop = true;
	while (($myrow) && ($continue_loop))
	{
		$drawdate_results = substr($myrow[0],0,10);
		if ($drawdate_results == $current_drawdate_results)
		{
			$start_counter = true;
		}
		$drawdate_results_end = $drawdate_results;
		if ($start_counter) { $i_counter = $i_counter + 1;}
	  if ($i_counter == $number_of_rows) {$continue_loop = false;}
	 $myrow = mysql_fetch_array($result);
	}
	$drawdate2 = $drawdate_results_end;
	/*
	/* - DECOMISSIONED BELOW USING ABOVE TO CALC - 16 sep 2015*/
	#subtract matrix
	$JAN_days = 31;
	$FEB_days = 28;    # default to 28  - below does a check for leap year
	$MAR_days = 31;
	$APR_days = 30;
	$MAY_days = 31;
	$JUN_days = 30;
	$JUL_days = 31;
	$AUG_days = 31;
	$SEP_days = 30;
	$OCT_days = 31;
	$NOV_days = 30;
	$DEC_days = 31;
	# strip drawdate and get month
	$drawdate_strip = explode('-',$drawdate);
	$draw_year = $drawdate_strip[0];
	$draw_month = $drawdate_strip[1];

	#check if drawdate year is a leap year
	if (db_leapyear($db,$draw_year))	{	 $FEB_days = 29;	}
	if ($draw_month == '01') {$number_of_days_to_subtract = $DEC_days;}
	else if ($draw_month == '02') {$number_of_days_to_subtract = $JAN_days;}
	else if ($draw_month == '03') {$number_of_days_to_subtract = $FEB_days;}
	else if ($draw_month == '04') {$number_of_days_to_subtract = $MAR_days;}
	else if ($draw_month == '05') {$number_of_days_to_subtract = $APR_days;}
	else if ($draw_month == '06') {$number_of_days_to_subtract = $MAY_days;}
	else if ($draw_month == '07') {$number_of_days_to_subtract = $JUN_days;}
	else if ($draw_month == '08') {$number_of_days_to_subtract = $JUL_days;}
	else if ($draw_month == '09') {$number_of_days_to_subtract = $AUG_days;}
	else if ($draw_month == '10') {$number_of_days_to_subtract = $SEP_days;}
	else if ($draw_month == '11') {$number_of_days_to_subtract = $OCT_days;}
	else if ($draw_month == '12') {$number_of_days_to_subtract = $DEC_days;}

	date_default_timezone_set('Africa/Johannesburg');

		#$number_of_days_to_subtract = 31;

		if ($number_of_months == 999) #ALL results
		{
			$drawdate2 = '1970-01-01';
		}
		else
		{
		 $number_of_days_to_subtract = $number_of_days_to_subtract*$number_of_months;
		 $date1 = strtotime($drawdate);
		 $date2 = $date1-(60*60*24*$number_of_days_to_subtract);
		 $drawdate2 = date('Y-m-d',$date2);
		}
	return $drawdate2;
}



function getLottoColor($val,$count_min_max_range,$count_max_min_range)
{
	#$count_min_max_range = $hot_frequency_count,
	#$count_max_min_range = $cold_frequency_count
	$orange = 'FF8E46';
	$red = 'bb3a26'; #'D64646';
	$blue = '4679BD'; #'008ED6';
	$orange = 'orange';
	$red = 'red'; #'D64646';
	$blue = 'blue'; #'008ED6';
	#green 058a42
	#blue 4679BD
	#red bb3a26
	#grey 434344 dark grey
  #color='AFD8F8' #ligh blue / grey
 	#color='F6BD0F' # yellow
	#color='8BBA00' # light green
	#color='FF8E46' # light orange / peach
	#color='008E8E' # cyan
	#color='D64646' # red
	#color='8E468E' # purple
	#color='588526' # dark green
	#color='B3AA00' # off leaf green
	#color='008ED6' # blue
	#color='9D080D' # dark red / maroon
	#color='A186BE' # purple - lilac
	$col = $orange;	# default
	if ($val >= $count_min_max_range)	{	$col = $red;	}
	else if ($val <= $count_max_min_range)	{	$col = $blue;	}
	return $col;
}

function retrieve_min_max_dates_feb2017($db,$number_of_months,$drawdate)
{
	$min_max_date_internal_array = array();
	#$date_range_min = get_daterange_lottonumbers($lottoname_id,$number_of_rows,$drawdate);
	$date_range_min = get_daterange_lottonumbers2016($db,$number_of_months,$drawdate);
	$min_count = $ball_max;
	$min_max_date_internal_array[0]['date_range_min'] = $date_range_min;
	return $min_max_date_internal_array;
}


function retrieve_min_max_count_aug2016($db,$lottocode,$pb_lucky_ind,$number_of_months,$drawdate,$LottoTable,$ball_max,$query_full)
{
	# params
	# db - db open connection pointer
	# lottocode -
	# pb_ind -  context of powerball for all params
	# number_of_months
	#....
	#print("ind|");print($pb_lucky_ind);print("|");
	$min_max_date_internal_array = array();
	$ball_number = 1;
	#initialise all variables max set  to low and min set to high
	$max_count = 0;
	# get the date range of number to pull (based on subscription)
	#$date_range_min = get_daterange_lottonumbers($lottoname_id,$number_of_rows,$drawdate);
	$date_range_min = get_daterange_lottonumbers2016($db,$number_of_months,$drawdate);
	$min_count = $ball_max;
	#$min_max_array = array();
# main loop : get counts for each ball
# not below ball_max would be ball_max_pb when pd_ind==1 as param above by value assigned when calling function.
while ($ball_number <= $ball_max)
{
	########
	## BUILD UP QUERY TO DO BALL COUNT  -tools-hot-cold-generator-new-calc.php works out per lottocode_in
	###
	#####   runs through all ball numbers 1,2,3,4,....45 (max) to do count of occurences (frequency)
	$sql_string = str_replace('{BALL}',$ball_number,$query_full);
	$sql_string = str_replace('{DRAWDATE}',$drawdate,$sql_string);
	$sql_string = str_replace('{DRAWDATE2}',$date_range_min,$sql_string);
	#print($sql_string);
	###

		#$sQuery = "select count(*) from ".$LottoTable." where ((ball1 = ".$ball_count.") or (ball2 = ".$ball_count.") or (ball3 = ".$ball_count.") or (ball4 = ".$ball_count.") or (ball5 = ".$ball_count.") or (ball6 = ".$ball_count."))  and drawdate >= '".$date_range_min."' and drawdate <= '".$drawdate."'";
		$count = db_get_count_ball_count($db,$sql_string);
		#print("$ball_number:");print($count);
		#exit;
		#$min_max_array[$ball_number-1]['ball_count'] = $count;
		###DETERMINE MAX & MIN
		### max count will be the peak bar in the bar chart, and min will be the lowwest bar

		#NEW INSERT INTO TABLE
		#$ball_col = "no_".$ball_count."_count";
		#algorithm - logic
		# Hot_Cold_Stats is the temp table used to store the counts of each ball, each ball is listed using id and the counter id per lotto.
		# column id = ball # (1,2,3,4,5....45..,49,,,,,hiigher for other lottos
		#colum count_col_name = the name of lotto - todo - pdate to be lottocode !
		# this is overwritten for each calculation - so reults are stored in return variable $count_min_max as comma delimeter
		#print($pb_lucky_ind);
		if (($pb_lucky_ind == "powerball") || ($pb_lucky_ind == 'lucky'))
		{	#print("db-call?");
			db_update_ball_count_pb_lucky($db,$count,$ball_number,$lottocode,$pb_lucky_ind);}
		else
		{ db_update_ball_count($db,$count,$ball_number,$lottocode);}

		$ball_number++;
}
	$count_min_max = 'decommisioned-aug2016';
	$min_max_date_internal_array[0]['count_min_max'] = $count_min_max;
	$min_max_date_internal_array[0]['date_range_min'] = $date_range_min;
	return $min_max_date_internal_array;
}
?>
