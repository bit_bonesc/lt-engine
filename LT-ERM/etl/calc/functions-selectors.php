<?php

function number_even($number){if ($number % 2 == 0) {return true;} else {return false;}	}
function number_low($number,$max_balls){$result = 0;$half_marker = (int)($max_balls/2);	if ($number <= $half_marker){return true;}	else {return false;}}
function number_low_LMH($number,$max_balls)	{$result = 0;$one_third_marker = (int)($max_balls/3);if ($number <= $one_third_marker){return true;} else {return false;}}
function number_high_LMH($number,$max_balls){$result = 0;$one_third_marker = (int)($max_balls/3);	$two_third_marker = $max_balls-$one_third_marker; if ($number >= $two_third_marker){return true;}	else {return false;}}
function generate_random_even_number($max_range){	$value = 0;	$value = rand(2,$max_range);	while (!number_even($value)) { $value = rand(2,$max_range);}	return $value;}
function generate_random_odd_number($max_range){	$value = 0;	$value = rand(1,$max_range);	while (number_even($value))	{ $value = rand(1,$max_range);}	return $value;}

# not optimised - reuse of number_low function so computatinaly expensive !
function generate_random_low_number($max_range){	$value = 0;	$value = rand(1,$max_range);	while (!number_low($value,$max_range))	{ $value = rand(1,$max_range);}	return $value;}
function generate_random_high_number($max_range){	$value = 0;	$value = rand(1,$max_range);	while (number_low($value,$max_range))	{ $value = rand(1,$max_range);}	return $value;}
function generate_random_low_number_LMH($max_range){	$value = 0;	$value = rand(1,$max_range);	while (!number_low_LMH($value,$max_range))	{ $value = rand(1,$max_range);}	return $value;}

function generate_random_high_number_LMH($max_range){	$value = 0;	$value = rand(1,$max_range);	while (!number_high_LMH($value,$max_range))	{ $value = rand(1,$max_range);}	return $value;}

##VERY inefficient way - hack for now.
function generate_random_median_number_LMH($max_range){	$value = 0;	$value = rand(1,$max_range);	while (number_high_LMH($value,$max_range) || number_low_LMH($value,$max_range))	{ $value = rand(1,$max_range);}	return $value;}
function is_duplicate_number($ball,$board,$count){	$i = 0;	$board_array = explode(',',$board);  while ($i < $count)	{		if ($ball == $board_array[$i]) { return true;}	$i++;	}	return false;}


function get_from_hotcold_month($db,$lottocode_in,$months_in,$pb_ind){
	//print(">in2");
	//taken code below straight from hot-cold-generator-selectors
	$lotto_hot_cold_median_array = array();
	$lotto_hot_cold_median_count = 0;
	$lotto_hot_cold_median_array = db_return_lotto_hot_cold_median_detail($db,$months_in,$lottocode_in,$pb_ind);
	$hot_cold_median_comma = $lotto_hot_cold_median_array[0]['hot_cold_median'];
	$string_array_hot_median_cold = explode("|",$hot_cold_median_comma);
	$hot_numbers = $string_array_hot_median_cold[0];
	$cold_numbers = $string_array_hot_median_cold[1];
	$median_numbers = $string_array_hot_median_cold[2];
	$string_hot_numbers = explode(",",$hot_numbers);
	$hot_number_count = $string_hot_numbers[0];
	#####decompose cold numbers, 1st occurance is always the count
	$string_cold_numbers = explode(",",$cold_numbers);
	$cold_number_count = $string_cold_numbers[0];
	#####decompose median numbers, 1st occurance is always the count
	$string_median_numbers = explode(",",$median_numbers);
	$median_number_count = $string_median_numbers[0];
	#SORT NUMBERS HIGHEST FREQ FIRST
	//print(">sort hot");
	$sorted_hot_numbers = sort_numbers($hot_numbers);#print($sorted_hot_numbers);
	//to-do: $sorted_hot_numbers = sort_numbers_ditributed($hot_numbers,$ball_max);#print($sorted_hot_numbers);
	//print(">sort med");
	$sorted_median_numbers = sort_numbers($median_numbers);
	//print(">sort cold");
	$sorted_cold_numbers = sort_numbers($cold_numbers); 

	return $sorted_hot_numbers.'|'.$sorted_median_numbers.'|'.$sorted_cold_numbers;
}


function valid_number($value)
{
	if ((is_numeric($value)) && ($value > 0) && ($value == round($value, 0))){return true;}
	else {return false;}
}


function generate_random_selected_from_hotcold_month($db,$lottocode_in,$months_in,$chart_type,$max_balls,$pb_ind)
	{
	#print(">in1");
	$hmc_string =get_from_hotcold_month($db,$lottocode_in,$months_in,$pb_ind);
    print(">hmc_string_array");print($hmc_string);
	$hmc_string_array = explode('|',$hmc_string);
	
	$sorted_hot_numbers = $hmc_string_array[0];
	$sorted_median_numbers = $hmc_string_array[1];
	$sorted_cold_numbers = $hmc_string_array[2];   
	
	print("> sorted_hot_numbers...".$sorted_hot_numbers);
	print("> sorted_median_numbers...".$sorted_median_numbers);
	print("> sorted_cold_numbers...".$sorted_cold_numbers);

	//print("clean list of invalid + 0 as it contains ,,,,,0,,,etc.");

	//$sorted_hot_numbers = clean_list($sorted_hot_numbers);
	
	// STAT FOR now is choose x random  Hot   - later do medium once figured out how to e.g x 2 H x 4 M - or mix up hot+medium 1st 5/6 H + 1st 6 M 
	$odd_array = '';
	$even_array = '';
	$low_array = '';
	$high_array = '';
	$val_array = explode(',',$sorted_hot_numbers);
	$i = 0;
	$continue_loop = true;
	#print(">start loop..");
		while ((isset($val_array[$i])) && valid_number($val_array[$i])) {
			#print(">val=".$val_array[$i]);
			if ($chart_type == 'OE'){
				if (number_even($val_array[$i])) { 
					$even_array = $even_array.','.$val_array[$i];
				}
				else {$odd_array = $odd_array.','.$val_array[$i];
				}
			}
			else if ($chart_type == 'LH'){
				if (number_low($val_array[$i],$max_balls)) { 
					$low_array = $low_array.','.$val_array[$i];
				}
				else {
					$high_array = $high_array.','.$val_array[$i];
				}
			}
		$i++;
		if ($i > 100) {print("fatal error. exit loop1."); exit;}
	}
	// ADD ON MEDIAN NUMBERS TO MAKE ENOUGH SELECTION FROM H + M
	$val_array = explode(',',$sorted_median_numbers);
	$i = 0;
	$continue_loop = true;
	#print(">start loop..");
		while ((isset($val_array[$i])) && (valid_number($val_array[$i]))) {
			#print(">val=".$val_array[$i]);
			if ($chart_type == 'OE'){
				if (number_even($val_array[$i])) { 
					$even_array = $even_array.','.$val_array[$i];
				}
				else {$odd_array = $odd_array.','.$val_array[$i];
				}
			}
			else if ($chart_type == 'LH'){
				if (number_low($val_array[$i],$max_balls)) { 
					$low_array = $low_array.','.$val_array[$i];
				}
				else {$high_array = $high_array.','.$val_array[$i];
				}
			}
		$i++;
		if ($i > 100) {print("fatal error. exit loop2."); exit;}
	}
	#print(">loop ended.");
	if ($chart_type == 'OE'){ 
		return $even_array.'|'.$odd_array;
	}
	else if ($chart_type == 'LH') {
		 return $low_array.'|'.$high_array;
		}
	else {
		return ',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1';
	}
}


function count_str_array($str_array)
{
	$count = 0;	$i=1;    //ignore [0] as currenly has a , as first char
	$val_array = explode(',',$str_array);
	while ((isset($val_array[$i])) && (valid_number($val_array[$i]))) {
	$i++;$count++;
	if ($i > 100) {print("fatal error. exit loop3."); exit;}
	}	 
	return $count;
}

function get_random_number_from_sorted($state,$max_count,$array_sorted)
{
	// default number
	$i=0;
	if ($state == 'odd') {$number = 1;}
	else if ($state =='even') {$number = 2;}
	else if ($state =='low') {$number = 1;}
	else if ($state =='high') {$number = 50;}
	$val_array = explode(',',$array_sorted);
	$random_index = rand(1,$max_count);
	$i = $random_index-1;
	if ((isset($val_array[$i])) && (valid_number($val_array[$i]))) {$number = $val_array[$i];}
	else
	{
		// try again 
		$random_index = rand(1,$max_count);
		$i = $random_index-1;
		if ((isset($val_array[$i])) && (valid_number($val_array[$i]))) {$number = $val_array[$i];}
	}
	return $number;
}







//this is a copy from hot-cold-generator.php --function sort_numbers($string_numbers)
function sort_numbers_NOT_NEEDED_AS_ABOVE_IS_INCLUDED($string_numbers)
{
	$hot_max_1 = '0';$hot_max_2 = '';$hot_max_3 = '';$hot_max_4 = '';$hot_max_5 = '';$hot_max_6 = '';$hot_max_7 = '';
	$hot_max_8 = '0';$hot_max_9 = '';$hot_max_10 = '';$hot_max_11 = '';$hot_max_12 = '';$hot_max_13 = '';$hot_max_14 = '';

	$hot_max_1_freq = '0';$hot_max_2_freq = '0';$hot_max_3_freq = '0';$hot_max_4_freq = '0';$hot_max_5_freq = '0';$hot_max_6_freq = '0';$hot_max_7_freq = '0';
	$hot_max_8_freq = '0';$hot_max_9_freq = '0';$hot_max_10_freq = '0';$hot_max_11_freq = '0';$hot_max_12_freq = '0';$hot_max_13_freq = '0';$hot_max_14_freq = '0';
	$string_numbers_array = explode(",",$string_numbers);
	$number_count = $string_numbers_array[0];
	$i=0;
	##SORT
	$frequency_array = array();
	while ($i < $number_count)
	{
		$number_freq_combo_array = $string_numbers_array[$i+1];
		$number_freq_combo = explode("-",$number_freq_combo_array);
		$number_part = $number_freq_combo[0];
		$frequency_part = $number_freq_combo[1];

			if ($frequency_part >= $hot_max_1_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				$hot_max_3 = $hot_max_2;				$hot_max_3_freq = $hot_max_2_freq;
				$hot_max_2 = $hot_max_1;				$hot_max_2_freq = $hot_max_1_freq;
				# asign new high
				$hot_max_1 = $number_part;			$hot_max_1_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_2_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				$hot_max_3 = $hot_max_2;				$hot_max_3_freq = $hot_max_2_freq;
				# asign new
				$hot_max_2 = $number_part;				$hot_max_2_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_3_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				$hot_max_4 = $hot_max_3;				$hot_max_4_freq = $hot_max_3_freq;
				# asign new
				$hot_max_3 = $number_part;				$hot_max_3_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_4_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				$hot_max_5 = $hot_max_4;				$hot_max_5_freq = $hot_max_4_freq;
				# asign new
				$hot_max_4 = $number_part;				$hot_max_4_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_5_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				$hot_max_6 = $hot_max_5;				$hot_max_6_freq = $hot_max_5_freq;
				# asign new
				$hot_max_5 = $number_part;				$hot_max_5_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_6_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				$hot_max_7 = $hot_max_6;				$hot_max_7_freq = $hot_max_6_freq;
				# asign new
				$hot_max_6 = $number_part;				$hot_max_6_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_7_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				$hot_max_8 = $hot_max_7;				$hot_max_8_freq = $hot_max_7_freq;
				# asign new
				$hot_max_7 = $number_part;				$hot_max_7_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_8_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				$hot_max_9 = $hot_max_8;				$hot_max_9_freq = $hot_max_8_freq;
				# asign new
				$hot_max_8 = $number_part;				$hot_max_8_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_9_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				$hot_max_10 = $hot_max_9;				$hot_max_10_freq = $hot_max_9_freq;
				# asign new
				$hot_max_9 = $number_part;				$hot_max_9_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_10_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				$hot_max_11 = $hot_max_10;				$hot_max_11_freq = $hot_max_10_freq;
				# asign new
				$hot_max_10 = $number_part;				$hot_max_10_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_11_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				$hot_max_12 = $hot_max_11;				$hot_max_12_freq = $hot_max_11_freq;
				# asign new
				$hot_max_11 = $number_part;				$hot_max_11_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_12_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				$hot_max_13 = $hot_max_12;				$hot_max_13_freq = $hot_max_12_freq;
				# asign new
				$hot_max_12 = $number_part;				$hot_max_12_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_13_freq)
			{
				#shift all others down
				$hot_max_14 = $hot_max_13;				$hot_max_14_freq = $hot_max_13_freq;
				# asign new
				$hot_max_13 = $number_part;				$hot_max_13_freq = $frequency_part;
			}
			else if ($frequency_part >= $hot_max_14_freq)
			{
				#shift all others down
				# asign new
				$hot_max_14 = $number_part;				$hot_max_14_freq = $frequency_part;
			}
			
		###  $frequency_array[$i][$frequency_part] = $number_part;
		$i++;
	}

$sorted_numbers1 = $hot_max_1.','.$hot_max_2.','.$hot_max_3.','.$hot_max_4.','.$hot_max_5.','.$hot_max_6.','.$hot_max_7;
$sorted_numbers2 = $hot_max_8.','.$hot_max_9.','.$hot_max_10.','.$hot_max_11.','.$hot_max_12.','.$hot_max_13.','.$hot_max_14;
$sorted_numbers = $sorted_numbers1.','.$sorted_numbers2;
return($sorted_numbers);
}




function function_get_next_draw_date($drawdate_snapshot_from,$scheduled_draws)
{
	##get day
	$day_count = 'undefined';
	$predicted_day = '';
	#$drawdate_snapshot_from = '2016-08-19';
	date_default_timezone_set('Africa/Johannesburg');
	#print($drawdate_snapshot_from);print("|");
	#print_r("<");print_r($scheduled_draws);print_r(">");
	$lotto_date1 = strtotime($drawdate_snapshot_from);
	$nextdrawdate_di = date('D, d F Y',$lotto_date1);
	#print("|");print($nextdrawdate_di);
	$drawdate_strip = explode(',',$nextdrawdate_di);
	$snapshot_day = $drawdate_strip[0];
	########
	#SPLIT UP scheduled_draws
	#work out number of entries: Mon,Tue,
	$schedule_array = explode(',',$scheduled_draws);
	$i=0;
	$found=false;
	$predicted_day_set='not-set';
	while  ($schedule_array[$i])
	{
		$schedule_day = $schedule_array[$i];
		if ($found) {
			$predicted_day = $schedule_day;
			$predicted_day_set='set';
			break;
		}
		if ($schedule_day == $snapshot_day) {
			$found=true;
		}
		$i++;
	}
	if (($found) && ($predicted_day_set == 'not-set'))
	{
		#means got to end - so will be the first one
		#print("aaahh");
		$predicted_day = $schedule_array[0];
	}
	else if (($found) && ($predicted_day_set == 'set')){
		#found and set already so do nothing!
	}
	else {
		print("warning: next-drawdate-calculated-not determined");
	}
	#print($predicted_day);

	###########CONFIG iN DB - TO DO######
	if (($snapshot_day=='Tue') && ($predicted_day=='Fri')) { $day_count=3;}
	if (($snapshot_day=='Fri') && ($predicted_day=='Tue')) { $day_count=4;}

	if (($snapshot_day=='Wed') && ($predicted_day=='Sat')) { $day_count=3;}
	if (($snapshot_day=='Sat') && ($predicted_day=='Wed')) { $day_count=4;}

	if (($snapshot_day=='Mon') && ($predicted_day=='Tue')) { $day_count=1;}
	if (($snapshot_day=='Tue') && ($predicted_day=='Wed')) { $day_count=1;}
	if (($snapshot_day=='Wed') && ($predicted_day=='Thu')) { $day_count=1;}
	if (($snapshot_day=='Thu') && ($predicted_day=='Fri')) { $day_count=1;}
	if (($snapshot_day=='Fri') && ($predicted_day=='Sat')) { $day_count=1;}
	if (($snapshot_day=='Sat') && ($predicted_day=='Sun')) { $day_count=1;}
	if (($snapshot_day=='Sun') && ($predicted_day=='Mon')) { $day_count=1;}

	if (($snapshot_day=='Wed') && ($predicted_day=='Fri'))  { $day_count=2;}
	if (($snapshot_day=='Thu') && ($predicted_day=='Sun'))  { $day_count=4;}
	if (($snapshot_day=='Sun') && ($predicted_day=='Thu'))  { $day_count=4;}
	if (($snapshot_day=='Mon') && ($predicted_day=='Wed'))  { $day_count=2;}
	if (($snapshot_day=='Mon') && ($predicted_day=='Thu'))  { $day_count=3;}
	if (($snapshot_day=='Thu') && ($predicted_day=='Mon'))  { $day_count=4;}
	#print("count|");print($day_count);
	#####################################

	$date1 = strtotime($drawdate_snapshot_from);
	$date2 = $date1+(60*60*24*$day_count);
	$predicted_drawdate = date('Y-m-d',$date2);
	#print("Predicted_date:");print($predicted_drawdate);
	#exit;
	return $predicted_drawdate;
}


## NOT USED YET !
function get_range_color($chart_type,$range)
{
	$color = '000';
	if ($chart_type == 'OE')
	{
		if ($range == 1) {$color = '093168';}
		if ($range == 2) {$color = '184480';}
		if ($range == 3) {$color = '2b5a98';}
		if ($range == 4) {$color = '4876b3';}
		if ($range == 5) {$color = '6a94cc';}
		if ($range == 6) {$color = '90b3e2';}
		if ($range == 7) {$color = '90b3e2';}
		if ($range == 8) {$color = '90b3e2';}
	}
	else if ($chart_type == 'LH')
	{
		if ($range == 1) {$color = '44734C';}
		if ($range == 2) {$color = '69B075';}
		if ($range == 3) {$color = '79CE88';}
		if ($range == 4) {$color = '89E898';}
		if ($range == 5) {$color = '95FFA7';}
		if ($range == 6) {$color = 'A1FFB1';}
		if ($range == 7) {$color = 'BDFFC8';}
		if ($range == 8) {$color = 'BDFFC8';}
	}
	return $color;
}


function selector_insert_entry_II($db,$months_in,$selector_code,$board_selected,$board_selected_color,$number_of_boards,$drawdate_snapshot_from,$drawdate_prediction_on,$lottocode_in,$ball4_active,$ball5_active,$ball6_active,$ball7_active,$powerball_active,$powerball2_active,$tablename)
{
	$LTcode = $months_in.'m/'.$selector_code;
	if ($number_of_boards == 2)
	{
		$entries = explode(":",$board_selected);
		$board_1 = $entries[0];
		$board_2 = $entries[1];
		$entries = explode(":",$board_selected_color);
		$board_1_color = $entries[0];
		$board_2_color = $entries[1];

	}
	else if ($number_of_boards == 1) {$board_1 = $board_selected;$board_1_color=$board_selected_color;}

	###BOARD1

	if ($powerball_active)
	{
		$board_1_array = explode("|",$board_1);
		$board_1 = $board_1_array[0];
		$board_1_pb = $board_1_array[1];
		$ball_db_pb = explode(",",$board_1_pb);
	}

		$ball_db = explode(",",$board_1);
		$ball1 = $ball_db[0];
		$ball2 = $ball_db[1];
		$ball3 = $ball_db[2];
		if ($ball4_active) {$ball4 = $ball_db[3];}
		if ($ball5_active) {$ball5 = $ball_db[4];}
		if ($ball6_active) {$ball6 = $ball_db[5];}
		if ($ball7_active) {$ball7 = $ball_db[6];}

		if ($powerball_active)  {$powerball = $ball_db_pb[0];}
		if ($powerball2_active) {$powerball2 = $ball_db_pb[1];}

		#####build up insert query
		$query_part1 = '';$query_part2 = '';$query_part3 = '';$query_part4 = '';$query_part5 = '';$query_part6 = '';$query_part7 = '';$query_part8 = '';
		$query_part9 = '';$query_part10 = '';$query_part11 = '';$query_part12 = '';$query_part13 = '';$query_part14 = '';$query_part15 = '';

		$query_part1 = "INSERT into ".$tablename." (board,ball_color,LTcode,drawdate_snapshot_from,drawdate_prediction_on,drawdate,lottocode,ball1,ball2,ball3";
		if ($ball4_active) $query_part2 = ",ball4";
		if ($ball5_active) $query_part3 = ",ball5";
		if ($ball6_active) $query_part4 = ",ball6";
		if ($ball7_active) $query_part5 = ",ball7";
		if ($powerball_active) $query_part6 = ",powerball";
		if ($powerball2_active) $query_part7 = ",powerball2";
		$query_part8 = ") values('1','".$board_1_color."','".$LTcode."','".$drawdate_snapshot_from."','".$drawdate_prediction_on."','".$drawdate_prediction_on."','".$lottocode_in."','$ball1','$ball2','$ball3'";
		if ($ball4_active) $query_part9 = ",'$ball4'";
		if ($ball5_active) $query_part10 = ",'$ball5'";
		if ($ball6_active) $query_part11 = ",'$ball6'";
		if ($ball7_active) $query_part12 = ",'$ball7'";
		if ($powerball_active) $query_part13 = ",'$powerball'";
		if ($powerball2_active) $query_part14 = ",'$powerball2'";
		$query_part15 = ")";
		#$sql = "INSERT into LM_LottoEntries (board,LMcode,num_draws,lotto_id,ball1,ball2,ball3,ball4,ball5,powerball,ref_number,datetime_entered,account_id,lottoname_id,drawdate,lottocode) values(2,
		#'$LM_code','$draws','$lotto_id','$ball1','$ball2','$ball3','$ball4','$ball5','$powerball', '$ref_number', NOW(), '$acc_id','$lottoname_id','".$Draw_Date_Insert_db."','".$lottocode."')";
		$sql = $query_part1.$query_part2.$query_part3.$query_part4.$query_part5.$query_part6.$query_part7.$query_part8.$query_part9.$query_part10.$query_part11.$query_part12.$query_part13.$query_part14.$query_part15;
		#print($sql);
		db_insert_chart_selectors($db,$sql);


		###BOARD2
	if ($number_of_boards == 2)
	{
		if ($powerball_active)
		{
			$board_2_array = explode("|",$board_2);
			$board_2 = $board_2_array[0];
			$board_2_pb = $board_2_array[1];
			$ball_db_pb = explode(",",$board_2_pb);
		}

			$ball_db = explode(",",$board_2);
			$ball1 = $ball_db[0];
			$ball2 = $ball_db[1];
			$ball3 = $ball_db[2];
			if ($ball4_active) {$ball4 = $ball_db[3];}
			if ($ball5_active) {$ball5 = $ball_db[4];}
			if ($ball6_active) {$ball6 = $ball_db[5];}
			if ($ball7_active) {$ball7 = $ball_db[6];}

			if ($powerball_active)  {$powerball = $ball_db_pb[0];}
			if ($powerball2_active) {$powerball2 = $ball_db_pb[1];}

			#####build up insert query
			$query_part1 = '';$query_part2 = '';$query_part3 = '';$query_part4 = '';$query_part5 = '';$query_part6 = '';$query_part7 = '';$query_part8 = '';
			$query_part9 = '';$query_part10 = '';$query_part11 = '';$query_part12 = '';$query_part13 = '';$query_part14 = '';$query_part15 = '';

			$query_part1 = "INSERT into ".$tablename." (board,ball_color,LTcode,drawdate_snapshot_from,drawdate_prediction_on,drawdate,lottocode,ball1,ball2,ball3";
			if ($ball4_active) $query_part2 = ",ball4";
			if ($ball5_active) $query_part3 = ",ball5";
			if ($ball6_active) $query_part4 = ",ball6";
			if ($ball7_active) $query_part5 = ",ball7";
			if ($powerball_active) $query_part6 = ",powerball";
			if ($powerball2_active) $query_part7 = ",powerball2";
			$query_part8 = ") values('2','".$board_2_color."','".$LTcode."','".$drawdate_snapshot_from."','".$drawdate_prediction_on."','".$drawdate_prediction_on."','".$lottocode_in."','$ball1','$ball2','$ball3'";
			if ($ball4_active) $query_part9 = ",'$ball4'";
			if ($ball5_active) $query_part10 = ",'$ball5'";
			if ($ball6_active) $query_part11 = ",'$ball6'";
			if ($ball7_active) $query_part12 = ",'$ball7'";
			if ($powerball_active) $query_part13 = ",'$powerball'";
			if ($powerball2_active) $query_part14 = ",'$powerball2'";
			$query_part15 = ")";
			#$sql = "INSERT into LM_LottoEntries (board,LMcode,num_draws,lotto_id,ball1,ball2,ball3,ball4,ball5,powerball,ref_number,datetime_entered,account_id,lottoname_id,drawdate,lottocode) values(2,
			#'$LM_code','$draws','$lotto_id','$ball1','$ball2','$ball3','$ball4','$ball5','$powerball', '$ref_number', NOW(), '$acc_id','$lottoname_id','".$Draw_Date_Insert_db."','".$lottocode."')";
			$sql = $query_part1.$query_part2.$query_part3.$query_part4.$query_part5.$query_part6.$query_part7.$query_part8.$query_part9.$query_part10.$query_part11.$query_part12.$query_part13.$query_part14.$query_part15;
			#print($sql);
			db_insert_chart_selectors($db,$sql);
		}
}


?>
