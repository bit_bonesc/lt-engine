<?php
#	$URL = $prod_URL."tools_hot_and_cold_generator_entries.php?m=".$cat_id."&drawdate=".$drawdate."&id=".$lottocode;
#http://localhost/lottotarget2/calc/tools-hot-cold-generator-new-calc.php?m=1&drawdate=2016-06-01&id=ZA-L
#0,4,3,3,2,2,2,0,0,0,0,0,44,14,13,4,48,47,25,29,28,46,27,26,done.
#params
#	-cat_id = months (change from rows ie. 8 to 1, 24 to 3
#	-drawdate = specific draw date of lottocode
#	-lottocode
# m = months
#
#  if drawdate not populated - run the and find the most resent date to poulate - where field is Results.{lotto-name}.cal_processed is null or 0  LIMIT 1
#  determines if lotto is powerball and also works out powerball min-max range
##
# CALCULATION FOR ODD EVEN
#   for the daterange window (e.g. 3 months) count for each draw the number of low and high and put into buckets
#    for each draw result
#				{
#						count low; count high
#						put (increment bucket count into correct bucket
#							0-6	1-5 2-4 3-3 4-2 5-1 6-0
#				}
#
/*include_once("../include/config.php");
include_once("../include/db-settings.php");
include_once("../include/db-connect.php");
include_once("../include/db-functions.php");
*/
include_once("../include/config-engineroom.php");
include_once("../include/db-settings-engineroom.php");
include_once("../include/db-connect-engineroom.php");
include_once("../include/db-functions-erm.php");
include_once("generate/hot-cold-generator.php");    ###added to reference as all code below is for orginal hot-cold-odd-evel-low-high-low-med-high-grp
include_once("generate/count-lottonumbers.php");

$cat_id  = '';$drawdate_in = '';$lottocode_in = '';
$chart_type = 'OE'; #default
if (isset($_REQUEST['m'])){	$months_in = $_REQUEST['m'];}
if (isset($_REQUEST['drawdate'])) {	$drawdate_in = $_REQUEST['drawdate'];}
if (isset($_REQUEST['id'])) {	$lottocode_in = $_REQUEST['id'];}
if (isset($_REQUEST['cat_id'])) {	$cat_id = $_REQUEST['cat_id'];}
if (isset($_REQUEST['charttype'])) {	$chart_type = $_REQUEST['charttype'];}
$get_drawdate = 0;
if ($drawdate_in == ''){	$get_drawdate = true;}
#print($chart_type);
#exit;
###
### phsedo code
## calculate odd even

function number_even($number)
{
	if ($number % 2 == 0) {return true;}
	else {return false;}
}

function number_low($number,$max_balls)
{
	$result = 0;
	$half_marker = (int)($max_balls/2);
	if ($number <= $half_marker){return true;}
	else {return false;}
}

function number_low_LMH($number,$max_balls)
{
	$result = 0;
	$one_third_marker = (int)($max_balls/3);
	#print($max_balls);print("<br>");print($one_third_marker);	print("<br>");
	#$two_third_marker = $one_third_marker*2;
	if ($number <= $one_third_marker){return true;}
	else {return false;}
}
function number_high_LMH($number,$max_balls)
{
	$result = 0;
	$one_third_marker = (int)($max_balls/3);
	#print("<br>1/3:");print($one_third_marker);	print("<br>");
	$two_third_marker = $max_balls-$one_third_marker;
	#print("<br>3/2:");print($two_third_marker);	print("<br>");
	if ($number >= $two_third_marker){return true;}
	else {return false;}
}

function number_range_GRP($start,$end,$number,$ball_max)
{
	# $start = 1; $end = 10;
	#	$start = 11;$end = 20;
	if (($number >= $start) && ($number <= $end)) {return true;}
	else {return false;}
}

function number_range_PAIR($bucket_part_A,$bucket_part_B,$numbers,$ball_max,$maxballs)
{
	# first check if in range
	if ($bucket_part_A >= $ball_max) { return false;}
	else if ($bucket_part_B > $ball_max) {return false;}
	# check against draw results
	$found_A = 0;	$found_B = 0;
	$drawresults = explode(",",$numbers);
	$i=0;
	while ($i < $maxballs)	 {
  	$number = $drawresults[$i];
		if ($number == $bucket_part_A) {$found_A = 1;}
		else if ($number == $bucket_part_B) {$found_B = 1;}
	  $i++;
	 }
	 if (($found_A) && ($found_B)) {return true;}
	 else {return false;}
}


##### START OF MAIN CODE BASE ########################################################
$db = db_connect($hostname, $username, $dbpassword, $databasename);

if ($chart_type=='HC')    # cater for HC seperately - TO-DO combine later to maintain code better !!!!
{
	$result = hot_cold_generator($db,$lottocode_in,$months_in,$drawdate_in,$cat_id,$get_drawdate);
	print($result);
  db_disconnect($db);
	exit;
}


$lotto_detail_array = array();
$lotto_detail_count = 0;

$lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode_in);
$x=0;
	 $guess_range = 3;
	 $LottoTable 	 = $lotto_detail_array[$x]['Lotto_Table'];
	 $ball1_active = $lotto_detail_array[$x]['ball1_active'];
	 $ball2_active = $lotto_detail_array[$x]['ball2_active'];
	 $ball3_active = $lotto_detail_array[$x]['ball3_active'];
	 if ($ball3_active) {$guess_range = 3;}
	 $ball4_active = $lotto_detail_array[$x]['ball4_active'];
	 if ($ball4_active) {$guess_range = 4;}
	 $ball5_active = $lotto_detail_array[$x]['ball5_active'];
	 if ($ball5_active) {$guess_range = 5;}
	 $ball6_active = $lotto_detail_array[$x]['ball6_active'];
	 if ($ball6_active) {$guess_range = 6;}
	 $ball7_active = $lotto_detail_array[$x]['ball7_active'];
	 if ($ball7_active) {$guess_range = 7;}
	 $bonus_active = $lotto_detail_array[$x]['bonus_active'];
	 $bonus2_active = $lotto_detail_array[$x]['bonus2_active'];
	 $bonus3_active = $lotto_detail_array[$x]['bonus3_active'];
	 $bonus4_active = $lotto_detail_array[$x]['bonus4_active'];
	 $powerball_active = $lotto_detail_array[$x]['powerball_active'];
	 $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
	 if ($powerball_active) {$guess_range_pb = 1;}
	 if ($powerball2_active) {$guess_range_pb = 2;}
	 $lucky1_active = $lotto_detail_array[$x]['lucky1_active'];
	 $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
	 if ($lucky1_active) {$guess_range_l = 1;}
	if ($lucky2_active) {$guess_range_l = 2;}
	 $ball_max = $lotto_detail_array[$x]['ball_max'];
	 $ball_max_pb = $lotto_detail_array[$x]['ball_max_pb'];
	 $ball_max_lucky = $lotto_detail_array[$x]['ball_max_lucky'];
$pb_ind = 0;
if ($powerball_active)
{
	$pb_ind = 1;
	#print("powerball included.");
}
else if ($lucky1_active)
{
	$lucky_ind = 1;  #lucky1 & lucky2 is always effective
}

####
##  BUILD UP QUERY
###
$query_ball1 = '';$query_ball2 = '';$query_ball3 = '';$query_ball4 = '';
$query_ball5 = '';$query_ball6 = '';$query_ball7 = '';
$query_ball_pb = '';
$query_ball_pb2 = '';
#$sQuery = "select count(*) from ".$LottoTable." where ((ball1 = ".$ball_count.") or (ball2 = ".$ball_count.") or (ball3 = ".$ball_count.") or (ball4 = ".$ball_count.") or (ball5 = ".$ball_count.") or (ball6 = ".$ball_count."))  and drawdate >= '".$date_range_min."' and drawdate <= '".$drawdate."'";
$drawdate = '{DRAWDATE}';
$date_range_min = '{DRAWDATE2}';	#TO-DO

$query_prefix = "SELECT ";
if ($ball1_active) {$query_ball1 = 'ball1';}
if ($ball2_active) {$query_ball2 = ',ball2';}
if ($ball3_active) {$query_ball3 = ',ball3';}
if ($ball4_active) {$query_ball4 = ',ball4';}
if ($ball5_active) {$query_ball5 = ',ball5';}
if ($ball6_active) {$query_ball6 = ',ball6';}
if ($ball7_active) {$query_ball7 = ',ball7';}

if ($pb_ind == 1)
	{
		if ($powerball_active)  {$query_ball_pb = 'powerball';}
		if ($powerball2_active)  {$query_ball_pb2 = ',powerball2';}
	}
	if ($lucky_ind == 1)
		{
			$query_attribute_name = 'lucky'; # default
			if  (($lottoCode == "AUS-ML") || ($lottoCode == "AUS-OL") || ($lottoCode == "AUS-SL") || ($lottoCode == "AUS-WL"))
				{$query_attribute_name = "supplementary";}

			$query_ball_lucky = $query_attribute_name;
		}

$query_middle = " FROM ".$LottoTable." WHERE ";
$query_suffix = "drawdate >= '".$date_range_min."' and drawdate <= '".$drawdate."'";

$query_full = $query_prefix.$query_ball1.$query_ball2.$query_ball3.$query_ball4.$query_ball5.$query_ball6.$query_ball7.$query_middle.$query_suffix;

if ($pb_ind == 1)
	{
		$query_full_pb = $query_prefix.$query_ball_pb.$query_ball_pb2.$query_middle.$query_suffix;
	}
else if ($lucky_ind == 1)
		{
			$query_full_lucky = $query_prefix.$query_ball_lucky.$query_middle.$query_suffix;
		}

#print($query_full);
#exit;


###STORE COUNTS IN DB - function retrieve_min_max
# include_once("charts/Data_LottoNumbers_All_New.php");

 $min_max_date_array = array();
 #$min_max_date_array = retrieve_min_max_count_aug2016($db,$lottocode_in,'',$months_in,$drawdate_in,$LottoTable,$ball_max,$query_full);
 $min_max_date_array = retrieve_min_max_dates_feb2017($db,$months_in,$drawdate_in);
 #$count_min_max = $min_max_date_array[0]['count_min_max'];
#retrieve min date from array retuned by function
 $date_range_min = $min_max_date_array[0]['date_range_min'];
# find the actual date in the lotto draw results that is closest to the min date
# should actually do this before the retrieve_min_max_count_jun2016 and pass as a param. - but tested and it works for now!
# inside retrieve_min_max_count_jun2016 uses months to calc start date and does a >= , so does the below db_call - so its in sync!
 $date_range_min_adjusted = db_get_date_start($db,$LottoTable,$date_range_min);
 #print($date_range_min_adjusted);

$query_full = str_replace('{DRAWDATE}',$drawdate_in,$query_full);
$query_full = str_replace('{DRAWDATE2}',$date_range_min,$query_full);
if ($pb_ind == 1){
		$query_full_pb = str_replace('{DRAWDATE}',$drawdate_in,$query_full_pb);
		$query_full_pb = str_replace('{DRAWDATE2}',$date_range_min,$query_full_pb);
	}
else if ($lucky_ind == 1){
		$query_full_lucky = str_replace('{DRAWDATE}',$drawdate_in,$query_full_lucky);
		$query_full_lucky = str_replace('{DRAWDATE2}',$date_range_min,$query_full_lucky);
	}
#print($query_full);

## RUN QUERY AND PROCESS
$count_odd_numbers = 0;
$count_even_numbers = 0;
$count_low_numbers = 0;
$count_high_numbers = 0;
$count_median_numbers = 0;
/* old
$bucket_0odd_6_even = 0;
$bucket_1odd_5_even = 0;
$bucket_2odd_4_even = 0;
$bucket_3odd_3_even = 0;
$bucket_4odd_2_even = 0;
$bucket_5odd_1_even = 0;
$bucket_6odd_0_even = 0;
*/
/* new */
$bucket_range_1 = 0;
$bucket_range_2 = 0;
$bucket_range_3 = 0;
$bucket_range_4 = 0;
$bucket_range_5 = 0;
$bucket_range_6 = 0;
$bucket_range_7 = 0;
$bucket_range_8 = 0;    # for now assumption is max of 7 balls for largetst lottery , therefore 8 buckets

###add buckets for LMH charts - 6 balls = 28 ; 5 balls = 21
## also use for OELH charys - 6 balls = 25;  5 balls = tbd

$bucket_range_9 = 0;   # also, resv if max 8 balls
$bucket_range_10 = 0;   # also # used for groups
$bucket_range_11 = 0;
$bucket_range_12 = 0;
$bucket_range_13 = 0;
$bucket_range_14 = 0;
$bucket_range_15 = 0;
$bucket_range_16 = 0;
$bucket_range_17 = 0;
$bucket_range_18 = 0;
$bucket_range_19 = 0;
$bucket_range_20 = 0;
$bucket_range_21 = 0;
$bucket_range_22 = 0;
$bucket_range_23 = 0;
$bucket_range_24 = 0;
$bucket_range_25 = 0;
$bucket_range_26 = 0;
$bucket_range_27 = 0;
$bucket_range_28 = 0;
$bucket_range_29 = 0;
$bucket_range_30 = 0;
$bucket_range_31 = 0;
$bucket_range_32 = 0;
$bucket_range_33 = 0;
$bucket_range_34 = 0;
$bucket_range_35 = 0;
$bucket_range_36 = 0;
$bucket_range_37 = 0;
$bucket_range_38 = 0;
$bucket_range_39 = 0;
$bucket_range_40 = 0;
$bucket_range_41 = 0;
$bucket_range_42 = 0;
$bucket_range_43 = 0;
$bucket_range_44 = 0;
$bucket_range_45 = 0;
$bucket_range_46 = 0;
$bucket_range_47 = 0;
$bucket_range_48 = 0;
$bucket_range_49 = 0;
$bucket_range_50 = 0;
$bucket_range_51 = 0;
$bucket_range_52 = 0;
$bucket_range_53 = 0;
$bucket_range_54 = 0;
$bucket_range_55 = 0;
$bucket_range_56 = 0;
$bucket_range_57 = 0;
$bucket_range_58 = 0;
$bucket_range_59 = 0;
$bucket_range_60 = 0;
$bucket_range_61 = 0;
$bucket_range_62 = 0;
$bucket_range_63 = 0;
$bucket_range_64 = 0;

#calculate range
###
$maxballs=$guess_range;
$maxballs_pb=$guess_range_pb;
$maxballs_l=$guess_range_l;
#print($maxballs);


if ($chart_type == 'PAIR'){
	/*******OPTIMISE MOVED TO OUTER LOOP AT TOP - otherwise it overrides val count !*****/
	$db_pair_count = 0;
	$pair_array = array();
	$pair_array = db_return_pair_buckets($db,$db_pair_count,$lottocode_in);
}

// MAIN LOOP THAT DOES CALC
	// 1.  DETERMINER - if row (each number) is  OE | LH | OELH(Combo) | PAIR | GRP
	// 2. BUCKET ASSIGNMENT - 

 if ($resultS1 = $db->query($query_full)){ $myrowS1 = $resultS1->fetch_row();}
 while ($myrowS1){
	 // 1. DETERMINER
	 
	 # 1) for each row count # even, # odd
	 # 2) assign to correct bucket.

	 #print("analyse result.");
	 $index=0;
	if ($chart_type == 'OE'){
		 $count_odd_numbers = 0;
  	 	 $count_even_numbers = 0;
		 if ($ball1_active) { $ball1 = $myrowS1[$index];           if (number_even($ball1)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball2_active) { $index++; $ball2 = $myrowS1[$index]; if (number_even($ball2)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball3_active) { $index++; $ball3 = $myrowS1[$index]; if (number_even($ball3)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball4_active) { $index++; $ball4 = $myrowS1[$index]; if (number_even($ball4)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball5_active) { $index++; $ball5 = $myrowS1[$index]; if (number_even($ball5)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball6_active) { $index++; $ball6 = $myrowS1[$index]; if (number_even($ball6)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		 if ($ball7_active) { $index++; $ball7 = $myrowS1[$index]; if (number_even($ball7)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
 		}
	else if ($chart_type == 'LH'){
		 $count_low_numbers = 0;
		 $count_high_numbers = 0;
 		 if ($ball1_active) { $ball1 = $myrowS1[$index]; 		   if (number_low($ball1,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball2_active) { $index++; $ball2 = $myrowS1[$index]; if (number_low($ball2,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball3_active) { $index++; $ball3 = $myrowS1[$index]; if (number_low($ball3,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball4_active) { $index++; $ball4 = $myrowS1[$index]; if (number_low($ball4,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball5_active) { $index++; $ball5 = $myrowS1[$index]; if (number_low($ball5,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball6_active) { $index++; $ball6 = $myrowS1[$index]; if (number_low($ball6,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
 		 if ($ball7_active) { $index++; $ball7 = $myrowS1[$index]; if (number_low($ball7,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} }
	  }
	 else if ($chart_type == 'OELH'){
		  //ODD EVEN + // LOW HIGH
		$count_odd_numbers = 0;
		$count_even_numbers = 0;
		$count_low_numbers = 0;
		$count_high_numbers = 0;
		  if ($ball1_active) { $ball1 = $myrowS1[$index];
			if (number_even($ball1)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball1,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;}
		}
		  if ($ball2_active) { $index++; $ball2 = $myrowS1[$index]; 
			if (number_even($ball2)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball2,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;}
		}
		  if ($ball3_active) { $index++; $ball3 = $myrowS1[$index]; 
			if (number_even($ball3)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball3,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;}
		}
		  if ($ball4_active) { $index++; $ball4 = $myrowS1[$index]; 
			if (number_even($ball4)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball4,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} 
		}
		  if ($ball5_active) { $index++; $ball5 = $myrowS1[$index]; 
			if (number_even($ball5)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball5,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;} 
		}
		  if ($ball6_active) { $index++; $ball6 = $myrowS1[$index]; 
			if (number_even($ball6)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball6,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;}
		}
		  if ($ball7_active) { $index++; $ball7 = $myrowS1[$index]; 
			if (number_even($ball7)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
			if (number_low($ball7,$ball_max)) {$count_low_numbers++;} else {$count_high_numbers++;}
		}
  	}
	else if ($chart_type == 'LMH')
  	 {
			 #print("OK");
			 #exit;
 		 $count_low_numbers = 0;
		 $count_median_numbers = 0;
 		 $count_high_numbers = 0;

  		 if ($ball1_active)
			 {
				 $ball1 = $myrowS1[$index];
				 if (number_low_LMH($ball1,$ball_max)) {$count_low_numbers++;}
				 else if (number_high_LMH($ball1,$ball_max)) {$count_high_numbers++;}
				 else {$count_median_numbers++;}
			 }
  		 if ($ball2_active)
			 {
				 $index++; $ball2 = $myrowS1[$index];
				 if (number_low_LMH($ball2,$ball_max)) {$count_low_numbers++;}
				 else if (number_high_LMH($ball2,$ball_max)) {$count_high_numbers++;}
				 else {$count_median_numbers++;}
			 }
  		 if ($ball3_active)
			 {
				 $index++; $ball3 = $myrowS1[$index];
				 if (number_low_LMH($ball3,$ball_max)) {$count_low_numbers++;}
				else if (number_high_LMH($ball3,$ball_max)) {$count_high_numbers++;}
				else {$count_median_numbers++;}
			 }
  		 if ($ball4_active)
			 {
				 $index++; $ball4 = $myrowS1[$index];
				 if (number_low_LMH($ball4,$ball_max)) {$count_low_numbers++;}
				else if (number_high_LMH($ball4,$ball_max)) {$count_high_numbers++;}
				else {$count_median_numbers++;}
			 }
  		 if ($ball5_active)
			 {
				 $index++; $ball5 = $myrowS1[$index];
				 if (number_low_LMH($ball5,$ball_max)) {$count_low_numbers++;}
				 else if (number_high_LMH($ball5,$ball_max)) {$count_high_numbers++;}
				 else {$count_median_numbers++;}
			 }
  		 if ($ball6_active)
			 {
				 $index++; $ball6 = $myrowS1[$index];
				 if (number_low_LMH($ball6,$ball_max)) {$count_low_numbers++;}
				 else if (number_high_LMH($ball6,$ball_max)) {$count_high_numbers++;}
				 else {$count_median_numbers++;}
			 }
  		 if ($ball7_active)
			 {
				 $index++; $ball7 = $myrowS1[$index];
				 if (number_low_LMH($ball7,$ball_max)) {$count_low_numbers++;}
				 else if (number_high_LMH($ball7,$ball_max)) {$count_high_numbers++;}
				 else {$count_median_numbers++;}
			 }
   	}
	else if ($chart_type == 'PAIR')
		{
			# build up results into string ,,,,
			if ($ball1_active) { $ball1 = $myrowS1[$index]; $numbers = $ball1;	}
			if ($ball2_active) { $index++; $ball2 = $myrowS1[$index];	$numbers = $numbers.','.$ball2; }
			if ($ball3_active) { $index++; $ball3 = $myrowS1[$index];	$numbers = $numbers.','.$ball3; }
			if ($ball4_active) { $index++; $ball4 = $myrowS1[$index];	$numbers = $numbers.','.$ball4; }
			if ($ball5_active) { $index++; $ball5 = $myrowS1[$index];	$numbers = $numbers.','.$ball5; }
			if ($ball6_active) { $index++; $ball6 = $myrowS1[$index];	$numbers = $numbers.','.$ball6; }
			if ($ball7_active) { $index++; $ball7 = $myrowS1[$index];	$numbers = $numbers.','.$ball7; }

			# create array - get from db Config_Chart_Pair
			# $array = db_get .....
			/*******OPTIMISE MOVE TO OUTER LOOP AT TOP - otherwise it overrides val count !*****/
			/*$db_pair_count = 0;
			$pair_array = array();
			$pair_array = db_return_pair_buckets($db,$db_pair_count,$lottocode_in);
			*/
			/******END OPTIMISE ***/
		  $i=0;    //reset
			while ($i < $db_pair_count)
			{
				#get left and right     e.g  1-49
				$bucket = $pair_array[$i]['bucket'];
				$bucket_parts = explode("-",$bucket);
				$bucket_part_A = $bucket_parts[0];
				$bucket_part_B = $bucket_parts[1];
				## do comparison
					if (number_range_PAIR($bucket_part_A,$bucket_part_B,$numbers,$ball_max,$maxballs)) {
						#$count_1_10_numbers++;
						# assign buckets value (use the memory array) to populate
						$pair_array[$i]['value']++;
					}
				$i++;
			 }   //pair_array loop
		}
		else if ($chart_type == 'GRP')
  	 {
			 #print("OK");
			 #exit;
 		 $count_1_10_numbers = 0;
		 $count_11_20_numbers = 0;
		 $count_21_30_numbers = 0;
		 $count_31_40_numbers = 0;
		 $count_41_50_numbers = 0;
		 $count_51_60_numbers = 0;
		 $count_61_70_numbers = 0;
		 $count_71_80_numbers = 0;
		 $count_81_90_numbers = 0;
		 $count_91_100_numbers = 0;

		
  		 if ($ball1_active)
			 {
				 $ball1 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball1,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball1,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball1,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball1,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball1,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball1,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball1,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball1,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball1,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball1,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball2_active)
			 {
				 $index++; $ball2 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball2,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball2,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball2,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball2,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball2,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball2,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball2,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball2,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball2,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball2,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball3_active)
			 {
				 $index++; $ball3 = $myrowS1[$index];
				if (number_range_GRP(1,10,$ball3,$ball_max)) {$count_1_10_numbers++;}
	 			else if (number_range_GRP(11,20,$ball3,$ball_max)){$count_11_20_numbers++;}
	 			else if (number_range_GRP(21,30,$ball3,$ball_max)){$count_21_30_numbers++;}
	 			else if (number_range_GRP(31,40,$ball3,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball3,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball3,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball3,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball3,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball3,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball3,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball4_active)
			 {
				 $index++; $ball4 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball4,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball4,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball4,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball4,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball4,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball4,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball4,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball4,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball4,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball4,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball5_active)
			 {
				 $index++; $ball5 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball5,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball5,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball5,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball5,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball5,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball5,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball5,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball5,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball5,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball5,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball6_active)
			 {
				 $index++; $ball6 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball6,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball6,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball6,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball6,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball6,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball6,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball6,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball6,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball6,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball6,$ball_max)) {$count_91_100_numbers++;}
			 }
  		 if ($ball7_active)
			 {
				 $index++; $ball7 = $myrowS1[$index];
				 if (number_range_GRP(1,10,$ball7,$ball_max)) {$count_1_10_numbers++;}
				 else if (number_range_GRP(11,20,$ball7,$ball_max)){$count_11_20_numbers++;}
				 else if (number_range_GRP(21,30,$ball7,$ball_max)){$count_21_30_numbers++;}
				 else if (number_range_GRP(31,40,$ball7,$ball_max)) {$count_31_40_numbers++;}
				 else if (number_range_GRP(41,50,$ball7,$ball_max)) {$count_41_50_numbers++;}
				 else if (number_range_GRP(51,60,$ball7,$ball_max)) {$count_51_60_numbers++;}
				 else if (number_range_GRP(61,70,$ball7,$ball_max)) {$count_61_70_numbers++;}
				 else if (number_range_GRP(71,80,$ball7,$ball_max)) {$count_71_80_numbers++;}
				 else if (number_range_GRP(81,90,$ball7,$ball_max)) {$count_81_90_numbers++;}
				 else if (number_range_GRP(91,100,$ball7,$ball_max)) {$count_91_100_numbers++;}
			 }
	   }
	   

  
	
// 2. BUCKET ASSIGNMENT
	 # assign to bucket
	 /* new */
	$range_ceiling = $maxballs;  # reset each time

	if ($chart_type == 'OE'){
		if (($count_odd_numbers == 0) && ($count_even_numbers == $range_ceiling)) {$bucket_range_1++; }
		$range_ceiling--;
		if (($count_odd_numbers == 1) && ($count_even_numbers == $range_ceiling)) {$bucket_range_2++; }
		$range_ceiling--;
		if (($count_odd_numbers == 2) && ($count_even_numbers == $range_ceiling)) {$bucket_range_3++; }
		$range_ceiling--;
		if (($count_odd_numbers == 3) && ($count_even_numbers == $range_ceiling)) {$bucket_range_4++; }
		$range_ceiling--;
		if (($count_odd_numbers == 4) && ($count_even_numbers == $range_ceiling)) {$bucket_range_5++; }
		$range_ceiling--;
		if (($count_odd_numbers == 5) && ($count_even_numbers == $range_ceiling)) {$bucket_range_6++; }
		$range_ceiling--;
		if (($count_odd_numbers == 6) && ($count_even_numbers == $range_ceiling)) {$bucket_range_7++; }
		$range_ceiling--;
		if (($count_odd_numbers == 7) && ($count_even_numbers == $range_ceiling)) {$bucket_range_8++; }
	}
	else if ($chart_type == 'LH'){
		if (($count_low_numbers == 0) && ($count_high_numbers == $range_ceiling)) {$bucket_range_1++; }
		$range_ceiling--;
		if (($count_low_numbers == 1) && ($count_high_numbers == $range_ceiling)) {$bucket_range_2++; }
		$range_ceiling--;
		if (($count_low_numbers == 2) && ($count_high_numbers == $range_ceiling)) {$bucket_range_3++; }
		$range_ceiling--;
		if (($count_low_numbers == 3) && ($count_high_numbers == $range_ceiling)) {$bucket_range_4++; }
		$range_ceiling--;
		if (($count_low_numbers == 4) && ($count_high_numbers == $range_ceiling)) {$bucket_range_5++; }
		$range_ceiling--;
		if (($count_low_numbers == 5) && ($count_high_numbers == $range_ceiling)) {$bucket_range_6++; }
		$range_ceiling--;
		if (($count_low_numbers == 6) && ($count_high_numbers == $range_ceiling)) {$bucket_range_7++; }
		$range_ceiling--;
		if (($count_low_numbers == 7) && ($count_high_numbers == $range_ceiling)) {$bucket_range_8++; }
	}
	else if ($chart_type == 'OELH'){
		if ($maxballs == 7)
		{
			//
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_1++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_2++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_3++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_4++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_5++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_6++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_7++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 7) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_8++; }
			
			//
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_9++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_10++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_11++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_12++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_13++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_14++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_15++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 6) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_16++; }

			//
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_17++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_18++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_19++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_20++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_21++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_22++;}
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_23++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 5) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_24++; }
			
			
			//
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_25++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_26++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_27++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_28++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_29++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_30++;}
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_31++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 4) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_32++; }
			
			
			//
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_33++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_34++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_35++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_36++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_37++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_38++;}
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_39++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 3) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_40++; }
			
		
			//
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_41++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_42++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_43++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_44++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_45++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_46++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_47++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 2) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_48++; }
			

			//
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_49++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_50++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_51++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_52++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_53++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_54++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_55++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 1) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_56++; }
		
			//
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 0) &&  ($count_high_numbers == 7)) {$bucket_range_57++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 1) &&  ($count_high_numbers == 6)) {$bucket_range_58++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 2) &&  ($count_high_numbers == 5)) {$bucket_range_59++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 3) &&  ($count_high_numbers == 4)) {$bucket_range_60++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 4) &&  ($count_high_numbers == 3)) {$bucket_range_61++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 5) &&  ($count_high_numbers == 2)) {$bucket_range_62++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 6) &&  ($count_high_numbers == 1)) {$bucket_range_63++; }
			if (($count_odd_numbers == 7) && ($count_even_numbers == 0) && ($count_low_numbers == 7) &&  ($count_high_numbers == 0)) {$bucket_range_64++; }
			
		}
		else if ($maxballs == 6)
		{
			//0O6E-0L6H,  0O6E-1L5H, 0O6E-2L4H, 0O6E-3L3H, 0O6E-4L2H, 0O6E-5L1H, 0O6E-6L0H, 
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_1++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_2++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_3++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_4++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_5++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_6++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 6) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_7++; }
			//1O5E-0L6H, 1O5E-1L5H, 1O5E-2L4H. 1O5E-3L3H,1O5E-4L2H, 1O5E-5L1H,1O5E-6L0H
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_8++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_9++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_10++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_11++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_12++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_13++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_14++; }
			//2O4E-0L6H, 2O4E-1L5H, 2O4E-2L4H, 2O4E-3L3H, 2O4E-4L2H, 2O4E-5L1H, 2O4E-6L0H
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_15++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_16++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_17++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_18++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_19++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_20++;}
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_21++; }
			
			//3O3E-0L6H,3O3E-1L5H, 3O3E-2L4H, 3O3E-3L3H, 3O3E-4L2H, 3O3E-5L1H,3O3E-6L0H,
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_22++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_23++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_24++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_25++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_26++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_27++;}
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_28++; }
			
			//4O2E-0L6H, 4O2E-1L5H, 4O2E-2L4H, 4O2E-3L3H, 4O2E-4L2H, 4O2E-5L1H, 4O2E-6L0H
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_29++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_30++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_31++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_32++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_33++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_34++;}
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_35++; }
		
			//5O1E-0L6H, 5O1E-1L5H, 5O1E-2L4H, 5O1E-3L3H, 5O1E-4L2H, 5O1E-5L1H,5O1E-6L0H
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_36++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_37++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_38++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_39++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_40++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_41++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_42++; }

			//6O0E-0L6H, 6O0E-1L5H, 6O0E-2L4H, 6O0E-3L3H, 6O0E-4L2H, 6O0E-5L1H,6O0E-6L0H
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 0) &&  ($count_high_numbers == 6)) {$bucket_range_43++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_44++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_45++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_46++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_47++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_47++; }
			if (($count_odd_numbers == 6) && ($count_even_numbers == 0) && ($count_low_numbers == 6) &&  ($count_high_numbers == 0)) {$bucket_range_49++; }
		}
		else if ($maxballs == 666)    //old
		{
			//1O5E-1L5H, 1O5E-2L4H. 1O5E-3L3H,1O5E-4L2H, 1O5E-5L1H,
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_1++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_2++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_3++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_4++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 5) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_5++; }
			//2O4E-1L5H, 2O4E-2L4H, 2O4E-3L3H, 2O4E-4L2H, 2O4E-5L1H,
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_6++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_7++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_8++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_9++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 4) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_10++;}
			//3O3E-1L5H, 3O3E-2L4H, 3O3E-3L3H, 3O3E-4L2H, 3O3E-5L1H,
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_11++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_12++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_13++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_14++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 3) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_15++;}
			//4O2E-1L5H, 4O2E-2L4H, 4O2E-3L3H, 4O2E-4L2H, 4O2E-5L1H,
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_16++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_17++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_18++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_19++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 2) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_20++;}
			//5O1E-1L5H, 5O1E-2L4H, 5O1E-3L3H, 5O1E-4L2H, 5O1E-5L1H
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 5)) {$bucket_range_21++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 2) &&  ($count_high_numbers == 4)) {$bucket_range_22++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 3) &&  ($count_high_numbers == 3)) {$bucket_range_25++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 4) &&  ($count_high_numbers == 2)) {$bucket_range_24++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 1) && ($count_low_numbers == 5) &&  ($count_high_numbers == 1)) {$bucket_range_25++; }
		}
		else if ($maxballs == 5)   
		{
			}//TO DO

			//'1O4E-1L4H', '1O4E-2L3H', '1O4E-3L2H', '1O4E-4L1H',
			//'2O3E-1L4H', '2O3E-2L3H', '2O3E-3L2H', '2O3E-4L1H',
			//'3O2E-1L4H', '3O2E-2L3H', '3O2E-3L2H', '3O2E-4L1H',
			//'4O1E-1L4H', '4O1E-2L3H', '4O1E-3L2H', '4O1E-4L1H'

			//'1O4E-1L4H', '1O4E-2L3H', '1O4E-3L2H', '1O4E-4L1H',
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_1++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_2++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_3++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_4++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_5++; }
			if (($count_odd_numbers == 0) && ($count_even_numbers == 5) && ($count_low_numbers == 5) &&  ($count_high_numbers == 0)) {$bucket_range_6++; }
	
			//'1O4E-1L4H', '1O4E-2L3H', '1O4E-3L2H', '1O4E-4L1H',
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_7++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_8++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_9++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_10++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_11++; }
			if (($count_odd_numbers == 1) && ($count_even_numbers == 4) && ($count_low_numbers == 5) &&  ($count_high_numbers == 0)) {$bucket_range_12++; }
			
				//'2O3E-1L4H', '2O3E-2L3H', '2O3E-3L2H', '2O3E-4L1H',
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_13++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_14++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_15++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_16++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_17++; }
			if (($count_odd_numbers == 2) && ($count_even_numbers == 3) && ($count_low_numbers == 5) &&  ($count_high_numbers == 0)) {$bucket_range_18++; }
		
			//'3O2E-1L4H', '3O2E-2L3H', '3O2E-3L2H', '3O2E-4L1H',
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_19++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_20++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_21++;}
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_22++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_23++; }
			if (($count_odd_numbers == 3) && ($count_even_numbers == 2) && ($count_low_numbers == 5) &&  ($count_high_numbers == 0)) {$bucket_range_24++; }
		
			//'4O1E-1L4H', '4O1E-2L3H', '4O1E-3L2H', '4O1E-4L1H'
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_25++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_26++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_27++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_28++;}
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_29++; }
			if (($count_odd_numbers == 4) && ($count_even_numbers == 1) && ($count_low_numbers == 5) &&  ($count_high_numbers == 5)) {$bucket_range_30++; }
		
			//'4O1E-1L4H', '4O1E-2L3H', '4O1E-3L2H', '4O1E-4L1H'
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 0) &&  ($count_high_numbers == 5)) {$bucket_range_31++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 1) &&  ($count_high_numbers == 4)) {$bucket_range_32++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 2) &&  ($count_high_numbers == 3)) {$bucket_range_33++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 3) &&  ($count_high_numbers == 2)) {$bucket_range_34++;}
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 4) &&  ($count_high_numbers == 1)) {$bucket_range_35++; }
			if (($count_odd_numbers == 5) && ($count_even_numbers == 0) && ($count_low_numbers == 5) &&  ($count_high_numbers == 5)) {$bucket_range_36++; }
		

	}
	else if ($chart_type == 'GRP')
	{
		# |1-10|-|11-20|-|21-30|-|31-40|-|41-50|
		#  0    - 0     - 0     -  0    - 6,
		# if (($count_low_numbers == 0) && ($count_median_numbers == 0) && ($count_high_numbers == 5)) {$bucket_range_1++; }
		# hack for now - too many permutations
		if ($ball_max >= 1) { $bucket_range_1 = $bucket_range_1 + $count_1_10_numbers; }
		if ($ball_max >= 11) { $bucket_range_2 = $bucket_range_2 + $count_11_20_numbers; }
		if ($ball_max >= 21) { $bucket_range_3 = $bucket_range_3 + $count_21_30_numbers; }
		if ($ball_max >= 31) { $bucket_range_4 = $bucket_range_4 + $count_31_40_numbers; }
		if ($ball_max >= 41) { $bucket_range_5 = $bucket_range_5 + $count_41_50_numbers; }
		if ($ball_max >= 51) { $bucket_range_6 = $bucket_range_6 + $count_51_60_numbers; }
		if ($ball_max >= 61) { $bucket_range_7 = $bucket_range_7 + $count_61_70_numbers; }
		if ($ball_max >= 71) { $bucket_range_8 = $bucket_range_9 + $count_71_80_numbers; }
		if ($ball_max >= 81) { $bucket_range_9 = $bucket_range_9 + $count_81_90_numbers; }
		if ($ball_max >= 91) { $bucket_range_10 = $bucket_range_10 + $count_91_100_numbers; }
	}
	else if ($chart_type == 'LMH')
	{
		#range if 6 balls
		#0-0-6,0-1-5,0-2-4,0-3-3,0-4-2,0-5-1,0-6-0,1-0-5,1-1-4,1-2-3,1-3-2,1-4-1,1-5-0,2-0-4,2-1-3,2-2-2,2-3-1,2-4-0,3-0-3,3-1-2,3-2-1,3-3-0,4-0-2,4-1-1,4-2-0,5-0-1,5-1-0,6-0-0
		#range if 5 balls
		#0-0-5,0-1-4,0-2-3,0-3-2,0-4-1,0-5-0,1-0-4,1-1-3,1-2-2,1-3-1,1-4-0,2-0-3,2-1-2,2-2-1,2-3-0,3-0-2,3-1-1,3-2-0,4-0-1,4-1-0,5-0-0
		if ($maxballs == 5)
		{
			#0-0-5,
			if (($count_low_numbers == 0) && ($count_median_numbers == 0) && ($count_high_numbers == 5)) {$bucket_range_1++; }
			#0-1-4,
			if (($count_low_numbers == 0) && ($count_median_numbers == 1) && ($count_high_numbers == 4)) {$bucket_range_2++; }
			#0-2-3,
			if (($count_low_numbers == 0) && ($count_median_numbers == 2) && ($count_high_numbers == 3)) {$bucket_range_3++; }
			#0-3-2,
			if (($count_low_numbers == 0) && ($count_median_numbers == 3) && ($count_high_numbers == 2)) {$bucket_range_4++; }
			#0-4-1,
			if (($count_low_numbers == 0) && ($count_median_numbers == 4) && ($count_high_numbers == 1)) {$bucket_range_5++; }
			#0-5-0,
			if (($count_low_numbers == 0) && ($count_median_numbers == 5) && ($count_high_numbers == 0)) {$bucket_range_6++; }
			#1-0-4,
			if (($count_low_numbers == 1) && ($count_median_numbers == 0) && ($count_high_numbers == 4)) {$bucket_range_7++; }
			#1-1-3,
			if (($count_low_numbers == 1) && ($count_median_numbers == 1) && ($count_high_numbers == 3)) {$bucket_range_8++; }
			#1-2-2,
			if (($count_low_numbers == 1) && ($count_median_numbers == 2) && ($count_high_numbers == 2)) {$bucket_range_9++; }
			#1-3-1,
			if (($count_low_numbers == 1) && ($count_median_numbers == 3) && ($count_high_numbers == 1)) {$bucket_range_10++; }
			#1-4-0,
			if (($count_low_numbers == 1) && ($count_median_numbers == 4) && ($count_high_numbers == 0)) {$bucket_range_11++; }
			#2-0-3,
			if (($count_low_numbers == 2) && ($count_median_numbers == 0) && ($count_high_numbers == 3)) {$bucket_range_12++; }
			#2-1-2,
			if (($count_low_numbers == 2) && ($count_median_numbers == 1) && ($count_high_numbers == 2)) {$bucket_range_13++; }
			#2-2-1,
			if (($count_low_numbers == 2) && ($count_median_numbers == 2) && ($count_high_numbers == 1)) {$bucket_range_14++; }
			#2-3-0,
			if (($count_low_numbers == 2) && ($count_median_numbers == 3) && ($count_high_numbers == 0)) {$bucket_range_15++; }
			#3-0-2,
			if (($count_low_numbers == 3) && ($count_median_numbers == 0) && ($count_high_numbers == 2)) {$bucket_range_16++; }
			#3-1-1,
			if (($count_low_numbers == 3) && ($count_median_numbers == 1) && ($count_high_numbers == 1)) {$bucket_range_17++; }
			#3-2-0,
			if (($count_low_numbers == 3) && ($count_median_numbers == 2) && ($count_high_numbers == 0)) {$bucket_range_18++; }
			#4-0-1,
			if (($count_low_numbers == 4) && ($count_median_numbers == 0) && ($count_high_numbers == 1)) {$bucket_range_19++; }
			#4-1-0,
			if (($count_low_numbers == 4) && ($count_median_numbers == 1) && ($count_high_numbers == 0)) {$bucket_range_20++; }
			#5-0-0
			if (($count_low_numbers == 5) && ($count_median_numbers == 0) && ($count_high_numbers == 0)) {$bucket_range_21++; }
		}

		else if ($maxballs == 6)
		{
			#0-0-6,
			if (($count_low_numbers == 0) && ($count_median_numbers == 0) && ($count_high_numbers == 6)) {$bucket_range_1++; }
			#0-1-5,
			if (($count_low_numbers == 0) && ($count_median_numbers == 1) && ($count_high_numbers ==5 )) {$bucket_range_2++; }
			#0-2-4,
			if (($count_low_numbers == 0) && ($count_median_numbers == 2) && ($count_high_numbers == 4)) {$bucket_range_3++; }
			#0-3-3,
			if (($count_low_numbers == 0) && ($count_median_numbers == 3) && ($count_high_numbers == 3)) {$bucket_range_4++; }
			#0-4-2,
			if (($count_low_numbers == 0) && ($count_median_numbers == 4) && ($count_high_numbers == 2)) {$bucket_range_5++; }
			#0-5-1,
			if (($count_low_numbers == 0) && ($count_median_numbers == 5) && ($count_high_numbers == 1)) {$bucket_range_6++; }
			#0-6-0,
			if (($count_low_numbers == 0) && ($count_median_numbers == 6) && ($count_high_numbers == 6)) {$bucket_range_7++; }
			#1-0-5,
			if (($count_low_numbers == 1) && ($count_median_numbers == 0) && ($count_high_numbers == 5)) {$bucket_range_8++; }
			#1-1-4,
			if (($count_low_numbers == 1) && ($count_median_numbers == 1) && ($count_high_numbers == 4)) {$bucket_range_9++; }
			#1-2-3,
			if (($count_low_numbers == 1) && ($count_median_numbers == 2) && ($count_high_numbers == 3)) {$bucket_range_10++; }
			#1-3-2,
			if (($count_low_numbers == 1) && ($count_median_numbers == 3) && ($count_high_numbers == 2)) {$bucket_range_11++; }
			#1-4-1,
			if (($count_low_numbers == 1) && ($count_median_numbers == 4) && ($count_high_numbers == 1)) {$bucket_range_12++; }
		  #1-5-0,
			if (($count_low_numbers == 1) && ($count_median_numbers == 5) && ($count_high_numbers == 0)) {$bucket_range_13++; }
			#2-0-4,
			if (($count_low_numbers == 2) && ($count_median_numbers == 0) && ($count_high_numbers == 4)) {$bucket_range_14++; }
			#2-1-3,
			if (($count_low_numbers == 2) && ($count_median_numbers == 1) && ($count_high_numbers == 3)) {$bucket_range_15++; }
			#2-2-2,
			if (($count_low_numbers == 2) && ($count_median_numbers == 2) && ($count_high_numbers == 2)) {$bucket_range_16++; }
			#2-3-1,
			if (($count_low_numbers == 2) && ($count_median_numbers == 3) && ($count_high_numbers == 1)) {$bucket_range_17++; }
			#2-4-0,
			if (($count_low_numbers == 2) && ($count_median_numbers == 4) && ($count_high_numbers == 0)) {$bucket_range_18++; }
			#3-0-3,
			if (($count_low_numbers == 3) && ($count_median_numbers == 0) && ($count_high_numbers == 3)) {$bucket_range_19++; }
			#3-1-2,
			if (($count_low_numbers == 3) && ($count_median_numbers == 1) && ($count_high_numbers == 2)) {$bucket_range_20++; }
			#3-2-1,
			if (($count_low_numbers == 3) && ($count_median_numbers == 2) && ($count_high_numbers == 1)) {$bucket_range_21++; }
			#3-3-0,
			if (($count_low_numbers == 3) && ($count_median_numbers == 3) && ($count_high_numbers == 0)) {$bucket_range_22++; }
			#4-0-2,
			if (($count_low_numbers == 4) && ($count_median_numbers == 0) && ($count_high_numbers == 2)) {$bucket_range_23++; }
			#4-1-1,
			if (($count_low_numbers == 4) && ($count_median_numbers == 1) && ($count_high_numbers == 1)) {$bucket_range_24++; }
			#4-2-0,
			if (($count_low_numbers == 4) && ($count_median_numbers == 2) && ($count_high_numbers == 0)) {$bucket_range_25++; }
			#5-0-1,
			if (($count_low_numbers == 5) && ($count_median_numbers == 0) && ($count_high_numbers == 1)) {$bucket_range_26++; }
			#5-1-0,
			if (($count_low_numbers == 5) && ($count_median_numbers == 1) && ($count_high_numbers == 0)) {$bucket_range_27++; }
			#6-0-0
			if (($count_low_numbers == 6) && ($count_median_numbers == 0) && ($count_high_numbers == 0)) {$bucket_range_28++; }
		}


		/*if (($count_low_numbers == 0) && ($count_high_numbers == $range_ceiling)) {$bucket_range_1++; }
		$range_ceiling--;
		if (($count_low_numbers == 1) && ($count_high_numbers == $range_ceiling)) {$bucket_range_2++; }
		$range_ceiling--;
		if (($count_low_numbers == 2) && ($count_high_numbers == $range_ceiling)) {$bucket_range_3++; }
		$range_ceiling--;
		if (($count_low_numbers == 3) && ($count_high_numbers == $range_ceiling)) {$bucket_range_4++; }
		$range_ceiling--;
		if (($count_low_numbers == 4) && ($count_high_numbers == $range_ceiling)) {$bucket_range_5++; }
		$range_ceiling--;
		if (($count_low_numbers == 5) && ($count_high_numbers == $range_ceiling)) {$bucket_range_6++; }
		$range_ceiling--;
		if (($count_low_numbers == 6) && ($count_high_numbers == $range_ceiling)) {$bucket_range_7++; }
		$range_ceiling--;
		if (($count_low_numbers == 7) && ($count_high_numbers == $range_ceiling)) {$bucket_range_8++; }
		*/
	}
	 ## assign color?
	 $myrowS1 = $resultS1->fetch_row();
 }   //main loop

 #print($bucket_range_1);print(" ");
 #print($bucket_range_2);print(" ");
 #print($bucket_range_3);print(" ");
 #print($bucket_range_4);print(" ");
 #print($bucket_range_5);print(" ");
 #exit;
 #print($bucket_5odd_1_even);print(" ");
 #print($bucket_6odd_0_even);print(" ");

 # INSERT INTO DB

/*
$range_1_part_a = '0'; $range_1_part_b = '6';
$range_2_part_a = '1'; $range_2_part_b = '5';
$range_3_part_a = '2'; $range_3_part_b = '4';
$range_4_part_a = '3'; $range_4_part_b = '3';
$range_5_part_a = '4'; $range_5_part_b = '2';
$range_6_part_a = '5'; $range_6_part_b = '1';
$range_7_part_a = '6'; $range_7_part_b = '0';
*/
### DEFAUTS COLOR : TO-DO   store in DB!
$range_1_color = '';$range_2_color = '';$range_3_color = '';$range_4_color = '';
$range_5_color = '';$range_6_color = '';$range_7_color = '';$range_8_color = '';
if ($chart_type == 'OE'){
	$range_1_color = 'blue06';//'093168';   
	$range_2_color = 'blue15';//'184480';
	$range_3_color = 'blue24';//'2b5a98';
	$range_4_color = 'blue33';//'4876b3';
	$range_5_color = 'blue42';//'6a94cc';
	$range_6_color = 'blue51';//'90b3e2';
	$range_7_color = 'blue60';//'90b3e2';
	$range_8_color = 'blue60';//'90b3e2';
}
else if ($chart_type == 'LH'){
	$range_1_color = 'green06';//'44734C';
	$range_2_color = 'green15';//'69B075';
	$range_3_color = 'green24';//'79CE88';
	$range_4_color = 'green33';//'89E898';
	$range_5_color = 'green42';//'95FFA7';
	$range_6_color = 'green51';//'A1FFB1';
	$range_7_color = 'green60';//'BDFFC8';
	$range_8_color = 'green60';//'BDFFC8';
}
else if ($chart_type == 'GRP') {
	$range_1_color = '093168';
	$range_2_color = '184480';
	$range_3_color = '2b5a98';
	$range_4_color = '4876b3';
	$range_5_color = '6a94cc';
	$range_6_color = '6a94cc';
	$range_7_color = '6a94cc';
	$range_8_color = '6a94cc';
	$range_9_color = '6a94cc';
	$range_10_color = '6a94cc';
}
else if ($chart_type == 'OELH'){
	$range_1_color = '44734C';
	$range_2_color = '69B075';
	$range_3_color = '79CE88';
	$range_4_color = '89E898';
	$range_5_color = '95FFA7';
	$range_6_color = 'A1FFB1';
	$range_7_color = 'BDFFC8';
	$range_8_color = 'BDFFC8';
	$range_9_color = 'BDFFC8';
	$range_10_color = 'BDFFC8';
	$range_11_color = 'BDFFC8';
	$range_12_color = 'BDFFC8';
	$range_13_color = 'BDFFC8';
	$range_14_color = 'BDFFC8';
	$range_15_color = 'BDFFC8';
	$range_16_color = 'BDFFC8';
	$range_17_color = 'BDFFC8';
	$range_18_color = 'BDFFC8';
	$range_19_color = 'BDFFC8';
	$range_20_color = 'BDFFC8';
	$range_21_color = 'BDFFC8';
	$range_22_color = 'BDFFC8';
	$range_23_color = 'BDFFC8';
	$range_24_color = 'BDFFC8';
	$range_25_color = 'BDFFC8';
	$range_26_color = 'BDFFC8';
	$range_27_color = 'BDFFC8';
	$range_28_color = 'BDFFC8';
	$range_29_color = 'BDFFC8';
	$range_30_color = 'BDFFC8';
	$range_31_color = 'BDFFC8';
	$range_32_color = 'BDFFC8';
	$range_33_color = 'BDFFC8';
	$range_34_color = 'BDFFC8';
	$range_35_color = 'BDFFC8';
	$range_36_color = 'BDFFC8';
	$range_37_color = 'BDFFC8';
	$range_38_color = 'BDFFC8';
	$range_39_color = 'BDFFC8';
	$range_40_color = 'BDFFC8';
	$range_41_color = 'BDFFC8';
	$range_42_color = 'BDFFC8';
	$range_43_color = 'BDFFC8';
	$range_44_color = 'BDFFC8';
	$range_45_color = 'BDFFC8';
	$range_46_color = 'BDFFC8';
	$range_47_color = 'BDFFC8';
	$range_48_color = 'BDFFC8';
	$range_49_color = 'BDFFC8';
	$range_50_color = 'BDFFC8';
	$range_51_color = 'BDFFC8';
	$range_52_color = 'BDFFC8';
	$range_53_color = 'BDFFC8';
	$range_54_color = 'BDFFC8';
	$range_55_color = 'BDFFC8';
	$range_56_color = 'BDFFC8';
	$range_57_color = 'BDFFC8';
	$range_58_color = 'BDFFC8';
	$range_59_color = 'BDFFC8';
	$range_60_color = 'BDFFC8';
	$range_61_color = 'BDFFC8';
	$range_62_color = 'BDFFC8';
	$range_63_color = 'BDFFC8';
	$range_64_color = 'BDFFC8';
}
else if ($chart_type == 'LMH'){
	$range_1_color = '44734C';
	$range_2_color = '69B075';
	$range_3_color = '79CE88';
	$range_4_color = '89E898';
	$range_5_color = '95FFA7';
	$range_6_color = 'A1FFB1';
	$range_7_color = 'BDFFC8';
	$range_8_color = 'BDFFC8';
	$range_9_color = 'BDFFC8';
	$range_10_color = 'BDFFC8';
	$range_11_color = 'BDFFC8';
	$range_12_color = 'BDFFC8';
	$range_13_color = 'BDFFC8';
	$range_14_color = 'BDFFC8';
	$range_15_color = 'BDFFC8';
	$range_16_color = 'BDFFC8';
	$range_17_color = 'BDFFC8';
	$range_18_color = 'BDFFC8';
	$range_19_color = 'BDFFC8';
	$range_20_color = 'BDFFC8';
	$range_21_color = 'BDFFC8';
	$range_22_color = 'BDFFC8';
	$range_23_color = 'BDFFC8';
	$range_24_color = 'BDFFC8';
	$range_25_color = 'BDFFC8';
	$range_26_color = 'BDFFC8';
	$range_27_color = 'BDFFC8';
	$range_28_color = 'BDFFC8';
}
else if ($chart_type == 'PAIR'){
	$range_7_matches_color = '44734C';
	$range_6_matches_color = '69B075';
	$range_5_matches_color = '79CE88';
	$range_4_matches_color = '89E898';
	$range_3_matches_color = '95FFA7';
	$range_2_matches_color = 'A1FFB1';
	$range_1_matches_color = 'BDFFC8';
	$range_default_color = 'BDFFC8';  # already set in db - Config_Chart_Pair - should be range_0_matches_color to be consistent with var naming !
}

$bucket_range_str = '';
$bucket_val_str = '';
$bucket_color_str = '';

#RANGE 1
$range_ceiling = $maxballs;    # important - reset
$range_1_part_a = 0;  $range_1_part_b = $range_ceiling;
$bucket_range_str = $range_1_part_a.'-'.$range_1_part_b;
$bucket_val_str = $bucket_range_1;
$bucket_color_str = $range_1_color;

#RANGE 2
$range_2_part_a = 1; $range_ceiling--;
$range_2_part_b = $range_ceiling; $bucket_range_str = $bucket_range_str.','.$range_2_part_a.'-'.$range_2_part_b;
$bucket_val_str = $bucket_val_str.','.$bucket_range_2;
$bucket_color_str = $bucket_color_str.','.$range_2_color;

# RANGE 3
$range_3_part_a = 2; $range_ceiling--;
$range_3_part_b = $range_ceiling; $bucket_range_str = $bucket_range_str.','.$range_3_part_a.'-'.$range_3_part_b;
$bucket_val_str = $bucket_val_str.','.$bucket_range_3;
$bucket_color_str = $bucket_color_str.','.$range_3_color;

# RANGE 4
if ($ball3_active){
	$range_4_part_a = 3; $range_ceiling--;
	$range_4_part_b = $range_ceiling;
	$bucket_range_str = $bucket_range_str.','.$range_4_part_a.'-'.$range_4_part_b;
	$bucket_val_str = $bucket_val_str.','.$bucket_range_4;
	$bucket_color_str = $bucket_color_str.','.$range_4_color;
}
# RANGE 5
if ($ball4_active){
	$range_5_part_a = 4; $range_ceiling--;
	$range_5_part_b = $range_ceiling;
	$bucket_range_str = $bucket_range_str.','.$range_5_part_a.'-'.$range_5_part_b;
	$bucket_val_str = $bucket_val_str.','.$bucket_range_5;
	$bucket_color_str = $bucket_color_str.','.$range_5_color;
}
# RANGE 6
if ($ball5_active){
	$range_6_part_a = 5; $range_ceiling--;
	$range_6_part_b = $range_ceiling;
	$bucket_range_str = $bucket_range_str.','.$range_6_part_a.'-'.$range_6_part_b;
	$bucket_val_str = $bucket_val_str.','.$bucket_range_6;
	$bucket_color_str = $bucket_color_str.','.$range_6_color;
}
# RANGE 7
if ($ball6_active){
	$range_7_part_a = 6; $range_ceiling--;
	$range_7_part_b = $range_ceiling; $bucket_range_str = $bucket_range_str.','.$range_7_part_a.'-'.$range_7_part_b;
	$bucket_val_str = $bucket_val_str.','.$bucket_range_7;
	$bucket_color_str = $bucket_color_str.','.$range_7_color;
}
# RANGE 8
if ($ball7_active){
	$range_8_part_a = 7; $range_ceiling--;
	$range_8_part_b = $range_ceiling; $bucket_range_str = $bucket_range_str.','.$range_8_part_a.'-'.$range_8_part_b;
	$bucket_val_str = $bucket_val_str.','.$bucket_range_8;
	$bucket_color_str = $bucket_color_str.','.$range_8_color;
}


if ($chart_type == 'OELH'){
	if ($maxballs == 5){
			# RANGE 7  - need for 5 balls !!!!!
			$bucket_val_str = $bucket_val_str.','.$bucket_range_7;
			$bucket_color_str = $bucket_color_str.','.$range_7_color;
		}
	if (($maxballs == 5) || ($maxballs == 6)){
		# RANGE 8  - need for 6 balls !!!!!
			$bucket_val_str = $bucket_val_str.','.$bucket_range_8;
			$bucket_color_str = $bucket_color_str.','.$range_8_color;
	}
	if (($maxballs == 5) || ($maxballs == 6) || ($maxballs == 7)){
		# RANGE 9  - need for 6 balls & 7 !!!!!
			$bucket_val_str = $bucket_val_str.','.$bucket_range_9;
			$bucket_color_str = $bucket_color_str.','.$range_9_color;
		# RANGE 10
			$bucket_val_str = $bucket_val_str.','.$bucket_range_10;
			$bucket_color_str = $bucket_color_str.','.$range_10_color;
		# RANGE 11
			$bucket_val_str = $bucket_val_str.','.$bucket_range_11;
			$bucket_color_str = $bucket_color_str.','.$range_11_color;
		# RANGE 12
			$bucket_val_str = $bucket_val_str.','.$bucket_range_12;
			$bucket_color_str = $bucket_color_str.','.$range_12_color;
		# RANGE 13
			$bucket_val_str = $bucket_val_str.','.$bucket_range_13;
			$bucket_color_str = $bucket_color_str.','.$range_13_color;
		# RANGE 14
			$bucket_val_str = $bucket_val_str.','.$bucket_range_14;
			$bucket_color_str = $bucket_color_str.','.$range_14_color;
		# RANGE 15
			$bucket_val_str = $bucket_val_str.','.$bucket_range_15;
			$bucket_color_str = $bucket_color_str.','.$range_15_color;
		# RANGE 16
			$bucket_val_str = $bucket_val_str.','.$bucket_range_16;
			$bucket_color_str = $bucket_color_str.','.$range_16_color;
		# RANGE 17
			$bucket_val_str = $bucket_val_str.','.$bucket_range_17;
			$bucket_color_str = $bucket_color_str.','.$range_17_color;
		# RANGE 18
			$bucket_val_str = $bucket_val_str.','.$bucket_range_18;
			$bucket_color_str = $bucket_color_str.','.$range_18_color;
		# RANGE 19
			$bucket_val_str = $bucket_val_str.','.$bucket_range_19;
			$bucket_color_str = $bucket_color_str.','.$range_19_color;
		# RANGE 20
			$bucket_val_str = $bucket_val_str.','.$bucket_range_20;
			$bucket_color_str = $bucket_color_str.','.$range_20_color;
		# RANGE 21
			$bucket_val_str = $bucket_val_str.','.$bucket_range_21;
			$bucket_color_str = $bucket_color_str.','.$range_21_color;
		# RANGE 22
			$bucket_val_str = $bucket_val_str.','.$bucket_range_22;
			$bucket_color_str = $bucket_color_str.','.$range_22_color;
		# RANGE 23
			$bucket_val_str = $bucket_val_str.','.$bucket_range_23;
			$bucket_color_str = $bucket_color_str.','.$range_23_color;
		# RANGE 24
			$bucket_val_str = $bucket_val_str.','.$bucket_range_24;
			$bucket_color_str = $bucket_color_str.','.$range_24_color;
		# RANGE 25
			$bucket_val_str = $bucket_val_str.','.$bucket_range_25;
			$bucket_color_str = $bucket_color_str.','.$range_25_color;
		# RANGE 26
		$bucket_val_str = $bucket_val_str.','.$bucket_range_26;
		$bucket_color_str = $bucket_color_str.','.$range_26_color;
		
		# RANGE 27
		$bucket_val_str = $bucket_val_str.','.$bucket_range_27;
		$bucket_color_str = $bucket_color_str.','.$range_27_color;

		# RANGE 28
		$bucket_val_str = $bucket_val_str.','.$bucket_range_28;
		$bucket_color_str = $bucket_color_str.','.$range_28_color;

		# RANGE 29
		$bucket_val_str = $bucket_val_str.','.$bucket_range_29;
		$bucket_color_str = $bucket_color_str.','.$range_29_color;

		# RANGE 30
		$bucket_val_str = $bucket_val_str.','.$bucket_range_30;
		$bucket_color_str = $bucket_color_str.','.$range_30_color;

		# RANGE 31
		$bucket_val_str = $bucket_val_str.','.$bucket_range_31;
		$bucket_color_str = $bucket_color_str.','.$range_31_color;

		# RANGE 32
		$bucket_val_str = $bucket_val_str.','.$bucket_range_32;
		$bucket_color_str = $bucket_color_str.','.$range_32_color;

		# RANGE 33
		$bucket_val_str = $bucket_val_str.','.$bucket_range_33;
		$bucket_color_str = $bucket_color_str.','.$range_33_color;

		# RANGE 34
		$bucket_val_str = $bucket_val_str.','.$bucket_range_34;
		$bucket_color_str = $bucket_color_str.','.$range_34_color;

		# RANGE 35
		$bucket_val_str = $bucket_val_str.','.$bucket_range_35;
		$bucket_color_str = $bucket_color_str.','.$range_35_color;

		# RANGE 36
		$bucket_val_str = $bucket_val_str.','.$bucket_range_36;
		$bucket_color_str = $bucket_color_str.','.$range_36_color;
	}
	if (($maxballs == 6) || ($maxballs == 7)){
		# RANGE 37
		$bucket_val_str = $bucket_val_str.','.$bucket_range_37;
		$bucket_color_str = $bucket_color_str.','.$range_37_color;

		# RANGE 38
		$bucket_val_str = $bucket_val_str.','.$bucket_range_38;
		$bucket_color_str = $bucket_color_str.','.$range_38_color;

		# RANGE 39
		$bucket_val_str = $bucket_val_str.','.$bucket_range_39;
		$bucket_color_str = $bucket_color_str.','.$range_39_color;

		# RANGE 40
		$bucket_val_str = $bucket_val_str.','.$bucket_range_40;
		$bucket_color_str = $bucket_color_str.','.$range_40_color;

		# RANGE 41
		$bucket_val_str = $bucket_val_str.','.$bucket_range_41;
		$bucket_color_str = $bucket_color_str.','.$range_41_color;

		# RANGE 42
		$bucket_val_str = $bucket_val_str.','.$bucket_range_42;
		$bucket_color_str = $bucket_color_str.','.$range_42_color;

		# RANGE 43
		$bucket_val_str = $bucket_val_str.','.$bucket_range_43;
		$bucket_color_str = $bucket_color_str.','.$range_43_color;

		# RANGE 44
		$bucket_val_str = $bucket_val_str.','.$bucket_range_44;
		$bucket_color_str = $bucket_color_str.','.$range_44_color;

		# RANGE 45
		$bucket_val_str = $bucket_val_str.','.$bucket_range_45;
		$bucket_color_str = $bucket_color_str.','.$range_45_color;

		# RANGE 46
		$bucket_val_str = $bucket_val_str.','.$bucket_range_46;
		$bucket_color_str = $bucket_color_str.','.$range_46_color;

		# RANGE 47
		$bucket_val_str = $bucket_val_str.','.$bucket_range_47;
		$bucket_color_str = $bucket_color_str.','.$range_47_color;

		# RANGE 48
		$bucket_val_str = $bucket_val_str.','.$bucket_range_48;
		$bucket_color_str = $bucket_color_str.','.$range_48_color;

		# RANGE 49
		$bucket_val_str = $bucket_val_str.','.$bucket_range_49;
		$bucket_color_str = $bucket_color_str.','.$range_49_color;
		}

	if  ($maxballs == 7){
		# RANGE 50
		$bucket_val_str = $bucket_val_str.','.$bucket_range_50;
		$bucket_color_str = $bucket_color_str.','.$range_50_color;
		# RANGE 51
		$bucket_val_str = $bucket_val_str.','.$bucket_range_51;
		$bucket_color_str = $bucket_color_str.','.$range_51_color;
		# RANGE 52
		$bucket_val_str = $bucket_val_str.','.$bucket_range_52;
		$bucket_color_str = $bucket_color_str.','.$range_52_color;
		# RANGE 53
		$bucket_val_str = $bucket_val_str.','.$bucket_range_53;
		$bucket_color_str = $bucket_color_str.','.$range_53_color;
		# RANGE 54
		$bucket_val_str = $bucket_val_str.','.$bucket_range_54;
		$bucket_color_str = $bucket_color_str.','.$range_54_color;
		# RANGE 55
		$bucket_val_str = $bucket_val_str.','.$bucket_range_55;
		$bucket_color_str = $bucket_color_str.','.$range_55_color;
		# RANGE 56
		$bucket_val_str = $bucket_val_str.','.$bucket_range_56;
		$bucket_color_str = $bucket_color_str.','.$range_56_color;
		# RANGE 57
		$bucket_val_str = $bucket_val_str.','.$bucket_range_57;
		$bucket_color_str = $bucket_color_str.','.$range_57_color;
		# RANGE 58
		$bucket_val_str = $bucket_val_str.','.$bucket_range_58;
		$bucket_color_str = $bucket_color_str.','.$range_58_color;
		# RANGE 59
		$bucket_val_str = $bucket_val_str.','.$bucket_range_59;
		$bucket_color_str = $bucket_color_str.','.$range_59_color;
		# RANGE 60
		$bucket_val_str = $bucket_val_str.','.$bucket_range_60;
		$bucket_color_str = $bucket_color_str.','.$range_60_color;
		# RANGE 61
		$bucket_val_str = $bucket_val_str.','.$bucket_range_61;
		$bucket_color_str = $bucket_color_str.','.$range_61_color;
		# RANGE 62
		$bucket_val_str = $bucket_val_str.','.$bucket_range_62;
		$bucket_color_str = $bucket_color_str.','.$range_62_color;
		# RANGE 63
		$bucket_val_str = $bucket_val_str.','.$bucket_range_63;
		$bucket_color_str = $bucket_color_str.','.$range_63_color;
		# RANGE 64
		$bucket_val_str = $bucket_val_str.','.$bucket_range_64;
		$bucket_color_str = $bucket_color_str.','.$range_64_color;
	}
}
if ($chart_type == 'LMH'){
	if ($maxballs == 5)	{
			# RANGE 7  - need for 5 balls !!!!!
			$bucket_val_str = $bucket_val_str.','.$bucket_range_7;
			$bucket_color_str = $bucket_color_str.','.$range_7_color;
		}
	if (($maxballs == 5) || ($maxballs == 6))	{
		# RANGE 8  - need for 6 balls !!!!!
			$bucket_val_str = $bucket_val_str.','.$bucket_range_8;
			$bucket_color_str = $bucket_color_str.','.$range_8_color;
		# RANGE 9
			$bucket_val_str = $bucket_val_str.','.$bucket_range_9;
			$bucket_color_str = $bucket_color_str.','.$range_9_color;
		# RANGE 10
			$bucket_val_str = $bucket_val_str.','.$bucket_range_10;
			$bucket_color_str = $bucket_color_str.','.$range_10_color;
		# RANGE 11
			$bucket_val_str = $bucket_val_str.','.$bucket_range_11;
			$bucket_color_str = $bucket_color_str.','.$range_11_color;
		# RANGE 12
			$bucket_val_str = $bucket_val_str.','.$bucket_range_12;
			$bucket_color_str = $bucket_color_str.','.$range_12_color;
		# RANGE 13
			$bucket_val_str = $bucket_val_str.','.$bucket_range_13;
			$bucket_color_str = $bucket_color_str.','.$range_13_color;
		# RANGE 14
			$bucket_val_str = $bucket_val_str.','.$bucket_range_14;
			$bucket_color_str = $bucket_color_str.','.$range_14_color;
		# RANGE 15
			$bucket_val_str = $bucket_val_str.','.$bucket_range_15;
			$bucket_color_str = $bucket_color_str.','.$range_15_color;
		# RANGE 16
			$bucket_val_str = $bucket_val_str.','.$bucket_range_16;
			$bucket_color_str = $bucket_color_str.','.$range_16_color;
		# RANGE 17
			$bucket_val_str = $bucket_val_str.','.$bucket_range_17;
			$bucket_color_str = $bucket_color_str.','.$range_17_color;
		# RANGE 18
			$bucket_val_str = $bucket_val_str.','.$bucket_range_18;
			$bucket_color_str = $bucket_color_str.','.$range_18_color;
		# RANGE 19
			$bucket_val_str = $bucket_val_str.','.$bucket_range_19;
			$bucket_color_str = $bucket_color_str.','.$range_19_color;
		# RANGE 20
			$bucket_val_str = $bucket_val_str.','.$bucket_range_20;
			$bucket_color_str = $bucket_color_str.','.$range_20_color;
		# RANGE 21
			$bucket_val_str = $bucket_val_str.','.$bucket_range_21;
			$bucket_color_str = $bucket_color_str.','.$range_21_color;
	}
	if ($maxballs == 6){
		# RANGE 22
			$bucket_val_str = $bucket_val_str.','.$bucket_range_22;
			$bucket_color_str = $bucket_color_str.','.$range_22_color;
		# RANGE 23
			$bucket_val_str = $bucket_val_str.','.$bucket_range_23;
			$bucket_color_str = $bucket_color_str.','.$range_23_color;
		# RANGE 24
			$bucket_val_str = $bucket_val_str.','.$bucket_range_24;
			$bucket_color_str = $bucket_color_str.','.$range_24_color;
		# RANGE 25
			$bucket_val_str = $bucket_val_str.','.$bucket_range_25;
			$bucket_color_str = $bucket_color_str.','.$range_25_color;
		# RANGE 26
			$bucket_val_str = $bucket_val_str.','.$bucket_range_26;
			$bucket_color_str = $bucket_color_str.','.$range_26_color;
		# RANGE 27
			$bucket_val_str = $bucket_val_str.','.$bucket_range_27;
			$bucket_color_str = $bucket_color_str.','.$range_27_color;
		# RANGE 28
			$bucket_val_str = $bucket_val_str.','.$bucket_range_28;
			$bucket_color_str = $bucket_color_str.','.$range_28_color;
		}
}
#override for now
if ($chart_type == 'LMH'){
 if ($maxballs == 6){
		 $bucket_range_str = '0-0-6,0-1-5,0-2-4,0-3-3,0-4-2,0-5-1,0-6-0,1-0-5,1-1-4,1-2-3,1-3-2,1-4-1,1-5-0,2-0-4,2-1-3,2-2-2,2-3-1,2-4-0,3-0-3,3-1-2,3-2-1,3-3-0,4-0-2,4-1-1,4-2-0,5-0-1,5-1-0,6-0-0';
	}
	else if ($maxballs == 5){
		$bucket_range_str = '0-0-5,0-1-4,0-2-3,0-3-2,0-4-1,0-5-0,1-0-4,1-1-3,1-2-2,1-3-1,1-4-0,2-0-3,2-1-2,2-2-1,2-3-0,3-0-2,3-1-1,3-2-0,4-0-1,4-1-0,5-0-0';
	}
   else if ($maxballs == 4) {
	$bucket_range_str = '0-0-4,0-1-3,0-2-2,0-3-1,0-4-0,1-0-3,1-1-2,1-2-1,1-3-0,2-0-2,2-1-1,2-2-0,3-0-1,3-1-0,4-0-0';
   }
	// if ($maxballs == 7)
	// if ($maxballs == 8)
}
#override for now
if ($chart_type == 'OELH'){
 	if ($maxballs == 6)  {
	//	$bucket_range_str = '1O5E-1L5H,1O5E-2L4H,1O5E-3L3H,1O5E-4L2H,1O5E-5L1H,2O4E-1L5H,2O4E-2L4H,2O4E-3L3H,2O4E-4L2H,2O4E-5L1H,3O3E-1L5H,3O3E-2L4H,3O3E-3L3H,3O3E-4L2H,3O3E-5L1H,4O2E-1L5H,4O2E-2L4H,4O2E-3L3H,4O2E-4L2H,4O2E-5L1H,5O1E-1L5H,5O1E-2L4H,5O1E-3L3H,5O1E-4L2H,5O1E-5L1H';
	//	$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16,cb17,cb18,cb19,cb20,cb21,cb22,cb23,cb24,cb25';	
	$bucket_range_str = '0O6E-0L6H,0O6E-1L5H,0O6E-2L4H,0O6E-3L3H,0O6E-4L2H,0O6E-5L1H,0O6E-6L0H,1O5E-0L6H,1O5E-1L5H,1O5E-2L4H,1O5E-3L3H,1O5E-4L2H,1O5E-5L1H,1O5E-6L0H,2O4E-0L6H,2O4E-1L5H,2O4E-2L4H,2O4E-3L3H,2O4E-4L2H,2O4E-5L1H,2O4E-6L0H,3O3E-0L6H,3O3E-1L5H,3O3E-2L4H,3O3E-3L3H,3O3E-4L2H,3O3E-5L1H,3O3E-6L0H,4O2E-0L6H,4O2E-1L5H,4O2E-2L4H,4O2E-3L3H,4O2E-4L2H,4O2E-5L1H,4O2E-6L0H,5O1E-0L6H,5O1E-1L5H,5O1E-2L4H,5O1E-3L3H,5O1E-4L2H,5O1E-5L1H,5O1E-6L0H,6O0E-0L6H,6O0E-1L5H,6O0E-2L4H,6O0E-3L3H,6O0E-4L2H,6O0E-5L1H,6O0E-6L0H';
	$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16,cb17,cb18,cb19,cb20,cb21,cb22,cb23,cb24,cb25,cb26,cb27,cb28,cb29,cb30,cb31,cb32,cb33,cb34,cb35,cb36,cb37,cb38,cb39,cb40,cb41,cb42,cb43,cb44,cb45,cb46,cb47,cb48,cb49';	 
	}
	else if ($maxballs == 5){
	//$bucket_range_str = 'to-do';
	//$bucket_range_str = '1O4E-1L4H,1O4E-2L3H,1O4E-3L2H,1O4E-4L1H,2O3E-1L4H,2O3E-2L3H,2O3E-3L2H,2O3E-4L1H,3O2E-1L4H,3O2E-2L3H,3O2E-3L2H,3O2E-4L1H,4O1E-1L4H,4O1E-2L3H,4O1E-3L2H,4O1E-4L1H';
	//$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16';	
	$bucket_range_str = '0O5E-0L5H,0O5E-1L4H,0O5E-2L3H,0O5E-3L2H,0O5E-4L1H,0O5E-5L0H,1O4E-0L5H,1O4E-1L4H,1O4E-2L3H,1O4E-3L2H,1O4E-4L1H,1O4E-5L0H,2O3E-0L5H,2O3E-1L4H,2O3E-2L3H,2O3E-3L2H,2O3E-4L1H,2O3E-5L0H,3O2E-0L5H,3O2E-1L4H,3O2E-2L3H,3O2E-3L2H,3O2E-4L1H,3O2E-5L0H,4O1E-0L5H,4O1E-1L4H,4O1E-2L3H,4O1E-3L2H,4O1E-4L1H,4O1E-5L0H,5O0E-0L5H,5O0E-1L4H,5O0E-2L3H,5O0E-3L2H,5O0E-4L1H,5O0E-5L0H';
	$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16,cb17,cb18,cb19,cb20,cb21,cb22,cb23,cb24,cb25,cb26,cb27,cb28,cb29,cb30,cb31,cb32,cb33,cb34,cb35,cb36';	
	}
	else if ($maxballs == 4){
	//$bucket_range_str = 'to-do';
	$bucket_range_str = '1O3E-1L3H,1O3E-2L2H,1O3E-3L1H,2O2E-1L3H,2O2E-2L2H,2O2E-3L1H,3O1E-1L3H,3O1E-2L2H,3O1E-3L1H';
	$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9';	
	}
	else if ($maxballs == 7) {
		//$bucket_range_str = '1O6E-1L6H,1O6E-2L5H,1O6E-3L4H,1O6E-4L3H,1O6E-5L2H,1O6E-6L1H,2O5E-1L6H,2O5E-2L5H,2O5E-3L4H,2O5E-4L3H,2O5E-5L2H,2O5E-6L1H,3O4E-1L6H,3O4E-2L5H,3O4E-3L4H,3O4E-4L3H,3O4E-5L2H,3O4E-6L1H,4O3E-1L6H,4O3E-2L5H,4O3E-3L4H,4O3E-4L3H,4O3E-5L2H,4O3E-6L1H,5O2E-1L6H,5O2E-2L5H,5O2E-3L4H,5O2E-4L3H,5O2E-5L2H,5O2E-6L1H,6O1E-1L6H,6O1E-2L5H,6O1E-3L4H,6O1E-4L3H,6O1E-5L2H,6O1E-6L1H';
		//$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16,cb17,cb18,cb19,cb20,cb21,cb22,cb23,cb24,cb25,cb26,cb27,cb28,cb29,cb30,cb31,cb32,cb33,cb34,cb35,cb36';	
		$bucket_range_str = 				  '0O7E-0L7H,0O7E-1L6H,0O7E-2L5H,0O7E-3L4H,0O7E-4L3H,0O7E-5L2H,0O7E-6L1H,0O7E-7L0H,';
		$bucket_range_str = $bucket_range_str.'1O6E-0L7H,1O6E-1L6H,1O6E-2L5H,1O6E-3L4H,1O6E-4L3H,1O6E-5L2H,1O6E-6L1H,1O6E-7L0H,';
		$bucket_range_str = $bucket_range_str.'2O5E-0L7H,2O5E-1L6H,2O5E-2L5H,2O5E-3L4H,2O5E-4L3H,2O5E-5L2H,2O5E-6L1H,2O5E-7L0H,';
		$bucket_range_str = $bucket_range_str.'3O4E-0L7H,3O4E-1L6H,3O4E-2L5H,3O4E-3L4H,3O4E-4L3H,3O4E-5L2H,3O4E-6L1H,3O4E-7L0H,';
		$bucket_range_str = $bucket_range_str.'4O3E-0L7H,4O3E-1L6H,4O3E-2L5H,4O3E-3L4H,4O3E-4L3H,4O3E-5L2H,4O3E-6L1H,4O3E-7L0H,';
		$bucket_range_str = $bucket_range_str.'5O2E-0L7H,5O2E-1L6H,5O2E-2L5H,5O2E-3L4H,5O2E-4L3H,5O2E-5L2H,5O2E-6L1H,5O2E-7L0H,';
		$bucket_range_str = $bucket_range_str.'6O1E-0L7H,6O1E-1L6H,6O1E-2L5H,6O1E-3L4H,6O1E-4L3H,6O1E-5L2H,6O1E-6L1H,6O1E-7L0H,';
		$bucket_range_str = $bucket_range_str.'7O0E-0L7H,7O0E-1L6H,7O0E-2L5H,7O0E-3L4H,7O0E-4L3H,7O0E-5L2H,7O0E-6L1H,7O0E-7L0H';
		//	$bucket_color_str = 'cb not used - 64 entries';	
		$bucket_color_str = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10,cb11,cb12,cb13,cb4,cb15,cb16,cb17,cb18,cb19,cb20,cb21,cb22,cb23,cb24,cb25,cb26,cb27,cb28,cb29,cb30,cb31,cb32,cb33,cb34,cb35,cb36,cb37,cb38,cb39,cb40,cb41,cb42,cb43,cb44,cb45,cb46,cb47,cb48,cb49,cb50,cb51,cb52,cb53,cb54,cb55,cb56,cb57,cb58,cb59,cb60,cb61,cb62,cb63,cb64';
		}
}
#override for now
if ($chart_type == 'GRP')
{
	//$bucket_range_str = '1-10,11-20,21-30,31-40,41-50';
	//build up dynamically
	$bucket_range_str = '';
	$bucket_val_str = '';
	$bucket_color_str = '';

	if ($ball_max >= 1) { 
		$bucket_range_str = '1-10';
		$bucket_val_str = $bucket_range_1;
		$bucket_color_str = $range_1_color;
	}
	if ($ball_max >= 11) { 
		$bucket_range_str = $bucket_range_str.',11-20';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_2;
		$bucket_color_str = $bucket_color_str.','.$range_2_color;
	}
	if ($ball_max >= 21) { 
		$bucket_range_str = $bucket_range_str.',21-30';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_3;
		$bucket_color_str = $bucket_color_str.','.$range_3_color;
	}
	if ($ball_max >= 31) { 
		$bucket_range_str = $bucket_range_str.',31-40';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_4;
		$bucket_color_str = $bucket_color_str.','.$range_4_color;
	}
	if ($ball_max >= 41) { 
		$bucket_range_str = $bucket_range_str.',41-50';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_5;
		$bucket_color_str = $bucket_color_str.','.$range_5_color;
	}
	if ($ball_max >= 51) { 
		$bucket_range_str = $bucket_range_str.',51-60';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_6;
		$bucket_color_str = $bucket_color_str.','.$range_6_color;
	}
	if ($ball_max >= 61) { 
		$bucket_range_str = $bucket_range_str.',61-70';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_7;
		$bucket_color_str = $bucket_color_str.','.$range_7_color;
	}
	if ($ball_max >= 71) { 
		$bucket_range_str = $bucket_range_str.',71-80';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_8;
		$bucket_color_str = $bucket_color_str.','.$range_8_color;
	}
	if ($ball_max >= 81) { 
		$bucket_range_str = $bucket_range_str.',81-90';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_9;
		$bucket_color_str = $bucket_color_str.','.$range_9_color;
	}
	if ($ball_max >= 100) { 
		$bucket_range_str = $bucket_range_str.',91-100';
		$bucket_val_str = $bucket_val_str.','.$bucket_range_10;
		$bucket_color_str = $bucket_color_str.','.$range_10_color;
	}
	//   bucket_range_str =  as per above cal
	//	 $bucket_val_str = $bucket_range_1.','.$bucket_range_2.','.$bucket_range_3.','.$bucket_range_4.','.$bucket_range_5;
	//	 $bucket_color_str = $range_1_color.','.$range_2_color.','.$range_3_color.','.$range_4_color.','.$range_5_color;
}
#overide
if ($chart_type == 'PAIR')
{
	$bucket_range_str = '';
	$bucket_val_str = '';
	$bucket_color_str = '';

	$i=0;    //reset
	while ($i < $db_pair_count)	{
		$bucket = $pair_array[$i]['bucket'];
		$bucket_val = $pair_array[$i]['value'];
		$bucket_color = $pair_array[$i]['color'];
		### only do where value is > 0 to save length on strings.
		if ($bucket_val > 0) {
			#override color
			if ($bucket_val == 1) {$bucket_color=$range_1_matches_color;}
			else if ($bucket_val == 2) {$bucket_color=$range_2_matches_color;}
			else if ($bucket_val == 3) {$bucket_color=$range_3_matches_color;}
			else if ($bucket_val == 4) {$bucket_color=$range_4_matches_color;}
			else if ($bucket_val == 5) {$bucket_color=$range_5_matches_color;}
			else if ($bucket_val == 6) {$bucket_color=$range_6_matches_color;}
			else if ($bucket_val >= 7) {$bucket_color=$range_7_matches_color;}
		  if ($bucket_range_str == '')	{	$bucket_range_str = $bucket;} 			else { $bucket_range_str = $bucket_range_str.','.$bucket;}
		  if ($bucket_val_str == '')		{ $bucket_val_str = $bucket_val;}		else { $bucket_val_str  = $bucket_val_str.','.$bucket_val;}
			if ($bucket_color_str == '')	{ $bucket_color_str = $bucket_color;} else { $bucket_color_str = $bucket_color_str.','.$bucket_color;}
	  }
		$i++;
	 }   //pair_array loop
	 ##TO-DO - strip off last ,
}

#$bucket_range_str = '0-6,1-5,2-4,3-3,4-2,5-1,6-0';
#$bucket_color_str = '093168,184480,2b5a98,4876b3,6a94cc,90b3e2,90b3e2';
#$bucket_val_str = $bucket_0odd_6_even.','.$bucket_1odd_5_even.','.$bucket_2odd_4_even.','.$bucket_3odd_3_even.','.$bucket_4odd_2_even.','.$bucket_5odd_1_even.','.$bucket_6odd_0_even;
#####DO SAME FOR POWERBALL ( PB1 + PB2)
# OE : 0-1,1-0
# LH : 0-1,1-0
##L-M-H :   0-0-1,0-1-0,1-0-0
############### P O W E R B A L L #################################################################################
if ($pb_ind)
{
	$bucket_range_1 = 0;$bucket_range_2 = 0;$bucket_range_3 = 0;$bucket_range_4 = 0;$bucket_range_5 = 0;$bucket_range_6 = 0;$bucket_range_7 = 0;$bucket_range_8 = 0;$bucket_range_9 = 0;

	#print($query_full_pb);exit;
	if ($resultS1 = $db->query($query_full_pb)){ $myrowS1 = $resultS1->fetch_row();}
	while ($myrowS1){
		# 1) for each row count # even, # odd
	
		if ($chart_type == 'OE'){
			$count_odd_numbers = 0;
			$count_even_numbers = 0;
			$index=0;
			if ($powerball_active)  { $ball1 = $myrowS1[$index]; if (number_even($ball1)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
			if ($powerball2_active) { $index++; $ball2 = $myrowS1[$index]; if (number_even($ball2)) {$count_even_numbers++;} else {$count_odd_numbers++;} }
		}
		else if ($chart_type == 'LH'){
			$count_low_numbers = 0;
			$count_high_numbers = 0;
			$index=0;
			if ($powerball_active)  { $ball1 = $myrowS1[$index]; if (number_low($ball1,$ball_max_pb)) {$count_low_numbers++;} else {$count_high_numbers++;} }
			if ($powerball2_active) { $index++; $ball2 = $myrowS1[$index]; if (number_low($ball2,$ball_max_pb)) {$count_low_numbers++;} else {$count_high_numbers++;} }
		}
		else if ($chart_type == 'OELH'){
			$count_odd_high_numbers = 0;
			$count_even_high_numbers = 0;
			$count_odd_low_numbers = 0;
			$count_even_low_numbers = 0;
			$index=0;
			if ($maxballs_pb == 1)  { 
				$ball1 = $myrowS1[$index]; 
				if      (!number_even($ball1) && (!number_low($ball1,$ball_max_pb))){ $count_odd_high_numbers++; } 
				else if (number_even($ball1)  && (!number_low($ball1,$ball_max_pb))) { $count_even_high_numbers++; } 
				else if (!number_even($ball1) && (number_low($ball1,$ball_max_pb))) { $count_odd_low_numbers++; } 
				else if (number_even($ball1)  && (number_low($ball1,$ball_max_pb)))  { $count_even_low_numbers++; } 
			}
			if ($maxballs_pb == 2) {
				$index=0; 
				$count_odd_numbers = 0;$count_even_numbers = 0;$count_low_numbers = 0;$count_high_numbers = 0;
				if ($powerball_active) { 
					$ball1 = $myrowS1[$index];
					if (number_even($ball1)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
					if (number_low($ball1,$ball_max_pb)) {$count_low_numbers++;} else {$count_high_numbers++;}
				}
				if ($powerball2_active) { 
					$index++; $ball2 = $myrowS1[$index]; 
					if (number_even($ball2)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
					if (number_low($ball2,$ball_max_pb)) {$count_low_numbers++;} else {$count_high_numbers++;}
				}
			//	 $index++; $ball2 = $myrowS1[$index]; if (number_even($ball2)) {$count_even_numbers++;} else {$count_odd_numbers++;} 
				}
		}
		else if ($chart_type == 'GRP'){
			$count_1_10_numbers = 0;
			$count_11_20_numbers = 0;
			$index=0;
			if ($powerball_active){
				$ball1 = $myrowS1[$index];
				if (number_range_GRP(1,10,$ball1,$ball_max_pb)) {$count_1_10_numbers++;}
				else if (number_range_GRP(11,20,$ball1,$ball_max_pb)){$count_11_20_numbers++;}
			}
			if ($powerball2_active){
				$index++; $ball2 = $myrowS1[$index];
				if (number_range_GRP(1,10,$ball2,$ball_max_pb)) {$count_1_10_numbers++;}
				else if (number_range_GRP(11,20,$ball2,$ball_max_pb)){$count_11_20_numbers++;}
			}
		}
		else if ($chart_type == 'LMH'){
			#exit;
			$count_low_numbers = 0;
			$count_median_numbers = 0;
			$count_high_numbers = 0;
			$index=0;
				if ($powerball_active){
					$ball1 = $myrowS1[$index];
					#print("<br>ball:");
					#print($ball1);
					if (number_low_LMH($ball1,$ball_max_pb)) {$count_low_numbers++; }
					else if (number_high_LMH($ball1,$ball_max_pb)) {$count_high_numbers++;}
					else {$count_median_numbers++;}
				}
				if ($powerball2_active){
					$index++; $ball2 = $myrowS1[$index];
					if (number_low_LMH($ball2,$ball_max_pb)) {$count_low_numbers++;}
					else if (number_high_LMH($ball2,$ball_max_pb)) {$count_high_numbers++;}
					else {$count_median_numbers++;}
				}
		}

		# 2) assign to bucket
			/* new */
		$range_ceiling = $maxballs_pb;  # reset each time
		if ($chart_type == 'OE'){
			if (($count_odd_numbers == 0) && ($count_even_numbers == $range_ceiling)) {$bucket_range_1++; }
			$range_ceiling--;
			if (($count_odd_numbers == 1) && ($count_even_numbers == $range_ceiling)) {$bucket_range_2++; }
			$range_ceiling--;
			if (($count_odd_numbers == 2) && ($count_even_numbers == $range_ceiling)) {$bucket_range_3++; }   // if x 2 powerballs
		}
		else if ($chart_type == 'LH'){
			if (($count_low_numbers == 0) && ($count_high_numbers == $range_ceiling)) {$bucket_range_1++; }
			$range_ceiling--;
			if (($count_low_numbers == 1) && ($count_high_numbers == $range_ceiling)) {$bucket_range_2++; }
			$range_ceiling--;
			if (($count_low_numbers == 2) && ($count_high_numbers == $range_ceiling)) {$bucket_range_3++; }
		}
		else if ($chart_type == 'OELH'){	
			if ($maxballs_pb == 1) {
					if (($count_odd_high_numbers > 0) ) {$bucket_range_1++; }
					// $range_ceiling--;
					if (($count_even_high_numbers > 0) ) {$bucket_range_2++; }
					// $range_ceiling--;
					if (($count_odd_low_numbers > 0) ) {$bucket_range_3++; }
					//  $range_ceiling--;
					if (($count_even_low_numbers > 0) ) {$bucket_range_4++; }

					/* pattern original 
					if (($count_odd_numbers == 0) && ($count_even_numbers == 1) && ($count_low_numbers == 0) &&  ($count_high_numbers == 1)) {$bucket_range_1++; }
					if (($count_odd_numbers == 0) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 0)) {$bucket_range_2++; }	
					if (($count_odd_numbers == 1) && ($count_even_numbers == 0) && ($count_low_numbers == 0) &&  ($count_high_numbers == 1)) {$bucket_range_3++; }
					if (($count_odd_numbers == 1) && ($count_even_numbers == 0) && ($count_low_numbers == 1) &&  ($count_high_numbers == 0)) {$bucket_range_4++; }
					*/
				}
			else if ($maxballs_pb == 2) {
				if (($count_odd_numbers == 0) && ($count_even_numbers == 2) && ($count_low_numbers == 0) &&  ($count_high_numbers == 2)) {$bucket_range_1++; }
				if (($count_odd_numbers == 0) && ($count_even_numbers == 2) && ($count_low_numbers == 1) &&  ($count_high_numbers == 1)) {$bucket_range_2++; }
				if (($count_odd_numbers == 0) && ($count_even_numbers == 2) && ($count_low_numbers == 2) &&  ($count_high_numbers == 0)) {$bucket_range_3++; }
			
				if (($count_odd_numbers == 1) && ($count_even_numbers == 1) && ($count_low_numbers == 0) &&  ($count_high_numbers == 2)) {$bucket_range_4++; }
				if (($count_odd_numbers == 1) && ($count_even_numbers == 1) && ($count_low_numbers == 1) &&  ($count_high_numbers == 1)) {$bucket_range_5++; }
				if (($count_odd_numbers == 1) && ($count_even_numbers == 1) && ($count_low_numbers == 2) &&  ($count_high_numbers == 0)) {$bucket_range_6++; }
				
				if (($count_odd_numbers == 2) && ($count_even_numbers == 0) && ($count_low_numbers == 0) &&  ($count_high_numbers == 2)) {$bucket_range_7++; }
				if (($count_odd_numbers == 2) && ($count_even_numbers == 0) && ($count_low_numbers == 1) &&  ($count_high_numbers == 1)) {$bucket_range_8++; }
				if (($count_odd_numbers == 2) && ($count_even_numbers == 0) && ($count_low_numbers == 2) &&  ($count_high_numbers == 0)) {$bucket_range_9++; }
			}
		}
		else if ($chart_type == 'LMH'){
			if ($maxballs_pb == 1){
					#0-0-1,
					if (($count_low_numbers == 0) && ($count_median_numbers == 0) && ($count_high_numbers == 1)) {$bucket_range_1++; }
					#0-1-0,
					if (($count_low_numbers == 0) && ($count_median_numbers == 1) && ($count_high_numbers == 0)) {$bucket_range_2++; }
					#1-0-0
					if (($count_low_numbers == 1) && ($count_median_numbers == 0) && ($count_high_numbers == 0)) {$bucket_range_3++; }
				}
			}
		else if ($chart_type == 'GRP'){
				# |1-10|-|11-20|
				# hack for now - too many permutations
				$bucket_range_1 = $bucket_range_1 + $count_1_10_numbers;
				$bucket_range_2 = $bucket_range_2 + $count_11_20_numbers;
				#print($bucket_range_2);
			}
		$myrowS1 = $resultS1->fetch_row();
	}  // end loop pb
		
### DEFAUTS COLOR : TO-DO   store in DB!
	if ($chart_type == 'OE'){
		$range_1_color = 'blue06';//'093168';//
		$range_2_color = 'blue51';//'184480';//
		$range_3_color = 'blue33';//'2b5a98';//
	}
	else if ($chart_type == 'LH'){
		$range_1_color = 'green06';//'44734C';
		$range_2_color = 'green51';//'69B075';
		$range_3_color = 'green33';//'79CE88';
	}
	else if ($chart_type == 'OELH'){
		$range_1_color = 'cb1';
		$range_2_color = 'cb2';
		$range_3_color = 'cb3';
		$range_4_color = 'cb4';
		$range_5_color = 'cb4';
		$range_6_color = 'cb4';
		$range_7_color = 'cb4';
		$range_8_color = 'cb4';
		$range_9_color = 'cb4';  // x 2 powerball
	}
	else if ($chart_type == 'LMH') {
			$range_1_color = '44734C';
			$range_2_color = '69B075';
			$range_3_color = '79CE88';
	}
	else if ($chart_type == 'GRP'){
			$range_1_color = '093168';
			$range_2_color = '184480';
			$range_3_color = '2b5a98';
		}


####### CONCATENATE VAL, COLOR
		$bucket_range_str_pb = '';
		$bucket_val_str_pb = '';
		$bucket_color_str_pb = '';
	#RANGE 1
		$range_ceiling = $maxballs_pb;    # important - reset
		$range_1_part_a = 0;  $range_1_part_b = $range_ceiling;
		$bucket_range_str_pb = $range_1_part_a.'-'.$range_1_part_b;
		$bucket_val_str_pb = $bucket_range_1;
		$bucket_color_str_pb = $range_1_color;
	#RANGE 2
		$range_2_part_a = 1; $range_ceiling--;
		$range_2_part_b = $range_ceiling;
		$bucket_range_str_pb = $bucket_range_str_pb.','.$range_2_part_a.'-'.$range_2_part_b;
		$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_2;
		$bucket_color_str_pb = $bucket_color_str_pb.','.$range_2_color;
	
	if (($chart_type == 'OE') || ($chart_type == 'LH')){
	# RANGE 3
		if ($powerball2_active){
			$range_3_part_a = 2; $range_ceiling--;
			$range_3_part_b = $range_ceiling;
			$bucket_range_str_pb = $bucket_range_str_pb.','.$range_3_part_a.'-'.$range_3_part_b;
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_3;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_3_color;
		}
	}
	else if ($chart_type == 'LMH'){
		# RANGE 3
		$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_3;
		$bucket_color_str_pb = $bucket_color_str_pb.','.$range_3_color;
	}
	else if ($chart_type == 'OELH'){
		$bucket_range_str_pb = '1O0E-0L1H,0O1E-0L1H,1O0E-1L0H,0O1E-1L0H';
		$bucket_color_str_pb = 'cb1,cb2,cb3,cb4';	
		# RANGE 3
		$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_3;
		$bucket_color_str_pb = $bucket_color_str_pb.','.$range_3_color;

		# RANGE 4
		$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_4;
		$bucket_color_str_pb = $bucket_color_str_pb.','.$range_4_color;

		if ($powerball2_active){
			$bucket_range_str_pb = '0O2E-0L2H,0O2E-1L1H,0O2E-2L0H,1O1E-0L2H,1O1E-1L1H,1O1E-2L0H,2O0E-0L2H,2O0E-1L1H,2O0E-2L0H';
			$bucket_color_str_pb = 'cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9';	
			# RANGE 5
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_5;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_5_color;
			# RANGE 6
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_6;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_6_color;
			# RANGE 7
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_7;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_7_color;
			# RANGE 8
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_8;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_8_color;
			# RANGE 9
			$bucket_val_str_pb = $bucket_val_str_pb.','.$bucket_range_9;
			$bucket_color_str_pb = $bucket_color_str_pb.','.$range_9_color;
		}
	}
	#override for now
	else if ($chart_type == 'LMH'){
		if ($maxballs_pb == 1){
				$bucket_range_str_pb = '0-0-1,0-1-0,1-0-0';
			}
		else if ($maxballs_pb == 2){
				$bucket_range_str_pb = 'to-do';
			}
	}
	else if ($chart_type == 'GRP'){
		$bucket_range_str_pb = '1-10,11-20';
		$bucket_val_str_pb = $bucket_range_1.','.$bucket_range_2;
		$bucket_color_str_pb = $range_1_color.','.$range_2_color;
	}

} # end if powerball  pb_ind
############### P O W E R B A L L - END #################################################################################

## DO SAME FOR LUCKY   ( L1/S1 + L2/S2)
//if ($lucky_ind)
//{
	#### TO -DO still
//}

########INSERT INTO DB#######

db_insert_entry_odd_even_low_high($db,$lottocode_in,$months_in,$drawdate_in,$date_range_min_adjusted,$bucket_range_str,$bucket_val_str,$bucket_color_str,'',$chart_type);

if ($pb_ind == 1){
	##TO DO   db_insert_entry_hot_cold($db,$lottocode_in,$months_in,$drawdate_in,$strCOMMA_val_pb,$strCOMMA_color_pb,$count_min_max_pb,'powerball',$date_range_min_adjusted);
	db_insert_entry_odd_even_low_high($db,$lottocode_in,$months_in,$drawdate_in,$date_range_min_adjusted,$bucket_range_str_pb,$bucket_val_str_pb,$bucket_color_str_pb,'powerball',$chart_type);
}
else if ($lucky_ind == 1){
	###TO-DO db_insert_entry_hot_cold($db,$lottocode_in,$months_in,$drawdate_in,$strCOMMA_val_lucky,$strCOMMA_color_lucky,$count_min_max_lucky,'lucky',$date_range_min_adjusted);
	db_insert_entry_odd_even_low_high($db,$lottocode_in,$months_in,$drawdate_in,$date_range_min_adjusted,$bucket_range_str_l,$bucket_val_str_l,$bucket_color_str_l,'lucky',$chart_type);
}


# INSERT POWERBALL AS WLL
db_disconnect($db);
print("1");
?>
