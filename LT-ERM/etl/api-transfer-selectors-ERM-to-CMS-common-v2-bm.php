<?php

include_once("include/config-engineroom.php");
include_once("include/db-settings-engineroom.php");
include_once("include/db-connect-engineroom.php");
include_once("include/db-functions-erm.php");

//$transfer_flag = 'default';   //REPLACE for api_transfer_selectors_ERM_to_CMS.php
  $transfer_flag = 'ballsmatched';   //REPLACE for api_transfer_selectors_ballsmatched_ERM_to_CMS.php

//if (isset($_REQUEST['flag'])) {	$transfer_flag = $_REQUEST['flag'];}
//else {
//  print("flag not set.exit");
 // exit;
//}
print("Transfer flag = ".$transfer_flag." ");

$rows_to_process_at_a_time=1;   # default to latest result   # put this in the config - to-do or per lottocode per draw date !
// done - below overrides from Config_Import table

$db = db_connect($hostname, $username, $dbpassword, $databasename);
get_configuration($db);
//$API_TRANSFER_DELAY = '90';
//$API_QUEUE = true;
//$DEV_API_URL = 'http://dev.??????/CMS/api/';
//PROD_API_URL = 'https://www.?????/CMS/api/';
//$API_LIVE_CMS = false;  //?  Queue AND Send.

print("Sleep..".$API_TRANSFER_DELAY." sec ");   # to allow for get_mail_results cron job to complete & transfer_mail_results_all
sleep($API_TRANSFER_DELAY);



##DO DEV FIRST
if (($API_LIVE_CMS_DEV) && (!db_import_selectors_active($db,'dev')))
{
    $flag='dev';
    db_import_selectors_active_set($db,$flag,'1');
    $db_lottonames_count = 0;
    $lottoname_array = array();
    //add to focre sequential processing for now
    
    //$lottoname_array = db_get_lottonames_api($db,$db_lottonames_count);
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);

    $i=0;
    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'];
      $api_active_selectors_transfer_in_progress_dev = $lottoname_array[$i]['api_active_selectors_transfer_in_progress_dev'];
      $api_active_selectors_transfer_in_progress_prod = $lottoname_array[$i]['api_active_selectors_transfer_in_progress_prod'];
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
        # $execute_url($prod_URL,$lottoname);
        # GATEKEEPER - ONLY ALLOW IN ONES THAT HAVE BEEN CODED CORRECTLY
        # $execute_url($prod_URL,"Arizona-The Pick");
        # old 9 aug 2016	execute_url($prod_URL,$lottoname);
        ##for testing
        if (($lottocode == 'ZA-P') ||  
            ($lottocode == 'ZA-PP') ||  
            ($lottocode == 'ZA-L') ||  
            ($lottocode == 'ZA-LP')||  
            ($lottocode == 'ZA-LP2')|| 
            ($lottocode == 'ZA-DL'))
        {
          if (!$api_active_selectors_transfer_in_progress_dev)
          {
            db_settransfer_inprogress($db,$lottocode,'selector','dev');
          ###  changed logic to read latest date and use it in the sql query overriden in function
            $rows_to_process_at_a_time=0;#110;   //1,2,3,6,12,18,24,36,48,60,999 x # of selectors x 2 boards
            $chart_type = 'HC';
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$chart_type,$flag,$transfer_flag);
            $chart_type = 'OE';
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$chart_type,$flag,$transfer_flag);
            $chart_type = 'LH';
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$chart_type,$flag,$transfer_flag);
            $chart_type = 'OELH';
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$chart_type,$flag,$transfer_flag);
            $chart_type = 'LMH';
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$chart_type,$flag,$transfer_flag);
           
            db_settransfer_complete($db,$lottocode,'selector','dev');
          }
        }
    $i++;
    //sleep($PROCESSING_DELAY);
    }
    print("DEV done.");
    db_import_selectors_active_set($db,$flag,'0');

  }

##DO PROD NEXT
//sleep($PROCESSING_DELAY);
if (($API_LIVE_CMS_PROD) && (!db_import_selectors_active($db,'prod')))
{
  $flag='prod';
  db_import_selectors_active_set($db,$flag,'1');
  $db_lottonames_count = 0;
  $lottoname_array = array();
  //$lottoname_array = db_get_lottonames_api($db,$db_lottonames_count);
  $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);

  $i=0;
  while ($i < $db_lottonames_count)
  {
    $lottoname = $lottoname_array[$i]['lottoname'];
    $lottocode = $lottoname_array[$i]['lottocode'] ;
    $api_active_selectors_transfer_in_progress_dev = $lottoname_array[$i]['api_active_selectors_transfer_in_progress_dev'];
    $api_active_selectors_transfer_in_progress_prod = $lottoname_array[$i]['api_active_selectors_transfer_in_progress_prod'];
    ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
      # $execute_url($prod_URL,$lottoname);
      # GATEKEEPER - ONLY ALLOW IN ONES THAT HAVE BEEN CODED CORRECTLY
      # $execute_url($prod_URL,"Arizona-The Pick");
      # old 9 aug 2016	execute_url($prod_URL,$lottoname);
      ##for testing
      if (($lottocode == 'ZA-P') ||  
      ($lottocode == 'ZA-PP') ||  
      ($lottocode == 'ZA-L') ||  
      ($lottocode == 'ZA-LP')||  
      ($lottocode == 'ZA-LP2')|| 
      ($lottocode == 'ZA-DL'))  #    if (($lottocode == 'ZA-L'))
      {
        if (!$api_active_selectors_transfer_in_progress_prod)
          {
            db_settransfer_inprogress($db,$lottocode,'selector','prod');

        ###  changed logic to read latest date and use it in the sql query overriden in function
          $rows_to_process_at_a_time=0;#110;   //1,2,3,6,12,18,24,36,48,60,999 x # of selectors x 2 boards
          $chart_type = 'HC';
          transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$chart_type,$flag,$transfer_flag);
          $chart_type = 'OE';
          transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$chart_type,$flag,$transfer_flag);
          $chart_type = 'LH';
          transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$chart_type,$flag,$transfer_flag);
          $chart_type = 'OELH';
          transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$chart_type,$flag,$transfer_flag);
          $chart_type = 'LMH';
          transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$chart_type,$flag,$transfer_flag);
          
          db_settransfer_complete($db,$lottocode,'selector','prod');
          }
      }
  $i++;
  //sleep($PROCESSING_DELAY);
  }
  print("PROD done.");
  db_import_selectors_active_set($db,$flag,'0');

}

db_disconnect($db);
exit;

//function transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$API_QUEUE,$API_LIVE_CMS,$chart_type)
function transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$API_URL,$chart_type,$flag,$transfer_flag)
{


  /********NEW */
if (($lottocode == 'ZA-L') || ($lottocode == 'ZA-LP') || ($lottocode == 'ZA-LP2') || ($lottocode == 'ZA-DL')){
  $haspowerball = 0;
}
else if (($lottocode == 'ZA-P') || ($lottocode == 'ZA-PP')){
  $haspowerball = 1;
}
$typename = 'selector';
$rows_to_process_at_a_time = db_get_rows_to_process($db,$typename,$haspowerball);
/**************** */



    ##Get lottoname details to build up query to get latest results.
    #$Chart_Table = 'Chart_Selector_Hot_Cold_LottoEntries';    ## TO -DO get config from DB Config_Chart_Selectors
    //$chart_type = 'HC';  now a param

    $Chart_Table =  db_return_chartselector_tablename($db,$chart_type);

    $lotto_detail_array = array();
    $lotto_detail_count = 0;
    $lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode);
    	 $x=0;
    	 $guess_range = 3;
    	 $LottoTable 	 = $lotto_detail_array[$x]['Lotto_Table'];
    	 $ball1_active = $lotto_detail_array[$x]['ball1_active'];
    	 $ball2_active = $lotto_detail_array[$x]['ball2_active'];
    	 $ball3_active = $lotto_detail_array[$x]['ball3_active'];
    	 if ($ball3_active) {$guess_range = 3;}
    	 $ball4_active = $lotto_detail_array[$x]['ball4_active'];
    	 if ($ball4_active) {$guess_range = 4;}
    	 $ball5_active = $lotto_detail_array[$x]['ball5_active'];
    	 if ($ball5_active) {$guess_range = 5;}
    	 $ball6_active = $lotto_detail_array[$x]['ball6_active'];
    	 if ($ball6_active) {$guess_range = 6;}
    	 $ball7_active = $lotto_detail_array[$x]['ball7_active'];
    	 if ($ball7_active) {$guess_range = 7;}
    	 $bonus_active = $lotto_detail_array[$x]['bonus_active'];
    	 $bonus2_active = $lotto_detail_array[$x]['bonus2_active'];
    	 $bonus3_active = $lotto_detail_array[$x]['bonus3_active'];
    	 $bonus4_active = $lotto_detail_array[$x]['bonus4_active'];
    	 $powerball_active = $lotto_detail_array[$x]['powerball_active'];
    	 $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
    	 $lucky1_active = $lotto_detail_array[$x]['lucky1_active'];
    	 $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
    	 $ball_max = $lotto_detail_array[$x]['ball_max'];
    	 $ball_max_pb = $lotto_detail_array[$x]['ball_max_pb'];
    	 $ball_max_lucky = $lotto_detail_array[$x]['ball_max_lucky'];
    	 $scheduled_draws = $lotto_detail_array[$x]['scheduled_draws'];
    $pb_ind = 0;
    if ($powerball_active)
    {
    	$pb_ind = 1;
    }

    $drawdate_in = '';   //initialise
      ### GET LATEST DATE
      if ($flag=='dev') {
        if ($transfer_flag == 'default'){
          $queryS1 = "select drawdate FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_dev = 0 order by drawdate desc LIMIT 1";
        }
        else if ($transfer_flag == 'ballsmatched'){
           $queryS1 = "select drawdate FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_dev = 0 and balls_matched >= 0 order by drawdate desc LIMIT 1";
        }
      }
      else if ($flag=='prod'){
        if ($transfer_flag == 'default'){
          $queryS1 = "select drawdate FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_prod = 0 order by drawdate desc LIMIT 1";
        }
        else if ($transfer_flag == 'ballsmatched'){
           $queryS1 = "select drawdate FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_prod = 0 and balls_matched >= 0 order by drawdate desc LIMIT 1";
         }
        }  
      #print($queryS1);
      if ($resultS1 = $db->query($queryS1)){ $myrowS1 = $resultS1->fetch_row();}
      if ($myrowS1)
      {
       $drawdate_in = $myrowS1[0];
      }
      $resultS1->close();

      ### GET TOTAL  No. of  ROWS TO PROCESS 

      if ($flag=='dev'){
        if ($transfer_flag == 'default'){
          $queryS1 = "select count(*) FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_dev = 0 and drawdate = '".$drawdate_in."' order by drawdate desc LIMIT 1";
        }
       else if ($transfer_flag == 'ballsmatched'){
          $queryS1 = "select count(*) FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_dev = 0 and balls_matched >= 0 and drawdate = '".$drawdate_in."' order by drawdate desc LIMIT 1";
    }
    }
      else if ($flag=='prod'){
        if ($transfer_flag == 'default'){
        $queryS1 = "select count(*) FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_prod = 0 and drawdate = '".$drawdate_in."' order by drawdate desc LIMIT 1";
      }
      else if ($transfer_flag == 'ballsmatched'){
        $queryS1 = "select count(*) FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_prod = 0 and balls_matched >= 0 and drawdate = '".$drawdate_in."' order by drawdate desc LIMIT 1";
      }
      }
        
      #print($queryS1);

      if ($resultS1 = $db->query($queryS1)){ $myrowS1 = $resultS1->fetch_row();}
      if ($myrowS1)
      {
       $total_rows_to_process = $myrowS1[0];
      }
      $resultS1->close();
        

       #####TO DO  CREATE OUTER LOOP TO send until TOTAL reached
       $rows_processed = 0;
       $catch_max_times = 4;        //  expected loop to run only 4 times    OELH has 550 selectors / 154 = 4 times !!!
       $catch_loop_counter = 0;
       while (($rows_processed < $total_rows_to_process)&&($catch_loop_counter < $catch_max_times))
       {
         //TO DO
         $rows_processed = $rows_processed+$rows_to_process_at_a_time; //  default is 154  - so for 550   will loop for 4 times
         $catch_loop_counter++;
      // }  //move down - to do - done

      #initialise
      $ball1_match = 0;
      $ball2_match = 0;
      $ball3_match = 0;
      $ball4_match = 0;
      $ball5_match = 0;
      $ball6_match = 0;
      $ball7_match = 0;
      $powerball_match = 0;
      $powerball2_match = 0;
      $win_division = 0;
      $win_division_plus = 0;
      

      ###########BUILD UP QUERY#################
      $query_part_b1 =  "";			$query_part_b2 =  "";			$query_part_b3 =  ""; $query_part_b4 =  "";
      $query_part_b5 =  "";			$query_part_b6 =  "";			$query_part_b7 =  "";
      $query_part_b8 =  "";			$query_part_b9 =  "";			$query_part_b10 =  "";		$query_part_b11 =  "";
      $query_part_b12 =  "";			$query_part_b13 =  "";			$query_part_b14 =  "";		$query_part_b15 =  "";$query_part_b16 =  "";

      #$query_part_start = "SELECT id, drawdate, ball1, ball2, ball3, ball4";
      $query_part_start = "select lottoentry_id,board,LTcode,drawdate_snapshot_from,drawdate_prediction_on,drawdate";
      $query_part_b1 = ",ball1,ball2,ball3";

      if ($ball4_active)	{$query_part_b2 = ",ball4";}
      if ($ball5_active) 	{$query_part_b3 = ",ball5";}
      if ($ball6_active)	{$query_part_b4 = ",ball6";}
      if ($ball7_active)	{$query_part_b5 = ",ball7";}
      if ($powerball_active)	{$query_part_b6 = ",powerball";}
      if ($powerball2_active)	{$query_part_b7 = ",powerball2";}
      $query_part_b8 = ",ball_color,win_division,win_division_plus";
      $query_part_b9 = ",ball1_match,ball2_match,ball3_match";
      if ($ball4_active)	{$query_part_b10 = ",ball4_match";}
      if ($ball5_active) 	{$query_part_b11 = ",ball5_match";}
      if ($ball6_active)	{$query_part_b12 = ",ball6_match";}
      if ($ball7_active)	{$query_part_b13 = ",ball7_match";}
      if ($bonus_active)	{$query_part_b14 = ",bonusball_match";}
      if ($powerball_active)	{$query_part_b15 = ",powerball_match";}
      if ($powerball2_active)	{$query_part_b16 = ",powerball2_match";}

      $query_part_b17 = ",balls_matched";
  ####    $query_part_b5 =  " ,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,powerball2,ball_color,win_division,win_division_plus,ball1_match,ball2_match,ball3_match,ball4_match,ball5_match,ball6_match,ball7_match,bonusball_match,powerball_match,powerball2_match,balls_matched";

  if ($flag=='dev'){  
  if ($transfer_flag == 'default'){
    $query_part_end = " FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_dev = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;
   }
  else if ($transfer_flag == 'ballsmatched'){
    $query_part_end = " FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_dev = 0 and balls_matched >= 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;
  }
}
  else if ($flag=='prod'){  
    if ($transfer_flag == 'default'){
      $query_part_end = " FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_prod = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;
    }
    else if ($transfer_flag == 'ballsmatched'){
      $query_part_end = " FROM ".$Chart_Table." where lottocode = '".$lottocode."' and transferred_to_CMS_ballsmatched_prod = 0 and balls_matched >= 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;
    }
  }
    
      $queryS1 = $query_part_start.
      $query_part_b1.
      $query_part_b2.
      $query_part_b3.
      $query_part_b4.
      $query_part_b5.
      $query_part_b6.
      $query_part_b7.
      $query_part_b8.
      $query_part_b9.
      $query_part_b10.
      $query_part_b11.
      $query_part_b12.
      $query_part_b13.
      $query_part_b14.
      $query_part_b15.
      $query_part_b16.
      $query_part_b17.
      $query_part_end;
      print($queryS1);
      #return($squeryS1);
      #exit;

       if ($resultS1 = $db->query($queryS1)){ $myrowS1 = $resultS1->fetch_row();}

       $params_payload = '';
       $params_count = 0;

       while ($myrowS1)
       {
         $uri = '';
        $Results_table_id       = $myrowS1[0];
        $board                  = $myrowS1[1];
        $LTcode                 = $myrowS1[2];
        $drawdate_snapshot_from = $myrowS1[3];
        $drawdate_prediction_on = $myrowS1[4];
        $drawdate               = $myrowS1[5];
        $ball1                  = $myrowS1[6];
        $ball2                  = $myrowS1[7];
        $ball3                  = $myrowS1[8];  $index = 8; $index++;
        if ($ball4_active)	{$ball4 = $myrowS1[$index]; $index++;}
        if ($ball5_active) 	{$ball5 = $myrowS1[$index]; $index++;}
        if ($ball6_active)	{$ball6 = $myrowS1[$index]; $index++;}
        if ($ball7_active)	{$ball7 = $myrowS1[$index]; $index++;}
        if ($powerball_active)	{$powerball = $myrowS1[$index]; $index++;}
        if ($powerball2_active)	{$powerball2 = $myrowS1[$index]; $index++;}

        #$query_part_b8 = ",ball_color,win_division,win_division_plus";
        #$query_part_b9 = ",ball1_match,ball2_match,ball3_match";

        $ball_color = $myrowS1[$index]; $index++;
        $win_division = $myrowS1[$index]; $index++;
        $win_division_plus = $myrowS1[$index]; $index++;
        $ball1_match = $myrowS1[$index]; $index++;
        $ball2_match = $myrowS1[$index]; $index++;
        $ball3_match  = $myrowS1[$index]; $index++;
        if ($ball4_active)	{$ball4_match  = $myrowS1[$index]; $index++;}
        if ($ball5_active) 	{$ball5_match  = $myrowS1[$index]; $index++;}
        if ($ball6_active)	{$ball6_match  = $myrowS1[$index]; $index++;}
        if ($ball7_active)	{$ball7_match  = $myrowS1[$index]; $index++;}
        if ($bonus_active)	{$bonusball_match = $myrowS1[$index]; $index++;}
        if ($powerball_active)	{$powerball_match = $myrowS1[$index]; $index++;}
        if ($powerball2_active)	{$powerball2_match = $myrowS1[$index]; $index++;}
        $balls_matched = $myrowS1[$index]; $index++;

        ### GROUP for uri

        $drawdates_array = $drawdate_snapshot_from.','.$drawdate_prediction_on.','.$drawdate;
        $balls_array = $ball1.','.$ball2.','.$ball3;
        if ($ball4_active)	{$balls_array = $balls_array.','.$ball4;}
        if ($ball5_active) 	{$balls_array = $balls_array.','.$ball5;}
        if ($ball6_active)	{$balls_array = $balls_array.','.$ball6;}
        if ($ball7_active)	{$balls_array = $balls_array.','.$ball7;}
        if ($powerball_active)	{$balls_array = $balls_array.','.$powerball;}
        if ($powerball2_active)	{$balls_array = $balls_array.','.$powerball2;}
        $win_division_array =$win_division.','.$win_division_plus;
        $matches_array = $ball1_match.','.$ball2_match.','.$ball3_match;
        if ($ball4_active)	{$matches_array = $matches_array.','.$ball4_match;}
        if ($ball5_active) 	{$matches_array = $matches_array.','.$ball5_match;}
        if ($ball6_active)	{$matches_array = $matches_array.','.$ball6_match;}
        if ($ball7_active)	{$matches_array = $matches_array.','.$ball7_match;}
        if ($bonus_active)	{$matches_array = $matches_array.','.$bonusball_match;}
        if ($powerball_active)	{$matches_array = $matches_array.','.$powerball_match;}
        if ($powerball2_active)	{$matches_array = $matches_array.','.$powerball2_match;}
        $matches_array = $matches_array.','.$balls_matched;

        #### strip LTCode as it has a / in and replace with :
        #### reverse on other side - importselectordata
        $LTcode_array = explode('/',$LTcode);
        $LTcode_array_1	 = $LTcode_array[0];
        $LTcode_array_2	 = $LTcode_array[1];
        $LTcode_new = $LTcode_array_1.':'.$LTcode_array_2;

        if ($powerball_active)
        {
          #### strip color as it has a | in and replace with :
          #### reverse on other side - importselectordata
          $color_array = explode('|',$ball_color);
          $color_array_1	 = $color_array[0];
          $color_array_2	 = $color_array[1];
          $ball_color_new = $color_array_1.':'.$color_array_2;
      }
      else {
          $ball_color_new = $ball_color;
      }

        //$uri = $lottocode.'/'.$board.'/'.$LTcode_new.'/'.$drawdates_array.'/'.$balls_array.'/'.$ball_color_new.'/'.$win_division_array.'/'.$matches_array;
        #print($uri);

        #exit;
        $update_attrib = 'false';
        if ($transfer_flag == 'default') { $update_attrib = 'false';}
        else if ($transfer_flag == 'ballsmatched') { $update_attrib = 'true';}
        
        //force for hack
      //  $uri = $params;
    // print("..sending..");

		   ########################
		   ### INSERT INTO DB/ API
		   ######################

			$found_lotto = 0;
     // $found_lotto = api_receive_selectordata($db,$lottocode,$uri,$API_URL,$chart_type,$flag);

      print("..json construct..");

      $params = '';
        $params = $params.'{';
        $params = $params.'"charttype": "'.$chart_type.'",';
        $params = $params.'"lottocode": "'.$lottocode.'",';
        $params = $params.'"board": "'.$board.'",';
        $params = $params.'"ltcode": "'.$LTcode_new.'",';
        $params = $params.'"dates": "'.$drawdates_array.'",';
        $params = $params.'"balls": "'.$balls_array.'",';
        $params = $params.'"color": "'.$ball_color_new.'",';
        $params = $params.'"windiv": "'.$win_division_array.'",';
        $params = $params.'"matches": "'.$matches_array.'",';
        $params = $params.'"update": "'.$update_attrib.'"';
        $params = $params.'}';

      if ($params_count == 0) {$params_payload = $params_payload.$params;}
      else {$params_payload = $params_payload.','.$params;}
      $params_count++;




		# update table to reflect api transferred.
		  //if ($found_lotto)
		  //{
		  ####TOT DO
      if ($transfer_flag == 'default'){ db_update_SelectorsAPI($db,$Chart_Table,$Results_table_id,$flag);}
      else if ($transfer_flag == 'ballsmatched'){ db_update_SelectorsAPI_ballsmatched($db,$Chart_Table,$Results_table_id,$flag);}
		  //}
		  //else
		  //{
			//  print("api send error- ");print($Results_table_id);
      //}
      //sleep(1); move

       $myrowS1 = $resultS1->fetch_row();
    }
  /*  else {
      print("...no results for ".$lottocode."...exit.");
      #$resultS1->close();
      return false;
    }
    */
    $resultS1->close();
    ########################
		   ### INSERT INTO DB/ API
       ######################
     //  print("..payload_full..".$params_payload);
     $found_lotto = 0;
     if ($params_count > 0) { 
       print("..sending..");
       $found_lotto = api_receive_selectordata($db,$lottocode,$params_payload,$params_count,$API_URL,$flag);
     }
     else { print("..nothing to send.");}

    }   //outer loop  - max 4 times 
    sleep(1);

}   #functionv end



//function api_receive_selectordata($db,$lottocode,$uri,$API_URL,$chart_type,$flag)
function api_receive_selectordata($db,$lottocode,$params_payload,$params_count,$API_URL,$flag)
{

  #http://dev.lottotarget.co.za/wp-json/importselectordata/v1/HC/ZA-L/2/999m/H-C-s/2017-09-02,2017-09-06,2017-09-06/25,36,10,23,2,48/blue,blue,blue,red,red,red/,/,,,,,,/'

 /* if ($API_QUEUE)
    {
      $url_prefix = $PROD_API_URL;
      #$url= $url_prefix.'api_receive_results.php?results='.$params;
      # wordpress API
      //$url= $url_prefix.'importselectordata/HC/'.$uri;
      $url= $url_prefix.'importselectordata/'.$chart_type.'/'.$uri;
      print("API_QUEUE:");
      print($url);
      db_insert_api_queue($db,$lottocode,$url);
      $force_true = 1;
      return $force_true;
    }
    else if ($API_LIVE_CMS)
    {
      */

      $url_prefix = $API_URL;
      #$url= $url_prefix.'api_receive_results.php?results='.$params;
      # wordpress API
     // $url= $url_prefix.'importselectordata/HC/'.$uri;
     //$uri = $lottocode.'/'.$board.'/'.$LTcode_new.'/'.$drawdates_array.'/'.$balls_array.'/'.$ball_color_new.'/'.$win_division_array.'/'.$matches_array;
     //$url= $url_prefix.'importselectordata/'.$chart_type.'/'.$uri;
     $url= $url_prefix.'importselectordata/v2';
     /*$params = '';
     $params = $params.'{';
     $params = $params.'"charttype": "'.$chart_type.'",';
     $params = $params.'"lottocode": "'.$lottocode.'",';
     $params = $params.'"board": "'.$board.'",';
     $params = $params.'"ltcode": "'.$LTcode_new.'",';
     $params = $params.'"dates": "'.$drawdates_array.'",';
     $params = $params.'"balls": "'.$balls_array.'",';
     $params = $params.'"color": "'.$ball_color_new.'",';
     $params = $params.'"windiv": "'.$win_division_array.'",';
     $params = $params.'"matches": "'.$matches_array.'"';
     $params = $params.'}';
     */
    $params = $uri;   //hack

      $params_outer = '{';
      $params_outer = $params_outer.'"count":"'.$params_count.'",';
      $params_outer = $params_outer.'"selectordata":[';
      $params_outer = $params_outer.$params_payload;
      $params_outer = $params_outer.']';
      $params_outer = $params_outer.'}';

      
      //$id = db_insert_api_queue($db,$lottocode,$url,$flag);
      //$id = db_insert_api_queueV2($db,$lottocode,$url,$flag,$params);
      $id = db_insert_api_queueV2($db,$lottocode,$url,$flag,$params_outer);
      
      #print("API_LIVE_CMS");
      #$contents = file_get_contents($url);
      #$return_code = str_get_html($contents);   # should be 1 or 0 !

      $ch = curl_init($url);
      $post = '{}';
     
      $post = $params_outer;
        
      $authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.e30.u4sMpRTjXGVdRo1KkOEeY52vfjjyak-Laj1bq1XQWTw";
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$post);    
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      $parsedBody = json_decode($result, true);
      $return_code = $parsedBody['result'];
      $error = $parsedBody['error'];
      if ($return_code == '1'){$return_code= '1';}
      else ($return_code = '0');
      print("<");print($return_code);print(">");
      db_update_api_queue($db,$id,$return_code);
      return($return_code);
}
#db_disconnect($db);
?>
