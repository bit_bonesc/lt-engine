<?php
## THIS IS A COPY OF api_transfer_results_ERM_to_CMS.php  -
## 2 changes
## 1. rows_to_process_at_a_time
## 2. 
include_once("include/config-engineroom.php");
include_once("include/db-settings-engineroom.php");
include_once("include/db-connect-engineroom.php");
include_once("include/db-functions-erm.php");
$rows_to_process_at_a_time=1;   # default to latest result   # put this in the config - to-do
if (isset($_REQUEST['rows']))
{
	$rows_to_process_at_a_time = $_REQUEST['rows'];
}
print("Rows to process..".$rows_to_process_at_a_time); 
$db = db_connect($hostname, $username, $dbpassword, $databasename);
get_configuration($db);

print("Sleep..".$API_TRANSFER_DELAY." sec ");   # to allow for get_mail_results cron job to complete & transfer_mail_results_all
sleep($API_TRANSFER_DELAY);

if ($API_QUEUE)     //decom 
  {
    // 28 jan 2019 - seperate prod and dev .$PROD_API_URL = $DEV_API_URL;
  }

##DO DEV FIRST
if ($API_LIVE_CMS_DEV)
{
  $flag='dev';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName
    $i=0;
    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$flag);
    $i++;
    sleep($PROCESSING_DELAY);
    }
    print("DEV done.");
}

##DO PROD NEXT
sleep($PROCESSING_DELAY);
if ($API_LIVE_CMS_PROD){
    $flag='prod';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName
    $i=0;
    while ($i < $db_lottonames_count) {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$flag);
    $i++;
    sleep($PROCESSING_DELAY);
    }
    print("PROD done.");
}
  db_disconnect($db);
exit;

function transfer_results_from_Tables($db,$lottocode_in,$rows_to_process_at_a_time,$API_URL,$flag)
{
    $lotto_detail_array = array();
    $lotto_detail_count = 0;
    $lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode_in);
    $x=0;  # should always be only 1 !!   - update db to LIMT 1...todo
      $lottoCode = $lotto_detail_array[$x]['lottoCode'];
      $lottonamedisplay = $lotto_detail_array[$x]['lottonamedisplay'];
      $lottocountrycode = $lotto_detail_array[$x]['lottocountrycode'];
      $Lotto_Table = $lotto_detail_array[$x]['Lotto_Table'];
      $nextdrawdate = $lotto_detail_array[$x]['drawdate'];  //force firt 10 char
      $nextdrawdate_diplay = $nextdrawdate;
      $ball1_active = $lotto_detail_array[$x]['ball1_active'];
      $ball2_active = $lotto_detail_array[$x]['ball2_active'];
      $ball3_active = $lotto_detail_array[$x]['ball3_active'];
      $ball4_active = $lotto_detail_array[$x]['ball4_active'];
      $ball5_active = $lotto_detail_array[$x]['ball5_active'];
      $ball6_active = $lotto_detail_array[$x]['ball6_active'];
      $ball7_active = $lotto_detail_array[$x]['ball7_active'];
      $bonus_active = $lotto_detail_array[$x]['bonus_active'];
      $powerball_active = $lotto_detail_array[$x]['powerball_active'];
      $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
      $lucky1_active =  $lotto_detail_array[$x]['lucky1_active'];
      $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
      $LottoName =  $lotto_detail_array[$x]['lottoname'];
      print("LottoName:");print($LottoName);
      $query_part_b5 =  "";			$query_part_b6 =  "";			$query_part_b7 =  "";
      $query_part_b8 =  "";			$query_part_b9 =  "";			$query_part_b10 =  "";		$query_part_b11 =  "";
      $query_part_start = "SELECT id, drawdate, ball1, ball2, ball3, ball4";
      if ($ball5_active) {$query_part_b5 = ",ball5"; }
      if ($ball6_active) {$query_part_b6 = ",ball6"; }
      if ($ball7_active) {$query_part_b7 = ",ball7"; }
      if ($bonus_active) {$query_part_b8 = ",bonusball"; }
      if ($powerball_active) {$query_part_b9 = ",powerball"; }
      if ($flag == 'dev'){
      $query_part_end = ",nextestimatedjackpot FROM ".$Lotto_Table." where transferred_to_CMS_dev = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;}
      else if ($flag=='prod'){
        $query_part_end = ",nextestimatedjackpot FROM ".$Lotto_Table." where transferred_to_CMS_prod = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;}
      $queryS1 = $query_part_start.$query_part_b5.$query_part_b6.$query_part_b7.$query_part_b8.$query_part_b9.$query_part_b10.$query_part_b11.$query_part_end;
      print($queryS1);
       if ($resultS1 = $db->query($queryS1)){ $myrowS1 = $resultS1->fetch_row();}
       while ($myrowS1)
       {
       //if ($myrowS1){
          $Results_table_id = $myrowS1[0];
          $drawdate = substr($myrowS1[1],0,10);
        $index=2;
        $ball1 = $myrowS1[$index];$index=$index+1; #1
        $ball2 = $myrowS1[$index];$index=$index+1; #2
        $ball3 = $myrowS1[$index];$index=$index+1; #3
        $ball4 = $myrowS1[$index];$index=$index+1; #4
        if ($ball5_active){	$ball5 = $myrowS1[$index]; $index=$index+1;	}
        if ($ball6_active){	$ball6 = $myrowS1[$index];	$index=$index+1;}
        if ($ball7_active){	$ball7 = $myrowS1[$index];$index=$index+1;}
        else if ($bonus_active)	{	$bonus = $myrowS1[$index]; 	$index=$index+1;}
        if ($powerball_active)	{	$powerball = $myrowS1[$index];	$index=$index+1;}
          $jackpot = $myrowS1[$index];$index=$index+1; #1
     //}
    // else {
    //   print("...no results for ".$lottoCode."...exit.");
    //   return false;
    // }
    // $resultS1->close();
     print("..sending..");
		
			 if (($lottoCode == 'ZA-L') || ($lottoCode == 'ZA-LP')|| ($lottoCode == 'ZA-LP2') || ($lottoCode == 'UK-L'))   #"South Africa-Lotto"   # 6 balls + bonusball
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5.'-'.$ball6.'|'.$bonus;
			}
			else if (($lottoCode ==  'ZA-P') || ($lottoCode == 'ZA-PP')|| ($lottoCode == 'UK-T'))# "South Africa-Powerball"  # 5 balls + powerball
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5.'|'.$powerball;
      }
      else if ($lottoCode ==  'ZA-DL') # "South Africa-DailyLotto"  # 5 balls 
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5;
      }
    	$found_lotto = 0;
      $found_lotto = api_receive_results($db,$lottoCode,$drawdate,$Results,$jackpot,$API_URL,$flag);
		  if ($found_lotto) {
        db_update_ResultsAPI($db,$Lotto_Table,$Results_table_id,$flag);
		  }
		  else {
			  print("api send error. ");
      }
      
      $myrowS1 = $resultS1->fetch_row();
    }
  /*  else {
      print("...no results for ".$lottocode."...exit.");
      #$resultS1->close();
      return false;
    }
    */
    $resultS1->close();

}   

function api_receive_results($db,$lottocode,$drawdate,$results,$jackpot,$API_URL,$flag)
{
  $results_prepared = $results;
  $results_split = explode('|',$results_prepared);
  $results_a = $results_split[0];
  if ($results_split[1]) {$results_b = $results_split[1];} else {$results_b = 'x';}
  $url = 'undefined url';
      $url_prefix = $API_URL;
          $url= $url_prefix.'importresultonly/';
          $params = '';
          $params = $params.'{';
          $params = $params.'"lottocode": "'.$lottocode.'",';
          $params = $params.'"drawdate": "'.$drawdate.'",';
          $params = $params.'"results_a": "'.$results_a.'",';
          $params = $params.'"results_b": "'.$results_b.'",';
          $params = $params.'"jackpot": "'.$jackpot.'"';
          $params = $params.'}';
       $id = db_insert_api_queueV2($db,$lottocode,$url,$flag,$params);

      $ch = curl_init($url);
      $post = '{}';
      $post = $params;
        
      $authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9";
      $authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.e30.u4sMpRTjXGVdRo1KkOEeY52vfjjyak-Laj1bq1XQWTw";
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$post);    
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);


      $parsedBody = json_decode($result, true);
      $return_code = $parsedBody['result'];
      $error = $parsedBody['error'];
      if ($return_code == '1'){$return_code= '1';}
      else {
        $return_code = '0';
        $return_code = '1'; //force for now !!!  need to handle resonse 'insert failed: Duplicate entry '2019-03-13' for key 'drawdate' 1"
      };
      print("<");print($return_code);print(">");
      db_update_api_queue($db,$id,$return_code);
      return($return_code);
}

?>
