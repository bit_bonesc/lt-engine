<?php 
function ip()
{ 
if(isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP']) return $_SERVER['HTTP_CLIENT_IP'];
if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) return $_SERVER['HTTP_X_FORWARDED_FOR']; 
if(isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED']) return $_SERVER['HTTP_X_FORWARDED']; 
if(isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR']) return $_SERVER['HTTP_FORWARDED_FOR']; 
if(isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED']) return $_SERVER['HTTP_FORWARDED']; 
if(isset($_SERVER['HTTP_X_COMING_FROM']) && $_SERVER['HTTP_X_COMING_FROM']) return $_SERVER['HTTP_X_COMING_FROM']; 
if(isset($_SERVER['HTTP_COMING_FROM']) && $_SERVER['HTTP_COMING_FROM']) return $_SERVER['HTTP_COMING_FROM'];
if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR']) return $_SERVER['REMOTE_ADDR']; 
return ''; 
}



function Random_GetLetterAlphabet()
{
	$COL = '';
	$color = rand(1,26);
	#if ($color == 1) { $COL = 'A';}
	if ($color == 1) { $COL = 'B';}
	if ($color == 2) { $COL = 'B';}
	if ($color == 3) { $COL = 'C';}
	if ($color == 4) { $COL = 'D';}
	#if ($color == 5) { $COL = 'E';}
	if ($color == 5) { $COL = 'F';}
	if ($color == 6) { $COL = 'F';}
	if ($color == 7) { $COL = 'G';}
	if ($color == 8) { $COL = 'H';}
	#if ($color == 9) { $COL = 'I';}
	if ($color == 9) { $COL = 'J';}
	if ($color == 10) { $COL = 'J';}
	if ($color == 11) { $COL = 'K';}
	if ($color == 12) { $COL = 'L';}
	if ($color == 13) { $COL = 'M';}
	if ($color == 14) { $COL = 'N';}
	#if ($color == 15) { $COL = 'O';}
	if ($color == 15) { $COL = 'P';}
	if ($color == 16) { $COL = 'P';}
	if ($color == 17) { $COL = 'Q';}
	if ($color == 18) { $COL = 'R';}
	if ($color == 19) { $COL = 'S';}
	if ($color == 20) { $COL = 'T';}
	#if ($color == 21) { $COL = 'U';}
	if ($color == 21) { $COL = 'V';}
	if ($color == 22) { $COL = 'V';}
	if ($color == 23) { $COL = 'W';}
	if ($color == 24) { $COL = 'X';}
	if ($color == 25) { $COL = 'Y';}
	if ($color == 26) { $COL = 'Z';}
return($COL);
}

function RandomGenerateUKMillionareRaffle()
{
	$result = '';
	//genrate random number for slot #1	
	$slot_n1 = Random_GetLetterAlphabet();
	$slot_n2 = Random_GetLetterAlphabet();
	$slot_n3 = Random_GetLetterAlphabet();
	$slot_n4 = rand(1,9);	
	$slot_n5 = rand(1,9);
	$slot_n6 = rand(1,9);
	$slot_n7 = rand(1,9);
	$slot_n8 = rand(1,9);
	$slot_n9 = rand(1,9);
	
	$result = $slot_n1.$slot_n2.$slot_n3.$slot_n4.$slot_n5.$slot_n6.$slot_n7.$slot_n8.$slot_n9;
	
	return($result);

}


function RandomGenerateMillionareRaffle()
{
	$result = '';
	$color = rand(1,12);
	if ($color == 1) { $COL = 'AQUA';}
	if ($color == 2) { $COL = 'BLUE';}
	if ($color == 3) { $COL = 'GOLD';}
	if ($color == 4) { $COL = 'GREY';}
	if ($color == 5) { $COL = 'JADE';}
	if ($color == 6) { $COL = 'LIME';}
	if ($color == 7) { $COL = 'NAVY';}
	if ($color == 8) { $COL = 'PINK';}
	if ($color == 9) { $COL = 'PLUM';}
	if ($color == 10) { $COL = 'ROSE';}
	if ($color == 11) { $COL = 'RUBY';}
	if ($color == 12) { $COL = 'TEAL';}
	
	
	//genrate random number for slot #1
	
	$slot_n1 = rand(1,9);
	$slot_n2 = rand(1,9);
	$slot_n3 = rand(1,9);
	$slot_n4 = rand(1,9);
	
	$slot_n5 = rand(1,9);
	$slot_n6 = rand(1,9);
	$slot_n7 = rand(1,9);
	$slot_n8 = rand(1,9);
	
	$result = $COL.' '.$slot_n1.$slot_n2.$slot_n3.$slot_n4.' '.$slot_n5.$slot_n6.$slot_n7.$slot_n8;
	
	return($result);
	
	//generate random number for slot #2
	
}






function formatMoney($number, $fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}



function scale_ball($ball)
{
	$modified_ball = $ball;
	if ($ball < 10)
	{
		#pad number on either side with spaces
		$modified_ball = "&nbsp;&nbsp;".$ball."&nbsp;&nbsp;";
	    $modified_ball = "0".$ball;
		#$modified_ball = "0".$ball;
	}
	return $modified_ball;
}


function get_ip_address()
{
$header_checks = array(
'HTTP_CLIENT_IP',
'HTTP_PRAGMA',
'HTTP_XONNECTION',
'HTTP_CACHE_INFO',
'HTTP_XPROXY',
'HTTP_PROXY',
'HTTP_PROXY_CONNECTION',
'HTTP_VIA',
'HTTP_X_COMING_FROM',
'HTTP_COMING_FROM',
'HTTP_X_FORWARDED_FOR',
'HTTP_X_FORWARDED',
'HTTP_X_CLUSTER_CLIENT_IP',
'HTTP_FORWARDED_FOR',
'HTTP_FORWARDED',
'ZHTTP_CACHE_CONTROL',
'REMOTE_ADDR'
);

foreach ($header_checks as $key)
if (array_key_exists($key, $_SERVER) === true)
foreach (explode(',', $_SERVER[$key]) as $ip)
{
$ip = trim($ip);
if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
return $ip;
}
}



function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function getIP()
{
$ipaddress = $_SERVER["REMOTE_ADDR"];
return $ipaddress;
}



function get_url_contents($url)
{
$crl = curl_init();
$timeout = 5;
$useragent = "Googlebot/2.1 ( http://www.googlebot.com/bot.html)";
curl_setopt ($crl, CURLOPT_USERAGENT, $useragent);
curl_setopt ($crl, CURLOPT_URL,$url);
curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
$ret = curl_exec($crl);
curl_close($crl);
return $ret;
}




function is_duplicates($b1,$b2,$b3,$b4,$b5,$b6,$pb)
{
$dup = false;


if  (($b1 == $b2) or ($b1 == $b3) or ($b1 == $b4) or ($b1 == $b5) or 
                     ($b2 == $b3) or ($b2 == $b4) or ($b2 == $b5) or
					  	   	         ($b3 == $b4) or ($b3 == $b5) or
											         ($b4 == $b5))
	{
		$dup = true;
	}
	
	
	if ((($b1 == $b6) or ($b2 == $b6)  or ($b3 == $b6) or ($b4 == $b6) or ($b5 == $b6)) && (!$pb))
	{
		$dup = true;
	}
	
	return $dup;
}

function check_win_results_uk_euromillions_New($rb1,$rb2,$rb3,$rb4,$rb5,$rl1,$rl2,$cb1,$cb2,$cb3,$cb4,$cb5,$cl1,$cl2)
{
	$ball1_match = 0;
	$ball2_match = 0;
	$ball3_match = 0;
	$ball4_match = 0;
	$ball5_match = 0;
	$lucky1_match = 0;
	$lucky2_match = 0;
	$win_div = 0;
	$count = 0;
	$count_bonus = 0;
	$win_div = 0;
	
	$count = 0;
	$count_lucky = 0;
	$lucky1_match = 0;
	$lucky2_match = 0;
	
	
	if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1; $ball1_match = 1;}
	if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1; $ball2_match = 1;}
	if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1; $ball3_match = 1;}
	if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1; $ball4_match = 1;}
	if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1; $ball5_match = 1;}
	
	if (($cl1 == $rl1) or ($cl1 == $rl2)) { $count_lucky = $count_lucky + 1; $lucky1_match = 1;}
	if (($cl2 == $rl1) or ($cl2 == $rl2)) { $count_lucky = $count_lucky + 1; $lucky2_match = 1;}
	
	
	if (($count == 1) && ($count_lucky == 2))		{ $win_div = 11;}
	else if (($count == 2) &&  ($count_lucky == 2))	{ $win_div = 8;}
	else if (($count == 2) &&  ($count_lucky == 1))	{ $win_div = 12;}
	else if (($count == 2) )						{ $win_div = 13;}
	else if (($count == 3) && ($count_lucky == 2))	{ $win_div = 7;}
	else if (($count == 3) && ($count_lucky == 1))	{ $win_div = 9;}
	else if ($count == 3)							{ $win_div = 10;} 
	else if (($count == 4) && ($count_lucky == 2)) 	{ $win_div = 4;}
	else if (($count == 4) && ($count_lucky == 1)) 	{ $win_div = 5;}
	else if ($count == 4)  							{ $win_div = 6;}
	else if (($count == 5) && ($count_lucky == 2)) 	{ $win_div = 1;}
	else if (($count == 5)  && ($count_lucky == 1))	{ $win_div = 2;}
	else if ($count == 5) 							{ $win_div = 3;}
	else 											{ $win_div = 0;}

	

return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$lucky1_match.'|'.$lucky2_match.'|';
}


function check_win_results_uk_lotto($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-----------------------
#Div 1 = Six Correct Numbers
#Div 2 = Five Correct Numbers + Bonus Ball
#Div 3 = Five Correct Numbers
#Div 4 = Four Correct Numbers 
#Div 5 = Three Correct Numbers 
#Div 6 = Two Correct Numbers
#----------------------------------
$win_div = 0;
$count = 0;
$count_bonus = 0;


$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$ball6_match = 0;


if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1; $ball5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1; $ball6_match = 1;}

if ($cb1 == $rbb) { $count_bonus =  1;}
else if ($cb2 == $rbb) { $count_bonus =  1;}  # TO-Do : add check that no duplicates if Cb1 != cb2 or cb1 != cb2 ...etc. e.g. if all 1's then calc incorrectly
else if ($cb3 == $rbb) { $count_bonus =  1;}
else if ($cb4 == $rbb) { $count_bonus =  1;}
else if ($cb5 == $rbb) { $count_bonus =  1;}
else if ($cb6 == $rbb) { $count_bonus =  1;}

if ($count == 2)							{ $win_div = 6;}
else if ($count == 3)						{ $win_div = 5;} 
else if ($count == 4) 						{ $win_div = 4;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

# added 19 june 2015
#update db


return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$ball6_match.'|';
}



function check_win_results_sa_lotto($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-----------------------
#Div 1 = Six Correct Numbers
#Div 2 = Five Correct Numbers + Bonus Ball
#Div 3 = Five Correct Numbers
#Div 4 = Four Correct Numbers + Bonus Ball
#Div 5 = Four Correct Numbers 
#Div 6 = Three Correct Numbers + Bonus Ball
#Div 7 = Three Correct Numbers 
#----------------------------------
$win_div = 0;
$count = 0;
$count_bonus = 0;


$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$ball6_match = 0;


if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1; $ball5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1; $ball6_match = 1;}

	 if ($cb1 == $rbb) { $count_bonus =  1;}
else if ($cb2 == $rbb) { $count_bonus =  1;}  # TO-Do : add check that no duplicates if Cb1 != cb2 or cb1 != cb2 ...etc. e.g. if all 1's then calc incorrectly
else if ($cb3 == $rbb) { $count_bonus =  1;}
else if ($cb4 == $rbb) { $count_bonus =  1;}
else if ($cb5 == $rbb) { $count_bonus =  1;}
else if ($cb6 == $rbb) { $count_bonus =  1;}

if (($count == 3) && ($count_bonus))		{ $win_div = 6;}
else if ($count == 3)						{ $win_div = 7;} 
else if (($count == 4) && ($count_bonus)) 	{ $win_div = 4;}
else if ($count == 4)  						{ $win_div = 5;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

# added 19 june 2015
#update db


return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$ball6_match.'|';
}


function check_win_results_sa_lotto_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-----------------------
#Div 1 = Six Correct Numbers
#Div 2 = Five Correct Numbers + Bonus Ball
#Div 3 = Five Correct Numbers
#Div 4 = Four Correct Numbers + Bonus Ball
#Div 5 = Four Correct Numbers 
#Div 6 = Three Correct Numbers + Bonus Ball
#Div 7 = Three Correct Numbers 
#----------------------------------
$win_div = 0;
$count = 0;
$count_bonus = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rb6_match = 0;$rbb_match=0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1; $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1; $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1; $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1; $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1; $cb5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1; $cb6_match = 1;}



if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5) or ($rb1 == $cb6)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5) or ($rb2 == $cb6)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5) or ($rb3 == $cb6)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5) or ($rb4 == $cb6)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5) or ($rb5 == $cb6)) { $rb5_match = 1;}
if (($rb6 == $cb1) or ($rb6 == $cb2) or ($rb6 == $cb3) or ($rb6 == $cb4) or ($rb6 == $cb5) or ($rb6 == $cb6)) { $rb6_match = 1;}



	 if ($cb1 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb2 == $rbb) { $count_bonus =  1; $rbb_match = 1;}  # TO-Do : add check that no duplicates if Cb1 != cb2 or cb1 != cb2 ...etc. e.g. if all 1's then calc incorrectly
else if ($cb3 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb4 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb5 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb6 == $rbb) { $count_bonus =  1; $rbb_match = 1;}

if (($count == 3) && ($count_bonus))		{ $win_div = 6;}
else if ($count == 3)						{ $win_div = 7;} 
else if (($count == 4) && ($count_bonus)) 	{ $win_div = 4;}
else if ($count == 4)  						{ $win_div = 5;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

#build up return result, windiv, cb_match, rb_match
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$cb6_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rb6_match.",".$rbb_match;
return $result;
}


function check_win_results_sa_lotto_plus($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
	#$rb1 - winning result ball 1
	#$rb2 - winning result ball 2
	#$rb3 - winning result ball 3
	#$rb4 - winning result ball 4
	#$rb5 - winning result ball 5
	#$rb6 - winning result ball 6
	#$rbb - winning result bonus ball
	
	#$cb1 - client selected ball 1
	#$cb2 - client selected ball 2
	#$cb3 - client selected ball 3
	#$cb4 - client selected ball 4
	#$cb5 - client selected ball 5
	#$cb6 - client selected ball 6
	
	#------RULES-----------------------
	#Div 1 = Six Correct Numbers
	#Div 2 = Five Correct Numbers + Bonus Ball
	#Div 3 = Five Correct Numbers
	#Div 4 = Four Correct Numbers + Bonus Ball
	#Div 5 = Four Correct Numbers 
	#Div 6 = Three Correct Numbers + Bonus Ball
	#Div 7 = Three Correct Numbers 
	#----------------------------------
$win_div = 0;
$count = 0;
$count_bonus = 0;
$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$ball6_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) ) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) ) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) ) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) ) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) ) { $count = $count + 1; $ball5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) ) { $count = $count + 1; $ball6_match = 1;}



	 if ($cb1 == $rbb) { $count_bonus =  1;}
else if ($cb2 == $rbb) { $count_bonus =  1;}
else if ($cb3 == $rbb) { $count_bonus =  1;}
else if ($cb4 == $rbb) { $count_bonus =  1;}
else if ($cb5 == $rbb) { $count_bonus =  1;}
else if ($cb6 == $rbb) { $count_bonus =  1;}

if (($count == 3) && ($count_bonus))		{ $win_div = 6;}
else if ($count == 3)						{ $win_div = 7;} 
else if (($count == 4) && ($count_bonus)) 	{ $win_div = 4;}
else if ($count == 4)  						{ $win_div = 5;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

#return $win_div;
return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$ball6_match.'|';
}



function check_win_results_uk_thuderball_New($rb1,$rb2,$rb3,$rb4,$rb5,$rpb,$cb1,$cb2,$cb3,$cb4,$cb5,$cpp)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rbb - winning result power ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cpp - client selected ball 6

#------RULES-----------------------
#Div 1 = 5 Correct Numbers + PowerBall 
#Div 2 = 5 Correct Numbers
#Div 3 = 4 Correct Numbers + PowerBall
#Div 4 = 4 Correct Numbers 
#Div 5 = 3 Correct Numbers + Powerball
#Div 6 = 3 Correct Numbers 
#Div 7 = 2 Correct Numbers + Powerball
#Div 8 = 1 Correct Number + Powerball
#Div 9 = 1 Powerball
#----------------------------------

$win_div = 0;
$count = 0;
$count_power = 0;
$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$powerball_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1; $ball5_match = 1;}

if ($cpp == $rpb) { $count_power =  1; $powerball_match = 1;}

if (($count == 0) && ($count_power))	{ $win_div = 9;}
else if (($count == 1) && ($count_power))	{ $win_div = 8;}
else if (($count == 2) && ($count_power))	{ $win_div = 7;}
else if (($count == 3) && ($count_power))	{ $win_div = 5;}
else if ($count == 3)						{ $win_div = 6;} 
else if (($count == 4) && ($count_power)) 	{ $win_div = 3;}
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_power)) 	{ $win_div = 1;}
else if ($count == 5)  						{ $win_div = 2;}
else 				  						{ $win_div = 0;}

#return $win_div;
return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$powerball_match.'|';
}



function check_win_results_sa_powerball_New($rb1,$rb2,$rb3,$rb4,$rb5,$rpb,$cb1,$cb2,$cb3,$cb4,$cb5,$cpp)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rbb - winning result power ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cpp - client selected ball 6

#------RULES-----------------------
#Div 1 = 5 Correct Numbers + PowerBall 
#Div 2 = 5 Correct Numbers
#Div 3 = 4 Correct Numbers + PowerBall
#Div 4 = 4 Correct Numbers 
#Div 5 = 3 Correct Numbers + Powerball
#Div 6 = 3 Correct Numbers 
#Div 7 = 2 Correct Numbers + Powerball
#Div 8 = 1 Correct Number + Powerball
#Div 9 = 1 Powerball
#----------------------------------

$win_div = 0;
$count = 0;
$count_power = 0;
$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$powerball_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1; $ball5_match = 1;}

if ($cpp == $rpb) { $count_power =  1; $powerball_match = 1;}

if (($count == 0) && ($count_power))	{ $win_div = 9;}
else if (($count == 1) && ($count_power))	{ $win_div = 8;}
else if (($count == 2) && ($count_power))	{ $win_div = 7;}
else if (($count == 3) && ($count_power))	{ $win_div = 5;}
else if ($count == 3)						{ $win_div = 6;} 
else if (($count == 4) && ($count_power)) 	{ $win_div = 3;}
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_power)) 	{ $win_div = 1;}
else if ($count == 5)  						{ $win_div = 2;}
else 				  						{ $win_div = 0;}

#return $win_div;
return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$powerball_match.'|';
}

function check_win_results_sa_powerball($rb1,$rb2,$rb3,$rb4,$rb5,$rpb,$cb1,$cb2,$cb3,$cb4,$cb5,$cpp)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rbb - winning result power ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cpp - client selected ball 6

#------RULES-----------------------
#Div 1 = 5 Correct Numbers + PowerBall 
#Div 2 = 5 Correct Numbers
#Div 3 = 4 Correct Numbers + PowerBall
#Div 4 = 4 Correct Numbers 
#Div 5 = 3 Correct Numbers + Powerball
#Div 6 = 3 Correct Numbers 
#Div 7 = 2 Correct Numbers + Powerball
#Div 8 = 1 Correct Number + Powerball
#Div 9 = 1 Powerball
#----------------------------------

$win_div = 0;
$count = 0;
$count_power = 0;
$ball1_match = 0;
$ball2_match = 0;
$ball3_match = 0;
$ball4_match = 0;
$ball5_match = 0;
$powerball_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1; $ball1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1; $ball2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1; $ball3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1; $ball4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1; $ball5_match = 1;}

if ($cpp == $rpb) { $count_power =  1; $powerball_match = 1;}

if (($count == 0) && ($count_power))	{ $win_div = 9;}
else if (($count == 1) && ($count_power))	{ $win_div = 8;}
else if (($count == 2) && ($count_power))	{ $win_div = 7;}
else if (($count == 3) && ($count_power))	{ $win_div = 5;}
else if ($count == 3)						{ $win_div = 6;} 
else if (($count == 4) && ($count_power)) 	{ $win_div = 3;}
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_power)) 	{ $win_div = 1;}
else if ($count == 5)  						{ $win_div = 2;}
else 				  						{ $win_div = 0;}

#return $win_div;
return $win_div.'|'.$ball1_match.'|'.$ball2_match.'|'.$ball3_match.'|'.$ball4_match.'|'.$ball5_match.'|'.$powerball_match.'|';
}


function check_win_results_sa_powerball_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rpb,$cb1,$cb2,$cb3,$cb4,$cb5,$cpp)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rbb - winning result power ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cpp - client selected ball 6

#------RULES-----------------------
#Div 1 = 5 Correct Numbers + PowerBall 
#Div 2 = 5 Correct Numbers
#Div 3 = 4 Correct Numbers + PowerBall
#Div 4 = 4 Correct Numbers 
#Div 5 = 3 Correct Numbers + Powerball
#Div 6 = 3 Correct Numbers 
#Div 7 = 2 Correct Numbers + Powerball
#Div 8 = 1 Correct Number + Powerball
#----------------------------------

$win_div = 0;
$count = 0;
$count_power = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rpb_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1;  $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1;  $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1;  $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1;  $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1;  $cb5_match = 1;}

if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5)) { $rb5_match = 1;}


if ($cpp == $rpb) { $count_power =  1; $rpb_match = 1;}

 if (($count == 1) && ($count_power))	{ $win_div = 8;}
else if (($count == 2) && ($count_power))	{ $win_div = 7;}
else if (($count == 3) && ($count_power))	{ $win_div = 5;}
else if ($count == 3)						{ $win_div = 6;} 
else if (($count == 4) && ($count_power)) 	{ $win_div = 3;}
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_power)) 	{ $win_div = 1;}
else if ($count == 5)  						{ $win_div = 2;}
else 				  						{ $win_div = 0;}

#return $win_div;
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rpb_match;
return $result;
}



function check_win_results_uk_lotto_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-------RULES FOR UK LOTTO----------------
#Div 1 = Six Correct Numbers (Match 6)
#Div 2 = Five Correct Numbers + Bonus Ball (Match 5)
#Div 3 = Five Correct Numbers (Match 5)

#Div 4 = Four Correct Numbers (Match 4)
#Div 5 = Three Correct Numbers (Match 3)

#----------------------------------

#'Match 6' = six numbers in one Lotto Entry matched to the Main Numbers	1: 13,983,816	Jackpot	52% of the Pools Fund plus the amount not won in the previous Lotto Draw in the Match Six and Match Five plus Bonus categories
#'Match 5+ Bonus' = five numbers in one Lotto Entry matched from the Main Numbers and remaining number matched to Bonus Number*	1: 2,330,636	�100,000	16% of the Pools Fund
#'Match 5' = five numbers in one Lotto Entry matched from the Main Numbers	1: 55,492	�1,500	10% of the Pools Fund
#'Match 4' = four numbers in one Lotto Entry matched from the Main Numbers	1: 1,033	�62	22% of the Pools Fund plus the amount not won in the Match Five category for that Lotto Draw
#'Match 3' = three numbers in one Lotto Entry matched from the Main Numbers	1: 57	�10	�10 Prize payable from The Prize Fund, plus any amount not won in the Match Four Category for that Lotto Draw
#Any Prize	1: 54		



$win_div = 0;
$count = 0;
$count_bonus = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rb6_match = 0;$rbb_match=0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1; $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1; $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1; $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1; $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1; $cb5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1; $cb6_match = 1;}



if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5) or ($rb1 == $cb6)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5) or ($rb2 == $cb6)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5) or ($rb3 == $cb6)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5) or ($rb4 == $cb6)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5) or ($rb5 == $cb6)) { $rb5_match = 1;}
if (($rb6 == $cb1) or ($rb6 == $cb2) or ($rb6 == $cb3) or ($rb6 == $cb4) or ($rb6 == $cb5) or ($rb6 == $cb6)) { $rb6_match = 1;}



	 if ($cb1 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb2 == $rbb) { $count_bonus =  1; $rbb_match = 1;}  # TO-Do : add check that no duplicates if Cb1 != cb2 or cb1 != cb2 ...etc. e.g. if all 1's then calc incorrectly
else if ($cb3 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb4 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb5 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb6 == $rbb) { $count_bonus =  1; $rbb_match = 1;}


if ($count == 3)						{ $win_div = 5;} 
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

#build up return result, windiv, cb_match, rb_match
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$cb6_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rb6_match.",".$rbb_match;
return $result;
}


function do_nothing() # example of form submit auto
{
?>
<form method="POST" name="myform_pp" action="http://localhost/lottomoney/my_account.php">
						<input type="hidden" name="p" value="<?php echo $option ?>" size="50" maxlength="260" />
						<input type="hidden" name="ref" value="<?php echo $ref_no ?>" size="50" maxlength="260" />
						<script language="Javascript">document.myform_pp.submit();</script>
</form>
<?php }



function randomiser($min,$max)
{
$random_number = 1;
$random_number = rand($min,$max);

return $random_number;

}



function randomize_lotto_entry($lottoname_id)
{
$ball1 = 0;
$ball2 = 0;
$ball3 = 0;
$ball4 = 0;
$ball5 = 0;
$ball6 = 0;
$powerball = 0;
$bonus = 0;
$lucky1 = 0;
$lucky2 = 0;


if (($lottoname_id == 1)  or ($lottoname_id ==2 )) # SA lotto, SA lotto plus
	{ $max_number = 49;}
else if ($lottoname_id == 3) # SA Powerball
	{ $max_number = 45; $max_powerball = 20;}
else if ($lottoname_id == 4) # UK Millions 
	{ $max_number = 59; }    # correct - the lotter
else if ($lottoname_id == 5) # Euro Millions
	{ $max_number = 50; $max_lucky1 = 11; $max_lucky2 = 11;} # correct the lotter
else if ($lottoname_id == 6) # USA Powerball
	{ $max_number = 59; $max_powerball = 35;}   # powerball 1-59; 1-35 confirmed www.usamega.com
else if ($lottoname_id == 7) # SuperEna
	{ $max_number = 90; }    # the lotter 90 , six balls, x3 a week


$ball1 = rand(1,$max_number);
$ball2 = rand(1,$max_number);
$ball3 = rand(1,$max_number);
$ball4 = rand(1,$max_number);
$ball5 = rand(1,$max_number);
$ball6 = rand(1,$max_number);
$powerball = rand(1,$max_powerball);
$lucky2 = rand(1,$max_lucky1);
$lucky1 = rand(1,$max_lucky2);


#check that balls are not same / duplicate as others for the seqence!
$count = 0;
while (is_duplicates($ball1,$ball2,$ball3,$ball4,$ball5,$ball6,0))
{
	$ball1 = rand(1,$max_number);
	$ball2 = rand(1,$max_number);
	$ball3 = rand(1,$max_number);
	$ball4 = rand(1,$max_number);
	$ball5 = rand(1,$max_number);
	
	if (($lottoname_id == 1) or ($lottoname_id == 2) or ($lottoname_id == 4) or ($lottoname_id == 7))
		{ $ball6 = rand(1,$max_number); }
		else
		{$ball6 = 0; } # take out of equation not needed for duplication test!!
	$count = $count + 1;
	if ($count == 2000) { break;}  #stop from looping !
}



if (($lottoname_id == 1)  or ($lottoname_id ==2 )) # SA lotto, SA lotto plus
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$ball6;}
else if ($lottoname_id == 3) # SA Powerball
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$powerball;}
else if ($lottoname_id == 4) # UK Millions 
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$ball6;}
else if ($lottoname_id == 5) # Euro Millions
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$lucky1." ".$lucky2;}
else if ($lottoname_id == 6) # USA Powerball
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$powerball;}
else if ($lottoname_id == 7) # SuperEna
	{ $ball_set = $ball1." ".$ball2." ".$ball3." ".$ball4." ".$ball5." ".$ball6." ".$bonus;}



return $ball_set;
#use explode function to extract balls
}


#TO- DO : find out divisional rules--- below is copy and paste  from sa lotto check win results function
function check_win_results_superena($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-----------------------
#Div 1 = Six Correct Numbers
#Div 2 = Five Correct Numbers + Bonus Ball
#Div 3 = Five Correct Numbers
#Div 4 = Four Correct Numbers + Bonus Ball
#Div 5 = Four Correct Numbers 
#Div 6 = Three Correct Numbers + Bonus Ball
#Div 7 = Three Correct Numbers 
#----------------------------------
#Match	Odds
#1 6	1 in 622,614,630
#2 5+Jolly Number	1 in 103,769,105
#3 5	1 in 1,250,230
#4 4	1 in 11,907
#5 3	1 in 327

$win_div = 0;
$count = 0;
$count_bonus = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1;}

	 if ($cb1 == $rbb) { $count_bonus =  1;}
else if ($cb2 == $rbb) { $count_bonus =  1;}
else if ($cb3 == $rbb) { $count_bonus =  1;}
else if ($cb4 == $rbb) { $count_bonus =  1;}
else if ($cb5 == $rbb) { $count_bonus =  1;}
else if ($cb6 == $rbb) { $count_bonus =  1;}


if ($count == 3)					    	{ $win_div = 5;} 
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}

return $win_div;
}


function check_win_results_euro_lotto_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rl1,$rl2,$cb1,$cb2,$cb3,$cb4,$cb5,$cl1,$cl2)
{

#TO-DO: RULES + win div below
#div match-main  match-star odds
#1.   5 2   1:116,531,800
#2.   5 1   1:6,473,989
#3.   5 0   1:3,236,995
#4.   4 2   1:517,920
#5.   4 1   1:28,774
#6.   4 0   1:14,387
#7.   3 2   1:11,771
#8.   3 1   1:654
#9.   2 2   1:822
#10.  3 0   1:327
#11.  1 2   1:157
#12.  2 1   1:46
#13.  2 0

$win_div = 0;
$count = 0;
$count_lucky1 = 0;
$count_lucky2 = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rl1_match = 0;$rl2_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1;  $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1;  $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1;  $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1;  $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1;  $cb5_match = 1;}

if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5)) { $rb5_match = 1;}


if ($cl1 == $rl1) { $count_lucky1 =  1; $rl1_match = 1;}
if ($cl2 == $rl2) { $count_lucky2 =  1; $rl2_match = 1;}

 if (($count == 1) && ($count_lucky1) && ($count_lucky2))	{ $win_div = 11;} 
else if (($count == 2) && ($count_lucky1) && ($count_lucky2))	{ $win_div = 9;}
else if (($count == 2) && (($count_lucky1) or ($count_lucky2)))	{ $win_div = 12;}
else if ($count == 2)						{ $win_div = 13;} 
else if (($count == 3) && ($count_lucky1) && ($count_lucky2)) 	{ $win_div = 7;}
else if (($count == 3) && (($count_lucky1) or ($count_lucky2)))	{ $win_div = 8;}
else if ($count == 3)						{ $win_div = 10;} 
else if (($count == 4) && ($count_lucky1) && ($count_lucky2)) 	{ $win_div = 4;}
else if (($count == 4) && (($count_lucky1) or ($count_lucky2)))	{ $win_div = 5;}
else if ($count == 4)  						{ $win_div = 6;}
else if (($count == 5) && ($count_lucky1) && ($count_lucky2)) 	{ $win_div = 1;}
else if (($count == 5) && (($count_lucky1) or ($count_lucky2))) { $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else 				  						{ $win_div = 0;}

#return $win_div;
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rl1_match.",".$rl2_match;
return $result;

}

function check_win_results_usa_powerball_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rpb,$cb1,$cb2,$cb3,$cb4,$cb5,$cpp)
{
#TO-DO - RULES
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rbb - winning result power ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cpp - client selected ball 6

#------RULES-----------------------
#Div 1 = 5 Correct Numbers + PowerBall 
#Div 2 = 5 Correct Numbers
#Div 3 = 4 Correct Numbers + PowerBall
#Div 4 = 4 Correct Numbers 
#Div 5 = 3 Correct Numbers + Powerball
#Div 6 = 3 Correct Numbers 
#Div 7 = 2 Correct Numbers + Powerball
#Div 8 = 1 Correct Number + Powerball
#Div 9 = 0 + Powerball
#----------------------------------
#A. Match 5 + POWERBALL = $40 million minimum jackpot!
#Match 5 + NO POWERBALL = $1,000,000
#Match 4 + POWERBALL = $10,000
#Match 4 + NO POWERBALL = $100
#Match 3 + POWERBALL = $100
#Match 3 + NO POWERBALL = $7
#Match 2 + POWERBALL = $7
#Match 1 + POWERBALL = $4
#Match 0 + POWERBALL = $4

$win_div = 0;
$count = 0;
$count_power = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rpb_match = 0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5)) { $count = $count + 1;  $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5)) { $count = $count + 1;  $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5)) { $count = $count + 1;  $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5)) { $count = $count + 1;  $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5)) { $count = $count + 1;  $cb5_match = 1;}

if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5)) { $rb5_match = 1;}


if ($cpp == $rpb) { $count_power =  1; $rpb_match = 1;}

if (($count == 0) && ($count_power)) 		{ $win_div = 9;}
else if (($count == 1) && ($count_power))	{ $win_div = 8;}
else if (($count == 2) && ($count_power))	{ $win_div = 7;}
else if (($count == 3) && ($count_power))	{ $win_div = 5;}
else if ($count == 3)						{ $win_div = 6;} 
else if (($count == 4) && ($count_power)) 	{ $win_div = 3;}
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_power)) 	{ $win_div = 1;}
else if ($count == 5)  						{ $win_div = 2;}
else 				  						{ $win_div = 0;}

#return $win_div;
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rpb_match;
return $result;
}


function check_win_results_superena_simulate($rb1,$rb2,$rb3,$rb4,$rb5,$rb6,$rbb,$cb1,$cb2,$cb3,$cb4,$cb5,$cb6)
{
#$rb1 - winning result ball 1
#$rb2 - winning result ball 2
#$rb3 - winning result ball 3
#$rb4 - winning result ball 4
#$rb5 - winning result ball 5
#$rb6 - winning result ball 6
#$rbb - winning result bonus ball

#$cb1 - client selected ball 1
#$cb2 - client selected ball 2
#$cb3 - client selected ball 3
#$cb4 - client selected ball 4
#$cb5 - client selected ball 5
#$cb6 - client selected ball 6

#------RULES-----------------------
#Div 1 = Six Correct Numbers
#Div 2 = Five Correct Numbers + Bonus Ball
#Div 3 = Five Correct Numbers
#Div 4 = Four Correct Numbers + Bonus Ball
#Div 5 = Four Correct Numbers 
#Div 6 = Three Correct Numbers + Bonus Ball
#Div 7 = Three Correct Numbers 
#----------------------------------
#Match	Odds
#1 6	1 in 622,614,630
#2 5+Jolly Number	1 in 103,769,105
#3 5	1 in 1,250,230
#4 4	1 in 11,907
#5 3	1 in 327

$win_div = 0;
$count = 0;
$count_bonus = 0;
$cb1_match = 0;$cb2_match = 0;$cb3_match = 0;$cb4_match = 0;$cb5_match = 0;$cb6_match = 0;
$rb1_match = 0;$rb2_match = 0;$rb3_match = 0;$rb4_match = 0;$rb5_match = 0;$rb6_match = 0;$rbb_match=0;

if (($cb1 == $rb1) or ($cb1 == $rb2) or ($cb1 == $rb3) or ($cb1 == $rb4) or ($cb1 == $rb5) or ($cb1 == $rb6)) { $count = $count + 1; $cb1_match = 1;}
if (($cb2 == $rb1) or ($cb2 == $rb2) or ($cb2 == $rb3) or ($cb2 == $rb4) or ($cb2 == $rb5) or ($cb2 == $rb6)) { $count = $count + 1; $cb2_match = 1;}
if (($cb3 == $rb1) or ($cb3 == $rb2) or ($cb3 == $rb3) or ($cb3 == $rb4) or ($cb3 == $rb5) or ($cb3 == $rb6)) { $count = $count + 1; $cb3_match = 1;}
if (($cb4 == $rb1) or ($cb4 == $rb2) or ($cb4 == $rb3) or ($cb4 == $rb4) or ($cb4 == $rb5) or ($cb4 == $rb6)) { $count = $count + 1; $cb4_match = 1;}
if (($cb5 == $rb1) or ($cb5 == $rb2) or ($cb5 == $rb3) or ($cb5 == $rb4) or ($cb5 == $rb5) or ($cb5 == $rb6)) { $count = $count + 1; $cb5_match = 1;}
if (($cb6 == $rb1) or ($cb6 == $rb2) or ($cb6 == $rb3) or ($cb6 == $rb4) or ($cb6 == $rb5) or ($cb6 == $rb6)) { $count = $count + 1; $cb6_match = 1;}


if (($rb1 == $cb1) or ($rb1 == $cb2) or ($rb1 == $cb3) or ($rb1 == $cb4) or ($rb1 == $cb5) or ($rb1 == $cb6)) { $rb1_match = 1;}
if (($rb2 == $cb1) or ($rb2 == $cb2) or ($rb2 == $cb3) or ($rb2 == $cb4) or ($rb2 == $cb5) or ($rb2 == $cb6)) { $rb2_match = 1;}
if (($rb3 == $cb1) or ($rb3 == $cb2) or ($rb3 == $cb3) or ($rb3 == $cb4) or ($rb3 == $cb5) or ($rb3 == $cb6)) { $rb3_match = 1;}
if (($rb4 == $cb1) or ($rb4 == $cb2) or ($rb4 == $cb3) or ($rb4 == $cb4) or ($rb4 == $cb5) or ($rb4 == $cb6)) { $rb4_match = 1;}
if (($rb5 == $cb1) or ($rb5 == $cb2) or ($rb5 == $cb3) or ($rb5 == $cb4) or ($rb5 == $cb5) or ($rb5 == $cb6)) { $rb5_match = 1;}
if (($rb6 == $cb1) or ($rb6 == $cb2) or ($rb6 == $cb3) or ($rb6 == $cb4) or ($rb6 == $cb5) or ($rb6 == $cb6)) { $rb6_match = 1;}

	 if ($cb1 == $rbb) { $count_bonus =  1;}
else if ($cb2 == $rbb) { $count_bonus =  1;}
else if ($cb3 == $rbb) { $count_bonus =  1;}
else if ($cb4 == $rbb) { $count_bonus =  1;}
else if ($cb5 == $rbb) { $count_bonus =  1;}
else if ($cb6 == $rbb) { $count_bonus =  1;}


	 if ($cb1 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb2 == $rbb) { $count_bonus =  1; $rbb_match = 1;}  # TO-Do : add check that no duplicates if Cb1 != cb2 or cb1 != cb2 ...etc. e.g. if all 1's then calc incorrectly
else if ($cb3 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb4 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb5 == $rbb) { $count_bonus =  1; $rbb_match = 1;}
else if ($cb6 == $rbb) { $count_bonus =  1; $rbb_match = 1;}

if ($count == 3)					    	{ $win_div = 5;} 
else if ($count == 4)  						{ $win_div = 4;}
else if (($count == 5) && ($count_bonus)) 	{ $win_div = 2;}
else if ($count == 5)  						{ $win_div = 3;}
else if ($count == 6)  						{ $win_div = 1;}
else 				  						{ $win_div = 0;}
#build up return result, windiv, cb_match, rb_match
$result = $win_div.",".$cb1_match.",".$cb2_match.",".$cb3_match.",".$cb4_match.",".$cb5_match.",".$cb6_match.",".$rb1_match.",".$rb2_match.",".$rb3_match.",".$rb4_match.",".$rb5_match.",".$rb6_match.",".$rbb_match;
return $result;
}



##TAKEN FROM Data_LottoNumbers_All_New.php#########
function get_daterange_lottonumbers_LowHigh($lottoname_id,$number_of_rows)
{
	#print("got here...");
	
if ($lottoname_id == 1)
{
	$Table_Name = 'SALottoDrawResults';	
}
else if ($lottoname_id == 2)
{
	$Table_Name = 'SALottoPlusDrawResults';
}
else if ($lottoname_id == 3)
{ 
	$Table_Name = 'SAPowerballDrawResults';
}
else if ($lottoname_id == 4)
{
	$Table_Name = 'UKLottoResults';	
}
else if ($lottoname_id == 5)
{
	$Table_Name = 'EuroMillionsDrawResults';	
}
else if ($lottoname_id == 6)
{
	$Table_Name = 'USAPowerballDrawResults';
}
else if ($lottoname_id == 7)
{
	$Table_Name = 'SuperEnaDrawResults';
}

#disabled
#$link = connectToDB();


if (($lottoname_id == 1) or ($lottoname_id == 2) or ($lottoname_id == 3) or ($lottoname_id == 4) or ($lottoname_id == 5) or ($lottoname_id == 6) or ($lottoname_id == 7))
{
	$sQuery = "select drawdate from ".$Table_Name." order by drawdate desc";
}
	
	$result = mysql_query($sQuery);
	$myrow = mysql_fetch_array($result);
		if (!$myrow){
	header("Location: ".$prod_URL."loginerror.php?code=could_not_get_lotto_numbers");
	break;
	}
	$current_drawdate_results = $myrow[0]; 



#print("current_drawdate_results:");
#print($current_drawdate_results);
#print("number_of_rows:");
#print($number_of_rows);

######WORKED OUT AS FOLLOWS
# 8 = 4 weeks x 2 per week
# 2 per week = 7 days
# 7 x 4 weeks  = 28 ,make 31 days.


date_default_timezone_set('Africa/Johannesburg');
if (($lottoname_id == 1) or ($lottoname_id == 2) or ($lottoname_id == 3) or ($lottoname_id == 4) or ($lottoname_id == 5) or ($lottoname_id == 6) or ($lottoname_id == 7))
{
$number_of_days_to_subtract = 31;
	
	if ($number_of_rows ==8) { $number_of_days_to_subtract = 31; }
	else if ($number_of_rows ==24) { $number_of_days_to_subtract = 31 * 3; }
	else if ($number_of_rows ==48) { $number_of_days_to_subtract = 31 * 6; }
	else if ($number_of_rows ==96) { $number_of_days_to_subtract = 31 * 12; }
	else if ($number_of_rows ==192) { $number_of_days_to_subtract = 31 * 24; }
	#if ($number_of_rows ==2000) { $number_of_days_to_subtract = 31 * 12 * 12; }  #12 years back
	
#print("number_of_days_to_subtract:");
#print($number_of_days_to_subtract);	

	$date1 = strtotime($current_drawdate_results);
	$date2 = $date1-(60*60*24*$number_of_days_to_subtract);
	$drawdate2 = date('Y-m-d',$date2);
	if ($number_of_rows ==2000) { $drawdate2 = '1999-01-01'; }  #ALL results
}

#print("drawdate2:");
#print($drawdate2);	

#exit;

return $drawdate2;	

}


?>