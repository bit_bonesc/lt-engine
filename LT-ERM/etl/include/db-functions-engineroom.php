<?php
#include_once("db-settings-engineroom.php");
#include_once("db-connect-engineroom.php");

#results_main.php
function db_get_count_all($db)
{
	$sql = "SELECT count(*) FROM LottoName where (active = 1 and countrycode <> 'ALL')";
	$fav_count = db_return_one_param($db,$sql);
	return($fav_count);
}

#results_main.php
function db_get_count_country($db,$countrycode_in)
{
	$sql = "SELECT count(*) FROM LottoName where (active = 1 and countrycode = '".$countrycode_in."')";
	$fav_count = db_return_one_param($db,$sql);
	return($fav_count);
}
#results_main.php
# NOTE PARAM $X returns a val as well - count!
function db_return_countries($db,&$x)
{
	$x=0;
	$rows = array();
	$sql = "SELECT distinct country, countrycode FROM LottoName order by country";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['country']=$row[0];
	  $rows[$x]['countrycode']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();


	return $rows;
}

#results_main.php
function db_return_countries_detail($db,&$x,$countrycode_in)
{
	$x=0;
	$rows = array();
	if ($countrycode_in == "ALL")
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2  FROM LottoName where (active = 1 and countrycode <> 'ALL') order by lottonamedisplay";
		}
		else
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
		where (active = 1 and countrycode = '".$countrycode_in."') order by lottonamedisplay";

		}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoCode']=$row[0];
	  $rows[$x]['lottonamedisplay']=$row[1];
	  $rows[$x]['jackpot']=$row[2];
	  $rows[$x]['currency_symbol']=$row[3];
	  $rows[$x]['lottocountrycode']=$row[4];
	  $rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
	  $rows[$x]['drawdate']=substr($row[6],0,10);
	  $rows[$x]['Lotto_Table']=$row[7];
	  $rows[$x]['ball1_active']=$row[8];
	  $rows[$x]['ball2_active']=$row[9];
	  $rows[$x]['ball3_active']=$row[10];
	  $rows[$x]['ball4_active']=$row[11];
	  $rows[$x]['ball5_active']=$row[12];
	  $rows[$x]['ball6_active']=$row[13];
	  $rows[$x]['ball7_active']=$row[14];
	  $rows[$x]['bonus_active']=$row[15];
	  $rows[$x]['powerball_active']=$row[16];
	  $rows[$x]['lucky1_active']=$row[17];
	  $rows[$x]['lucky2_active']=$row[18];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


function db_return_language_detail($db,&$x,$langcode,$page)
{
	$x=0;
	#$rows = array();
	$rows = '';

	#$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
	#	where (active = 1 and countrycode = '".$countrycode_in."') order by lottonamedisplay";
  $sql = "SELECT var,".$langcode." from Config_Language where langcode = '".$langcode."' and page = '".$page."'";
	print($rows);
	exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x][$row[0]]=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}



function db_return_countries_detail_spectrum_ZAL($db,&$x,$countrycode_in)
{
	$x=0;
	$rows = array();
	if ($countrycode_in == "ALL")
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2  FROM LottoName where (active = 1 and countrycode <> 'ALL') order by lottonamedisplay";
		}
		else
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
		where (active = 1 and countrycode = '".$countrycode_in."') order by lottonamedisplay";

		}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoCode']=$row[0];
	  $rows[$x]['lottonamedisplay']=$row[1];
	  $rows[$x]['jackpot']=$row[2];
	  $rows[$x]['currency_symbol']=$row[3];
	  $rows[$x]['lottocountrycode']=$row[4];
	  $rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
	  $rows[$x]['drawdate']=substr($row[6],0,10);
	  $rows[$x]['Lotto_Table']=$row[7];
	  $rows[$x]['ball1_active']=$row[8];
	  $rows[$x]['ball2_active']=$row[9];
	  $rows[$x]['ball3_active']=$row[10];
	  $rows[$x]['ball4_active']=$row[11];
	  $rows[$x]['ball5_active']=$row[12];
	  $rows[$x]['ball6_active']=$row[13];
	  $rows[$x]['ball7_active']=$row[14];
	  $rows[$x]['bonus_active']=$row[15];
	  $rows[$x]['powerball_active']=$row[16];
	  $rows[$x]['lucky1_active']=$row[17];
	  $rows[$x]['lucky2_active']=$row[18];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}

#etl/transfer_mail_results_multiple.php
function db_get_lottonames($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "select lottoname,lottocode from LottoName where active = 1 and active_tester_transfer_results = 1";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}
function db_get_lottonames_api($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "select lottoname,lottocode from LottoName where active = 1 and active_tester_transfer_results = 1 and api_active = 1";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

#
function db_get_date_start($db,$LottoName,$date_range_min)
{
	$sql = "select drawdate from ".$LottoName." where drawdate >= '".$date_range_min."' order by drawdate asc LIMIT 1";
	$x=0;
	$startdate = '';

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $startdate=$row[0];
	}
	$result->close();
	return $startdate;
}


#tools-hot-cold-generator-new-calc.php
function db_get_count_ball_count($db,$sql)
{
	$x=0;
	$count_val = 0;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $count_val=$row[0];
	}
	$result->close();
	return $count_val;
}



#tools-hot-cold-generator-new-calc.php
function db_update_ball_count($db,$count,$ball_number,$lottocode)
{
	#test if exists fitst
	$sql = "Select ball_count from ChartStats_Hot_Cold where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: no nnnn%s\n", $db->error);
	 exit; ## does not exist so insert then
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 print("no rows..inserting..");
	 db_insert_ball_count($db,$count,$ball_number,$lottocode);
	}
	else
	{
		$sql = "update ChartStats_Hot_Cold set ball_count = ".$count." where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
		$result = $db->query($sql);
		if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		 exit; ## does not exist so insert then
			}
	}

	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
function db_update_ball_count_pb($db,$count,$ball_number,$lottocode)
{
	#test if exists fitst
	$sql = "Select pb_count from ChartStats_Hot_Cold where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: no nnnn%s\n", $db->error);
	 exit; ## does not exist so insert then
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 print("no rows..inserting..");
	 db_insert_ball_count_pb($db,$count,$ball_number,$lottocode);
	}
	else
	{
		$sql = "update ChartStats_Hot_Cold set pb_count = ".$count." where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
		$result = $db->query($sql);
		if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		 exit; ## does not exist so insert then
			}
	}

	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function db_insert_api_queueLM($db,$lottocode,$url,$targetsite)
{
	#$targetsite ='LM.UK';
	$sql = "insert into Queue_API_OutBound (lottocode,url,targetsite) values ('".$lottocode."','".$url."','".$targetsite."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$sql = "select id from Queue_API_OutBound order by id desc limit 1";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $id=$row[0];
	}
	$result->close();
	#print("id:"); print($id);
	return $id;
}

function db_insert_api_queue($db,$lottocode,$url)
{
	$sql = "insert into Queue_API_OutBound (lottocode,url) values ('".$lottocode."','".$url."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$sql = "select id from Queue_API_OutBound order by id desc limit 1";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $id=$row[0];
	}
	$result->close();
	#print("id:"); print($id);
	return $id;
}


function db_update_api_queue($db,$id,$return_code)
{
	$sql = "update Queue_API_OutBound set processed = ".$return_code." where id = ".$id;
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
}


#tools-hot-cold-generator-new-calc.php
function db_insert_ball_count_pb($db,$count,$ball_number,$lottocode)
{
	$sql = "insert into ChartStats_Hot_Cold (lottocode,ball_number,pb_count) values ('".$lottocode."','".$ball_number."','".$count."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#tools-hot-cold-generator-new-calc.php

function db_insert_entry_hot_cold($db,$lottocode,$number_of_months,$drawdate,$strCOMMA_val,$strCOMMA_color,$count_min_max,$pb_ind,$date_range_min)
{
	$sql = "INSERT into Chart_Hot_Cold (num_months,pb_ind,drawdate,val,color,min_max,lottocode,drawdate_start)
	values (".$number_of_months.",".$pb_ind.",'".$drawdate."','".$strCOMMA_val."','".$strCOMMA_color."','".$count_min_max."','".$lottocode."','".$date_range_min."')";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



#tools-hot-cold-generator-new-calc.php
function db_get_ball_counts($db,$lottocode,$direction)
{
$x=0;
	$rows = array();
	$sql = "select ball_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' order by ball_count ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}
function db_get_ball_counts_pb($db,$lottocode,$direction)
{
$x=0;
	$rows = array();
	$sql = "select pb_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' and pb_count is not null order by ball_count ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}




function db_get_ball_counts_by_number($db,&$x,$lottocode,$direction)
{
	$x=0;
	$rows = array();
	$sql = "select ball_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' order by ball_number ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_ball_counts_by_number_pb($db,&$x,$lottocode,$direction)
{
	$x=0;
	$rows = array();
	$sql = "select pb_count, ball_number from ChartStats_Hot_Cold where (lottocode = '".$lottocode."') and pb_count is not null order by ball_number ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}


#etl/transfer_fix.php
function db_get_lottonames_9($db,&$x)
{
	$x=0;


	$rows = array();

	$sql = "select lottoname,lottocode,lottotable from LottoName where active = 1 and active_tester_transfer_results = 9";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];
	  $rows[$x]['lottotable']=$row[2];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_mail_results.php
function db_get_lotto_detail_f1($db,&$x,$lotto_name)
{
	$x=0;
	$rows = array();

	$sql	 = "select id,LottoName,DrawDate,DrawNumber,Results,Jackpot from ResultsRawEmailFeed_Master where LottoName = '".$lotto_name."' and (transferred is null or transferred = 0)";
	#print($sql);


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $rows[$x]['DrawDate']=$row[2];
	  $rows[$x]['DrawNumber']=$row[3];
	  $rows[$x]['Results']=$row[4];
	  $rows[$x]['Jackpot']=$row[5];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


function db_get_lotto_detail_source2($db,&$x,$lotto_name)
{
	$x=0;
	$rows = array();

	$sql	 = "select id,LottoName,DrawDate,DrawNumber,Results,Jackpot from ResultsRawEmailFeed_Source2 where LottoName = '".$lotto_name."' and (transferred is null or transferred = 0) order by id";
	#print($sql);

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $rows[$x]['DrawDate']=$row[2];
	  $rows[$x]['DrawNumber']=$row[3];
	  $rows[$x]['Results']=$row[4];
	  $rows[$x]['Jackpot']=$row[5];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_mail_results.php
function db_get_lotto_detail_f2($db,&$x,$LottoName)
{
	$x=0;
	$rows = array();

	$sql = "select LottoTable, ball5,ball6,ball7,bonus,powerball,lucky1,lucky2,lottocode from LottoName where lottoname = '".$LottoName."'";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['LottoTable']=$row[0];
	  $rows[$x]['ball5']=$row[1];
	  $rows[$x]['ball6']=$row[2];
	  $rows[$x]['ball7']=$row[3];
	  $rows[$x]['bonus']=$row[4];
	  $rows[$x]['powerball']=$row[5];
	  $rows[$x]['lucky1']=$row[6];
	  $rows[$x]['lucky2']=$row[7];
	  $rows[$x]['lottocode']=$row[8];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_fix.php
function db_get_lotto_detail_f3($db,&$x,$LottoTable)
{
	$x=0;
	$rows = array();

	$sql = "select drawno,drawdate from ".$LottoTable." order by drawdate";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawno']=$row[0];
	  $rows[$x]['drawdate']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}



function get_date_range_HC($db,$lottocode_in,&$x)
{
	$x=0;
	$rows = array();

	$sql = "select num_months,drawdate,drawdate_start from Chart_Hot_Cold where lottocode = '".$lottocode_in."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['num_months']=$row[0];
	  $rows[$x]['drawdate']=$row[1];
	  $rows[$x]['drawdate_start']=$row[2];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}




	#get_results_from_email4.php
function db_return_lotto_detail($db,&$x,$lottocode_in)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,powerball2,lucky1,lucky2,ball_max, ball_max_pb,ball_max_lucky,lottoname FROM LottoName
		where lottocode = '".$lottocode_in."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoCode']=$row[0];
	  $rows[$x]['lottonamedisplay']=$row[1];
	  $rows[$x]['jackpot']=$row[2];
	  $rows[$x]['currency_symbol']=$row[3];
	  $rows[$x]['lottocountrycode']=$row[4];
	  $rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
	  $rows[$x]['drawdate']=substr($row[6],0,10);
	  $rows[$x]['Lotto_Table']=$row[7];
	  $rows[$x]['ball1_active']=$row[8];
	  $rows[$x]['ball2_active']=$row[9];
	  $rows[$x]['ball3_active']=$row[10];
	  $rows[$x]['ball4_active']=$row[11];
	  $rows[$x]['ball5_active']=$row[12];
	  $rows[$x]['ball6_active']=$row[13];
	  $rows[$x]['ball7_active']=$row[14];
	  $rows[$x]['bonus_active']=$row[15];
	  $rows[$x]['powerball_active']=$row[16];
	  $rows[$x]['powerball2_active']=$row[17];
	  $rows[$x]['lucky1_active']=$row[18];
	  $rows[$x]['lucky2_active']=$row[19];
	  $rows[$x]['ball_max']=$row[20];
	  $rows[$x]['ball_max_pb']=$row[21];
	  $rows[$x]['ball_max_lucky']=$row[22];
		$rows[$x]['lottoname']=$row[23];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#getData-HC.php
function db_return_chart_HC_detail($db,&$x,$lottocode_in,$months)
{
	$x=0;
	$rows = array();

	$sql = "select drawdate,val,color,min_max from Chart_Hot_Cold where lottocode = '".$lottocode_in."' and num_months = '".$months."' and pb_ind = 0 order by drawdate desc LIMIT 1";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawdate']=$row[0];
	  $rows[$x]['val']=$row[1];
	  $rows[$x]['color']=$row[2];
	  $rows[$x]['min_max']=$row[3];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}
#getData-HC.php
function db_return_chart_HC_detail_pb($db,&$x,$lottocode_in,$months)
{
	$x=0;
	$rows = array();
	$sql = "select drawdate,val,color,min_max from Chart_Hot_Cold where lottocode = '".$lottocode_in."' and num_months = '".$months."' and pb_ind = 1 order by drawdate desc LIMIT 1";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawdate']=$row[0];
	  $rows[$x]['val']=$row[1];
	  $rows[$x]['color']=$row[2];
	  $rows[$x]['min_max']=$row[3];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


# process-signin-form.php

function db_return_account_detail_signin($db,$email,&$return_code)
{
	$return_code = 0;
	$rows = array();
	$sql = "SELECT firstname, surname, active, account_id, MD5(UNIX_TIMESTAMP() + account_id + RAND(UNIX_TIMESTAMP())) sGUID, pwd_hash, sGUIDRegister FROM Accounts WHERE email = '$email'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $rows['firstname']=$row[0];
	  $rows['surname']=$row[1];
	  $rows['active']=$row[2];
	  $rows['account_id']=$row[3];
	  $rows['sGUID']=$row[4];
	  $rows['pwd_hash']=$row[5];
		$rows['sGUIDRegister'] = $row[6];
	  $return_code = 1;
	}
	return $rows;
}

# api_transfer_results_all_LMUK.php
function  db_update_ResultsAPI_LMUK($db,$LottoTable,$Results_table_id)
{
	$sql = "Update ".$LottoTable." Set transferred_to_LMUK = '1' Where id = ".$Results_table_id;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

# api_transfer_results_all.php
function  db_update_ResultsAPI($db,$LottoTable,$Results_table_id)
{
	$sql = "Update ".$LottoTable." Set transferred_to_CMS = '1' Where id = ".$Results_table_id;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

# process-signin-form.php
function db_update_accounts($db,$account_id,$sGUID)
{
	$sql = "Update Accounts Set sGUID = '".$sGUID."' Where account_id = '".$account_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


# process-signin-form.php
function db_update_audit($db,$account_id,$sGUID,$ipaddress)
{
	$Description = 'Logging On. SessionID ='.$sGUID;
	$sql = "insert into Audit (account_id,description,logdate,type,ipaddress) values ('".$account_id."','".$Description."',NOW(),'1','".$ipaddress."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonus,$jackpot)
{

	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonus."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function  db_insert_lottoresults_7balls_bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$bonus,$jackpot)
{

	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$bonus."','".$jackpot."')";
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function  db_insert_lottoresults_7balls_4bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$bonusball1,$bonusball2,$bonusball3,$bonusball4,$ball1_2,$ball2_2,$ball3_2,$ball4_2,$ball5_2,$ball6_2,$ball7_2,$bonusball1_2,$bonusball2_2,$bonusball3_2,$bonusball4_2,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball1,bonusball2,bonusball3,bonusball4,ball1_2,ball2_2,ball3_2,ball4_2,ball5_2,ball6_2,ball7_2,bonusball1_2,bonusball2_2,bonusball3_2,bonusball4_2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$bonusball1."','".$bonusball2."','".$bonusball3."','".$bonusball4."','".$ball1_2."','".$ball2_2."','".$ball3_2."','".$ball4_2."','".$ball5_2."','".$ball6_2."','".$ball7_2."','".$bonusball1_2."','".$bonusball2_2."','".$bonusball3_2."','".$bonusball4_2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



function db_insert_lottoresults_6balls_only($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$jackpot)
{

	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_6balls($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball1_2,$ball2_2,$ball3_2,$ball4_2,$ball5_2,$ball6_2_new,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball1_2,ball2_2,ball3_2,ball4_2,ball5_2,ball6_2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball1_2."','".$ball2_2."','".$ball3_2."','".$ball4_2."','".$ball5_2."','".$ball6_2_new."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_only($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$jackpot)
{

	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_4balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$powerball,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$powerball,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$powerball,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_2lucky($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$lucky1,$lucky2,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,lucky1,lucky2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$lucky1."','".$lucky2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_2supplementary($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$supplementary1,$supplementary2,$jackpot,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,supplementary1,supplementary2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$supplementary1."','".$supplementary2."','".$jackpot."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_bonus_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonus,$powerball,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonus."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_2bonusball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonusball1,$bonusball2,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,bonusball2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonusball1."','".$bonusball2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
	return true;
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_7balls_2supplementary($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$supplementary1,$supplementary2,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,supplementary1,supplementary2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$supplementary1."','".$supplementary2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_7balls_2bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$supplementary1,$supplementary2,$jackpot)
{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball,bonusball2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$supplementary1."','".$supplementary2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_update_jackpot($db,$lottocode,$jackpot)
{
	 $sql = "update LottoName set estimatedjackpot = '".$jackpot."' where lottocode = '".$lottocode."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_fix.php
function db_update_lottotable($db,$LottoTable,$drawno,$id,$drawdate)
{
	 $sql = "update ".$LottoTable." set id = '".$id."' where drawno = '".$drawno."' and drawdate = '".$drawdate."'";
	 $sql = "update ".$LottoTable." set id = '".$id."' where drawdate = '".$drawdate."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_update_RawEmailFeed($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_update_RawEmailFeed_Master($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed_Master set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function db_update_RawEmailFeed_Source2($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed_Source2 set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/get_results_from_email.php

function db_log_incoming_message($db,$LottoName, $DrawDate, $EmailDate, $EmailSubject, $EmailBody,$Source,$Results,$DrawNumber,$Jackpot,$rawfeed_tablename)
{

	  # $time = time();
	  $sql = "insert into ".$rawfeed_tablename." (LottoName, DrawDate, EmailDate, EmailSubject, EmailBody,Source,Results,DrawNumber,Jackpot) values ('".$LottoName."', '".$DrawDate."', '".$EmailDate."', '".$EmailSubject."', '".$EmailBody."','".$Source."','".$Results."','".$DrawNumber."','".$Jackpot."')";
	 $result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
}
function db_log_incoming_message_error($db,$LottoName, $DrawDate, $EmailDate, $EmailSubject, $EmailBody,$Source,$Results,$DrawNumber,$Jackpot)
{

	  # $time = time();
	  $sql = "insert into ResultsRawEmailFeedExceptionLog (LottoName, DrawDate, EmailDate, EmailSubject, EmailBody,Source,Results,DrawNumber,Jackpot) values ('".$LottoName."', '".$DrawDate."', '".$EmailDate."', '".$EmailSubject."', '".$EmailBody."','".$Source."','".$Results."','".$DrawNumber."','".$Jackpot."')";
	 $result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}


}

##charts.php


function db_return_name($db,$lottocode_in)
{

	$displayname = '';

	$sql = "SELECT lottonamedisplay from LottoName WHERE lottocode = '$lottocode_in'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $displayname=$row[0];
	}

	$result->close();

	return $displayname;
}




function db_return_one_param($db,$sql)
{
	if ($result = $db->query($sql))
	{
		$col1 = 0;
		$row = $result->fetch_row();
		$col1 = $row[0];
		$result->close();
	}
return($col1);
}




#etl/get_results_from_email.php
function db_return_email_reader_lottonames($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottocode,Lottoname_search_criteria FROM Config_EmailReader WHERE config_type = 'LottoName' and Source = 'theLotter'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottocode']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}
?>
