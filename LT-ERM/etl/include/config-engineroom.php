<?php
# GLOGALS

$LIVE=true;

$EMAIL_SERVER = '';
$EMAIL_LOGIN = '';
$EMAIL_PWD = '';
$MAIL_PROCESS_INBOX = '';
$MAIL_PROCESS_INQUEUE = '';

$SOURCE2_EMAIL_SERVER = '';
$SOURCE2_EMAIL_LOGIN = '';
$SOURCE2_EMAIL_PWD = '';

$SOURCE1_ACTIVE = '';
$SOURCE2_ACTIVE = '';
$SOURCE1_TABLENAME = '';
$SOURCE2_TABLENAME = '';

$SOURCE1_CRAWL_ACTIVE  = '';
$SOURCE1_CRAWL_TABLENAME = '';
$SOURCE1_CRAWL_DESCRIPTION = '';

$SOURCE2_CRAWL_ACTIVE  = '';
$SOURCE2_CRAWL_TABLENAME = '';
$SOURCE2_CRAWL_DESCRIPTION = '';

$PROD_API_URL = '';
$DEV_API_URL = '';

$API_QUEUE = '';
$API_LIVE_CMS = '';
$API_LIVE_ER_LMUK = '';
$API_TRANSFER_DELAY = '';

$ERM_PROD_URL = '';
$ERM_DEV_URL = '';
$PROCESSING_DELAY='';

$Download_Dir_prod = $_SERVER['DOCUMENT_ROOT']."/downloads/";
#$Download_Dir_prod = "/home/".$local_home_dir."/downloads/";
$Download_Dir = $Download_Dir_prod;

/*if (!$LIVE)  #LOCALHOST
{
 #$prod_URL = "http://localhost/lottotarget2/";
 #$Download_Dir_local = $_SERVER['DOCUMENT_ROOT']."/lottotarget2"."/downloads/";
 #$Download_Dir = $Download_Dir_local;
}
*/


function get_configuration($db)
{
	GLOBAL $EMAIL_SERVER;
    GLOBAL $EMAIL_LOGIN;
    GLOBAL $EMAIL_PWD;
	GLOBAL $MAIL_PROCESS_INBOX;
	GLOBAL $MAIL_PROCESS_INQUEUE;

	GLOBAL $SOURCE2_EMAIL_SERVER;
	GLOBAL $SOURCE2_EMAIL_LOGIN;
	GLOBAL $SOURCE2_EMAIL_PWD;

	GLOBAL $SOURCE1_ACTIVE;
	GLOBAL $SOURCE2_ACTIVE;
	GLOBAL $SOURCE3_ACTIVE;
	

	GLOBAL $SOURCE1_TABLENAME;
	GLOBAL $SOURCE2_TABLENAME;
	GLOBAL $SOURCE3_TABLENAME;

	GLOBAL $SOURCE1_CRAWL_ACTIVE;
	GLOBAL $SOURCE1_CRAWL_TABLENAME;
	GLOBAL $SOURCE1_CRAWL_DESCRIPTION;

	GLOBAL $SOURCE2_CRAWL_ACTIVE;
	GLOBAL $SOURCE2_CRAWL_TABLENAME;
	GLOBAL $SOURCE2_CRAWL_DESCRIPTION;

	GLOBAL $PROD_API_URL;
	GLOBAL $DEV_API_URL;

	GLOBAL $API_QUEUE;
	GLOBAL $API_LIVE_CMS_DEV;
	GLOBAL $API_LIVE_CMS_PROD;
	GLOBAL $API_LIVE_ER_LMUK;
	GLOBAL $API_LIVE_ER_LMEU;    
	GLOBAL $API_LIVE_ER_LMCA;    
	GLOBAL $API_LIVE_ER_LMUS; 
	GLOBAL $API_TRANSFER_DELAY;

	GLOBAL $ERM_PROD_URL;
	GLOBAL $ERM_DEV_URL;

	GLOBAL $PROCESSING_DELAY;

	$EMAIL_SERVER = db_get_config_variable_value($db,'email_server');
	$EMAIL_LOGIN  = db_get_config_variable_value($db,'email_login');
	$EMAIL_PWD    = db_get_config_variable_value($db,'email_pwd');
	$MAIL_PROCESS_INBOX = db_get_config_variable_value($db,'mail_process_inbox');
	$MAIL_PROCESS_INQUEUE = db_get_config_variable_value($db,'mail_process_inqueue');

	$SOURCE2_EMAIL_SERVER  = db_get_config_variable_value($db,'source2_email_server');
	$SOURCE2_EMAIL_LOGIN   = db_get_config_variable_value($db,'source2_email_login');
	$SOURCE2_EMAIL_PWD     = db_get_config_variable_value($db,'source2_email_pwd');

	$SOURCE1_ACTIVE     = db_get_config_variable_value($db,'source1_active');
	$SOURCE2_ACTIVE     = db_get_config_variable_value($db,'source2_active');
	
	$SOURCE1_TABLENAME     = db_get_config_variable_value($db,'source1_tablename');
	$SOURCE2_TABLENAME     = db_get_config_variable_value($db,'source2_tablename');

	$SOURCE1_CRAWL_ACTIVE = db_get_config_variable_value($db,'source1_crawl_active');
	$SOURCE1_CRAWL_TABLENAME = db_get_config_variable_value($db,'source1_crawl_tablename');
	$SOURCE1_CRAWL_DESCRIPTION = db_get_config_variable_value($db,'source1_crawl_description');

	$SOURCE2_CRAWL_ACTIVE = db_get_config_variable_value($db,'source2_crawl_active');
	$SOURCE2_CRAWL_TABLENAME = db_get_config_variable_value($db,'source2_crawl_tablename');
	$SOURCE2_CRAWL_DESCRIPTION = db_get_config_variable_value($db,'source2_crawl_description');

	$PROD_API_URL     = db_get_config_variable_value($db,'prod_api_URL');
	$DEV_API_URL     = db_get_config_variable_value($db,'dev_api_URL');
	$API_QUEUE     = db_get_config_variable_value($db,'API_outbound_queue_active');
	$API_LIVE_CMS_DEV     = db_get_config_variable_value($db,'API_outbound_cms_active_dev');
	$API_LIVE_CMS_PROD     = db_get_config_variable_value($db,'API_outbound_cms_active_prod');
	
	$API_LIVE_ER_LMUK     = db_get_config_variable_value($db,'API_outbound_ER_active_LMUK');
	$API_LIVE_ER_LMEU     = db_get_config_variable_value($db,'API_outbound_ER_active_LMEU');
	$API_LIVE_ER_LMCA     = db_get_config_variable_value($db,'API_outbound_ER_active_LMCA');
	$API_LIVE_ER_LMUS     = db_get_config_variable_value($db,'API_outbound_ER_active_LMUS');
	$API_TRANSFER_DELAY     = db_get_config_variable_value($db,'api_transfer_delay_sec');
	$PROCESSING_DELAY     = db_get_config_variable_value($db,'processing_delay_sec');
	
	 $ERM_PROD_URL = db_get_config_variable_value($db,'prod_URL');
	 $ERM_DEV_URL = db_get_config_variable_value($db,'dev_URL');
}

function db_get_config_variable_value($db,$variable)
{
	$value = '';
    $sql = "SELECT value from Config_EngineRoom where param = '".$variable."'";
	$result = $db->query($sql);
	if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		  exit;
		}
	$row = $result->fetch_row();
	if($row)	{	  $value = $row[0];	}
	$result->close();
	return $value;
}

?>
