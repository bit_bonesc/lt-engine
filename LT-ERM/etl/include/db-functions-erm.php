<?php


function db_draw_exists($db,$LottoTable,$DrawDate)
{
	$exists = 0;
		$sql = "SELECT id FROM ".$LottoTable." WHERE drawdate = '".$DrawDate."'";
		/*try {	
			$query = $db->query($sql);
			#$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$row = $query->fetch(\PDO::FETCH_ASSOC);
			if (!$row) { 
				$exists=0;
		}
		else
		{
			$exists=1;
		}
		} catch (PDOException $e){
				//echo '{"error": {"text": '.$e->getMessage().'}}';
				$exists=0;
			}
			*/
	$result = $db->query($sql);
	if (!$result) {
		$exists=0;
	  //printf("Query failed: %s\n", $db->error);
	}
	else
	{
		$row = $result->fetch_row();
		if (!$row) { 
			$exists=0;
		}
		else{
			$exists=1;
		}
		
	}

	return $exists;
}



function db_get_lottoname($db,$lottocode)
{
	$x=0;
	$rows = array();
	$lottoname = '';
	$sql = "SELECT lottoname,lottocode FROM LottoName WHERE lottocode = '".$lottocode."'";
	//print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row){
		$lottoname=$row[0];
	}

	$result->close();
	return $lottoname;
}


function db_update_queue_inbound($db,$id)
{
	$sql = "UPDATE Queue_API_InBound SET processed = '1' where id = ".$id;
	$result = $db->query($sql);
	if (!$result) {
		printf("update failed: %s\n", $db->error);
		exit;
	}
}

function db_get_queue_inbound($db,&$x,$limit)
{
	$x=0;
	$rows = array();
	$sql = "select id,lottocode,params,source from Queue_API_InBound where processed = '0' LIMIT ".$limit;

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['lottocode']=$row[1];
	  $rows[$x]['params']=$row[2];
	  $rows[$x]['source']=$row[3];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}


function db_insert_queue_inbound($db,$lottocode_in,$lottoresults,$source){
	
	$sql = "insert into Queue_API_InBound (lottocode,params,source) values ('".$lottocode_in."','".$lottoresults."','".$source."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function  db_update_SelectorsAPI($db,$LottoTable,$Results_table_id,$flag)
{
//	print($sql);
	if ($flag=='dev'){
		$sql = "Update ".$LottoTable." Set transferred_to_CMS_dev = '1' Where lottoentry_id = ".$Results_table_id;}
	else if ($flag=='prod'){
			$sql = "Update ".$LottoTable." Set transferred_to_CMS_prod = '1' Where lottoentry_id = ".$Results_table_id;}
			
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function  db_update_SelectorsAPI_ballsmatched($db,$LottoTable,$Results_table_id,$flag)
{
//	print($sql);
	if ($flag=='dev'){
		$sql = "Update ".$LottoTable." Set transferred_to_CMS_ballsmatched_dev = '1' Where lottoentry_id = ".$Results_table_id;}
	if ($flag=='prod'){
			$sql = "Update ".$LottoTable." Set transferred_to_CMS_ballsmatched_prod = '1' Where lottoentry_id = ".$Results_table_id;}
			
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function db_import_selectors_active_set($db,$flag,$state)
{
	if ($flag=='dev'){
		$sql = "UPDATE Config_Import SET active_import_selectors_dev = '".$state."' WHERE typename = 'selector'";
	}
	else if ($flag=='prod'){
			$sql = "UPDATE Config_Import SET active_import_selectors_prod = '".$state."' WHERE typename = 'selector'";
	}
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}

}


function db_import_selectors_active($db,$flag)
{
	$tablename='';
	if ($flag=='dev'){
	$sql = "select active_import_selectors_dev from Config_Import where typename = 'selector'";
	}
	else if ($flag=='prod'){
		$sql = "select active_import_selectors_prod from Config_Import where typename = 'selector'";
		}
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $import_state=$row[0];
	}
	$result->close();

	if ($import_state == '1') {return true;}
	else if ($import_state == '0') {return false;}
	else {return false;}
}


function db_update_chart_selectors($db,$sql)
{
	$result = $db->query($sql);
	if (!$result) {
		printf("insert failed: %s\n", $db->error);
		exit;
	}
}

function db_return_chartselector_detail($db,&$x,$lottocode)
{
	$x=0;
	$rows = array();
	$sql = "select chart_table_name,chart_type from Config_Chart_Selectors where active = '1'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['charttable']=$row[0];
		$rows[$x]['charttype']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_config_winnings_divison($db,$lottocode,&$x)
{
	$x=0;
	$rows = array();
	$sql = "select win_div,win_div_formula from Config_Winnings_Division where lottocode = '".$lottocode."' order by order_to_check asc";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['win_div']=$row[0];
	  $rows[$x]['win_div_formula']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_return_lotto_chart_detail($db,$months,$lottocode_in,$pb_ind,$chart_type)
{
	if ($chart_type == "OE") { $tablename = "Chart_Odd_Even"; $attribute = "oddevenrange";}
	else if ($chart_type == "LH") { $tablename = "Chart_Low_High"; $attribute = "lowhighrange";}
	else if ($chart_type == "LMH") { $tablename = "Chart_Low_Median_High"; $attribute = "lowmedianhighrange";}
	else if ($chart_type == "OELH") { $tablename = "Chart_Odd_Even_Low_High"; $attribute = "comborange";}
	
	$x=0;
	$rows = array();
	$sql = "select ".$attribute.", color from ".$tablename." where num_months = ".$months." and lottocode = '".$lottocode_in."' and pb_ind = '".$pb_ind."' order by drawdate desc LIMIT 1";
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['chart_range']=$row[0];
		$rows[$x]['range_color']=$row[1];
		$row = $result->fetch_row();
		 $x++;
	}
	$result->close();
	return $rows;
}


function db_insert_chart_selectors($db,$sql)
{
	$result = $db->query($sql);
	if (!$result) {
		printf("insert failed: %s\n", $db->error);
		exit;
	}
}

function db_return_lotto_hot_cold_median_detail($db,$months,$lottocode_in,$pb_ind)
{
	$x=0;
	$rows = array();
	$sql = "select hot_cold_median from Chart_Hot_Cold where num_months = ".$months." and lottocode = '".$lottocode_in."' and pb_ind = '".$pb_ind."' order by drawdate desc LIMIT 1";
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['hot_cold_median']=$row[0];
		$row = $result->fetch_row();
		 $x++;
	}
	$result->close();
	return $rows;
}


function db_return_chartselector_tablename($db,$chart_type)
{
	$tablename='';

	$sql = "select chart_table_name from Config_Chart_Selectors where active = '1' and chart_type = '".$chart_type."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $tablename=$row[0];
	}
	$result->close();
	return $tablename;
}

function db_leapyear($db,$year)
{
	$sql = "SELECT id FROM LeapYears where year =  ".$year;
	$result = $db->query($sql);
	if (!$result) {
	  return false;
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 return false;
	}
	else
	{
		$result->close();
		return true;
	}
}

function db_insert_entry_odd_even_low_high($db,$lottocode,$number_of_months,$drawdate,$date_range_min,$bucket_range_str,$bucket_val_str,$bucket_color_str,$pb_lucky_ind,$chart_type)
{
	$pb_ind = 0;
	$lucky_ind = 0;
	$tablename = 'PLACEHOLDER';
	if ($pb_lucky_ind == 'powerball') {$pb_ind = 1;}
	if ($pb_lucky_ind == 'lucky') {$lucky_ind = 1;}

	if ($chart_type == 'OE') {$tablename = 'Chart_Odd_Even'; $range_attribute = 'oddevenrange';}
	else if ($chart_type == 'LH') {$tablename = 'Chart_Low_High'; $range_attribute = 'lowhighrange';}
	else if ($chart_type == 'OELH') {$tablename = 'Chart_Odd_Even_Low_High'; $range_attribute = 'comborange';}
	else if ($chart_type == 'LMH') {$tablename = 'Chart_Low_Median_High'; $range_attribute = 'lowmedianhighrange';}
	else if ($chart_type == 'GRP') {$tablename = 'Chart_Group'; $range_attribute = 'grouprange';}
	else if ($chart_type == 'PAIR') {$tablename = 'Chart_Pair'; $range_attribute = 'pairrange';}

	$sql = "INSERT into ".$tablename." (num_months,pb_ind,lucky_ind,drawdate,val,color,".$range_attribute.",lottocode,drawdate_start)
	values (".$number_of_months.",".$pb_ind.",".$lucky_ind.",'".$drawdate."','".$bucket_val_str."','".$bucket_color_str."','".$bucket_range_str."','".$lottocode."','".$date_range_min."')";
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_return_pair_buckets($db,&$x,$lottocode)
{
	$x=0;
	$rows = array();
	$sql = "select bucket,value,color from Config_Chart_Pair";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['bucket']=$row[0];
		$rows[$x]['value']=$row[1];
		$rows[$x]['color']=$row[2];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;

}

function db_get_ball_counts_by_number_pb_lucky($db,&$x,$lottocode,$direction,$pb_lucky_ind)
{
	if ($pb_lucky_ind == 'powerball') $attribute = 'pb_count';
	else if ($pb_lucky_ind == 'lucky') $attribute = 'lucky_count';

$x=0;
	$rows = array();
	$sql = "select ".$attribute.", ball_number from ChartStats_Hot_Cold where (lottocode = '".$lottocode."') and ".$attribute." is not null order by ball_number ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_ball_frequency_pb_lucky($db,&$x,$lottocode,$direction,$pb_lucky_ind)
{
	if ($pb_lucky_ind == 'powerball') $attribute = 'pb_count';
	else if ($pb_lucky_ind == 'lucky') $attribute = 'lucky_count';
$x=0;
	$rows = array();
	$sql = "select  ".$attribute.", count(".$attribute.") from ChartStats_Hot_Cold where (lottocode = '".$lottocode."') and ".$attribute." is not null group by ".$attribute." ".$direction;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['count_sum']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_ball_frequency($db,&$x,$lottocode,$direction)
{
	$x=0;
	$rows = array();
	$sql = "select  ball_count, count(ball_count) from ChartStats_Hot_Cold where (lottocode = '".$lottocode."') and ball_count is not null group by ball_count ".$direction;
  #print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['count_sum']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

 function db_update_crawl_source($db,$rawfeed_tablename,$lottocode,$lottoname,$drawdate,$jackpot,$nextdrawdate,$drawresult,$source_crawl_description)
{
	$sql = "insert into ".$rawfeed_tablename." (lottocode,LottoName,DrawDate,Results,Jackpot,NextDrawDate,Source) values ('".$lottocode."','".$lottoname."','".$drawdate."','".$drawresult."','".$jackpot."','".$nextdrawdate."','".$source_crawl_description."')";
	//print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



function db_insert_fib_tri_calc($db,$lottocode,$drawdate,$fib,$tri,$fib_numbers,$tri_numbers,$fib_pb,$tri_pb,$fib_numbers_pb,$tri_numbers_pb,$pb)
{
	if ($pb) {
	$sql = "insert into Chart_Fib_Tri (lottocode,drawdate,fib,tri,fib_numbers,tri_numbers,fib_pb,tri_pb,fib_numbers_pb,tri_numbers_pb) values ('".$lottocode."','".$drawdate."','".$fib."','".$tri."','".$fib_numbers."','".$tri_numbers."','".$fib_pb."','".$tri_pb."','".$fib_numbers_pb."','".$tri_numbers_pb."')";
	}
	else {
	$sql = "insert into Chart_Fib_Tri (lottocode,drawdate,fib,tri,fib_numbers,tri_numbers) values ('".$lottocode."','".$drawdate."','".$fib."','".$tri."','".$fib_numbers."','".$tri_numbers."')";
	}
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



function db_insert_ball_count($db,$count,$ball_number,$lottocode)
{
	$sql = "insert into ChartStats_Hot_Cold (lottocode,ball_number,ball_count) values ('".$lottocode."','".$ball_number."','".$count."')";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_insert_ball_count_pb_lucky($db,$count,$ball_number,$lottocode,$pb_lucky_ind)
{
	if ($pb_lucky_ind == 'powerball') $attribute = 'pb_count';
	else if ($pb_lucky_ind == 'lucky') $attribute = 'lucky_count';

	$sql = "insert into ChartStats_Hot_Cold (lottocode,ball_number,".$attribute.") values ('".$lottocode."','".$ball_number."','".$count."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_update_ball_count_pb_lucky($db,$count,$ball_number,$lottocode,$pb_lucky_ind)
{
	#test if exists first
	if ($pb_lucky_ind == 'powerball') $attribute = 'pb_count';
	else if ($pb_lucky_ind == 'lucky') $attribute = 'lucky_count';

	$sql = "Select ".$attribute." from ChartStats_Hot_Cold where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: no nnnn%s\n", $db->error);
	 exit; ## does not exist so insert then
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 print("no rows..inserting..");
	 db_insert_ball_count_pb_lucky($db,$count,$ball_number,$lottocode,$pb_lucky_ind);
	}
	else
	{
		$sql = "update ChartStats_Hot_Cold set ".$attribute." = ".$count." where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
		$result = $db->query($sql);
		if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		 exit; ## does not exist so insert then
			}
	}

	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



function db_settransfer_inprogress($db,$lottocode,$type,$environment)
{
#	update LottoName set api_active_charts_transfer_in_progress_dev = 0
#	update LottoName set api_active_charts_transfer_in_progress_prod = 0
#	update LottoName set api_active_selectors_transfer_in_progress_dev = 0
#	update LottoName set api_active_selectors_transfer_in_progress_prod = 0
if ($type == 'chart')
{
	if ($environment == 'dev'){
	$sql = "UPDATE LottoName SET api_active_charts_transfer_in_progress_dev = '1' WHERE lottocode = '".$lottocode."'";
	}
	else if ($environment == 'prod'){
		$sql = "UPDATE LottoName SET api_active_charts_transfer_in_progress_prod = '1' WHERE lottocode = '".$lottocode."'";
		}
}
else if ($type == 'selector')
{
	if ($environment == 'dev'){
	$sql = "UPDATE LottoName SET api_active_selectors_transfer_in_progress_dev = '1' WHERE lottocode = '".$lottocode."'";
	}
	else if ($environment == 'prod'){
		$sql = "UPDATE LottoName SET api_active_selectors_transfer_in_progress_prod = '1' WHERE lottocode = '".$lottocode."'";
		}
}
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}

function db_settransfer_complete($db,$lottocode,$type,$environment)
{
#	update LottoName set api_active_charts_transfer_in_progress_dev = 0
#	update LottoName set api_active_charts_transfer_in_progress_prod = 0
#	update LottoName set api_active_selectors_transfer_in_progress_dev = 0
#	update LottoName set api_active_selectors_transfer_in_progress_prod = 0
if ($type == 'chart')
{
	if ($environment == 'dev'){
	$sql = "UPDATE LottoName SET api_active_charts_transfer_in_progress_dev = '0' WHERE lottocode = '".$lottocode."'";
	}
	else if ($environment == 'prod'){
		$sql = "UPDATE LottoName SET api_active_charts_transfer_in_progress_prod = '0' WHERE lottocode = '".$lottocode."'";
		}
}
else if ($type == 'selector')
{
	if ($environment == 'dev'){
	$sql = "UPDATE LottoName SET api_active_selectors_transfer_in_progress_dev = '0' WHERE lottocode = '".$lottocode."'";
	}
	else if ($environment == 'prod'){
		$sql = "UPDATE LottoName SET api_active_selectors_transfer_in_progress_prod = '0' WHERE lottocode = '".$lottocode."'";
		}
}
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}

function db_update_queue_crawl($db,$id,$status,$Queue_Table,$html_result)
{
	$html_result_prep = '';
	$html_result_prep = mysqli_real_escape_string($db,$html_result);
	$sql = "UPDATE ".$Queue_Table." SET processed = '".$status."', response_raw = '".$html_result_prep."'  WHERE id = '".$id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}

function db_update_queue_crawl_draw($db,$id,$Queue_Table,$drawdate,$result)
{
	//$html_result_prep = '';
	//$html_result_prep = mysqli_real_escape_string($db,$parsed_result);
	$sql = "UPDATE ".$Queue_Table." SET processed_html = '1', drawdate='".$drawdate."', drawresult = '".$result."'  WHERE id = '".$id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}


function db_update_queue($db,$id,$status,$Queue_Table)
{
	$sql = "UPDATE ".$Queue_Table." SET processed = '".$status."' WHERE id = '".$id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}

function db_get_queue_items_to_process($db,&$x,$Queue_Table)
{
	$x=0;
	$rows = array();
	$sql = "SELECT id, lottocode,url FROM ".$Queue_Table." where processed = '0' or processed is null ORDER BY id asc";
 // print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		printf("Query failed: %s\n", $db->error);
		return false;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['id']=$row[0];
		$rows[$x]['lottocode']=$row[1];
		$rows[$x]['url']=$row[2];
		$row = $result->fetch_row();
		$x++;
	}
	$result->close();
	return $rows;
}


function db_get_queue_items_to_process_html($db,&$x,$Queue_Table)
{
	$x=0;
	$rows = array();
	$sql = "SELECT id, lottocode,response_raw FROM ".$Queue_Table." where processed_html = '0' or processed_html is null ORDER BY id asc";
 // print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		printf("Query failed: %s\n", $db->error);
		return false;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['id']=$row[0];
		$rows[$x]['lottocode']=$row[1];
		$rows[$x]['response_raw']=$row[2];
		$row = $result->fetch_row();
		$x++;
	}
	$result->close();
	return $rows;
}

function db_return_queue_detail($db,&$x,$lottocode)
{
	$x=0;
	$rows = array();
	$sql = "SELECT charcode, queuename FROM Config_Chart_Queues WHERE active = '1'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['charcode']=$row[0];
		$rows[$x]['queuename']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_return_queue_detail_crawl($db,&$x,$lottocode)
{
	$x=0;
	$rows = array();
	$sql = "SELECT charcode, queuename FROM Config_Chart_Queues WHERE active_crawl = '1'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['charcode']=$row[0];
		$rows[$x]['queuename']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}


function db_get_count_all($db)
{
	$sql = "SELECT count(*) FROM LottoName where (active = 1 and countrycode <> 'ALL')";
	$fav_count = db_return_one_param($db,$sql);
	return($fav_count);
}

#
#results_main.php
function db_get_count_country($db,$countrycode_in)
{
	$sql = "SELECT count(*) FROM LottoName where (active = 1 and countrycode = '".$countrycode_in."')";
	$fav_count = db_return_one_param($db,$sql);
	return($fav_count);
}


function db_insert_queue($db,$url,$lottocode,$queuename)
{
	$sql = "INSERT into ".$queuename." (lottocode,url) VALUES ('".$lottocode."','".$url."')";

	$result = $db->query($sql);
	if (!$result) {
		printf("insert failed: %s\n", $db->error);
		return false;
	}
return true;
}

function db_return_lotto_table_processing_detail($db,&$x,$lottotable,$ind,$chart_type)
{
	if ($chart_type == "HC") { $attribute = "queue_calc_HC";}
	else if ($chart_type == "OE") { $attribute = "queue_calc_OE";}
	else if ($chart_type == "LH") { $attribute = "queue_calc_LH";}
	else if ($chart_type == "OELH") { $attribute = "queue_calc_OELH";}
	else if ($chart_type == "LMH") { $attribute = "queue_calc_LMH";}
	else if ($chart_type == "GRP") { $attribute = "queue_calc_GRP";}
	else if ($chart_type == "PAIR") { $attribute = "queue_calc_PAIR";}
	else if ($chart_type == "SELECTORS") { $attribute = "queue_calc_selectors";}
	else if ($chart_type == "SELECTORS-SUMMARY") { $attribute = "queue_calc_selectors_summary";}
	else if ($chart_type == "FIBONACCI-TRIANGULAR") { $attribute = "queue_calc_FIB_TRI";}	
	$x=0;
	$rows = array();
	$sql = "SELECT drawdate FROM ".$lottotable." WHERE ".$attribute." = '".$ind."' OR ".$attribute." is null ORDER BY drawdate desc LIMIT 1";
  #print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		printf("Query failed: %s\n", $db->error);
		return false;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['drawdate']=substr($row[0],0,10);
		$row = $result->fetch_row();
		$x++;
	}
	$result->close();
	return $rows;
}


function db_get_months_config($db,&$x)
{
	$x=0;
	$rows = array();
	$sql = "SELECT value FROM Config_Chart_Periods WHERE param = 'var_month'";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		printf("Query failed: %s\n", $db->error);
		exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['months']=$row[0];
		$row = $result->fetch_row();
		$x++;
	}
	$result->close();
	return $rows;
}

#results_main.php
# NOTE PARAM $X returns a val as well - count!
function db_return_countries($db,&$x)
{
	$x=0;
	$rows = array();
	$sql = "SELECT distinct country, countrycode FROM LottoName order by country";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['country']=$row[0];
	  $rows[$x]['countrycode']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

#results_main.php
function db_return_countries_detail($db,&$x,$countrycode_in)
{
	$x=0;
	$rows = array();
	if ($countrycode_in == "ALL")
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2  FROM LottoName where (active = '1' and countrycode <> 'ALL') order by lottonamedisplay";
		}
		else
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
		where (active = 1 and countrycode = '".$countrycode_in."') order by lottonamedisplay";

		}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoCode']=$row[0];
	  $rows[$x]['lottonamedisplay']=$row[1];
	  $rows[$x]['jackpot']=$row[2];
	  $rows[$x]['currency_symbol']=$row[3];
	  $rows[$x]['lottocountrycode']=$row[4];
	  $rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
	  $rows[$x]['drawdate']=substr($row[6],0,10);
	  $rows[$x]['Lotto_Table']=$row[7];
	  $rows[$x]['ball1_active']=$row[8];
	  $rows[$x]['ball2_active']=$row[9];
	  $rows[$x]['ball3_active']=$row[10];
	  $rows[$x]['ball4_active']=$row[11];
	  $rows[$x]['ball5_active']=$row[12];
	  $rows[$x]['ball6_active']=$row[13];
	  $rows[$x]['ball7_active']=$row[14];
	  $rows[$x]['bonus_active']=$row[15];
	  $rows[$x]['powerball_active']=$row[16];
	  $rows[$x]['lucky1_active']=$row[17];
	  $rows[$x]['lucky2_active']=$row[18];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}
#
#
#
#
#
function db_return_language_detail($db,&$x,$langcode,$page)
{
	$x=0;
	#$rows = array();
	$rows = '';

	#$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
	#	where (active = 1 and countrycode = '".$countrycode_in."') order by lottonamedisplay";
  $sql = "SELECT var,".$langcode." from Config_Language where langcode = '".$langcode."' and page = '".$page."'";
	print($rows);
	exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x][$row[0]]=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}



function db_return_countries_detail_spectrum_ZAL($db,&$x,$countrycode_in)
{
	$x=0;
	$rows = array();
	if ($countrycode_in == "ALL")
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2  FROM LottoName where (active = '1' and countrycode <> 'ALL') order by lottonamedisplay";
		}
		else
		{
		$sql = "SELECT lottoCode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonus,powerball,lucky1,lucky2 FROM LottoName
		where (active = '1' and countrycode = '".$countrycode_in."') order by lottonamedisplay";

		}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoCode']=$row[0];
	  $rows[$x]['lottonamedisplay']=$row[1];
	  $rows[$x]['jackpot']=$row[2];
	  $rows[$x]['currency_symbol']=$row[3];
	  $rows[$x]['lottocountrycode']=$row[4];
	  $rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
	  $rows[$x]['drawdate']=substr($row[6],0,10);
	  $rows[$x]['Lotto_Table']=$row[7];
	  $rows[$x]['ball1_active']=$row[8];
	  $rows[$x]['ball2_active']=$row[9];
	  $rows[$x]['ball3_active']=$row[10];
	  $rows[$x]['ball4_active']=$row[11];
	  $rows[$x]['ball5_active']=$row[12];
	  $rows[$x]['ball6_active']=$row[13];
	  $rows[$x]['ball7_active']=$row[14];
	  $rows[$x]['bonus_active']=$row[15];
	  $rows[$x]['powerball_active']=$row[16];
	  $rows[$x]['lucky1_active']=$row[17];
	  $rows[$x]['lucky2_active']=$row[18];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_mail_results_multiple.php
function db_get_lottonames($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottoname,lottocode FROM LottoName WHERE active = 1 AND active_tester_transfer_results = '1'";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_lottonames_crawler($db,&$x,$flag)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottocode,lottoname_slug,source_url,drawdate_slug,country_slug FROM Config_CrawlReader WHERE active = '1'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottocode']=$row[0];
	  $rows[$x]['lottoname_slug']=$row[1];
	  $rows[$x]['source_url']=$row[2];
	  $rows[$x]['drawdate_slug']=$row[3];
	  $rows[$x]['country_slug']=$row[4];
	
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}
function db_get_lottonames_api_results_only($db,&$x,$flag)
{
	$x=0;
	$rows = array();
	if ($flag=='dev'){
		$sql = "SELECT lottoname,lottocode FROM LottoName WHERE active = '1' AND active_tester_transfer_results = '1' AND api_active_dev = '1'";}
	else if ($flag=='prod'){
		$sql = "SELECT lottoname,lottocode FROM LottoName WHERE active = '1' AND active_tester_transfer_results = '1' AND api_active_prod = '1'";}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}


function db_get_lottonames_api($db,&$x,$flag)
{
	$x=0;
	$rows = array();
	if ($flag=='dev'){
		$sql = "SELECT lottoname,lottocode,api_active_charts_transfer_in_progress_dev,api_active_charts_transfer_in_progress_prod,api_active_selectors_transfer_in_progress_dev,api_active_selectors_transfer_in_progress_prod, powerball FROM LottoName WHERE active = '1' AND active_tester_transfer_results = '1' AND active_transfer_charts = '1' AND api_active_dev = '1'";}
	else if ($flag=='prod'){
		$sql = "SELECT lottoname,lottocode,api_active_charts_transfer_in_progress_dev,api_active_charts_transfer_in_progress_prod,api_active_selectors_transfer_in_progress_dev,api_active_selectors_transfer_in_progress_prod,powerball FROM LottoName WHERE active = '1' AND active_tester_transfer_results = '1' AND active_transfer_charts = '1'AND api_active_prod = '1'";}

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];
	  $rows[$x]['api_active_charts_transfer_in_progress_dev']=$row[2];
	  $rows[$x]['api_active_charts_transfer_in_progress_prod']=$row[3];
	  $rows[$x]['api_active_selectors_transfer_in_progress_dev']=$row[4];
	  $rows[$x]['api_active_selectors_transfer_in_progress_prod']=$row[5];
	  $rows[$x]['powerball']=$row[6];


	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_lottonames_generate_chart($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottoname,lottocode FROM LottoName WHERE active = '1' AND active_tester_transfer_results = 1 AND generate_chart_active = 1";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

#
function db_get_date_start($db,$LottoName,$date_range_min)
{
	$sql = "select drawdate from ".$LottoName." where drawdate >= '".$date_range_min."' order by drawdate asc LIMIT 1";
	$x=0;
	$startdate = '';

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $startdate=$row[0];
	}
	$result->close();
	return $startdate;
}


#tools-hot-cold-generator-new-calc.php
function db_get_count_ball_count($db,$sql)
{
	$x=0;
	$count_val = 0;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $count_val=$row[0];
	}
	$result->close();
	return $count_val;
}
#PASSED
#tools-hot-cold-generator-new-calc.php
function db_update_ball_count($db,$count,$ball_number,$lottocode)
{
	#test if exists fitst
	$sql = "Select ball_count from ChartStats_Hot_Cold where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: no nnnn%s\n", $db->error);
	 exit; ## does not exist so insert then
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 print("no rows..inserting..");
	 db_insert_ball_count($db,$count,$ball_number,$lottocode);
	}
	else
	{
		$sql = "update ChartStats_Hot_Cold set ball_count = ".$count." where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
		$result = $db->query($sql);
		if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		 exit; ## does not exist so insert then
			}
	}

	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
function db_update_ball_count_pb($db,$count,$ball_number,$lottocode)
{
	#test if exists fitst
	$sql = "Select pb_count from ChartStats_Hot_Cold where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
	#print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: no nnnn%s\n", $db->error);
	 exit; ## does not exist so insert then
	}
	$row = $result->fetch_row();
	if (!$row)
	{
	 print("no rows..inserting..");
	 db_insert_ball_count_pb($db,$count,$ball_number,$lottocode);
	}
	else
	{
		$sql = "update ChartStats_Hot_Cold set pb_count = ".$count." where ball_number = ".$ball_number." and lottocode = '".$lottocode."'" ;
		$result = $db->query($sql);
		if (!$result) {
		  printf("Query failed: %s\n", $db->error);
		 exit; ## does not exist so insert then
			}
	}

	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function db_insert_api_queueLM($db,$lottocode,$url,$targetsite)
{
	#$targetsite ='LM.UK';
	$sql = "insert into Queue_API_OutBound (lottocode,url,targetsite) values ('".$lottocode."','".$url."','".$targetsite."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$sql = "select id from Queue_API_OutBound order by id desc limit 1";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $id=$row[0];
	}
	$result->close();
	#print("id:"); print($id);
	return $id;
}

function db_insert_api_queue($db,$lottocode,$url,$flag)
{
	$sql = "insert into Queue_API_OutBound (lottocode,url,env) values ('".$lottocode."','".$url."','".$flag."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$sql = "select id from Queue_API_OutBound order by id desc limit 1";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $id=$row[0];
	}
	$result->close();
	#print("id:"); print($id);
	return $id;
}

function db_insert_api_queueV2($db,$lottocode,$url,$flag,$params)
{
	$sql = "insert into Queue_API_OutBound (lottocode,url,env,params,type) values ('".$lottocode."','".$url."','".$flag."','".$params."','POST')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$sql = "select id from Queue_API_OutBound order by id desc limit 1";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $id=$row[0];
	}
	$result->close();
	#print("id:"); print($id);
	return $id;
}


function db_update_api_queue($db,$id,$return_code)
{
	$sql = "update Queue_API_OutBound set processed = ".$return_code." where id = ".$id;
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
}


#tools-hot-cold-generator-new-calc.php
function db_insert_ball_count_pb($db,$count,$ball_number,$lottocode)
{
	$sql = "insert into ChartStats_Hot_Cold (lottocode,ball_number,pb_count) values ('".$lottocode."','".$ball_number."','".$count."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#tools-hot-cold-generator-new-calc.php


function db_insert_entry_hot_cold($db,$lottocode,$number_of_months,$drawdate,$strCOMMA_val,$strCOMMA_color,$count_min_max,$pb_lucky_ind,$date_range_min)
{
	$pb_ind = 0;
	$lucky_ind = 0;
	if ($pb_lucky_ind == 'powerball') {$pb_ind = 1;}
	if ($pb_lucky_ind == 'lucky') {$lucky_ind = 1;}

	$sql = "INSERT INTO Chart_Hot_Cold (num_months,pb_ind,lucky_ind,drawdate,val,color,hot_cold_median,lottocode,drawdate_start) VALUES (".$number_of_months.",".$pb_ind.",".$lucky_ind.",'".$drawdate."','".$strCOMMA_val."','".$strCOMMA_color."','".$count_min_max."','".$lottocode."','".$date_range_min."')";
	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



#tools-hot-cold-generator-new-calc.php
function db_get_ball_counts($db,$lottocode,$direction)
{
$x=0;
	$rows = array();
	$sql = "select ball_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' order by ball_count ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}
function db_get_ball_counts_pb($db,$lottocode,$direction)
{
$x=0;
	$rows = array();
	$sql = "select pb_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' and pb_count is not null order by ball_count ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}




function db_get_ball_counts_by_number($db,&$x,$lottocode,$direction)
{
	$x=0;
	$rows = array();
	$sql = "select ball_count, ball_number from ChartStats_Hot_Cold where lottocode = '".$lottocode."' order by ball_number ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}

function db_get_ball_counts_by_number_pb($db,&$x,$lottocode,$direction)
{
	$x=0;
	$rows = array();
	$sql = "select pb_count, ball_number from ChartStats_Hot_Cold where (lottocode = '".$lottocode."') and pb_count is not null order by ball_number ".$direction;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['ball_count']=$row[0];
	  $rows[$x]['ball_number']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}


#etl/transfer_fix.php
function db_get_lottonames_9($db,&$x)
{
	$x=0;


	$rows = array();

	$sql = "select lottoname,lottocode,lottotable from LottoName where active = '1' and active_tester_transfer_results = 9";


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottoname']=$row[0];
	  $rows[$x]['lottocode']=$row[1];
	  $rows[$x]['lottotable']=$row[2];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_mail_results.php
function db_get_lotto_detail_f1($db,&$x,$lotto_name)
{
	$x=0;
	$rows = array();

	$sql	 = "select id,LottoName,DrawDate,DrawNumber,Results,Jackpot,drawdate_convert from ResultsRawEmailFeed_Master where LottoName = '".$lotto_name."' and (transferred is null or transferred = 0)";
	#print($sql);


	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $rows[$x]['DrawDate']=$row[2];
	  $rows[$x]['DrawNumber']=$row[3];
	  $rows[$x]['Results']=$row[4];
	  $rows[$x]['Jackpot']=$row[5];
	  $rows[$x]['drawdate_convert']=$row[6];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


function db_get_lotto_detail_source4($db,&$x,$lotto_name)
{
	$x=0;
	$rows = array();

	$sql	 = "select id,LottoName,DrawDate,DrawNumber,Results,Jackpot from ResultsRawEmailFeed_Source4_import where LottoName = '".$lotto_name."' and (transferred is null or transferred = 0) order by id";
	#print($sql);

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $rows[$x]['DrawDate']=$row[2];
	  $rows[$x]['DrawNumber']=$row[3];
	  $rows[$x]['Results']=$row[4];
	  $rows[$x]['Jackpot']=$row[5];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}

function db_get_lotto_detail_source2($db,&$x,$lotto_name)
{
	$x=0;
	$rows = array();

	$sql	 = "select id,LottoName,DrawDate,DrawNumber,Results,Jackpot from ResultsRawEmailFeed_Source2 where LottoName = '".$lotto_name."' and (transferred is null or transferred = '0') order by id";
	#print($sql);

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $rows[$x]['DrawDate']=$row[2];
	  $rows[$x]['DrawNumber']=$row[3];
	  $rows[$x]['Results']=$row[4];
	  $rows[$x]['Jackpot']=$row[5];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_mail_results.php
function db_get_lotto_detail_f2($db,&$x,$LottoName)
{
	$x=0;
	$rows = array();

	//$sql = "select LottoTable, ball5,ball6,ball7,bonus,powerball,lucky1,lucky2,lottocode from LottoName where lottoname = '".$LottoName."'";
	$sql = "select LottoTable, ball4,ball5,ball6,ball7,ball8,ball9,ball10,ball11,ball12,ball13,ball14,ball15,bonus,bonus2,bonus3,bonus4,powerball,powerball2,lucky1,lucky2,lottocode from LottoName where lottoname = '".$LottoName."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['LottoTable']=$row[0];
	  $rows[$x]['ball4']=$row[1];
	  $rows[$x]['ball5']=$row[2];
	  $rows[$x]['ball6']=$row[3];
	  $rows[$x]['ball7']=$row[4];
	  $rows[$x]['ball8']=$row[5];
	  $rows[$x]['ball9']=$row[6];
	  $rows[$x]['ball10']=$row[7];
	  $rows[$x]['ball11']=$row[8];
	  $rows[$x]['ball12']=$row[9];
	  $rows[$x]['ball13']=$row[10];
	  $rows[$x]['ball14']=$row[11];
	  $rows[$x]['ball15']=$row[12];
	  $rows[$x]['bonus']=$row[13];
	  $rows[$x]['bonus2']=$row[13];
	  $rows[$x]['bonus3']=$row[13];
	  $rows[$x]['bonus4']=$row[13];
	  $rows[$x]['powerball']=$row[14];
	  $rows[$x]['powerball2']=$row[15];
	  $rows[$x]['lucky1']=$row[16];
	  $rows[$x]['lucky2']=$row[17];
	  $rows[$x]['lottocode']=$row[18];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


#etl/transfer_fix.php
function db_get_lotto_detail_f3($db,&$x,$LottoTable)
{
	$x=0;
	$rows = array();

	$sql = "select drawno,drawdate from ".$LottoTable." order by drawdate";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawno']=$row[0];
	  $rows[$x]['drawdate']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}



function get_date_range_HC($db,$lottocode_in,&$x)
{
	$x=0;
	$rows = array();

	$sql = "select num_months,drawdate,drawdate_start from Chart_Hot_Cold where lottocode = '".$lottocode_in."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['num_months']=$row[0];
	  $rows[$x]['drawdate']=$row[1];
	  $rows[$x]['drawdate_start']=$row[2];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}




#get_results_from_email4.php
function db_return_lotto_detail($db,&$x,$lottocode_in)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottocode, lottonamedisplay, estimatedjackpot, currency_symbol, countrycode, country, drawdate,lottotable,ball1,ball2,ball3,ball4,ball5,ball6,ball7,ball8,ball9,ball10,ball11,ball12,ball13,ball14,ball15,bonus,bonus2,bonus3,bonus4,powerball,powerball2,lucky1,lucky2,ball_max, ball_max_pb,ball_max_lucky,lottoname,scheduled_draws FROM LottoName WHERE lottocode = '".$lottocode_in."'";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		printf("Query failed: %s\n", $db->error);
		exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
		$rows[$x]['lottoCode']=$row[0];
		$rows[$x]['lottonamedisplay']=$row[1];
		$rows[$x]['jackpot']=$row[2];
		$rows[$x]['currency_symbol']=$row[3];
		$rows[$x]['lottocountrycode']=$row[4];
		$rows[$x]['LABEL_COUNTRY_LOTTO']=$row[5];
		$rows[$x]['drawdate']=substr($row[6],0,10);
		$rows[$x]['Lotto_Table']=$row[7];
		$rows[$x]['ball1_active']=$row[8];
		$rows[$x]['ball2_active']=$row[9];
		$rows[$x]['ball3_active']=$row[10];
		$rows[$x]['ball4_active']=$row[11];
		$rows[$x]['ball5_active']=$row[12];
		$rows[$x]['ball6_active']=$row[13];
		$rows[$x]['ball7_active']=$row[14];
		$rows[$x]['ball8_active']=$row[15];
		$rows[$x]['ball9_active']=$row[16];
		$rows[$x]['ball10_active']=$row[17];
		$rows[$x]['ball11_active']=$row[18];
		$rows[$x]['ball12_active']=$row[19];
		$rows[$x]['ball13_active']=$row[20];
		$rows[$x]['ball14_active']=$row[21];
		$rows[$x]['ball15_active']=$row[22];
		$rows[$x]['bonus_active']=$row[23];
		$rows[$x]['bonus2_active']=$row[24];
		$rows[$x]['bonus3_active']=$row[25];
		$rows[$x]['bonus4_active']=$row[26];
		$rows[$x]['powerball_active']=$row[27];
		$rows[$x]['powerball2_active']=$row[28];
		$rows[$x]['lucky1_active']=$row[29];
		$rows[$x]['lucky2_active']=$row[30];
		$rows[$x]['ball_max']=$row[31];
		$rows[$x]['ball_max_pb']=$row[32];
		$rows[$x]['ball_max_lucky']=$row[33];
		$rows[$x]['lottoname']=$row[34];
		$rows[$x]['scheduled_draws']=$row[35];

		$row = $result->fetch_row();
		$x++;
	}
	$result->close();

	return $rows;
}

#PASSED2
#getData-HC.php
function db_return_chart_HC_detail($db,&$x,$lottocode_in,$months)
{
	$x=0;
	$rows = array();

	$sql = "select drawdate,val,color,min_max from Chart_Hot_Cold where lottocode = '".$lottocode_in."' and num_months = '".$months."' and pb_ind = 0 order by drawdate desc LIMIT 1";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawdate']=$row[0];
	  $rows[$x]['val']=$row[1];
	  $rows[$x]['color']=$row[2];
	  $rows[$x]['min_max']=$row[3];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}
#getData-HC.php
function db_return_chart_HC_detail_pb($db,&$x,$lottocode_in,$months)
{
	$x=0;
	$rows = array();
	$sql = "select drawdate,val,color,min_max from Chart_Hot_Cold where lottocode = '".$lottocode_in."' and num_months = '".$months."' and pb_ind = 1 order by drawdate desc LIMIT 1";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['drawdate']=$row[0];
	  $rows[$x]['val']=$row[1];
	  $rows[$x]['color']=$row[2];
	  $rows[$x]['min_max']=$row[3];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}


# process-signin-form.php

function db_return_account_detail_signin($db,$email,&$return_code)
{
	$return_code = 0;
	$rows = array();
	$sql = "SELECT firstname, surname, active, account_id, MD5(UNIX_TIMESTAMP() + account_id + RAND(UNIX_TIMESTAMP())) sGUID, pwd_hash, sGUIDRegister FROM Accounts WHERE email = '$email'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $rows['firstname']=$row[0];
	  $rows['surname']=$row[1];
	  $rows['active']=$row[2];
	  $rows['account_id']=$row[3];
	  $rows['sGUID']=$row[4];
	  $rows['pwd_hash']=$row[5];
		$rows['sGUIDRegister'] = $row[6];
	  $return_code = 1;
	}
	return $rows;
}

# api_transfer_results_all_LMUK.php
function  db_update_ResultsAPI_LMUK($db,$LottoTable,$Results_table_id)
{
	$sql = "Update ".$LottoTable." Set transferred_to_LMUK = '1' Where id = ".$Results_table_id;
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

# api_transfer_results_all.php
function  db_update_ResultsAPI($db,$LottoTable,$Results_table_id,$flag)
{
	if ($flag=='dev'){
		$sql = "Update ".$LottoTable." Set transferred_to_CMS_dev = '1' Where id = ".$Results_table_id;}
	else if ($flag=='prod'){
	$sql = "Update ".$LottoTable." Set transferred_to_CMS_prod = '1' Where id = ".$Results_table_id;}
	
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function  db_update_Results_Fib($db,$LottoTable,$Results_table_id,$flag)
{
	if ($flag=='dev'){
		$sql = "Update ".$LottoTable." Set transferred_to_CMS_dev = '1' Where id = ".$Results_table_id;}
	else if ($flag=='prod'){
	$sql = "Update ".$LottoTable." Set transferred_to_CMS_prod = '1' Where id = ".$Results_table_id;}
	
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

# process-signin-form.php
function db_update_accounts($db,$account_id,$sGUID)
{
	$sql = "Update Accounts Set sGUID = '".$sGUID."' Where account_id = '".$account_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


# process-signin-form.php
function db_update_audit($db,$account_id,$sGUID,$ipaddress)
{
	$Description = 'Logging On. SessionID ='.$sGUID;
	$sql = "insert into Audit (account_id,description,logdate,type,ipaddress) values ('".$account_id."','".$Description."',NOW(),'1','".$ipaddress."')";

	$result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#PASSED3 
#etl/transfer_mail_Results.php

function db_insert_lottoresults_15balls($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$ball8,$ball9,$ball10,$ball11,$ball12,$ball13,$ball14,$ball15,$new_jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,ball8,ball9,ball10,ball11,ball12,ball13,ball14,ball15,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$ball8."','".$ball9."','".$ball10."','".$ball11."','".$ball12."','".$ball13."','".$ball14."','".$ball15."','".$new_jackpot."')";
	//print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
			

function db_insert_lottoresults_6balls_bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonus,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{

	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonus."','".$jackpot."')";
	//print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_insert_lottoresults_5balls_bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$bonus,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,bonusball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$bonus."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function  db_insert_lottoresults_7balls_bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$bonus,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$bonus."','".$jackpot."')";
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function  db_insert_lottoresults_7balls_4bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$bonusball1,$bonusball2,$bonusball3,$bonusball4,$ball1_2,$ball2_2,$ball3_2,$ball4_2,$ball5_2,$ball6_2,$ball7_2,$bonusball1_2,$bonusball2_2,$bonusball3_2,$bonusball4_2,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball,bonusball2,bonusball3,bonusball4,ball1_2,ball2_2,ball3_2,ball4_2,ball5_2,ball6_2,ball7_2,bonusball_2,bonusball2_2,bonusball3_2,bonusball4_2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$bonusball1."','".$bonusball2."','".$bonusball3."','".$bonusball4."','".$ball1_2."','".$ball2_2."','".$ball3_2."','".$ball4_2."','".$ball5_2."','".$ball6_2."','".$ball7_2."','".$bonusball1_2."','".$bonusball2_2."','".$bonusball3_2."','".$bonusball4_2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}



function db_insert_lottoresults_6balls_only($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_6balls($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball1_2,$ball2_2,$ball3_2,$ball4_2,$ball5_2,$ball6_2_new,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball1_2,ball2_2,ball3_2,ball4_2,ball5_2,ball6_2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball1_2."','".$ball2_2."','".$ball3_2."','".$ball4_2."','".$ball5_2."','".$ball6_2_new."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_only($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
#etl/transfer_mail_Results.php
function db_insert_lottoresults_4balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$powerball,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$powerball,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$powerball,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_insert_lottoresults_7balls_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$powerball,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
#etl/transfer_mail_Results.php
function db_insert_lottoresults_5balls_2lucky($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$lucky1,$lucky2,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,powerball,powerball2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$lucky1."','".$lucky2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
#PASSED
#etl/transfer_mail_Results.php
# duplicate params - jackpot   - 8 ju 2018  php 7 ?
#function db_insert_lottoresults_6balls_2supplementary($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$supplementary1,$supplementary2,$jackpot,$jackpot)
function db_insert_lottoresults_6balls_2supplementary($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$supplementary1,$supplementary2,$jackpot1)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,supplementary1,supplementary2,nextestimatedjackpot) 
	values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$supplementary1."','".$supplementary2."','".$jackpot1."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_bonus_powerball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonus,$powerball,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,powerball,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonus."','".$powerball."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_6balls_2bonusball($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$bonusball1,$bonusball2,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,bonusball,bonusball2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$bonusball1."','".$bonusball2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
	return true;
}


#etl/transfer_mail_Results.php
function db_insert_lottoresults_7balls_2supplementary($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$supplementary1,$supplementary2,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,supplementary1,supplementary2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$supplementary1."','".$supplementary2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_insert_lottoresults_7balls_2bonus($db,$LottoTable,$DrawNumber,$new2_emaildate,$ball1_new,$ball2,$ball3,$ball4,$ball5,$ball6,$ball7,$supplementary1,$supplementary2,$jackpot)
{
	if (!db_draw_exists($db,$LottoTable,$new2_emaildate))
	{
	$sql = "insert into ".$LottoTable." (drawno,drawdate,ball1,ball2,ball3,ball4,ball5,ball6,ball7,bonusball,bonusball2,nextestimatedjackpot) values ('".$DrawNumber."','".$new2_emaildate."','".$ball1_new."','".$ball2."','".$ball3."','".$ball4."','".$ball5."','".$ball6."','".$ball7."','".$supplementary1."','".$supplementary2."','".$jackpot."')";
	#print($sql);
	#exit;
	$result = $db->query($sql);
	if (!$result) {
		$dberror = $db->error;
		printf("insert failed: %s\n", $db->error);
		$spos = 0; 
		$spos = strpos($dberror,'uplicate entry');
		if ($spos > 0) { 
			#do nothing - drawdate already in
		} else {
			printf("exit.");
			exit;
		}
	}
}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_update_jackpot($db,$lottocode,$jackpot)
{
	 $sql = "update LottoName set estimatedjackpot = '".$jackpot."' where lottocode = '".$lottocode."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}


function db_update_lottotable($db,$LottoTable,$DrawDate,$status,$chart_type)
{
	if ($chart_type == "HC") { $attribute = "queue_calc_HC";}
	else if ($chart_type == "OE") { $attribute = "queue_calc_OE";}
	else if ($chart_type == "LH") { $attribute = "queue_calc_LH";}
	else if ($chart_type == "OELH") { $attribute = "queue_calc_OELH";}
	else if ($chart_type == "LMH") { $attribute = "queue_calc_LMH";}
	else if ($chart_type == "GRP") { $attribute = "queue_calc_GRP";}
	else if ($chart_type == "PAIR") { $attribute = "queue_calc_PAIR";}
	else if ($chart_type == "SELECTORS") { $attribute = "queue_calc_selectors";}
	else if ($chart_type == "SELECTORS-SUMMARY") { $attribute = "queue_calc_selectors_summary";}
	else if ($chart_type == "FIBONACCI-TRIANGULAR") { $attribute = "queue_calc_FIB_TRI";}
	
	$sql = "UPDATE ".$LottoTable." Set ".$attribute." = '".$status."' WHERE drawdate = '".$DrawDate."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  return false;
	}
	return true;
}

#etl/transfer_fix.php
/* decom dg 13 jun 18
function db_update_lottotable($db,$LottoTable,$drawno,$id,$drawdate)
{
	 $sql = "update ".$LottoTable." set id = '".$id."' where drawno = '".$drawno."' and drawdate = '".$drawdate."'";
	 $sql = "update ".$LottoTable." set id = '".$id."' where drawdate = '".$drawdate."'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}
*/

#etl/transfer_mail_Results.php
function db_update_RawEmailFeed($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/transfer_mail_Results.php
function db_update_RawEmailFeed_Master($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed_Master set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_update_RawEmailFeed_Source4($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed_Source4_import set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

function db_update_RawEmailFeed_Source2($db,$RawFeed_id)
{
	 $sql = "update ResultsRawEmailFeed_Source2 set transferred = 1 where id = '".$RawFeed_id."'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	#$result = mysql_query($sQuery) or die("could not retrieve record");
}

#etl/get_results_from_email.php

function db_log_incoming_message($db,$LottoName, $DrawDate, $EmailDate, $EmailSubject, $EmailBody,$Source,$Results,$DrawNumber,$Jackpot,$rawfeed_tablename,$drawdate_convert)
{

	  # $time = time();
	  $sql = "insert into ".$rawfeed_tablename." (LottoName, DrawDate, EmailDate, EmailSubject, EmailBody,Source,Results,DrawNumber,Jackpot,drawdate_convert) values ('".$LottoName."', '".$DrawDate."', '".$EmailDate."', '".$EmailSubject."', '".$EmailBody."','".$Source."','".$Results."','".$DrawNumber."','".$Jackpot."','".$drawdate_convert."')";
	 $result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}
}
function db_log_incoming_message_error($db,$LottoName, $DrawDate, $EmailDate, $EmailSubject, $EmailBody,$Source,$Results,$DrawNumber,$Jackpot)
{
	  # $time = time();
	  $sql = "insert into ResultsRawEmailFeedExceptionLog (LottoName, DrawDate, EmailDate, EmailSubject, EmailBody,Source,Results,DrawNumber,Jackpot) values ('".$LottoName."', '".$DrawDate."', '".$EmailDate."', '".$EmailSubject."', '".$EmailBody."','".$Source."','".$Results."','".$DrawNumber."','".$Jackpot."')";
	 $result = $db->query($sql);
	if (!$result) {
	  printf("insert failed: %s\n", $db->error);
	  exit;
	}


}
##charts.php


function db_return_name($db,$lottocode_in)
{

	$displayname = '';

	$sql = "SELECT lottonamedisplay from LottoName WHERE lottocode = '$lottocode_in'";
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $displayname=$row[0];
	}

	$result->close();

	return $displayname;
}



function db_get_rows_to_process($db,$typename,$haspowerball)
{
	$rows_to_process = 0;
	if ($haspowerball) { 
		$sql = "select rows_at_a_time_pp from Config_Import WHERE typename = '".$typename."'";
	}
	else {
		$sql = "select rows_at_a_time from Config_Import WHERE typename = '".$typename."'";
	}
	print($sql);
	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	if ($row)
	{
	  $rows_to_process=$row[0];
	}

	$result->close();

	return $rows_to_process;

}

function db_return_one_param($db,$sql)
{
	if ($result = $db->query($sql))
	{
		$col1 = 0;
		$row = $result->fetch_row();
		$col1 = $row[0];
		$result->close();
	}
return($col1);
}

#etl/get_results_from_email.php
function db_return_email_reader_lottonames($db,&$x)
{
	$x=0;
	$rows = array();

	$sql = "SELECT lottocode,Lottoname_search_criteria FROM Config_EmailReader WHERE config_type = 'LottoName' and Source = 'theLotter'";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['lottocode']=$row[0];
	  $rows[$x]['LottoName']=$row[1];
	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();

	return $rows;
}
?>
