<?php
###DB CONNECT####

function db_connect($hostname, $username, $dbpassword, $databasename)
{
	 $db = new mysqli($hostname, $username, $dbpassword, $databasename);
	if($db->connect_errno > 0){
		die('Unable to connect to database [' . $db->connect_error . ']');
		exit;
	}

	/* change character set to utf8 */
   if (!$db->set_charset("utf8")) {
       #printf("Error loading character set utf8: %s\n", $db->error);
   } else {
       #printf("Current character set: %s\n", $db->character_set_name());
   }

	return ($db);
}

function db_disconnect($db)
{
	$db->close();
}
##END DB CONNECT####
?>
