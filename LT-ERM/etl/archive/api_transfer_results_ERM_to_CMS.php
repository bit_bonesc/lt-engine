<?php
include_once("include/config-engineroom.php");
include_once("include/db-settings-engineroom.php");
include_once("include/db-connect-engineroom.php");
include_once("include/db-functions-erm.php");

$rows_to_process_at_a_time=1;   # default to latest result   # put this in the config - to-do

$db = db_connect($hostname, $username, $dbpassword, $databasename);
get_configuration($db);
//$API_TRANSFER_DELAY = '90';
//$API_QUEUE = true;
//$DEV_API_URL = 'http://dev.??????/CMS/api/';
//PROD_API_URL = 'https://www.?????/CMS/api/';
//$API_LIVE_CMS = false;  //?  Queue AND Send.

print("Sleep..".$API_TRANSFER_DELAY." sec ");   # to allow for get_mail_results cron job to complete & transfer_mail_results_all
sleep($API_TRANSFER_DELAY);


if ($API_QUEUE)     //decom 
  {
    // 28 jan 2019 - seperate prod and dev .$PROD_API_URL = $DEV_API_URL;
  }

$flag='dev';    //default
##DO DEV FIRST
if ($API_LIVE_CMS_DEV)
{
  $flag='dev';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    //$lottoname_array = db_get_lottonames_api($db,$db_lottonames_count);
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName
    $i=0;
    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
          //  transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$API_QUEUE,$API_LIVE_CMS,'dev');
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$DEV_API_URL,$flag);
    $i++;
    sleep($PROCESSING_DELAY);
    }
    print("DEV done.");
}

##DO PROD NEXT
sleep($PROCESSING_DELAY);
if ($API_LIVE_CMS_PROD)
{
    $flag='prod';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    //$lottoname_array = db_get_lottonames_api($db,$db_lottonames_count);
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName

    $i=0;

    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
            transfer_results_from_Tables($db,$lottocode,$rows_to_process_at_a_time,$PROD_API_URL,$flag);
    $i++;
    sleep($PROCESSING_DELAY);
    }
    print("PROD done.");
}
  
  db_disconnect($db);
exit;


//function transfer_results_from_Tables($db,$lottocode_in,$rows_to_process_at_a_time,$API_URL,$API_QUEUE,$API_LIVE_CMS,$flag)
function transfer_results_from_Tables($db,$lottocode_in,$rows_to_process_at_a_time,$API_URL,$flag)
{
  $jackpot = '';
  
    ##Get lottoname details to build up query to get latest results.
    $lotto_detail_array = array();
    $lotto_detail_count = 0;
    $lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode_in);

    $x=0;  # should always be only 1 !!   - update db to LIMT 1...todo

      $lottoCode = $lotto_detail_array[$x]['lottoCode'];
      $lottonamedisplay = $lotto_detail_array[$x]['lottonamedisplay'];
      $lottocountrycode = $lotto_detail_array[$x]['lottocountrycode'];
      $Lotto_Table = $lotto_detail_array[$x]['Lotto_Table'];
      $nextdrawdate = $lotto_detail_array[$x]['drawdate'];  //force firt 10 char
      $nextdrawdate_diplay = $nextdrawdate;
      #$jackpot = $lotto_detail_array[$x]['jackpot'];
      #$currency_symbol = $lotto_detail_array[$x]['currency_symbol'];
      $ball1_active = $lotto_detail_array[$x]['ball1_active'];
      $ball2_active = $lotto_detail_array[$x]['ball2_active'];
      $ball3_active = $lotto_detail_array[$x]['ball3_active'];
      $ball4_active = $lotto_detail_array[$x]['ball4_active'];
      $ball5_active = $lotto_detail_array[$x]['ball5_active'];
      $ball6_active = $lotto_detail_array[$x]['ball6_active'];
      $ball7_active = $lotto_detail_array[$x]['ball7_active'];
      $bonus_active = $lotto_detail_array[$x]['bonus_active'];
      $bonus2_active = $lotto_detail_array[$x]['bonus2_active'];
      $powerball_active = $lotto_detail_array[$x]['powerball_active'];
      $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
      $lucky1_active =  $lotto_detail_array[$x]['lucky1_active'];
      $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
      $LottoName =  $lotto_detail_array[$x]['lottoname'];
      print("LottoName:");print($LottoName);

      ###########BUILD UP QUERY#################
      $query_part_b5 =  "";			$query_part_b6 =  "";			$query_part_b7 =  "";
      $query_part_b8 =  "";			$query_part_b9 =  "";			$query_part_b10 =  "";		$query_part_b11 =  "";

      #$query_part_start = "SELECT id, drawdate, ball1, ball2, ball3, ball4";
      $query_part_start = "SELECT id, drawdate, ball1, ball2, ball3, ball4";
      if ($ball5_active) {$query_part_b5 = ",ball5"; }
      if ($ball6_active) {$query_part_b6 = ",ball6"; }
      if ($ball7_active) {$query_part_b7 = ",ball7"; }

      if ($bonus_active) {$query_part_b8 = ",bonusball"; }
      if ($bonus2_active) {{$query_part_b9 = ",bonusball2"; }}
      if ($powerball_active) {$query_part_b10 = ",powerball"; }
      if ($powerball2_active) {$query_part_b11 = ",powerball2"; }

      if ($flag == 'dev'){
      $query_part_end = ",nextestimatedjackpot FROM ".$Lotto_Table." where transferred_to_CMS_dev = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;}
      else if ($flag=='prod'){
        $query_part_end = ",nextestimatedjackpot FROM ".$Lotto_Table." where transferred_to_CMS_prod = 0 order by drawdate desc LIMIT ".$rows_to_process_at_a_time;}
      
      $queryS1 = $query_part_start.$query_part_b5.$query_part_b6.$query_part_b7.$query_part_b8.$query_part_b9.$query_part_b10.$query_part_b11.$query_part_end;

      print($queryS1);

       if ($resultS1 = $db->query($queryS1)){ $myrowS1 = $resultS1->fetch_row();}

       if ($myrowS1)
       {
         $Results_table_id = $myrowS1[0];
         $drawdate = substr($myrowS1[1],0,10);
       $index=2;
       $ball1 = $myrowS1[$index];$index=$index+1; #1
       $ball2 = $myrowS1[$index];$index=$index+1; #2
       $ball3 = $myrowS1[$index];$index=$index+1; #3
       $ball4 = $myrowS1[$index];$index=$index+1; #4
       if ($ball5_active){	$ball5 = $myrowS1[$index]; $index=$index+1;	}
       if ($ball6_active){	$ball6 = $myrowS1[$index];	$index=$index+1;}
       if ($ball7_active){	$ball7 = $myrowS1[$index];$index=$index+1;}

       else if ($bonus_active)	{	$bonus = $myrowS1[$index]; 	$index=$index+1;}
       if ($powerball_active)	{	$powerball = $myrowS1[$index];	$index=$index+1;}
        $jackpot = $myrowS1[$index];$index=$index+1; #1
     }
     else {
       print("...no results for ".$lottoCode."...exit.");
       #$resultS1->close();
       return false;
     }
     $resultS1->close();


     print("..sending..");

		   ########################
		   ### INSERT INTO DB/ API
       ######################
       $Results = '';
       $build_results_successful = 0;

			 if (($lottoCode == 'ZA-L') || ($lottoCode == 'ZA-LP')|| ($lottoCode == 'ZA-LP2') || ($lottoCode == 'UK-L'))   #"South Africa-Lotto"   # 6 balls + bonusball
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5.'-'.$ball6.'|'.$bonus;
        $build_results_successful = 1;
			}
			else if (($lottoCode ==  'ZA-P') || ($lottoCode == 'ZA-PP')|| ($lottoCode == 'UK-T'))# "South Africa-Powerball"  # 5 balls + powerball
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5.'|'.$powerball;
        $build_results_successful = 1;
      }
      else if ($lottoCode ==  'ZA-DL') # "South Africa-DailyLotto"  # 5 balls 
			{
        $Results = $ball1.'-'.$ball2.'-'.$ball3.'-'.$ball4.'-'.$ball5;
        $build_results_successful = 1;
      }
      else{
        print("lottoCode not enabled to build Results - ".$lottoCode);
        $build_results_successful = 0;
      }
      #print($Results);
			$found_lotto = 0;
      //$found_lotto = api_receive_results($db,$lottoCode,$drawdate,$Results,$jackpot,$API_URL,$API_QUEUE,$API_LIVE_CMS,$flag);
      if ($build_results_successful) {
        $found_lotto = api_receive_results($db,$lottoCode,$drawdate,$Results,$jackpot,$API_URL,$flag);
      }

		# update table to reflect api transferred.
		  if ($found_lotto) {
        db_update_ResultsAPI($db,$Lotto_Table,$Results_table_id,$flag);
		  }
		  else {
			  print("api send error. ");
		  }
}   #functionv end



//function api_receive_results($db,$lottocode,$drawdate,$results,$jackpot,$API_URL,$API_QUEUE,$API_LIVE_CMS,$flag)
function api_receive_results($db,$lottocode,$drawdate,$results,$jackpot,$API_URL,$flag)
{
  #build up STRING
  #$results_prepared = str_replace(' ','-',$results);
  $results_prepared = $results;
  #$params = $lottocode.','.$drawdate.','.$results_prepared.','.$jackpot;
  #$url_prefix = 'http://localhost/lottotarget2/api/api_receive_results.php?results=ZA-P,2016-10-10,1-2-3-4-5|1,5000';
  #$url_prefix = 'http://localhost/lottotarget2/api/';
  #print($url);
  #exit;
  #to-do log to db - or in log file - use db to config dbbug verbosity.

  #6 jun 2017 - use wordpress api
  #     //dev.lottotarget.co.za/wp-json/importresults/v1/ZA/ZA-L/2018-05-23/1-2-3-4-5-6/5/6000
  #http://dev.lottotarget.co.za/wp-json/importresults/v1/ZA/ZA-L/2017-05-26/37-31-13-26-29/7/19000000

  $results_split = explode('|',$results_prepared);
  $results_a = $results_split[0];
  if (isset($results_split[1])) {$results_b = $results_split[1];} else {$results_b = 'x';}
  $url = 'undefined url';
  #print("url berfore:");
  #print($url);

  /*   decom - 29 jan 2019
  if ($API_QUEUE)    
    {
      $url_prefix = $API_URL;
      #$url= $url_prefix.'api_receive_results.php?results='.$params;
      # wordpress API
      $url= $url_prefix.'importresults/'.$lottocode.'/'.$drawdate.'/'.$results_a.'/'.$results_b.'/'.$jackpot;
      #print("API_QUEUE:");
      #print($url);
      db_insert_api_queue($db,$lottocode,$url);
      $force_true = 1;
      return $force_true;
    }
    else if ($API_LIVE_CMS)
    {
      */

      $url_prefix = $API_URL;
      #$url= $url_prefix.'api_receive_results.php?results='.$params;
      # wordpress API
      // old - $url= $url_prefix.'importresults/'.$lottocode.'/'.$drawdate.'/'.$results_a.'/'.$results_b.'/'.$jackpot;
     // if ($flag=='dev'){
          $url= $url_prefix.'importresults/';
          $params = '';
          $params = $params.'{';
          $params = $params.'"lottocode": "'.$lottocode.'",';
          $params = $params.'"drawdate": "'.$drawdate.'",';
          $params = $params.'"results_a": "'.$results_a.'",';
          $params = $params.'"results_b": "'.$results_b.'",';
          $params = $params.'"jackpot": "'.$jackpot.'"';
          $params = $params.'}';
     // }

     // $id = db_insert_api_queue($db,$lottocode,$url,$flag);
       $id = db_insert_api_queueV2($db,$lottocode,$url,$flag,$params);
      
      #print("API_LIVE_CMS");
      #print($url);
      // OK HERE HAVE TO PUT IN HEADERS AND USE CURL ?
      #TO-DO - get from db

      $ch = curl_init($url);
      $post = '{}';
      $post = $params;
        
      //$authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9";
      $authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.e30.u4sMpRTjXGVdRo1KkOEeY52vfjjyak-Laj1bq1XQWTw";
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$post);    
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      //return json_decode($result);
      //$contents = file_get_contents($url);
      //$return_code = str_get_html($contents);   # should be 1 or 0 !
      #$return_code = str_get_html($result);   # should be 1 or 0 !
      //$return_code = $result;   # should be 1 or 0 !

      $parsedBody = json_decode($result, true);
    
      $return_code = $parsedBody['result'];
      $error = $parsedBody['error'];
      if ($return_code == '1'){$return_code= '1';}
      else {
        $return_code = '0';
        $return_code = '1'; //force for now !!!  need to handle resonse 'insert failed: Duplicate entry '2019-03-13' for key 'drawdate' 1"
      };
      print("<");print($return_code);print(">");
      db_update_api_queue($db,$id,$return_code);
      return($return_code);
   // }
}


function jwt_request($token, $post) {

  $ch = curl_init('https://APPURL.com/api/json.php'); // INITIALISE CURL

  header('Content-Type: application/json');
  $post = json_encode($post); // Create JSON string from data ARRAY
  $authorization = "Authorization: Bearer ".$token; // **Prepare Autorisation Token**
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // **Inject Token into Header**
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $result = curl_exec($ch);
  curl_close($ch);
  return json_decode($result);

}
#db_disconnect($db);
?>
