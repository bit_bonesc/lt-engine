<?php
include_once("include/config-engineroom.php");
include_once("include/db-settings-engineroom.php");
include_once("include/db-connect-engineroom.php");
include_once("include/db-functions-erm.php");

$rows_to_process_at_a_time=1;   # default to latest result   # put this in the config - to-do

$db = db_connect($hostname, $username, $dbpassword, $databasename);
get_configuration($db);
//$API_TRANSFER_DELAY = '90';
//$API_QUEUE = true;
//$DEV_API_URL = 'http://dev.??????/CMS/api/';
//PROD_API_URL = 'https://www.?????/CMS/api/';
//$API_LIVE_CMS = false;  //?  Queue AND Send.

//print("Sleep..".$API_TRANSFER_DELAY." sec ");   # to allow for get_mail_results cron job to complete & transfer_mail_results_all
//sleep($API_TRANSFER_DELAY);


if ($API_QUEUE)     //decom 
  {
    // 28 jan 2019 - seperate prod and dev .$PROD_API_URL = $DEV_API_URL;
  }

##DO DEV FIRST
if ($API_LIVE_CMS_DEV)
{
  $flag='dev';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName
    $i=0;
    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
            api_process_results($db,$lottocode,$flag);
    $i++;
    sleep(1);
    }
    print("DEV done.");
}

##DO PROD NEXT
//sleep($PROCESSING_DELAY);
if ($API_LIVE_CMS_PROD)
{
    $flag='prod';
    $db_lottonames_count = 0;
    $lottoname_array = array();
    $lottoname_array = db_get_lottonames_api($db,$db_lottonames_count,$flag);    //api_active flag in LottoName

    $i=0;

    while ($i < $db_lottonames_count)
    {
      $lottoname = $lottoname_array[$i]['lottoname'];
      $lottocode = $lottoname_array[$i]['lottocode'] ;
      ?></br> <?php print("FETCH..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
            api_process_results($db,$lottocode,$flag);
    $i++;
    sleep(1);
    }
    print("PROD done.");
}
  db_disconnect($db);
exit;



//function api_receive_results($db,$lottocode,$drawdate,$results,$jackpot,$API_URL,$API_QUEUE,$API_LIVE_CMS,$flag)
function api_process_results($db,$lottocode,$flag)
{
     $id = 0;   
     $db_queue_count = 0;
     $queue_array = array();
     $queue_array =  db_get_api_queue($db,$db_queue_count,$lottocode,$flag);
     $i=0;
     while ($i < $db_queue_count)
     {
       $id = $queue_array[$i]['id'];
       $url = $queue_array[$i]['url'];
       ?></br> <?php print("FETCH..");print($i);print(":");print($id);print(":");print($url);?></br> <?php
           $result = execute_url($url);
           print("[");print($result);print("]");
           $return_code = $result;   # should be 1 or 0 !
           if ($return_code == '1'){$return_code= '1';}
           else ($return_code = '0');
           db_update_api_queue($db,$id,$return_code);
     $i++;
     sleep(1);
     }
}


function db_get_api_queue($db,&$x,$lottocode,$flag)
{
	$x=0;
	$rows = array();
	
	$sql = "SELECT id,url FROM Queue_API_OutBound WHERE lottocode = '".$lottocode."' AND `env` = '".$flag."' and (processed = '0' or processed is null)";

	$result = $db->query($sql);
	if (!$result) {
	  printf("Query failed: %s\n", $db->error);
	  exit;
	}
	$row = $result->fetch_row();
	while($row)
	{
	  $rows[$x]['id']=$row[0];
	  $rows[$x]['url']=$row[1];

	  $row = $result->fetch_row();
	   $x++;
	}
	$result->close();
	return $rows;
}






function execute_url($url) {
  $ch = 0;
  $ch = curl_init($url);
  $post = '{}';
  $authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.e30.u4sMpRTjXGVdRo1KkOEeY52vfjjyak-Laj1bq1XQWTw";
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS,$post);    
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}

?>
