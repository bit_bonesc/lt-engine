<?php
include_once("../include/config-engineroom.php");
include_once("../include/db-settings-engineroom.php");
include_once("../include/db-connect-engineroom.php");
include_once("../include/db-functions-erm.php");

require_once('simple_html_dom.php');

print("start...");
#$url = "https://www.nationallottery.co.za/results/lotto/";
#$url = 'http://www.lotteryextreme.com/south_africa/lotto-results(2020-01-05)';


$rows_to_process_at_a_time=1; 
print("db connect...");
$db = db_connect($hostname, $username, $dbpassword, $databasename);
print("get config...");
get_configuration($db);

$CRAWL_DELAY = 1;   //1 sec to-do add ro config

// loop - get list of lotto's + drawdates to crawl - get from db in queue.    Queue_crawler
// end loop

$db_lottonames_count = 0;
$lottoname_array = array();
//$lottoname_array = db_get_lottonames_api($db,$db_lottonames_count);
$flag='notused';
print("get lotto to crawl...");
$lottoname_array = db_get_lottonames_crawler($db,$db_lottonames_count,$flag);   // Config_CrawlReader WHERE active = '1'";
$i=0;
while ($i < $db_lottonames_count)
{
    $lottocode = $lottoname_array[$i]['lottocode'];
    $lottoname = $lottoname_array[$i]['lottoname_slug'];
    $drawdate = $lottoname_array[$i]['drawdate_slug'];
    $country = $lottoname_array[$i]['country_slug'];
    $sourceurl = $lottoname_array[$i]['source_url'];
   // $lottocode = 'ZA-L';
    //$drawdate = '2020-01-05';
    //$country = 'south_africa';
    //$lottoname = 'lotto';
    // build up source url
    $url = 'http://www.lotteryextreme.com/'.$country.'/'.$lottoname.'-results('.$drawdate.')';
    $url = $sourceurl.$country.'/'.$lottoname.'-results('.$drawdate.')';
    print("crawling:".$url);
    $source_url = $url;
      
    ?></br> <?php print("CRAWL..");print($i);print(":");print($lottocode);print(":");print($lottoname);?></br> <?php
          $crawl_result = crawl($db,$lottocode,$rows_to_process_at_a_time,$source_url);
          print('Final crawl result for db source3: {'.$lottocode.'||'.$drawdate.'||'.$crawl_result.'}    ');  
          // insert into db source 3 ?

  $i++;
  sleep($CRAWL_DELAY);
}



function crawl($db,$lottocode_in,$rows_to_process_at_a_time,$source_url)
{
  $jackpot = '';
##Get lottoname details to build up query to get latest results.
  $lotto_detail_array = array();
  $lotto_detail_count = 0;
  $lotto_detail_array = db_return_lotto_detail($db,$lotto_detail_count,$lottocode_in);

  $x=0;  # should always be only 1 !!   - update db to LIMT 1...todo

  $lottoCode = $lotto_detail_array[$x]['lottoCode'];
  $lottonamedisplay = $lotto_detail_array[$x]['lottonamedisplay'];
  $lottocountrycode = $lotto_detail_array[$x]['lottocountrycode'];
  $Lotto_Table = $lotto_detail_array[$x]['Lotto_Table'];
  $nextdrawdate = $lotto_detail_array[$x]['drawdate'];  //force firt 10 char
  $nextdrawdate_diplay = $nextdrawdate;
  #$jackpot = $lotto_detail_array[$x]['jackpot'];
  #$currency_symbol = $lotto_detail_array[$x]['currency_symbol'];
  $ball1_active = $lotto_detail_array[$x]['ball1_active'];
  $ball2_active = $lotto_detail_array[$x]['ball2_active'];
  $ball3_active = $lotto_detail_array[$x]['ball3_active'];
  $ball4_active = $lotto_detail_array[$x]['ball4_active'];
  $ball5_active = $lotto_detail_array[$x]['ball5_active'];
  $ball6_active = $lotto_detail_array[$x]['ball6_active'];
  $ball7_active = $lotto_detail_array[$x]['ball7_active'];
  $bonus_active = $lotto_detail_array[$x]['bonus_active'];
  $bonus2_active = $lotto_detail_array[$x]['bonus2_active'];
  $bonus3_active = $lotto_detail_array[$x]['bonus3_active'];
  $bonus4_active = $lotto_detail_array[$x]['bonus4_active'];

  $powerball_active = $lotto_detail_array[$x]['powerball_active'];
  $powerball2_active = $lotto_detail_array[$x]['powerball2_active'];
  $lucky1_active =  $lotto_detail_array[$x]['lucky1_active'];
  $lucky2_active =  $lotto_detail_array[$x]['lucky2_active'];
  $LottoName =  $lotto_detail_array[$x]['lottoname'];
  print("LottoName:");print($LottoName);


#########  LOCAL CONFIG FOR NOW  ZA-L #########
$b4_active = $ball4_active;
$b5_active = $ball5_active;
$b6_active = $ball6_active;
$b7_active = $ball7_active;
$b8_active = 0;
$bb_active = $bonus_active;
$bb2_active = $bonus2_active;
$bb3_active = $bonus3_active;
$bb4_active = $bonus4_active;
$pb_active = $powerball_active;
$pb2_active = $powerball2_active;

###########################################

  print("file get url...".$source_url);
  //21 jan 2019	$contents = file_get_contents($url);
  #$contents  = file_get_html($url)->plaintext;
  $ch = curl_init($source_url);
  $post = '{}';
  //$authorization = "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.e30.u4sMpRTjXGVdRo1KkOEeY52vfjjyak-Laj1bq1XQWTw";
  //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
  //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  
 //  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
   
 
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);


  //curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   // 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
   // 'Accept-Encoding: gzip, deflate',
   // 'Accept-Language: en-US,en;q=0.9',
   // 'Cookie: _ga=GA1.2.1842518271.1581102485; ck_country=south_africa; lr_adv=no; _gid=GA1.2.514374100.1582210129',
   // 'Connection: keep-alive',
   // 'Host: www.lotteryextreme.com',
   // 'Referer: http://www.lotteryextreme.com/south_africa/lotto-results(2019-11-09)',
   // 'Upgrade-Insecure-Requests: 1',
   // 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36'
  //));
  

  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   // 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
   // 'Accept-Encoding: gzip, deflate',
    'Accept-Language: en-US,en;q=0.9',
   // 'Cookie: _ga=GA1.2.1842518271.1581102485; ck_country=south_africa; lr_adv=no; _gid=GA1.2.514374100.1582210129',
    'Connection: keep-alive',
    'Host: www.lotteryextreme.com',
    'Referer: http://www.lotteryextreme.com/south_africa/lotto-results',
    'Upgrade-Insecure-Requests: 1',
    'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36'
  ));

//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//https://alvinalexander.com/php/php-curl-examples-curl_setopt-json-rest-web-service
//$ch = curl_init('http://localhost:8080/stocks/add');
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_HTTPHEADER, array(
 //   'Content-Type: application/json',
 //   'Content-Length: ' . strlen($data_string))
//);
curl_setopt($ch, CURLOPT_TIMEOUT, 5);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);


  $result = curl_exec($ch);
  curl_close($ch);
  //$return_code = $result; 
  
  $contents = $result;
  $html = str_get_html($contents);
  print($html);

  foreach($html->find('table[class=results2]') as $div)
  //foreach($html->find('table[class=qx2]') as $div)
  {
    $found_str = $div->innertext;
   // print($found_str);
    //strip <tr>s
  }


  print('</br>');
  $Results=str_replace('<tr>','',$found_str);

  $Results=str_replace('</tr>','',$Results);

  print('full result: <'.$Results.'>    ');

  $Results=str_replace('<td>',' ',$Results);
  $Results=str_replace('</td>','',$Results);

  print('full result stripped: <'.$Results.'>    ');
  //print('bb only: </br>');
  
  //foreach($html->find('td[class=xy]') as $div)
  //foreach($html->find('table[class=qx2]') as $div)
  //{
   $found_str2 = $div->innertext;
  //print($found_str2);
    //strip <tr>s
  //}

 $Results=str_replace('<td class="xy"> ','',$Results);
 $Results=ltrim($Results);

 $Results=str_replace(' ',',',$Results);

  print('Final result: {'.$lottocode.'|'.$drawdate.'|'.$Results.'}    ');  

  // save result into staging table   - source 3
  // format  space + | 
  $b1 = '';
  $b2 = '';
  $b3 = '';
  $b4 = '';
  $b5 = '';
  $b6 = '';
  $b7 = '';
  $b8 = '';
  $bb = '';
  $bb2 = '';
  $bb3 = '';
  $bb4 = '';
  $pb = '';
  $pb2 = '';
  
  $result_array = explode(',',$Results);
  $y=0;
  $b1 = $result_array[$y];$y++;
  $b2 = $result_array[$y];$y++;
  $b3 = $result_array[$y];$y++;
  
    if ($b4_active) { $b4 = $result_array[$y];$y++;}
    if ($b5_active) { $b5 = $result_array[$y];$y++;}
    if ($b6_active) { $b6 = $result_array[$y];$y++;}
    if ($b7_active) { $b7 = $result_array[$y];$y++;}
    if ($b8_active) { $b8 = $result_array[$y];$y++;}

    if ($bb_active) { $bb = $result_array[$y];$y++;}
    if ($bb2_active) { $bb2 = $result_array[$y];$y++;}
    if ($bb3_active) { $bb3 = $result_array[$y];$y++;}
    if ($bb4_active) { $bb4 = $result_array[$y];$y++;}
    if ($pb_active) { $pb = $result_array[$y];$y++;}
    if ($pb2_active) { $pb2 = $result_array[$y];$y++;}

  // ok now build up again 

  $result_final = '';
  $result_final = $b1.' '.$b2.' '.$b3;
  if ($b4_active) { $result_final = $result_final.' '.$b4; } 
  if ($b5_active) { $result_final = $result_final.' '.$b5; } 
  if ($b6_active) { $result_final = $result_final.' '.$b6; } 
  if ($b7_active) { $result_final = $result_final.' '.$b7; } 
  if ($b8_active) { $result_final = $result_final.' '.$b8; } 

  if ($bb_active) { $result_final = $result_final.'|'.$bb; } 
  if ($bb2_active) { $result_final = $result_final.' '.$bb2; } 
  if ($bb3_active) { $result_final = $result_final.' '.$bb3; } 
  if ($bb4_active) { $result_final = $result_final.' '.$bb4; } 
  if ($pb_active) { $result_final = $result_final.'|'.$pb; } 
  if ($pb2_active) { $result_final = $result_final.' '.$pb2; } 


  print('Final result for db source3: {'.$lottocode.'||'.$drawdate.'||'.$result_final.'}    ');  

  // Start date, end date - 
 //input - lottocode, startdate , or year, initial  { takeon or daily}
 // calc- drawdates into array for year
 // use db to configure and put on to queue to process - that way can pregerate all urls and monitor. 
 // also include source   , eg lottoextreme, magayo, ... others.
 // timer  - floor + ceiling - random wait interval min+max
 // timer ongoing ?  schedule to get daily results - must be different time's also random vs pre-set:  hr:min:sec range 00-09:01-59:05-55 --? but if its on a queue then queue needs t seqence quasi random


 return $result_final;
  }   //crawler

?>
