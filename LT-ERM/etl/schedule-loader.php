<?php
###accepts in
# HC_scheduler_loader.php?id=ZA-P&drawdate=
# by default  $charttype = 'ALL'
# for test / manual purposes use $charttype = OE | LH | OE | SELECTORS
/*include_once("../include/config-engineroom.php");
include_once("../include/db-settings-engineroom.php");
include_once("../include/db-connect-engineroom.php");
include_once("../include/db-functions-erm.php");
*/

/*include_once("../include/config-engineroom.php");
include_once("../include/db-settings-engineroom.php");
include_once("../include/db-connect-engineroom.php");
include_once("../include/db-functions-erm.php");
*/

include_once("include/config-engineroom.php");
include_once("include/db-settings-engineroom.php");
include_once("include/db-connect-engineroom.php");
include_once("include/db-functions-erm.php");


$rows_to_process_at_a_time=1;   # default to latest result    ## TO-DO: make this a DB param !!!!!!!!!!
$charttype = '';
if (isset($_REQUEST['charttype'])) {	$charttype = $_REQUEST['charttype'];}
else {$charttype = 'ALL';}   # DEFAULT
$db = db_connect($hostname, $username, $dbpassword, $databasename);
get_configuration($db);
if($LIVE) {$home_URL = $ERM_PROD_URL;} else {$home_URL = $ERM_DEV_URL;}

$db_lottonames_count = 0;
$lottoname_array = array();
$lottoname_array = db_get_lottonames_generate_chart($db,$db_lottonames_count);   //only generate charts & selectors for generate_chart_active

$db_months_count = 0;
$months_array = array();
# get from the db the months to run the hot & cold (db.Config_Chart_Periods)
$months_array = db_get_months_config($db,$db_months_count);
#print("months:");print($db_months_count);
#generate URL and insert into Queue db.Chart_Hot_Cold_Scheduler_Queue

### new added  - only do active ones !
$active_HC = false;
$active_OE = false;
$active_LH = false;
$active_OELH = false;
$active_LMH = false;
$active_GRP = false;
$active_PAIR = false;
$active_SELECTORS = false;
$active_SELECTORS_SUMMARY = false;
$active_FIBONACCI_NUMBERS = false;
$active_TRIANGULAR_NUMBERS = false;

$Queue_Table_HC = '';
$Queue_Table_OE = '';
$Queue_Table_OELH = '';
$Queue_Table_LH = '';
$Queue_Table_LMH = '';
$Queue_Table_GRP = '';
$Queue_Table_PAIR = '';
$Queue_Table_SELECTORS = '';
$Queue_Table_SELECTORS_SUMMARY = '';
$Queue_Table_FIBONACCI_TRIANGULAR = '';

$Queue_Table = ''; #'Chart_Hot_Cold_Scheduler_Queue';
$queue_detail_array = array();
$queue_count = 0;
$y=0;
$lottocode_future = '';  //future use.
$queue_detail_array = db_return_queue_detail($db,$queue_count,$lottocode_future);
while ($queue_count > 0)
{
  $Queue_Table = $queue_detail_array[$y]['queuename'];   // - used now 11 june 2018
  $chart_code =  $queue_detail_array[$y]['charcode'];
  $y++;
  $queue_count = $queue_count - 1;

  // ASSIGN 
  if ($chart_code == 'HC') { $active_HC = true; $Queue_Table_HC = $Queue_Table; }
  else if ($chart_code == 'OE') { $active_OE = true; $Queue_Table_OE = $Queue_Table;}
  else if ($chart_code == 'LH') { $active_LH = true; $Queue_Table_LH = $Queue_Table;}
  else if ($chart_code == 'OELH') { $active_OELH = true;$Queue_Table_OELH = $Queue_Table;}
  else if ($chart_code == 'LMH') { $active_LMH = true;$Queue_Table_LMH = $Queue_Table;}
  else if ($chart_code == 'GRP') { $active_GRP = true;$Queue_Table_GRP = $Queue_Table;}
  else if ($chart_code == 'PAIR') { $active_PAIR = true;$Queue_Table_PAIR = $Queue_Table;}
  else if ($chart_code == 'SELECTORS') { $active_SELECTORS = true;$Queue_Table_SELECTORS = $Queue_Table;}
  else if ($chart_code == 'SELECTORS-SUMMARY') { $active_SELECTORS_SUMMARY = true;$Queue_Table_SELECTORS_SUMMARY = $Queue_Table;}
  else if ($chart_code == 'FIBONACCI-TRIANGULAR') { $active_FIBONACCI_TRIANGULAR = true;$Queue_Table_FIBONACCI_TRIANGULAR = $Queue_Table;}
 }  

$i=0;
while ($i < $db_lottonames_count)
{
   
   $lottoname = $lottoname_array[$i]['lottoname'];
   $lottocode = $lottoname_array[$i]['lottocode'] ;
   ?></br> <?php print("FETCH..");print($lottoname);?></br> <?php

   ## ALL
   if ($active_SELECTORS) {
   #STEP0#### get from Results_LottoTable last results (drawdate) to process. ind=0 gets unprocessed entries (1) then set to 1
   $page =  'engine/LT-ERM/etl/calc/calc-selector-winnings.php';
   schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','SELECTORS',$Queue_Table_SELECTORS);
   }
   if ($active_SELECTORS_SUMMARY) {
    $page =  'engine/LT-ERM/etl/calc/calc-selector-winnings-summary.php';
    schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','SELECTORS-SUMMARY',$Queue_Table_SELECTORS_SUMMARY); 
   }
   if ($active_FIBONACCI_TRIANGULAR) {
    $page =  'engine/LT-ERM/etl/calc/calc-number-analysis-fib-tri.php';
    schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','FIBONACCI-TRIANGULAR',$Queue_Table_FIBONACCI_TRIANGULAR); 
   }


   ##CALCULATE HOT COLD CHARTS
   if (($active_HC) && (($charttype == 'HC') || ($charttype == 'ALL')))
   {
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';

     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
  	 schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','HC',$Queue_Table_HC);
     #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
     $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','HC',$Queue_Table_HC);
    }

   ## CALCULATE ODD EVEN CHARTS
   if (($active_OE) && (($charttype == 'OE') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     #$page =  'engine/LT-ERM/etl/calc/tools-odd-even-low-high-generator-new-calc.php';
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','OE',$Queue_Table_OE);
    #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
     ## TO DO
    $page = 'engine/LT-ERM/etl/calc/generate-selectors.php';
     #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','OE',$Queue_Table_OE);
   }

   ## CACLULATE LOW HIGH CHARTS
   if (($active_LH) && (($charttype == 'LH') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','LH',$Queue_Table_LH);
      #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
     $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
      #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','LH',$Queue_Table_LH);
   } 
   
   ## CACLULATE COMBO : ODD EVEN - LOW HIGH CHARTS
   if (($active_OELH) && (($charttype == 'OELH') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','OELH',$Queue_Table_OELH);
      #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
     $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
      #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','OELH',$Queue_Table_OELH);
   }


   ## CACLULATE LOW MED HIGH CHARTS
   if (($active_LMH) && (($charttype == 'LMH') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','LMH',$Queue_Table_LMH);
    #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
    #to-do $page =  'CMS/calc/tools-low-high-generator-selectors.php';
    #to-do $page =  'CMS/calc/tools-odd-even-low-high-generator-selectors.php';
     $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
                       #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','LMH',$Queue_Table_LMH);
   }
   ## CACLULATE GROUP CHARTS
   if (($active_GRP) && (($charttype == 'GRP') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','GRP',$Queue_Table_GRP);
    #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
    #to-do $page =  'CMS/calc/tools-low-high-generator-selectors.php';
    #to-do $page =  'CMS/calc/tools-odd-even-low-high-generator-selectors.php';
    ##to-do $page =  'CMS/calc/tools-odd-even-low-high-low-med-high-generator-selectors.php';
    $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
                       #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
    ##to-do schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','GRP');
   }
   ## CACLULATE PAIR CHARTS
   if (($active_PAIR) && (($charttype == 'PAIR') || ($charttype == 'ALL')))
   {
    # tools-odd-even-low-high-generator-new-calc.php?m=3&drawdate=2016-06-03&id=ZA-P&charttype=LH
     $page =  'engine/LT-ERM/etl/calc/generate-chart.php';
     #STEP1### get from Results_LottoTable last result (drawdate) to process CHARTS. ind=1 gets unprocessed enties (0 or null) then set to 1
     schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'0','PAIR',$Queue_Table_PAIR);
    #STEP2#### get from Results_LottoTable last results (drawdate) to process SELECTORS. ind=2 gets unprocessed entries (1) then set to 2
    #to-do $page =  'CMS/calc/tools-low-high-generator-selectors.php';
    #to-do $page =  'CMS/calc/tools-odd-even-low-high-generator-selectors.php';
    ##to-do $page =  'CMS/calc/tools-odd-even-low-high-low-med-high-generator-selectors.php';
    $page =  'engine/LT-ERM/etl/calc/generate-selectors.php';
                       #tools-odd-even-low-high-generator-selectors.php?m=3&drawdate=2017-03-11&id=ZA-L&charttype=LH
    ##to-do schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,'1','GRP');
   }
   ## CACLULATE  TRIPLET  CHARTS

   ## CALCULATE COMBO - ODD-EVEN : HOT-COLD   (ODD-EVEN frequency)

   ## CALCULATE COMBO - LOW-HIGH : LOW-HIGH   (LOW-HIGH frequency)

   ## CALCULATE COMBO PRO - ODD-EVENfrequency : LOW-HIGHfrequency

$i++;
}
db_disconnect($db);
exit;

function schedule_for_processing($db,$lottocode,$rows_to_process_at_a_time,$months_array,$db_months_count,$home_URL,$page,$indicator,$charttype,$Queue_Table)
{
  #TO - DO read from database table: Config_Chart_Selectors

  /* decom  //11 june 2019
  if ($charttype == 'HC') {$queuename = 'Chart_Hot_Cold_Scheduler_Queue';}
  if ($charttype == 'OE') {$queuename = 'Chart_Odd_Even_Scheduler_Queue';}
  if ($charttype == 'LH') {$queuename = 'Chart_Low_High_Scheduler_Queue';}
  if ($charttype == 'OELH') {$queuename = 'Chart_Odd_Even_Low_High_Scheduler_Queue';}
  if ($charttype == 'LMH') {$queuename = 'Chart_Low_Median_High_Scheduler_Queue';}
  if ($charttype == 'GRP') {$queuename = 'Chart_Group_Scheduler_Queue';}
  if ($charttype == 'PAIR') {$queuename = 'Chart_Pair_Scheduler_Queue';}
  if ($charttype == 'SELECTORS') {$queuename = 'Calc_SelectorWinnings_Scheduler_Queue';}
  if ($charttype == 'SELECTORS-SUMMARY') {$queuename = 'Calc_SelectorWinningsSummary_Scheduler_Queue';}
  */

  $queuename = $Queue_Table;   //11 june 2019

  $db_lottonames_count = 0;
  $lottoname_array = array();
  $lottoname_array = db_return_lotto_detail($db,$db_lottonames_count,$lottocode);
  $i=0;
  print("count:");print($db_lottonames_count);
        #$LottoName = $lottoname_array[$i]['Lotto_Name'] ;
        #$DrawDate = $lottoname_array[$i]['drawdate'] ;
        #print("DrawDate:");print($DrawDate);
        #print(" DrawNumber:|");print($DrawNumber);
        $LottoTable  =  $lottoname_array[$i]['Lotto_Table'];
        #$lottocode = $lottoname_array_2[$x]['lottocode'];
        print(" LottoTable:|");print($LottoTable);print("|");

        $db_lottotable_count = 0;
        $lottotable_array = array();
        $lottotable_array = db_return_lotto_table_processing_detail($db,$db_lottotable_count,$LottoTable,$indicator,$charttype);
               
        $i=0;

        while ($i < $db_lottotable_count)
        {
         # get latest results from <LottoTable> where queue_HC = 0 and drawdate is latest LIMIT 1
         # get params from db (config) on number of months to run .ie. 1,2.3.6.12, 24,48,60,999(all)
         # insert into db queue url:
         #Chart_Hot_Cold_Scheduler_Queue
         #ZA-P    localhost/lottotarget2/calc/tools-hot-cold-generator-new-calc.php?m=1&drawdate=2016-05-31&id=ZA-P
         $DrawDate = $lottotable_array[$i]['drawdate'];
         print("DrawDate:");print($DrawDate);
         $j=0;
         if (($charttype == "HC") || ($charttype == "OE")|| ($charttype == "LH")|| ($charttype == "OELH")|| ($charttype == "LMH")|| ($charttype == "GRP")|| ($charttype == "PAIR"))
         {
           while ($j < $db_months_count)
           {
             $months=$months_array[$j]['months'];
             #$page =  'calc/tools-hot-cold-generator-new-calc.php';
             ###3###http://localhost/lottotargetWP/CMS/calc/LT_calc_selector_winnings.php?drawdate=2016-06-01&id=ZA-L
             $url = $home_URL.$page.'?m='.$months.'&drawdate='.$DrawDate.'&id='.$lottocode.'&charttype='.$charttype;
             ?></br><?php print("URL:");print($url);
             #$queued = db_insert_HC_queue($db,$url,$lottocode);

             $queued = db_insert_queue($db,$url,$lottocode,$queuename);
             $j++;
           }
           if ($indicator == 0) {$updated = db_update_lottotable($db,$LottoTable,$DrawDate,'1',$charttype);}
           if ($indicator == 1) {$updated = db_update_lottotable($db,$LottoTable,$DrawDate,'2',$charttype);}
         }
      
       else if ( ($charttype == "SELECTORS") ||
      ($charttype == "SELECTORS-SUMMARY") ||
       ($charttype == "FIBONACCI-TRIANGULAR"))
       {
         $url = $home_URL.$page.'?drawdate='.$DrawDate.'&id='.$lottocode.'&charttype='.$charttype;
         ?></br><?php print("URL:");print($url);
         $queued = db_insert_queue($db,$url,$lottocode,$queuename);
          $updated = db_update_lottotable($db,$LottoTable,$DrawDate,'1',$charttype);
       }

       $i++;
      if ($i >= $rows_to_process_at_a_time) { print("ALERT:reached max rows to process..return.");return;}
       }
}

